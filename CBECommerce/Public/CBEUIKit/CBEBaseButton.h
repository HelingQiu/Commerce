//
//  CBEBaseButton.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBEBaseButton : UIButton

@property (nonatomic , assign) CGRect imageViewRect;
@property (nonatomic , assign) CGRect titleLabelRect;

@end

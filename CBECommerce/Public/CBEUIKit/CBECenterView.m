//
//  CBECenterView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/2.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBECenterView.h"

@implementation CBECenterView
{
    UIImageView *_leftView;
    UILabel *_rightLabel;
    
    CGSize _imageSize;
    UIFont *_font;
    NSString *_text;
    CGFloat _margin;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}
- (void)setupUI
{
    _leftView = [UIImageView new];
    [self addSubview:_leftView];
    
    _rightLabel = [UILabel new];
    [self addSubview:_rightLabel];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    float titleWidth = [CommonUtils widthForString:_text Font:_font andWidth:self.width];
    float imgWidth = _imageSize.width;
    
    float leftMargin = (self.width - _margin - titleWidth - imgWidth)/2.0;
    
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(leftMargin);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(self->_imageSize);
    }];
    
    [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftView.mas_right).offset(self->_margin);
        make.right.equalTo(self.mas_right);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(self.height);
    }];
}

- (void)configWith:(NSString *)imageName imageSize:(CGSize)imageSize textFont:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)alignment text:(NSString *)text margin:(CGFloat)margin
{
    _imageSize = imageSize;
    _font = font;
    _text = text;
    _margin = margin;
    
    [_leftView setImage:IMAGE_NAMED(imageName)];
    [_rightLabel setFont:font];
    [_rightLabel setText:text];
    [_rightLabel setTextColor:color];
    [_rightLabel setTextAlignment:alignment];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

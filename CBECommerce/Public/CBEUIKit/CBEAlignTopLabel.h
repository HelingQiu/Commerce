//
//  CBEAlignTopLabel.h
//  CBECommerce
//
//  Created by Rainer on 2019/1/21.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEAlignTopLabel : UILabel

@end

NS_ASSUME_NONNULL_END

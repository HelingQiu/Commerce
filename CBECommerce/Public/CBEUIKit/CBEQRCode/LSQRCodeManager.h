//
//  LSQRCodeManager.h
//  LSQRCode
//
//  Created by metbao1 on 2018/6/22.
//  Copyright © 2018年 杨荣. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol LSScanQRCodeProtocol;

NS_ASSUME_NONNULL_BEGIN

// 生成的二维码图片默认尺寸
static CGFloat LSQRCodeImageDefaultSize = 150.0f;
// 二维码水印默认尺寸
static CGFloat LSQRCodeWatermarkImageDefaultSize = 45.0f;

typedef void(^LSQRCodeManagerSuccessBlock)(NSString * _Nullable result);
typedef void(^LSQRCodeManagerFailureBlock)(NSError * _Nullable error);

// 扫描动画框显示效果配置key
typedef NSString * LSQRCodeScanningOptionsKey;

// 扫描动画时间，这个key对应的值传NSNumber类型或NSString类型
FOUNDATION_EXPORT LSQRCodeScanningOptionsKey const LSQRCodeScanningAnimationTimeKey;
// 扫描线对应图片，这个key对应的值传UIImage类型
FOUNDATION_EXPORT LSQRCodeScanningOptionsKey const LSQRCodeScanningLineImageKey;
// 有效扫描范围边框的颜色，这个key对应的值传UIColor类型
FOUNDATION_EXPORT LSQRCodeScanningOptionsKey const LSQRCodeScanningViewColorKey;


/**
 二维码处理工具类，功能包括生成二维码，扫描二维码，识别图片二维码
 */
@interface LSQRCodeManager : NSObject

#pragma mark - 生成二维码相关方法
/**
 根据字符串生成二维码
 
 @param stringToGenerate 用于生成二维码的字符串
 @return 生成的二维码图片
 */
+ (UIImage * _Nullable)generateQRCodeImageWithString:(NSString *)stringToGenerate;

/**
 根据字符串生成二维码

 @param stringToGenerate 用于生成二维码的字符串
 @param imageSize 生成图片尺寸，由于二维码是正方形的，所以只传一个CGFloat类型的值就可以了，宽高都用这个值
 @return 生成的二维码图片
 */
+ (UIImage * _Nullable)generateQRCodeImageWithString:(NSString *)stringToGenerate imageSize:(CGFloat)imageSize;

/**
 根据字符串和水印图生成二维码

 @param stringToGenerate 用于生成二维码的字符串
 @param imageSize 生成图片尺寸，由于二维码是正方形的，所以只传一个CGFloat类型的值就即可，宽高都用这个值
 @param watermarkImage 水印图片，水印图片默认加在二维码的中间，传宽高一样的图片以避免图片变形，图片最好不要太大
 @return 生成的二维码图片
 */
+ (UIImage * _Nullable)generateQRCodeImageWithString:(NSString *)stringToGenerate imageSize:(CGFloat)imageSize watermarkImage:(UIImage * _Nullable)watermarkImage;

/**
 根据字符串和水印图生成二维码

 @param stringToGenerate 用于生成二维码的字符串
 @param imageSize 生成图片尺寸，由于二维码是正方形的，所以只传一个CGFloat类型的值即可，宽高都用这个值
 @param watermarkImage 水印图片，水印图片默认加在二维码的中间，传宽高一样的图片以避免图片变形
 @param watermarkImageSize 水印图片尺寸, 水印图片也默认正方形，所以只穿一个CGFloat类型的值即可，宽高都用这个值，尺寸不要超过二维码尺寸的30%，最多也只取二维码大小的30%
 @return 生成的二维码图片
 */
+ (UIImage * _Nullable)generateQRCodeImageWithString:(NSString *)stringToGenerate imageSize:(CGFloat)imageSize watermarkImage:(UIImage * _Nullable)watermarkImage watermarkImageSize:(CGFloat)watermarkImageSize;

#pragma mark - 扫描二维码相关方法

/**
 扫描二维码
 @note 只是单纯扫描二维码功能的实现，内部没有封装UI

 @param view 用于显示摄像头捕获数据的view
 @param scanAreaSize 有效扫描区域的尺寸，大小最好不要超过view.bounds.size
 @param completionHandler 扫描成功回调，回调一个字符串结果
 @param failureHandler 扫描失败回调，回调一个error
 */
- (void)scanQRCodeOnView:(UIView *)view scanAreaSize:(CGSize)scanAreaSize completionHandler:(LSQRCodeManagerSuccessBlock _Nullable)completionHandler failureHandler:(LSQRCodeManagerFailureBlock _Nullable)failureHandler;


/**
 扫描二维码

 @param view 用于显示摄像头捕获数据的view
 @param scanAreaSize 有效扫描区域的尺寸，大小最好不要超过view.bounds.size
 @param isNeedScanningAnimation 是否需要扫描动画，如果需要会使用内部封装好的view
 @param scanningOptions 扫描框的配置选项
 @param completionHandler 扫描成功回调，回调一个字符串结果
 @param failureHandler 扫描识别回调，回调一个error
 */
- (void)scanQRCodeOnView:(UIView *)view scanAreaSize:(CGSize)scanAreaSize isNeedScanningAnimation:(BOOL)isNeedScanningAnimation scanningOptions:(NSDictionary <LSQRCodeScanningOptionsKey, id> * _Nullable)scanningOptions completionHandler:(LSQRCodeManagerSuccessBlock _Nullable)completionHandler failureHandler:(LSQRCodeManagerFailureBlock _Nullable)failureHandler;


/**
 扫描二维码（使用自定义的扫描动画）

 @param view 用于显示摄像头捕获数据的view
 @param scanAreaSize 有效扫描区域的尺寸，大小最好不要超过view.bounds.size
 @param scanningAnimationView 自定义扫描动画view，需要遵守LSScanQRCodeProtocol协议
 @param completionHandler 扫描成功回调，回调一个字符串结果
 @param failureHandler 扫描识别回调，回调一个error
 */
- (void)scanQRCodeOnView:(UIView *)view scanAreaSize:(CGSize)scanAreaSize scanningAnimationView:(UIView <LSScanQRCodeProtocol> *)scanningAnimationView completionHandler:(LSQRCodeManagerSuccessBlock _Nullable)completionHandler failureHandler:(LSQRCodeManagerFailureBlock _Nullable)failureHandler;

/**
 停止扫描
 */
- (void)stopScanning;

/**
 开始扫描
 */
- (void)startScanning;

#pragma mark - 识别图片中的二维码相关方法
/**
 识别二维码图片
 @note 调用这个方法等同于调用: [LSQRCodeManager recognizeQRCodeImageWithImage:image isNeedToCompressImage: YES]；如果压缩后的图片识别无结果，会默认再用原图识别一次

 @param qrCodeImage 二维码图片
 @return 识别结果，如果无结果返回nil
 */
+ (NSString * _Nullable)recognizeQRCodeImageWithImage:(UIImage *)qrCodeImage;

/**
 识别二维码图片
 
 @param qrCodeImage 二维码图片
 @param isNeedToCompressImage 是否需要压缩图片，YES需要压缩，NO不压缩用原图进行识别
 @return 识别结果，如果无结果返回nil
 */
+ (NSString * _Nullable)recognizeQRCodeImageWithImage:(UIImage *)qrCodeImage isNeedToCompressImage:(BOOL)isNeedToCompressImage;


@end

NS_ASSUME_NONNULL_END

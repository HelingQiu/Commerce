//
//  LSScanQRCodeShadowView.h
//  LSQRCode
//
//  Created by 韩云彬 on 2018/6/25.
//  Copyright © 2018年 杨荣. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSQRCodeManager.h"
#import "LSScanQRCodeProtocol.h"

@interface LSScanQRCodeShadowView : UIView <LSScanQRCodeProtocol>

/**
 扫描遮罩视图初始化

 @param frame 视图frame
 @param scanAreaFrame 有效扫码区域的frame
 @param scanningOptions 扫描配置
 @return 遮罩视图
 */
- (instancetype)initWithFrame:(CGRect)frame scanAreaFrame:(CGRect)scanAreaFrame scanningOptions:(NSDictionary <LSQRCodeScanningOptionsKey, id> *)scanningOptions;

@end

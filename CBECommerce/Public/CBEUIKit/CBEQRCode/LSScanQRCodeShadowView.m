//
//  LSScanQRCodeShadowView.m
//  LSQRCode
//
//  Created by 韩云彬 on 2018/6/25.
//  Copyright © 2018年 杨荣. All rights reserved.
//

#import "LSScanQRCodeShadowView.h"
#import <AVFoundation/AVFoundation.h>

typedef NS_ENUM(NSInteger, LSScanQRCodeTorchButtonMode) {
    LSScanQRCodeTorchButtonModeDefault = 0,
    LSScanQRCodeTorchButtonModeOpenTorch,
    LSScanQRCodeTorchButtonModeCloseTorch,
};

@interface LSScanQRCodeShadowView ()

@property (nonatomic, strong) UIView *scanningLineView;

@property (nonatomic, assign) CGFloat scanAreaOriginX;
@property (nonatomic, assign) CGFloat scanAreaOriginY;
@property (nonatomic, assign) CGFloat scanAreaWidth;
@property (nonatomic, assign) CGFloat scanAreaMaxY;

@property (nonatomic, assign) NSTimeInterval animationTime;

/** 按钮 */
@property (nonatomic, strong) UIButton *torchButton;

@property (nonatomic, assign) LSScanQRCodeTorchButtonMode torchMode;

@end

@implementation LSScanQRCodeShadowView

#pragma mark - LSScanQRCodeProtocol
- (void)startScanningAnimation {
    
    [self endScanningAnimation];
    
    if (self.scanningLineView) {
        self.scanningLineView.hidden = NO;
    }
    
    [self scanningAnimation];
}

- (void)endScanningAnimation {
    
    if (self.scanningLineView) {
        self.scanningLineView.hidden = YES;
        self.scanningLineView.frame = CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY, self.scanAreaWidth, 1);
    }
}

- (void)showOpenTorch {
    
    if (self.torchMode == LSScanQRCodeTorchButtonModeCloseTorch) {
        return;
    }
    
    self.torchButton.hidden = YES;
    self.torchMode = LSScanQRCodeTorchButtonModeOpenTorch;
    [self.torchButton setTitle:@"点击开启闪光灯" forState:UIControlStateNormal];
}

- (void)showCloseTorch {
    
    self.torchMode = LSScanQRCodeTorchButtonModeCloseTorch;
    [self.torchButton setTitle:@"点击关闭闪光灯" forState:UIControlStateNormal];
}

#pragma mark -
- (instancetype)initWithFrame:(CGRect)frame scanAreaFrame:(CGRect)scanAreaFrame scanningOptions:(NSDictionary<LSQRCodeScanningOptionsKey,id> *)scanningOptions {
    if (self = [super initWithFrame:frame]) {
        
        self.animationTime = 2.0f;
        if (scanningOptions[LSQRCodeScanningAnimationTimeKey] && ([scanningOptions[LSQRCodeScanningAnimationTimeKey] isKindOfClass:[NSString class]] || [scanningOptions[LSQRCodeScanningAnimationTimeKey] isKindOfClass:[NSNumber class]])) {
            self.animationTime = [scanningOptions[LSQRCodeScanningAnimationTimeKey] doubleValue];
        }
        
        [self setupUIWithScanAreaFrame:scanAreaFrame scanningOptions:scanningOptions];
    }
    return self;
}

#pragma mark - setupUI
- (void)setupUIWithScanAreaFrame:(CGRect)scanAreaFrame scanningOptions:(NSDictionary<LSQRCodeScanningOptionsKey,id> *)scanningOptions {
    
    // 添加一个中间镂空的图层用于显示有效扫描区域
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:0];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRect:scanAreaFrame];
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor colorWithWhite:0.3 alpha:0.7].CGColor;
    [self.layer addSublayer:fillLayer];
    
    self.scanAreaOriginX = CGRectGetMinX(scanAreaFrame);
    self.scanAreaOriginY = CGRectGetMinY(scanAreaFrame);
    self.scanAreaWidth = CGRectGetWidth(scanAreaFrame);
    self.scanAreaMaxY = CGRectGetMaxY(scanAreaFrame);

    UIColor *color = [UIColor redColor];
    if (scanningOptions[LSQRCodeScanningViewColorKey] && [scanningOptions[LSQRCodeScanningViewColorKey] isKindOfClass:[UIColor class]]) {
        color = scanningOptions[LSQRCodeScanningViewColorKey];
    }
    
    // 添加扫码边框, 8条框
    CGFloat scanAreaHeight = CGRectGetHeight(scanAreaFrame);
    
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY, 2, 10)];
    lineView1.backgroundColor = color;
    [self addSubview:lineView1];

    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY, 10, 2)];
    lineView2.backgroundColor = color;
    [self addSubview:lineView2];

    UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX + self.scanAreaWidth - 10, self.scanAreaOriginY, 10, 2)];
    lineView3.backgroundColor = color;
    [self addSubview:lineView3];

    UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX + self.scanAreaWidth - 2, self.scanAreaOriginY, 2, 10)];
    lineView4.backgroundColor = color;
    [self addSubview:lineView4];

    UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY + scanAreaHeight - 10, 2, 10)];
    lineView5.backgroundColor = color;
    [self addSubview:lineView5];

    UIView *lineView6 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY + scanAreaHeight - 2, 10, 2)];
    lineView6.backgroundColor = color;
    [self addSubview:lineView6];

    UIView *lineView7 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX + self.scanAreaWidth - 10, self.scanAreaOriginY + scanAreaHeight - 2, 10, 2)];
    lineView7.backgroundColor = color;
    [self addSubview:lineView7];

    UIView *lineView8 = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX + self.scanAreaWidth - 2, self.scanAreaOriginY + scanAreaHeight - 10, 2, 10)];
    lineView8.backgroundColor = color;
    [self addSubview:lineView8];
    
    if (scanningOptions[LSQRCodeScanningLineImageKey] && [scanningOptions[LSQRCodeScanningLineImageKey] isKindOfClass:[UIImage class]]) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY, self.scanAreaWidth, 2)];
        imageView.image = scanningOptions[LSQRCodeScanningLineImageKey];
        self.scanningLineView = imageView;
    } else {
        self.scanningLineView = [[UIView alloc] initWithFrame:CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY, self.scanAreaWidth, 2)];
        self.scanningLineView.backgroundColor = color;
    }
    [self addSubview:self.scanningLineView];
    
    self.torchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.torchButton.backgroundColor = [UIColor clearColor];
    [self.torchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.torchButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    self.torchButton.frame = CGRectMake(self.scanAreaOriginX, self.scanAreaMaxY - 30, self.scanAreaWidth, 30);
    [self.torchButton addTarget:self action:@selector(torchButtonOnClickAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.torchButton];
    self.torchButton.hidden = YES;
}

- (void)scanningAnimation {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:self.animationTime animations:^{
            self.scanningLineView.frame = CGRectMake(self.scanAreaOriginX, self.scanAreaMaxY, self.scanAreaWidth, 2);
        } completion:^(BOOL finished) {
            if (finished) {
                self.scanningLineView.frame = CGRectMake(self.scanAreaOriginX, self.scanAreaOriginY, self.scanAreaWidth, 2);
                [self scanningAnimation];
            }
        }];
    });
}

#pragma mark - 按钮点击事件
- (void)torchButtonOnClickAction {
    
    if (self.torchMode == LSScanQRCodeTorchButtonModeOpenTorch) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOn];//开
        [device unlockForConfiguration];
        [self showCloseTorch];
    } else if (self.torchMode == LSScanQRCodeTorchButtonModeCloseTorch) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOff];
        [device unlockForConfiguration];
        self.torchButton.hidden = YES;
        self.torchMode = LSScanQRCodeTorchButtonModeDefault;
    }
    
}

@end

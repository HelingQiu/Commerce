//
//  LSScanQRCodeProtocol.h
//  LSQRCode
//
//  Created by 韩云彬 on 2018/8/7.
//  Copyright © 2018年 杨荣. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 扫描二维码协议
 */
@protocol LSScanQRCodeProtocol <NSObject>

/** 开始扫码动画 */
- (void)startScanningAnimation;

/** 结束扫描动画 */
- (void)endScanningAnimation;

/** 展示打开闪光灯按钮 */
- (void)showOpenTorch;

/** 展示关闭闪光灯按钮 */
- (void)showCloseTorch;

@end

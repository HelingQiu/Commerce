//
//  LSQRCodeManager.m
//  LSQRCode
//
//  Created by metbao1 on 2018/6/22.
//  Copyright © 2018年 杨荣. All rights reserved.
//

#import "LSQRCodeManager.h"
#import <CoreImage/CoreImage.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "LSScanQRCodeShadowView.h"

LSQRCodeScanningOptionsKey const LSQRCodeScanningAnimationTimeKey = @"LSQRCodeScanningAnimationTimeKey";
LSQRCodeScanningOptionsKey const LSQRCodeScanningLineImageKey = @"LSQRCodeScanningLineImageKey";
LSQRCodeScanningOptionsKey const LSQRCodeScanningViewColorKey = @"LSQRCodeScanningViewColorKey";

@interface LSQRCodeManager () <AVCaptureMetadataOutputObjectsDelegate, AVCaptureVideoDataOutputSampleBufferDelegate>

/** 输入数据源 */
@property (nonatomic, strong) AVCaptureDeviceInput *input;
/** 输出数据源 */
@property (nonatomic, strong) AVCaptureMetadataOutput *output;

@property (nonatomic, strong) AVCaptureVideoDataOutput *lightOutput;

/** 输入输出的中间桥梁 负责把捕获的音视频数据输出到输出设备中 */
@property (nonatomic, strong) AVCaptureSession *session;
/** 相机拍摄预览图层 */
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;


@property (nonatomic, copy) LSQRCodeManagerSuccessBlock completionHandler;
@property (nonatomic, copy) LSQRCodeManagerFailureBlock failureHandler;

/** 展示相机拍摄图层的view */
@property (nonatomic, weak) UIView *view;
/** 扫码遮罩视图，用于呈现扫描动画 */
@property (nonatomic, weak) UIView <LSScanQRCodeProtocol> *scanningAnimationView;
/** 有效扫描区域的尺寸 */
@property (nonatomic, assign) CGSize scanAreaSize;

/** 有效扫描范围 */
@property (nonatomic, assign) CGRect metadataOutputRectOfInterest;
/** 有效扫描区域的frame */
@property (nonatomic, assign) CGRect scanAreaFrame;

/** 扫描页面配置选项 */
@property (nonatomic, strong) NSDictionary <LSQRCodeScanningOptionsKey, id> *scanningOptions;
/** 是否需要扫描动画 */
@property (nonatomic, assign) BOOL isNeedScanningAnimation;

/** app进入后台前是否正在扫描 */
@property (nonatomic, assign) BOOL isScanningBeforeApplicationEnterBackground;

@end

@implementation LSQRCodeManager

- (instancetype)init {
    self = [super init];
    if (self) {
        
        // 监听app进入后台和回到前台的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    return self;
}


- (void)dealloc {
    
    [self stopScanning];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 通知事件
- (void)appDidEnterBackground {
    
    // 当app进入后台时，如果正在扫描，那么停止扫描
    if (self.session.isRunning) {
        self.isScanningBeforeApplicationEnterBackground = YES;
        [self stopScanning];
    } else {
        self.isScanningBeforeApplicationEnterBackground = NO;
    }
}

- (void)appDidBecomeActive {
    
    // 如果app进入后台前正在扫描，那么回到前台时重新扫描
    if (!self.isScanningBeforeApplicationEnterBackground) {
        return;
    }
    
    [self startScanning];
}

- (void)deviceOrientationDidChange {
    
    self.previewLayer.connection.videoOrientation = [self videoOrientation];
}

#pragma mark - 生成二维码相关方法
+ (UIImage *)generateQRCodeImageWithString:(NSString *)stringToGenerate {
    
    return [self generateQRCodeImageWithString:stringToGenerate imageSize:LSQRCodeImageDefaultSize watermarkImage:nil watermarkImageSize:LSQRCodeWatermarkImageDefaultSize];
}

+ (UIImage *)generateQRCodeImageWithString:(NSString *)stringToGenerate imageSize:(CGFloat)imageSize {
    
    return [self generateQRCodeImageWithString:stringToGenerate imageSize:imageSize watermarkImage:nil watermarkImageSize:LSQRCodeWatermarkImageDefaultSize];
}

+ (UIImage *)generateQRCodeImageWithString:(NSString *)stringToGenerate imageSize:(CGFloat)imageSize watermarkImage:(UIImage *)watermarkImage {
    
    return [self generateQRCodeImageWithString:stringToGenerate imageSize:imageSize watermarkImage:watermarkImage watermarkImageSize:watermarkImage.size.width];
}

+ (UIImage *)generateQRCodeImageWithString:(NSString *)stringToGenerate imageSize:(CGFloat)imageSize watermarkImage:(UIImage *)watermarkImage watermarkImageSize:(CGFloat)watermarkImageSize {
    
    // 如果传过来的尺寸有一个小于0
    if (imageSize <= 0.0f || watermarkImageSize <= 0.0f) {
        NSLog(@"LSQRCodeManager类调用错误信息：调用方法: %s，传入的二维码尺寸或水印尺寸不能小于0！", __func__);
        return nil;
    }
    
    // 如果传入的字符串为空
    if (!stringToGenerate) {
        return nil;
    }
    
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    NSData *data = [stringToGenerate dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];// 通过kvo方式给一个字符串，生成二维码
    [filter setValue:@"H" forKey:@"inputCorrectionLevel"];// 设置二维码的纠错水平，越高纠错水平越高，可以污损的范围越大
    CIImage *outPutImage = [filter outputImage];//拿到二维码图片
    
    CGRect extent = CGRectIntegral(outPutImage.extent);
    CGFloat scale = MIN(imageSize/CGRectGetWidth(extent), imageSize/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    //创建一个DeviceGray颜色空间
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    
    //CGBitmapContextCreate(void * _Nullable data, size_t width, size_t height, size_t bitsPerComponent, size_t bytesPerRow, CGColorSpaceRef  _Nullable space, uint32_t bitmapInfo)
    //width：图片宽度像素
    //height：图片高度像素
    //bitsPerComponent：每个颜色的比特值，例如在rgba-32模式下为8
    //bitmapInfo：指定的位图应该包含一个alpha通道。
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    
    //创建CoreGraphics image
    CGImageRef bitmapImage = [context createCGImage:outPutImage fromRect:extent];
    
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef); CGImageRelease(bitmapImage);
    
    // 绘图
    UIImage *outputImage = [UIImage imageWithCGImage:scaledImage];
    UIGraphicsBeginImageContextWithOptions(outputImage.size, NO, [[UIScreen mainScreen] scale]);
    [outputImage drawInRect:CGRectMake(0,0 , imageSize, imageSize)];
    
    // 将logo水印绘制到二维码上
    if (watermarkImage) {
        //把logo水印画到生成的二维码图片上，注意尺寸不要太大（最大不超过二维码图片的%30），太大会造成扫不出来
        // 如果传入的水印尺寸大于二维码尺寸的30%，将尺寸改为二维码尺寸的30%
        if (watermarkImageSize / imageSize > 0.3f) {
            watermarkImageSize = round(imageSize * 0.3f);
        }
        [watermarkImage drawInRect:CGRectMake((imageSize - watermarkImageSize)/2.0, (imageSize - watermarkImageSize)/2.0, watermarkImageSize, watermarkImageSize)];
    }
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return finalImage;
}

#pragma mark - 扫描二维码相关方法
- (void)scanQRCodeOnView:(UIView *)view scanAreaSize:(CGSize)scanAreaSize completionHandler:(LSQRCodeManagerSuccessBlock)completionHandler failureHandler:(LSQRCodeManagerFailureBlock)failureHandler {
    
    [self scanQRCodeOnView:view scanAreaSize:scanAreaSize isNeedScanningAnimation:NO scanningOptions:nil completionHandler:completionHandler failureHandler:failureHandler];
}

- (void)scanQRCodeOnView:(UIView *)view scanAreaSize:(CGSize)scanAreaSize isNeedScanningAnimation:(BOOL)isNeedScanningAnimation scanningOptions:(NSDictionary <LSQRCodeScanningOptionsKey, id> *)scanningOptions completionHandler:(LSQRCodeManagerSuccessBlock)completionHandler failureHandler:(LSQRCodeManagerFailureBlock)failureHandler {
    
    [self scanQRCodeOnView:view scanAreaSize:scanAreaSize scanningAnimationView:nil isNeedScanningAnimation:isNeedScanningAnimation scanningOptions:scanningOptions completionHandler:completionHandler failureHandler:failureHandler];
}

- (void)scanQRCodeOnView:(UIView *)view scanAreaSize:(CGSize)scanAreaSize scanningAnimationView:(UIView<LSScanQRCodeProtocol> *)scanningAnimationView completionHandler:(LSQRCodeManagerSuccessBlock)completionHandler failureHandler:(LSQRCodeManagerFailureBlock)failureHandler {
    
    [self scanQRCodeOnView:view scanAreaSize:scanAreaSize scanningAnimationView:scanningAnimationView isNeedScanningAnimation:YES scanningOptions:nil completionHandler:completionHandler failureHandler:failureHandler];
}

- (void)scanQRCodeOnView:(UIView *)view scanAreaSize:(CGSize)scanAreaSize scanningAnimationView:(UIView<LSScanQRCodeProtocol> *)scanningAnimationView isNeedScanningAnimation:(BOOL)isNeedScanningAnimation scanningOptions:(NSDictionary <LSQRCodeScanningOptionsKey, id> *)scanningOptions completionHandler:(LSQRCodeManagerSuccessBlock)completionHandler failureHandler:(LSQRCodeManagerFailureBlock)failureHandler {
    
    self.completionHandler = completionHandler;
    self.failureHandler = failureHandler;
    self.view = view;
    self.scanAreaSize = scanAreaSize;
    self.scanningOptions = scanningOptions;
    self.isNeedScanningAnimation = isNeedScanningAnimation;
    if (scanningAnimationView && [scanningAnimationView conformsToProtocol:@protocol(LSScanQRCodeProtocol)]) {
        self.scanningAnimationView = scanningAnimationView;
        if (![view.subviews containsObject:scanningAnimationView]) {
            [view addSubview:scanningAnimationView];
        }
    }

    void (^notAuthorizationBlock)(void) = ^{
        NSError *error = [NSError errorWithDomain:@"没有获得摄像头权限" code:-1 userInfo:nil];
        if (failureHandler) {
            failureHandler(error);
        }
    };
    
    // 摄像头权限
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    // 权限判断
    if (status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted) { // 无权限
        notAuthorizationBlock();
    } else if (status == AVAuthorizationStatusNotDetermined) { // 未申请权限
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{ // 允许使用摄像头
                    [self setupCaptureSession];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    notAuthorizationBlock();
                });
            }
        }];
    } else { // 有权限
        [self setupCaptureSession];
    }
}

- (void)stopScanning {
    
    if (self.session.isRunning) {
        [self.session stopRunning];
    }
    
    if (self.scanningAnimationView) {
        [self.scanningAnimationView endScanningAnimation];
    }
    
    // 如果扫描结束的时候，闪光灯不是关闭的，那么关闭闪光灯
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch] && device.torchMode != AVCaptureTorchModeOff) {
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOff];
        [device unlockForConfiguration];
    }
}

- (void)startScanning {
    
    if (self.session) {
        if (self.session.isRunning) {
            [self.session stopRunning];
        }
        [self.session startRunning];
        [self setupScanningAnimationIfNeeded];
    }
}

#pragma mark 扫描二维码辅助方法
// 配置输入输出流session
- (void)setupCaptureSession {

    // 如果session已经创建，那么直接开始扫描
    if (self.session) {
        [self startScanning];
        return;
    }
    
    // 如果上一次使用的是前置摄像头？切换成后置？
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];  //获取摄像设备
    NSError *error = nil;
    self.input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];//创建输出流
    
    if (error) { // 如果输出流创建失败，回调error
        if (self.failureHandler) {
            self.failureHandler(error);
        }
        return;
    }
    
    // 创建输出数据源
    self.output = [[AVCaptureMetadataOutput alloc] init];
    [self.output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // 用于判断亮度的输出流
    self.lightOutput = [[AVCaptureVideoDataOutput alloc] init];
    [self.lightOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    // Session设置
    self.session = [[AVCaptureSession alloc] init];
    [self.session setSessionPreset:AVCaptureSessionPresetHigh];
    
    if ([self.session canAddInput:self.input]) {
        [self.session addInput:self.input];
    }
    if ([self.session canAddOutput:self.output]) {
        [self.session addOutput:self.output];
    }
    if ([self.session canAddOutput:self.lightOutput]) {
        [self.session addOutput:self.lightOutput];
    }

    // 设置扫码预览图层
    self.previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.previewLayer.frame = self.view.bounds;
    self.previewLayer.connection.videoOrientation = [self videoOrientation];

    [self.view.layer addSublayer:self.previewLayer];
    
    [self calculateMetadataOutputRectOfInterestAndScanAreaFrame];
    
    // 设置扫码支持的编码格式，这里只支持二维码
    self.output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
    // 设置有效的扫描范围
    self.output.rectOfInterest = self.metadataOutputRectOfInterest;
    
    [self.session startRunning];
    
    [self setupScanningAnimationIfNeeded];
}

- (void)setupScanningAnimationIfNeeded {
    
    if (!self.isNeedScanningAnimation) {
        return;
    }
    
    if (self.scanningAnimationView) {
        [self.scanningAnimationView startScanningAnimation];
    } else {
        LSScanQRCodeShadowView *shadowView = [[LSScanQRCodeShadowView alloc] initWithFrame:self.view.bounds scanAreaFrame:self.scanAreaFrame scanningOptions:self.scanningOptions];
        [self.view addSubview:shadowView];
        [shadowView startScanningAnimation];
        self.scanningAnimationView = shadowView;
    }
}

// 计算有效的扫描范围和扫描区域的frame
- (void)calculateMetadataOutputRectOfInterestAndScanAreaFrame {
    
    CGSize size = self.previewLayer.frame.size;
    
    CGFloat width1 = size.width;
    CGFloat height1 = size.height;
    CGFloat width2 = self.scanAreaSize.width;
    CGFloat height2 = self.scanAreaSize.height;
    
    // 根据有效扫码范围宽高和输入输出显示图层的宽高计算出最终的有效扫码范围的比例
    CGFloat ratio1 = width2 / width1;
    CGFloat ratio2 = height2 / height1;
    
    if (ratio1 > 1) {
        ratio2 = ratio2 / ratio1;
        ratio1 = 1.0f;
    }
    
    if (ratio2 > 1) {
        ratio1 = ratio1 / ratio2;
        ratio2 = 1.0f;
    }
    
    CGFloat originX = (1.0f - ratio1) / 2.0f;
    CGFloat originY = (1.0f - ratio2) / 2.0f;
    
    self.metadataOutputRectOfInterest = CGRectMake(originY, originX, ratio2, ratio1);
    self.scanAreaFrame = CGRectMake(width1 * originX, height1 * originY, width1 * ratio1, height1 * ratio2);
}

- (AVCaptureVideoOrientation)videoOrientation {
    
    UIInterfaceOrientation statusBarOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    AVCaptureVideoOrientation orientation = AVCaptureVideoOrientationPortrait;
    switch (statusBarOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
            orientation = AVCaptureVideoOrientationLandscapeLeft;
            break;
        case UIInterfaceOrientationLandscapeRight:
            orientation = AVCaptureVideoOrientationLandscapeRight;
            break;
        case UIInterfaceOrientationPortrait:
            orientation = AVCaptureVideoOrientationPortrait;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            orientation = AVCaptureVideoOrientationPortraitUpsideDown;
            break;
        default:
            orientation = AVCaptureVideoOrientationPortrait;
            break;
    }
    return orientation;
}

#pragma mark - 识别图片中的二维码相关方法
+ (NSString *)recognizeQRCodeImageWithImage:(UIImage *)qrCodeImage {
    
    return [self recognizeQRCodeImageWithImage:qrCodeImage isNeedToCompressImage:YES];
}

+ (NSString *)recognizeQRCodeImageWithImage:(UIImage *)qrCodeImage isNeedToCompressImage:(BOOL)isNeedToCompressImage {
    
    // 如果传入的图片为空，返回空结果
    if (!qrCodeImage) {
        return nil;
    }
    
    UIImage *resultImage;
    
    if (isNeedToCompressImage) { // 如果需要压缩图片
        //  如果二维码图片太大，进行宽高等比例压缩以提高识别效率，压缩的最大宽度取设备屏幕宽度
        CGFloat width = qrCodeImage.size.width;
        CGFloat height = qrCodeImage.size.height;
        CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
        if (width > screenWidth) {
            height = height * screenWidth / width;
            width = screenWidth;
        }
        CGRect rect = CGRectMake(0, 0, width, height);
        UIGraphicsBeginImageContext(rect.size);
        [qrCodeImage drawInRect:rect];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    } else {
        resultImage = qrCodeImage;
    }
    
    // 生成Detector
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy: CIDetectorAccuracyHigh}];
    NSData *imageData = UIImagePNGRepresentation(resultImage);
    CIImage *ciImage = [CIImage imageWithData:imageData];
    NSArray *features = [detector featuresInImage:ciImage];

    if (features.count > 0) { // 如果识别有结果，取出结果
        CIQRCodeFeature *feature = [features objectAtIndex:0];
        NSString *scannedResult = feature.messageString;
        return scannedResult;
    } else { // 如果失败无结果
        if (isNeedToCompressImage) { // 如果是压缩图片后进行的识别且无结果，那么用原图再识别一次
            return [self recognizeQRCodeImageWithImage:qrCodeImage isNeedToCompressImage:NO];
        }
        return nil;
    }
}

#pragma mark - 代理方法
#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)output didOutputMetadataObjects:(NSArray<__kindof AVMetadataObject *> *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    
    [self stopScanning];
    
    if (self.completionHandler) {
        self.completionHandler([metadataObjects.firstObject stringValue]);
    }
}

#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    
    CFDictionaryRef metadataDict = CMCopyDictionaryOfAttachments(NULL,sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    NSDictionary *metadata = [[NSMutableDictionary alloc] initWithDictionary:(__bridge NSDictionary*)metadataDict];
    CFRelease(metadataDict);
    NSDictionary *exifMetadata = [[metadata objectForKey:(NSString *)kCGImagePropertyExifDictionary] mutableCopy];
    float brightnessValue = [[exifMetadata objectForKey:(NSString *)kCGImagePropertyExifBrightnessValue] floatValue];
    
    // 根据brightnessValue的值来打开和关闭闪光灯
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    BOOL flag = [device hasTorch];// 判断设备是否有闪光灯
    if (!flag) {
        return;
    }
    
    if (!self.isNeedScanningAnimation && !self.scanningAnimationView) {
        return;
    }
    
    if (brightnessValue < 0.0) {
        [self.scanningAnimationView showOpenTorch];
    }
}

@end

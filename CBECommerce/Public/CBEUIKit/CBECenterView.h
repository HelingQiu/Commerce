//
//  CBECenterView.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/2.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBECenterView : UIView

- (void)configWith:(NSString *)imageName imageSize:(CGSize)imageSize textFont:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)alignment text:(NSString *)text margin:(CGFloat)margin;

@end

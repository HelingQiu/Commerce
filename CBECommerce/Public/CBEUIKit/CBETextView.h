//
//  CBETextView.h
//  CBECommerce
//
//  Created by Rainer on 2019/1/24.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBETextView : UITextView

@end

NS_ASSUME_NONNULL_END

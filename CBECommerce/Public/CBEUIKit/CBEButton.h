//
//  CBEButton.h
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/8/16.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBEButton : UIButton
/**
 UIButton初始化
 
 @param frame 按钮frame
 @param title 按钮标题
 @param titleColor 按钮标题颜色
 @param titleFontSize 按钮标题字体大小
 @param backgroundColor 按钮背景颜色
 @param didClicked 按钮点击后回调
 @return UIButton
 */
- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize backgroundColor:(UIColor *)backgroundColor didClicked:(void(^)())didClicked;

/**
 UIButton初始化
 
 @param frame 按钮frame
 @param title 按钮标题
 @param titleColor 按钮标题颜色
 @param titleFontSize 按钮标题字体大小
 @param backgroundColor 按钮背景颜色
 @param superView  superView
 @param didClicked 按钮点击后回调
 @return UIButton
 */
- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize backgroundColor:(UIColor *)backgroundColor inView:(UIView *)superView didClicked:(void(^)())didClicked;


/**
 UIButton初始化
 
 @param frame 按钮frame
 @param title 按钮标题
 @param titleColor 按钮标题颜色
 @param titleFontSize 按钮标题字体大小
 @param backgroundColor 按钮背景颜色
 @param backgroundImageName 按钮背景图片
 @param highBackgroundImageName 按钮点击或长按后背景颜色
 @param didClicked 按钮点击后回调
 @return UIButton
 */
- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize backgroundColor:(UIColor *)backgroundColor backgroundImageName:(NSString *)backgroundImageName highBackgroundImageName:(NSString *)highBackgroundImageName didClicked:(void(^)())didClicked;


- (instancetype)initWithFrame:(CGRect)frame image:(UIImage *)image highImage:(UIImage *)highImage  didClicked:(void(^)())didClicked;

@end

//
//  CBEPopView.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/30.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEPopView : UIView

/**
 *  创建一个弹出下拉控件
 *
 *  @param frame      尺寸
 *  @param selectData 选择控件的数据源
 *  @param action     点击回调方法
 *  @param animate    是否动画弹出
 */
+ (void)addPopViewSelectWithWindowFrame:(CGRect)frame
                             selectData:(NSArray *)selectData
                                 images:(NSArray *)images
                                 action:(void(^)(NSInteger index))action
                               animated:(BOOL)animate;
/**
 *  手动隐藏
 */
+ (void)hiden;

@end

NS_ASSUME_NONNULL_END

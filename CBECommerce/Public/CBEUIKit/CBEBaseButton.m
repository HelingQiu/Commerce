//
//  CBEBaseButton.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseButton.h"

@implementation CBEBaseButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setExclusiveTouch:YES];
    }
    return self;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return self.imageViewRect;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return self.titleLabelRect;
}

@end

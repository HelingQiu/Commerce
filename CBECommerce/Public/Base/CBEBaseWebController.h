//
//  CBEBaseWebController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import <WebKit/WebKit.h>

@interface CBEBaseWebController : UIViewController

@property (nonatomic, strong) WKWebView *webView;

@end

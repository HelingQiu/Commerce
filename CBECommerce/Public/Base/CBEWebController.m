//
//  CBEWebController.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEWebController.h"

@interface CBEWebController ()

@end

@implementation CBEWebController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
}
//
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"title"]){
        if (object == self.webView) {
            [self setBarTitle:self.webView.title];
        }
    }
}
@end

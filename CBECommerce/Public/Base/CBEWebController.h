//
//  CBEWebController.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseWebController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEWebController : CBEBaseWebController

@property (nonatomic, copy) NSString *urlString;

@end

NS_ASSUME_NONNULL_END

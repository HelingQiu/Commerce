//
//  CBEBaseTabBarController.m
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/7/30.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTabBarController.h"
#import "AppDelegate.h"
#import "CBEBaseNavigationController.h"
#import "CBEUserModel.h"
#import "CBELoginController.h"
#import "CBEHomeController.h"
#import "CBEConnectionsListController.h"
#import "CBENewsController.h"
#import "CBEMessageListController.h"
#import "CBEMineController.h"

@interface CBEBaseTabBarController ()<UITabBarControllerDelegate>

@end

@implementation CBEBaseTabBarController

+ (instancetype)instance{
    AppDelegate *delegete = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *vc = delegete.window.rootViewController;
    if ([vc isKindOfClass:[CBEBaseTabBarController class]]) {
        return (CBEBaseTabBarController *)vc;
    }else{
        return nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = self;
    
    CBEHomeController *homeController = [[CBEHomeController alloc] init];
    CBEBaseNavigationController *homeNav = [[CBEBaseNavigationController alloc] initWithRootViewController:homeController];
    [self addChildViewController:homeNav];
    [self addTitle:@"首页" normalImageName:@"tab_icon_home02" selectedImageName:@"tab_icon_home01" viewController:homeController tag:0];
    
    CBEMessageListController *messageController = [[CBEMessageListController alloc] init];
    CBEBaseNavigationController *messageNav = [[CBEBaseNavigationController alloc] initWithRootViewController:messageController];
    [self addChildViewController:messageNav];
    [self addTitle:@"消息" normalImageName:@"tab_icon_message02" selectedImageName:@"tab_icon_message01" viewController:messageController tag:1];
    
    CBENewsController *newsController = [[CBENewsController alloc] init];
    CBEBaseNavigationController *newsNav = [[CBEBaseNavigationController alloc] initWithRootViewController:newsController];
    [self addChildViewController:newsNav];
    [self addTitle:@"资讯" normalImageName:@"tab_icon_news02" selectedImageName:@"tab_icon_news01" viewController:newsController tag:2];
    
    CBEConnectionsListController *humanController = [[CBEConnectionsListController alloc] init];
    CBEBaseNavigationController *humanNav = [[CBEBaseNavigationController alloc] initWithRootViewController:humanController];
    [self addChildViewController:humanNav];
    [self addTitle:@"人脉" normalImageName:@"tab_icon_renmai02" selectedImageName:@"tab_icon_renmai01" viewController:humanController tag:3];
    
    CBEMineController *mineController = [[CBEMineController alloc] init];
    CBEBaseNavigationController *mineNav = [[CBEBaseNavigationController alloc] initWithRootViewController:mineController];
    [self addChildViewController:mineNav];
    [self addTitle:@"我的" normalImageName:@"tab_icon_me02" selectedImageName:@"tab_icon_me01" viewController:mineController tag:4];
    
    BOOL isLogin = [CBEUserModel sharedInstance].isLogin;
    if (!isLogin) {
        [self setSelectedIndex:2];
    }
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    // 未选中字体颜色
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:kTabbarTextColor} forState:UIControlStateNormal];
    // 选中字体颜色
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:kMainBlueColor} forState:UIControlStateSelected];
}

- (void)addTitle:(NSString *)title normalImageName:(NSString *)normalImageName selectedImageName:(NSString *)selectedImageName viewController:(UIViewController *)viewController tag:(NSInteger)tag
{
    UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:[[UIImage imageNamed:normalImageName] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)] selectedImage:[[UIImage imageNamed:selectedImageName] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)]];
    tabBarItem.tag = tag;
    viewController.tabBarItem = tabBarItem;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    CBEBaseNavigationController *baseNavigationController = (CBEBaseNavigationController *)viewController;
    BOOL isLogin = [CBEUserModel sharedInstance].isLogin;
    if (![baseNavigationController.visibleViewController isKindOfClass:[CBEHomeController class]] && ![baseNavigationController.visibleViewController isKindOfClass:[CBENewsController class]]){
        if (!isLogin) {
            CBELoginController *loginController = [[CBELoginController alloc] init];
            [self presentViewController:loginController animated:YES completion:nil];
            return NO;
        }else{
            [self doLogin];
            return YES;
        }
    } else {
        return YES;
    }
    return YES;
}
- (void)doLogin {
//    if ([CBEUserModel sharedInstance].isLogin) {
//        [[[NIMSDK sharedSDK] loginManager] autoLogin:[CBEUserModel sharedInstance].userInfo.userId token:[CBEUserModel sharedInstance].userInfo.yxToken];
//    }else{
        [[[NIMSDK sharedSDK] loginManager] login:[CBEUserModel sharedInstance].userInfo.userId
                                           token:[CBEUserModel sharedInstance].userInfo.yxToken
                                      completion:^(NSError *error) {
                                          
                                          if (error == nil) {
                                              
                                          }else{
                                              NSString *toast = [NSString stringWithFormat:@"登录失败 code: %zd",error.code];
                                              NSLog(@"%@",toast);
                                          }
                                      }];
//    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

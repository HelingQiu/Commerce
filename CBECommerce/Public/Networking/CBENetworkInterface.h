//
//  CBENetworkInterface.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#ifndef CBENetworkInterface_h
#define CBENetworkInterface_h

#define Host @"http://39.108.175.213:8081/mobile-api"
//#define Host @"http://192.168.0.101:8081/mobile-api"

//获取图形验证码
#define kGetImgVerify @"/getImgVerify"
//获取短信验证码
#define kGetSmsVerifyByImg @"/getSmsVerifyByImg"
//短信登录
#define kSMSDologin @"/doLogin"
//微信登录
#define kWxLogin @"/wxLogin"
//微信绑定
#define kBindWXAccount @"/bindAccount"

//城市列表
#define kCityListApi @"/provinceCity/list"
//首页推荐
#define kHomePageApi @"/index"
//首页搜索
#define kHomeSearchApi @"/queryGroupByKeyword"
//活动查询
#define kQueryActivityListPage @"/activity/queryActivityListPage"
//活动详情
#define kQueryActivityById @"/activity/queryActivityById"
//活动类型
#define kQueryActivityTypeAll @"/activity/queryActivityTypeAll"
//活动标签
#define kQueryActivityTagsAll @"/activity/queryActivityTagsAll"
//活动报名
#define kActivityApply @"/activity/activityApply"

//资讯首页
#define kInformationIndex @"/information/index"
//实操
#define kTopLevelChange @"/information/topLevelChange"
//根据id获取资讯列表
#define kGetInfosByTypeId @"/information/getInfosByTypeId"
//根据二级id获取三四级类型
#define kTwoTypeChange @"/infoType/twoTypeChange"
//资讯检索，根据类型ID和关键字
#define kSelectByKeyword @"/information/selectByKeyword"
//增加阅读数
#define kNewsAddReadCount @"/information/read"
//资讯详情
#define kNewsDetailApi @"/information/mobileDetail"
//收藏和点赞
#define kCollectionAndDianzanApi @"/collectRecord/add"
//取消收藏和点赞
#define kCancelCollectionAndDianzanApi @"/collectRecord/deleteByRealIdAndType"

//我的订单列表
#define kQueryMyOrderByUserId @"/account/queryMyOrderByUserId"
//订单详情
#define kQueryOrderInfoById @"/account/queryOrderInfoByUserId"
//调起支付
#define kCreateOrder @"/pay/createOrder"
//取消活动或者申请退款
#define kActivityRefund @"/account/activityRefund"
#define kGetEticketApi @"/account/getEticket"

//个人中心 参与方数据
#define kMineCustomerInfo @"/myMineClient"
//活动收藏列表
#define kCollectRecordList @"/activity/collectRecordList"
//资讯收藏列表
#define kInformationcollectList @"/information/collectRecordList"

//主办方我的数据
#define kGetSponsorMineService @"/myMineService"
//主办方账单查询
#define kQueryAccountServiceBillList @"/account/serviceBillList"
//账单详情
#define kQueryAccountBillById @"/account/queryAccountBillById"
//申请提现列表
#define kActivityWithdrawList @"/account/activityWithdrawList"
//提现记录
#define kDepositRecordApi @"/account/withdrawList"
//添加支付宝账号
#define kAddZfbAccount @"/sponsor/addzfbAccount"
//七牛存储
#define kGetQiniuToken @"/qinin/getToken"
//编辑主办方信息
#define kModifyOrAddSponsorInfo @"/sponsor/modifyOrAddSponsorInfo"
//获取主办方信息
#define kGetSponsorByUserId @"/sponsor/getSponsorByUserId"
//提现获取验证码
#define kGetWithdrawSmsCode @"/account/getWithdrawSmsCode"
//申请提现提交
#define kApplyDepositApi @"/account/activityWithdraw"
//编辑或新增个人认证信息
#define kModifyOrAddUserAuthInfo @"/sponsor/modifyOrAddUserAuthInfo"
//获取个人认证信息
#define kGetUserAuthByUserId @"/sponsor/getUserAuthByUserId"
//验票接口
#define kVerifyEqTicket @"/account/verifyEqTicket"
//修改电子票
#define kCompleteEqTicket @"/account/completeEqTicket"
//修改手机号发送短信
#define kGetModifiMobileSmsCode @"/user/getModifiMobileSmsCode"
//修改手机号
#define kModifiMobile @"/user/modifiMobile"
//新增意见反馈
#define kAddFeedback @"/feedback/add"

//行业
#define kGetIndustryOrDirectionOrJobTitle @"/industry/getIndustryOrDirectionOrJobTitle"
//学校检索
#define kSearchSchoolApi @"/eduExp/selectSchoolByKeyword"
//编辑名片
#define kEditUserInfo @"/user/editUserInfo"
//人脉首页
#define kQueryUserList @"/user/queryUserList"
//获取名片信息
#define kGetUserInfoApi @"/user/getUserInfo"
//绑定行业和行业方向
#define kUpdateIndustry @"/user/updateIndustry"

//创建群聊
#define kCreateGroup @"/group/saveGroup"
//我的好友
#define kMyFriendApi @"/friend/selectMyFriend"
//我的群聊
#define kMyGroupApi @"/group/queryGroupList"
//代办审批
#define kTodoListApi @"/friend/selectTodoList"
//推荐群和好友
#define kLogRecommend @"/group/backlogRecommend"
//添加好友
#define kAddFriendApi @"/friend/adduserFriend"
//查询群成员
#define kQueryGroupUser @"/group/queryGroupUser"
//修改群名称
#define kEditGroupName @"/group/editGroup"
//退出或者解散群聊
#define kLeaveGroup @"/group/leaveGroup"
//添加群成员
#define kAddGroupUserList @"/group/addGroupUser"
//删除群成员
#define kKickGroupMember @"/group/kickGroup"
//添加好友或者拒绝好友审批
#define kApplyFriendApi @"/friend/applyFriend"
//搜索好友
#define kSearchFriendsApi @"/user/queryNotFriendList"

#endif /* CBENetworkInterface_h */

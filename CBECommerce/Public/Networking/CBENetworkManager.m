//
//  CBENetworkManager.m
//  CBECommerce
//
//  Created by don on 2018/9/13.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBENetworkManager.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>

#define CBE_ERROR [NSError errorWithDomain:@"com.YKNSURLErrorCancelled.ErrorDomain" code:-999 userInfo:@{NSLocalizedDescriptionKey:@"网络出现错误，请检查网络连接"}]

static CBENetworkManager *instance = nil;
static AFNetworkReachabilityStatus _networkReachabilityStatus;

@interface CBENetworkManager ()

@property (nonatomic, strong) AFHTTPSessionManager *manager;// 网络管理工具
@property (nonatomic, strong) NSMutableArray *sessionTaskArry;//任务数组

@end

@implementation CBENetworkManager

+ (void)load{
    //开始监听网络
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        _networkReachabilityStatus = status;
    }];
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    });
    return instance;
}

- (NSString *)baseUrl
{
    if (_baseUrl.length == 0) {
        return Host;
    }
    return _baseUrl;
}

- (NSMutableArray *)sessionTaskArry
{
    if (!_sessionTaskArry) {
        _sessionTaskArry = [NSMutableArray array];
    }
    return _sessionTaskArry;
}

//由于这是一个单例 因此如果更改设置后，需要在别的请求中更改回来
- (AFHTTPSessionManager *)manager
{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        [_manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
        
        NSLog(@"%@----====%@",[CBEUserModel sharedInstance].token,_manager.requestSerializer.HTTPRequestHeaders);
        if (self.requestType == HTTPRequestTypeJSON) {
            _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        }
        [_manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        if (self.responseType == HTTPResponseTypeDATA) {
            _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        }else if (self.responseType == HTTPResponseTypeXML) {
            _manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
        }
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
        _manager.requestSerializer.timeoutInterval = self.timeout<=0?CEBNetWorkTimeoutInterval:self.timeout;
        
        for (NSString *key in self.httpHeadDict.allKeys) {
            if (self.httpHeadDict[key] != nil) {
                [_manager.requestSerializer setValue:self.httpHeadDict[key] forHTTPHeaderField:key];
            }
        }
        _manager.operationQueue.maxConcurrentOperationCount = 5;
        _manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _manager.securityPolicy.allowInvalidCertificates = YES;
        [_manager.securityPolicy setValidatesDomainName:NO];
    }
    [_manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
    return _manager;
}

- (void)setRequestType:(HTTPRequestType)requestType
{
    if (requestType == HTTPRequestTypeHTTP) {
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    }
    [_manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [_manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
}

- (void)setResponseType:(HTTPResponseType)responseType
{
    _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    if (responseType == HTTPResponseTypeDATA) {
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else if (responseType == HTTPResponseTypeXML) {
        _manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    }
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
}

- (void)setHttpField
{
    [_manager.requestSerializer setValue:[CBEUserModel sharedInstance].token forHTTPHeaderField:@"token"];
}

- (void)removeHttpField
{
    
}

- (void)getRequestWithUrl:(NSString *)urlPath
                parameter:(NSDictionary *)params
          timeoutInterval:(NSInteger)timeoutInterval
                 progress:(CBENetWorkProgress)progress
                  succeed:(CBENetWorkSucceed)succeed
                  failure:(CBENetWorkFailure)failure
{
    [self requestAdvWithUrl:[NSString stringWithFormat:@"%@%@",self.baseUrl,urlPath] httpRequestMode:HTTPRequestModeGET parameters:params timeoutInterval:timeoutInterval progress:progress succeed:succeed failure:failure];
}

- (void)postRequestWithUrl:(NSString *)urlPath
                 parameter:(NSDictionary *)params
           timeoutInterval:(NSInteger)timeoutInterval
                  progress:(CBENetWorkProgress)progress
                   succeed:(CBENetWorkSucceed)succeed
                   failure:(CBENetWorkFailure)failure
{
    [self requestAdvWithUrl:[NSString stringWithFormat:@"%@%@",self.baseUrl,urlPath] httpRequestMode:HTTPRequestModePOST parameters:params timeoutInterval:timeoutInterval progress:progress succeed:succeed failure:failure];
}


- (void)requestAdvWithUrl:(NSString *)urlPath
          httpRequestMode:(HTTPRequestMode)httpRequestMode
               parameters:(id)parameters
          timeoutInterval:(NSInteger)timeoutInterval
                 progress:(CBENetWorkProgress)progress
                  succeed:(CBENetWorkSucceed)succeed
                  failure:(CBENetWorkFailure)failure
{
    //网络不行时直接返回失败
    if (_networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable) {
        if (failure){
            failure(CBE_ERROR);
        }
        return;
    }
    
    switch (httpRequestMode) {
        case HTTPRequestModeGET:
        {
            NSURLSessionDataTask *task = [self.manager GET:urlPath parameters:parameters progress:progress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self.sessionTaskArry removeObject:task];
                if (self.openDebugMode) {
                    NSLog(@"请求地址:%@",urlPath);
                    NSLog(@"请求参数:%@",parameters);
                    NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
                    NSLog(@"后台返回的数据：%@",responseObject);
                }
                succeed(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                [self.sessionTaskArry removeObject:task];
                if (self.openDebugMode) {
                    NSLog(@"请求地址:%@",urlPath);
                    NSLog(@"请求参数:%@",parameters);
                    NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
                    NSLog(@"后台返回的数据：%@",error.userInfo);
                }
                failure(error);
            }];
            
            [self.sessionTaskArry addObject:task];
        }
            break;
        case HTTPRequestModePOST:{
            
            NSURLSessionDataTask *task = [self.manager POST:urlPath parameters:parameters progress:progress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self.sessionTaskArry removeObject:task];
                if (self.openDebugMode) {
                    NSLog(@"请求地址:%@",urlPath);
                    NSLog(@"请求参数:%@",parameters);
                    NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
                    NSLog(@"后台返回的数据：%@",responseObject);
                }
                succeed(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                [self.sessionTaskArry removeObject:task];
                if (self.openDebugMode) {
                    NSLog(@"请求地址:%@",urlPath);
                    NSLog(@"请求参数:%@",parameters);
                    NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
                    NSLog(@"后台返回的数据：%@",error.userInfo);
                }
                failure(error);
            }];
            [self.sessionTaskArry addObject:task];
        }
        default:
            break;
    }
}

- (void)uploadWithImage:(UIImage *)image
                    url:(NSString *)urlPath
               filename:(NSString *)filename
                   name:(NSString *)name
               mimeType:(NSString *)mimeType
             parameters:(id)parameters
               progress:(CBENetWorkProgress)progress
                succeed:(CBENetWorkSucceed)succeed
                failure:(CBENetWorkFailure)failure
{
    //网络不行时直接返回失败
    if (_networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable) {
        if (failure){
            failure(CBE_ERROR);
        }
        return;
    }
    NSString *path = [NSString stringWithFormat:@"%@%@",self.baseUrl,urlPath];
    NSURLSessionDataTask *task = [_manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);//压缩
        NSString *imageFileName = filename;
        if (filename == nil || ![filename isKindOfClass:[NSString class]] || filename.length == 0) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            imageFileName = [NSString stringWithFormat:@"%@.jpg", str];
        }
        
        // 上传图片，以文件流的格式
        [formData appendPartWithFileData:imageData name:name fileName:imageFileName mimeType:mimeType];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        progress(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.sessionTaskArry removeObject:task];
        if (self.openDebugMode) {
            NSLog(@"请求地址:%@",urlPath);
            NSLog(@"请求参数:%@",parameters);
            NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
            NSLog(@"后台返回的数据：%@",responseObject);
        }
        succeed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self.sessionTaskArry removeObject:task];
        if (self.openDebugMode) {
            NSLog(@"请求地址:%@",urlPath);
            NSLog(@"请求参数:%@",parameters);
            NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
            NSLog(@"后台返回的数据：%@",error.userInfo);
        }
        failure(error);
    }];
    [self.sessionTaskArry addObject:task];
}

- (void)downloadWithUrl:(NSString *)urlPath
             saveToPath:(NSString *)saveToPath
               progress:(CBENetWorkProgress)progress
                succeed:(CBENetWorkSucceed)succeed
                failure:(CBENetWorkFailure)failure
{
    //网络不行时直接返回失败
    if (_networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable) {
        if (failure){
            failure(CBE_ERROR);
        }
        return;
    }
    NSString *path = [NSString stringWithFormat:@"%@%@",self.baseUrl,urlPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    NSURLSessionDownloadTask *task = [_manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        progress(downloadProgress);
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        if (saveToPath) {
            documentsDirectoryURL = [NSURL URLWithString:saveToPath];
        }
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        [self.sessionTaskArry removeObject:task];
        
        if (error == nil) {
            if (self.openDebugMode) {
                NSLog(@"请求地址:%@",urlPath);
                NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
                NSLog(@"后台返回的数据：%@",response);
            }
            succeed(filePath.path);
        }else{
            if (self.openDebugMode) {
                NSLog(@"请求地址:%@",urlPath);
                NSLog(@"请求方式:%@",task.currentRequest.HTTPMethod);
                NSLog(@"后台返回的数据：%@",error.userInfo);
            }
            failure(error);
        }
    }];
    [task resume];
    [self.sessionTaskArry addObject:task];
}

- (void)cancelAllRequest
{
    @synchronized(self) {
        [self.sessionTaskArry enumerateObjectsUsingBlock:^(NSURLSessionDataTask * _Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([task isKindOfClass:[NSURLSessionDataTask class]]) {
                [task cancel];
            }
        }];
        
        [self.sessionTaskArry removeAllObjects];
    };
}


- (void)cancelRequestWithURL:(NSString *)url
{
    if (url == nil) {
        return;
    }
    @synchronized(self) {
        [self.sessionTaskArry enumerateObjectsUsingBlock:^(NSURLSessionDataTask * _Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([task isKindOfClass:[NSURLSessionDataTask class]]
                &&  [self urlString:task.currentRequest.URL.absoluteString  ContainsString:url]) {
                [task cancel];
                [self.sessionTaskArry removeObject:task];
                return;
            }
        }];
    };
}

- (BOOL)urlString:(NSString*)url ContainsString:(NSString *)string {
    if (string == nil) return NO;
    return [url rangeOfString:string].location != NSNotFound;
}

//url编码(含有中文需进行url编码)
- (NSString *)cbeURLEncode:(NSString *)url {
    NSString *newString =
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)url,
                                                              NULL,
                                                              CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
    if (newString) {
        return newString;
    }
    
    return url;
}

@end

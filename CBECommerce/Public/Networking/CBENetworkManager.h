//
//  CBENetworkManager.h
//  CBECommerce
//
//  Created by don on 2018/9/13.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CBENetWorkProgress)(NSProgress *progress);
typedef void(^CBENetWorkSucceed)(id data);
typedef void(^CBENetWorkFailure)(NSError *error);

static NSTimeInterval CEBNetWorkTimeoutInterval = 30;

typedef NS_ENUM(NSUInteger, HTTPRequestMode) {
    HTTPRequestModePOST = 0,
    HTTPRequestModeGET
};
typedef NS_ENUM(NSUInteger, HTTPRequestType) {
    HTTPRequestTypeJSON = 0,
    HTTPRequestTypeHTTP
};
typedef NS_ENUM(NSUInteger, HTTPResponseType) {
    HTTPResponseTypeJSON = 0,
    HTTPResponseTypeDATA,
    HTTPResponseTypeXML
};
@interface CBENetworkManager : NSObject

@property (nonatomic, copy) NSString *baseUrl;
@property (nonatomic, assign) NSTimeInterval timeout;
@property (nonatomic, assign) BOOL openDebugMode;
@property (nonatomic, strong) NSDictionary *httpHeadDict;
@property (nonatomic, assign) HTTPRequestType requestType;//默认json
@property (nonatomic, assign) HTTPResponseType responseType;//默认json

+ (instancetype)sharedInstance;
- (void)setHttpField;

- (void)getRequestWithUrl:(NSString *)urlPath
                parameter:(NSDictionary *)params
          timeoutInterval:(NSInteger)timeoutInterval
                 progress:(CBENetWorkProgress)progress
                  succeed:(CBENetWorkSucceed)succeed
                  failure:(CBENetWorkFailure)failure;

- (void)postRequestWithUrl:(NSString *)urlPath
                 parameter:(NSDictionary *)params
           timeoutInterval:(NSInteger)timeoutInterval
                  progress:(CBENetWorkProgress)progress
                   succeed:(CBENetWorkSucceed)succeed
                   failure:(CBENetWorkFailure)failure;

- (void)uploadWithImage:(UIImage *)image
                    url:(NSString *)urlPath
               filename:(NSString *)filename
                   name:(NSString *)name
               mimeType:(NSString *)mimeType
             parameters:(id)parameters
               progress:(CBENetWorkProgress)progress
                succeed:(CBENetWorkSucceed)succeed
                failure:(CBENetWorkFailure)failure;

- (void)cancelAllRequest;

- (void)cancelRequestWithURL:(NSString *)url;

@end

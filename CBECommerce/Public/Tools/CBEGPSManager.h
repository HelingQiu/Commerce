//
//  CBEGPSManager.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CBEGPSManager : NSObject<CLLocationManagerDelegate>
{
    CLLocationManager *manager;
    
    // block的申明 定义
    void (^saveGpsCallback) (double lat, double lng);
    void (^failCallback) (NSError *error);
}
//纬度
@property (nonatomic, assign) double latitude;
//经度
@property (nonatomic, assign) double longitude;
//国家
@property (nonatomic, copy) NSString *country;
//省 直辖市
@property (nonatomic, copy) NSString *administrativeArea;
//地级市 直辖市区
@property (nonatomic, copy) NSString *locality;
//县 区
@property (nonatomic, copy) NSString *subLocality;
//街道
@property (nonatomic, copy) NSString *thoroughfare;
//子街道
@property (nonatomic, copy) NSString *subThoroughfare;


+ (id)sharedGpsManager;
// 申明一个 cb的对象
+ (void)getGps:(void (^)(double lat, double lng))cb fail:(void (^) (NSError *error))fail;
+ (void)stop;

@end

//
//  CBEBackAlphaBlackView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/25.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBackAlphaBlackView.h"

@implementation CBEBackAlphaBlackView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor ls_colorWithHex:0x000000 andAlpha:0.7];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

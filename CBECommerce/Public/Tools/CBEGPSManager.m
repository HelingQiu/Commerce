//
//  CBEGPSManager.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEGPSManager.h"

@implementation CBEGPSManager
{
    BOOL _isLocationSuccess;
}
+ (id)sharedGpsManager {
    static id s;
    if (s == nil) {
        s = [[CBEGPSManager alloc] init];
    }
    return s;
}

- (id)init {
    self = [super init];
    if (self) {
        // 打开定位 然后得到数据
        manager = [[CLLocationManager alloc] init];
        manager.delegate = self;
        manager.desiredAccuracy = kCLLocationAccuracyBest;
        
        // 兼容iOS8.0版本
        /* Info.plist里面加上2项
         NSLocationAlwaysUsageDescription      Boolean YES
         NSLocationWhenInUseUsageDescription   Boolean YES
         */
        
        // 请求授权 requestWhenInUseAuthorization用在>=iOS8.0上
        // respondsToSelector: 前面manager是否有后面requestWhenInUseAuthorization方法
        // 1. 适配 动态适配
        if ([manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [manager requestWhenInUseAuthorization];
            [manager requestAlwaysAuthorization];
        }
        // 2. 另外一种适配 systemVersion 有可能是 8.1.1
        float osVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (osVersion >= 8) {
            [manager requestWhenInUseAuthorization];
            [manager requestAlwaysAuthorization];
        }
    }
    return self;
}

- (void)getGps:(void (^)(double lat, double lng))cb fail:(void (^) (NSError *error))fail
{
    if ([CLLocationManager locationServicesEnabled] == FALSE) {
        return;
    }
    
    _isLocationSuccess = NO;
    
    // block一般赋值需要copy
    saveGpsCallback = [cb copy];
    failCallback = [fail copy];
    // 停止上一次的
    [manager stopUpdatingLocation];
    // 开始新的数据定位
    [manager startUpdatingLocation];
}

+ (void)getGps:(void (^)(double lat, double lng))cb fail:(void (^) (NSError *error))fail
{
    [[CBEGPSManager sharedGpsManager] getGps:cb fail:fail];
}

- (void)stop
{
    [manager stopUpdatingLocation];
}

+ (void)stop
{
    [[CBEGPSManager sharedGpsManager] stop];
}

// 每隔一段时间就会调用
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (locations.count) {
        CLLocation *loc = [locations lastObject];
        CLLocationCoordinate2D l = loc.coordinate;
        double lat = l.latitude;
        double lnt = l.longitude;
        
        self.latitude = lat;
        self.longitude = lnt;
        //根据经纬度反向地理编译出地址信息
        CLGeocoder * geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:loc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            if (placemarks.count > 0) {
                CLPlacemark *placemark = placemarks.firstObject; //存储位置信息
                self.country = placemark.country;
                self.administrativeArea = placemark.administrativeArea;
                if ([placemark.locality containsString:@"市"]) {
                    self.locality = [placemark.locality stringByReplacingOccurrencesOfString:@"市" withString:@""];
                }else{
                    self.locality = placemark.locality;
                }
                
                self.subLocality = placemark.subLocality;
                self.thoroughfare = placemark.thoroughfare;
                self.subThoroughfare = placemark.subThoroughfare;
            }
            // 使用blocks 调用blocks
            if (![CommonUtils isBlankString:self.locality] && !self->_isLocationSuccess) {
                self->_isLocationSuccess = YES;
                if (self->saveGpsCallback) {
                    self->saveGpsCallback(lat, lnt);
                }
            }
        }];
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    if (![CommonUtils isBlankString:self.locality] && !self->_isLocationSuccess) {
        if (failCallback) {
            failCallback(error);
        }
    }
}

@end

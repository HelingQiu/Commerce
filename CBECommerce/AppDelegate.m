//
//  AppDelegate.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/21.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "AppDelegate.h"
#import "CBEBaseTabBarController.h"
#import "CBEUserModel.h"
#import "CBELoginController.h"
// 引入JPush功能所需头文件
#import "JPUSHService.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

#import <KSGuaidViewManager.h>

#import <UMCommon/UMCommon.h>
#import <UMShare/UMShare.h>
#import "NTESSDKConfigDelegate.h"
#import "NTESBundleSetting.h"
#import "NTESDemoConfig.h"
#import <AlipaySDK/AlipaySDK.h>
#import <IQKeyboardManager.h>
#import "WXApi.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <JJException.h>
#import <NIMKit.h>
#import "CBESessionConfig.h"

@interface AppDelegate ()<JPUSHRegisterDelegate>

@property (nonatomic, strong) NTESSDKConfigDelegate *sdkConfigDelegate;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //捕获异常防止闪退
    [JJException configExceptionCategory:JJExceptionGuardAllExceptZombie];
    [JJException startGuardException];    
    
    NSArray *array = @[];
    if (IS_IPHONE_4S) {
        array = @[IMAGE_NAMED(@"iphone4_1"),
                  IMAGE_NAMED(@"iphone4_2"),
                  IMAGE_NAMED(@"iphone4_3")];
    }else if (IS_IPHONE_5S) {
        array = @[IMAGE_NAMED(@"iphonese_1"),
                  IMAGE_NAMED(@"iphonese_2"),
                  IMAGE_NAMED(@"iphonese_3")];
    }else if (IS_IPHONE_6S) {
        array = @[IMAGE_NAMED(@"iphone8_1"),
                  IMAGE_NAMED(@"iphone8_2"),
                  IMAGE_NAMED(@"iphone8_3")];
    }else if (IS_IPHONE_6S_PLUS) {
        array = @[IMAGE_NAMED(@"iphone8plus_1"),
                  IMAGE_NAMED(@"iphone8plus_2"),
                  IMAGE_NAMED(@"iphone8plus_3")];
    }else if (IS_IPHONE_X_XS) {
        array = @[IMAGE_NAMED(@"iphone8plus_1"),
                  IMAGE_NAMED(@"iphone8plus_2"),
                  IMAGE_NAMED(@"iphone8plus_3")];
    }else if (IS_IPHONE_XR) {
        array = @[IMAGE_NAMED(@"iphonexr_1"),
                  IMAGE_NAMED(@"iphonexr_2"),
                  IMAGE_NAMED(@"iphonexr_3")];
    }else{
        array = @[IMAGE_NAMED(@"iphonexsmax_1"),
                  IMAGE_NAMED(@"iphonexsmax_2"),
                  IMAGE_NAMED(@"iphonexsmax_3")];
    }
    KSGuaidManager.images = array;
    KSGuaidManager.shouldDismissWhenDragging = YES;
    KSGuaidManager.currentPageIndicatorTintColor = kMainBlueColor;
    KSGuaidManager.pageIndicatorTintColor = [UIColor ls_colorWithHexString:@"#D5F4FF"];
    
    [KSGuaidManager begin];
    
    // 创建window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    // 设置window根视图
    self.window.rootViewController = [self getWindowRootController];
    // 显示window
    [self.window makeKeyAndVisible];
    
//    if (@available(iOS 11.0, *)){
//        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
//    }
    //未定位或者选择城市时，设置默认选中全国
    [self startDefaultCity];
    //高德地图
    [AMapServices sharedServices].apiKey = (NSString *)AMapAPIKey;
    //极光推送
    [self startJPushWith:launchOptions];
    //友盟
    [UMConfigure initWithAppkey:kUmengAppKey
                        channel:kUmengChannel];
    [self configUSharePlatforms];
    [WXApi registerApp:kWeixinAppKey];
    //网易云信初始化
    [self setupNIMSDK];
    // 设置本地数据库
    [self setupDB];
    
    return YES;
}

- (void)setupNIMSDK
{
    //配置额外配置信息 （需要在注册 appkey 前完成
    self.sdkConfigDelegate = [[NTESSDKConfigDelegate alloc] init];
    [[NIMSDKConfig sharedConfig] setDelegate:self.sdkConfigDelegate];
    [[NIMSDKConfig sharedConfig] setShouldSyncUnreadCount:YES];
    [[NIMSDKConfig sharedConfig] setMaxAutoLoginRetryTimes:10];
    [[NIMSDKConfig sharedConfig] setMaximumLogDays:[[NTESBundleSetting sharedConfig] maximumLogDays]];
    [[NIMSDKConfig sharedConfig] setShouldCountTeamNotification:[[NTESBundleSetting sharedConfig] countTeamNotification]];
    [[NIMSDKConfig sharedConfig] setAnimatedImageThumbnailEnabled:[[NTESBundleSetting sharedConfig] animatedImageThumbnailEnabled]];
    
    
    //appkey 是应用的标识，不同应用之间的数据（用户、消息、群组等）是完全隔离的。
    //如需打网易云信 Demo 包，请勿修改 appkey ，开发自己的应用时，请替换为自己的 appkey 。
    //并请对应更换 Demo 代码中的获取好友列表、个人信息等网易云信 SDK 未提供的接口。
    NSString *appKey        = [[NTESDemoConfig sharedConfig] appKey];
    NIMSDKOption *option    = [NIMSDKOption optionWithAppKey:appKey];
    option.apnsCername      = [[NTESDemoConfig sharedConfig] apnsCername];
    option.pkCername        = [[NTESDemoConfig sharedConfig] pkCername];
    [[NIMSDK sharedSDK] registerWithOption:option];
    
    //注册自定义消息的解析器
//    [NIMCustomObject registerCustomDecoder:[NTESCustomAttachmentDecoder new]];
    
    //注册 NIMKit 自定义排版配置
//    [[NIMKit sharedKit] registerLayoutConfig:[NTESCellLayoutConfig new]];
    
    BOOL isUsingDemoAppKey = [[NIMSDK sharedSDK] isUsingDemoAppKey];
    [[NIMSDKConfig sharedConfig] setTeamReceiptEnabled:isUsingDemoAppKey];
    
    if ([[CBEUserModel sharedInstance] isLogin]) {
        [[[NIMSDK sharedSDK] loginManager] login:[CBEUserModel sharedInstance].userInfo.userId
                                           token:[CBEUserModel sharedInstance].userInfo.yxToken
                                      completion:^(NSError *error) {
                                          
                                          if (error == nil) {
                                              
                                          }else{
                                              NSString *toast = [NSString stringWithFormat:@"登录失败 code: %zd",error.code];
                                              NSLog(@"%@",toast);
                                          }
                                      }];
    }
    [NIMKit sharedKit].config.cellBackgroundColor = UIColorFromRGB(0xF5F5F5);
}

- (void)startDefaultCity
{
    [CommonUtils getDataWithKey:kLocationCityName Completion:^(BOOL finish, id obj) {
        if(!obj) {
            [CommonUtils storageDataWithObject:kDefaultCityName Key:kLocationCityName Completion:^(BOOL finish, id obj) {
                
            }];
        }
    }];
}

- (void)startJPushWith:(NSDictionary *)launchOptions
{
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    [JPUSHService setupWithOption:launchOptions
                           appKey:kJPushAppKey
                          channel:kJPushChannel
                 apsForProduction:NO];
}

- (void)configUSharePlatforms
{
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:kWeixinAppKey appSecret:kWeixinSecret redirectURL:@"http://mobile.umeng.com/social"];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    [[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite)]];
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:kQQAppKey  appSecret:kQQSecret redirectURL:@"http://mobile.umeng.com/social"];
}

#pragma mark - 获取widow根视图
- (UIViewController *)getWindowRootController
{
    CBEBaseTabBarController *tabBarVC = [[CBEBaseTabBarController alloc] init];
    return tabBarVC;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}
#pragma mark- JPUSHRegisterDelegate

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler  API_AVAILABLE(ios(10.0)){
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler  API_AVAILABLE(ios(10.0)){
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [MagicalRecord cleanUp];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
        if ([url.host isEqualToString:@"safepay"]) {
            //跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
            }];
        }else if([url.scheme isEqualToString:kWeixinAppKey]){
            NSString *urlString = [url absoluteString];
            if ([urlString containsString:@"pay"]) {
                NSArray *array = [urlString componentsSeparatedByString:@"?"];
                NSString *resultCode = [array lastObject];
                array = [resultCode componentsSeparatedByString:@"&"];
                NSString *ret = [array lastObject];
                int retCode = [[[ret componentsSeparatedByString:@"="] lastObject] intValue];
                if (retCode == 0) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPaySuccessed object:@(1)];
                }else{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPaySuccessed object:@(0)];
                }
            }
            return YES;
        }
        return YES;
    }
    
    return result;
}
// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    if (!result) {
        if ([url.host isEqualToString:@"safepay"]) {
            //跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
            }];
        }else if([url.scheme isEqualToString:kWeixinAppKey]){
            NSString *urlString = [url absoluteString];
            if ([urlString containsString:@"pay"]) {
                NSArray *array = [urlString componentsSeparatedByString:@"?"];
                NSString *resultCode = [array lastObject];
                array = [resultCode componentsSeparatedByString:@"&"];
                NSString *ret = [array lastObject];
                int retCode = [[[ret componentsSeparatedByString:@"="] lastObject] intValue];
                if (retCode == 0) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPaySuccessed object:@(1)];
                }else{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPaySuccessed object:@(0)];
                }
            }
            return YES;
        }
        return YES;
    }
    return result;
}
#pragma mark - 初始化数据库
- (void)setupDB
{
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:[self dbStore]];
    [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelError];//coredata只输出错误日志
}
#pragma mark - 数据库名称
- (NSString *)dbStore
{
    return [NSString stringWithFormat:@"kuajinghelp.sqlite"];
}
#pragma mark 删除数据库并重新初始化，用于账号切换
- (BOOL)cleanAndResetupDB
{
    NSString *dbStore = [self dbStore];
    
    NSError *error1 = nil;
    NSError *error2 = nil;
    NSError *error3 = nil;
    
    NSURL *storeURL = [NSPersistentStore MR_urlForStoreName:dbStore];//获取数据库路径
    NSURL *walURL = [[storeURL URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-wal"];
    NSURL *shmURL = [[storeURL URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-shm"];
    
    [MagicalRecord cleanUp];//清除缓存
    //删除数据库文件
    if([[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error1] && [[NSFileManager defaultManager] removeItemAtURL:walURL error:&error2] && [[NSFileManager defaultManager] removeItemAtURL:shmURL error:&error3]){
        [self setupDB];//重新初始化
        return YES;
    }
    else{
        NSLog(@"Error1 : %@", error1.description);
        NSLog(@"Error2 : %@", error2.description);
        NSLog(@"Error3 : %@", error3.description);
        return NO;
    }
}
@end

//
//  CEBVentorKeys.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#ifndef CEBVentorKeys_h
#define CEBVentorKeys_h

#define kUmengAppKey @"5bdbc828f1f5564bed000050"
#define kUmengChannel @"development"

#define kJPushAppKey @"950596139115f0ed905aaf89"
#define kJPushChannel @"development"

#define kWeixinAppKey @"wx2af6a50a430547c3"
#define kWeixinSecret @"30191580762519f49c4d475570f1ec70"

#define kQQAppKey @"101522040"
#define kQQSecret @"7ed4d46fbb921d9d85204dff8d9dd33b"

#define AMapAPIKey @"2d2f28436ff41da8967a906509f49290"

//支付成功回调
#define kPaySuccessed @"paySuccessed"
//首页查询通知
#define kHomeSearchMore @"homeSearchNote"
//置顶通知
#define kSessionTopNotification @"SessionTopNotification"
//修改群名称通知
#define kEditGroupNameSuccessNotification @"groupNameNotification"
//添加群成员通知
#define kAddGroupMemberNotification @"addGroupMemberNotification"

#endif /* CEBVentorKeys_h */

//
//  CBEColorAndFontDefines.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/2.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#ifndef CBEColorAndFontDefines_h
#define CBEColorAndFontDefines_h

//RGB颜色
#define kRGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
// 随机颜色
#define KRandomColor kRGB(arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256))

// 十六进制颜色
#define kCOLOR_WITH_HEX(hexValue) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16)) / 255.0 green:((float)((hexValue & 0xFF00) >> 8)) / 255.0 blue:((float)(hexValue & 0xFF)) / 255.0 alpha:1.0f]

#pragma mark - UIColor宏定义
#define UIColorFromRGBA(rgbValue, alphaValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:alphaValue]

#define UIColorFromRGB(rgbValue) UIColorFromRGBA(rgbValue, 1.0)

//字体大小
#define kFont(a) [UIFont systemFontOfSize:a]
#define kBoldFont(a) [UIFont boldSystemFontOfSize:a]
#define Message_Font_Size   14        // 普通聊天文字大小
#define Notification_Font_Size   10   // 通知文字大小
#define Chatroom_Message_Font_Size 16 // 聊天室聊天文字大小

#define kMainBlueColor [UIColor ls_colorWithHexString:@"#53C6F0"]//蓝色主色调
#define kMainDefaltGrayColor [UIColor ls_colorWithHexString:@"#F5F5F5"]//默认灰色背景色
#define kMainTextColor [UIColor ls_colorWithHexString:@"#999999"]//文本颜色
#define kTabbarTextColor [UIColor ls_colorWithHexString:@"##92A1B3"]//tabbar文字颜色
#define kMainBlackColor [UIColor ls_colorWithHexString:@"#222222"]//主黑色调
#define kMainSubTextColor [UIColor ls_colorWithHexString:@"#666666"]

//主橙色
#define kMainOrangeColor [UIColor ls_colorWithHexString:@"#FC6B3F"]
//分割线颜色
#define kMainLineColor [UIColor ls_colorWithHexString:@"#E6E6E6"]

#endif /* CBEColorAndFontDefines_h */

//
//  Commons.h
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/7/30.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#ifndef Commons_h
#define Commons_h

#define kSCREEN_HEIGHT  MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width)
#define kSCREEN_WIDTH   MIN([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width)
#define UISreenWidthScale   kSCREEN_WIDTH / 320

#define kWidthRatio (kSCREEN_WIDTH == 375?1:(kSCREEN_WIDTH == 320?320/375.0:414/375.0))
#define kHeightScale(a) a*kWidthRatio

#define kWeakSelf  typeof(self) __weak weakSelf = self;// 弱引用self
#define kStrongSelf  typeof(weakSelf) __strong strongSelf = weakSelf;// 强引用weakSelf
#define kBlockSelf __block typeof(self) blockSelf = self;// __block

#define weakify(o) autoreleasepool{} __weak typeof(o) o##Weak = o; //弱引用
#define strongify(o) autoreleasepool{} __strong typeof(o) o = o##Weak;//强引用

#define kDeviceSystemVersion [[[UIDevice currentDevice] systemVersion] floatValue] // 设备系统版本号
#define kIOS8  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define kIOS10 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define kIOS11 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)

//像素除以2 H
#define kHalf(a) (a/2.0f)
#define k6sHeight 667.0f

#define NavBarHeight 64
#define iPhoneXNavBarHeight 88
#define kStatusHeight (KIsiPhoneX ? 44:20)
#define KNavigationBarHeight (KIsiPhoneX ? 88:64)//导航栏高度
#define KiPhoneBottomNoSafeAreaDistance (KIsiPhoneX ? 34:0)//底部非安全区域的高度（iPhoneX是34，其它iPhone是0）
#define KIsiPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)||([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)

// IPHONE_4S
#define IS_IPHONE_4S ([UIScreen instancesRespondToSelector:@selector(currentMode)]? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
// IPHONE_5
#define IS_IPHONE_5S ([UIScreen instancesRespondToSelector:@selector(currentMode)]? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
// IPHONE_6
#define IS_IPHONE_6S ([UIScreen instancesRespondToSelector:@selector(currentMode)]? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
// IPHONE_6P
#define IS_IPHONE_6S_PLUS ([UIScreen instancesRespondToSelector:@selector(currentMode)]? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
//IPHONE_X 1125*2436
#define IS_IPHONE_X_XS ([UIScreen instancesRespondToSelector:@selector(currentMode)]? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
//IPHONE_XR
#define IS_IPHONE_XR ([UIScreen instancesRespondToSelector:@selector(currentMode)]? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)
//IPHONE_XS_MAX
#define IS_IPHONE_XS_MAX ([UIScreen instancesRespondToSelector:@selector(currentMode)]? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)

#define kTabbarHeight (KiPhoneBottomNoSafeAreaDistance + 49)

#define kPadding10 10
#define kPadding15 15

#define StringFormat(...) [NSString stringWithFormat:__VA_ARGS__]




#define IMAGE_NAMED(a) [UIImage imageNamed:a]

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

#define dispatch_async_main_safe(block)\
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}

// 单例宏定义.h
#define singleton_interface(class) +(instancetype) shared##class;
// .m
#define singleton_implementation(class)         \
static class *_instance;                        \
\
+(id) allocWithZone : (struct _NSZone *) zone { \
static dispatch_once_t onceToken;           \
dispatch_once(&onceToken, ^{                \
_instance = [super allocWithZone:zone]; \
});                                         \
\
return _instance;                           \
}                                               \
\
+(instancetype) shared##class                   \
{                                           \
if (_instance == nil) {                     \
_instance = [[class alloc] init];       \
}                                           \
\
return _instance;                            \
}


#endif /* Commons_h */

//
//  CBELocalDataKey.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#ifndef CBELocalDataKey_h
#define CBELocalDataKey_h

#define kDefaultCityName @"全国"//默认全国
#define kLocationLng @"longitude"//经度
#define kLocationLat @"latitude"//纬度
#define kLocationCityName @"cityName"//定位或者选择城市
#define kLocationCityCode @"cityCode"//定位或者选择城市code

#define kSearchCityNotification @"searchCityNote" //搜索城市通知

#endif /* CBELocalDataKey_h */

//
//  FriendInfo+CoreDataClass.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface FriendInfo : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "FriendInfo+CoreDataProperties.h"

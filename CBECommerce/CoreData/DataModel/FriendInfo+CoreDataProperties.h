//
//  FriendInfo+CoreDataProperties.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//
//

#import "FriendInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FriendInfo (CoreDataProperties)

+ (NSFetchRequest<FriendInfo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *friendId;
@property (nullable, nonatomic, copy) NSString *userMobile;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyJobTitle;
@property (nullable, nonatomic, copy) NSString *companyName;
@property (nullable, nonatomic, copy) NSString *userId;

@end

NS_ASSUME_NONNULL_END

//
//  GroupInfo+CoreDataProperties.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//
//

#import "GroupInfo+CoreDataProperties.h"

@implementation GroupInfo (CoreDataProperties)

+ (NSFetchRequest<GroupInfo *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"GroupInfo"];
}

@dynamic groupAvatar;
@dynamic name;
@dynamic remark;
@dynamic teamid;
@dynamic tid;
@dynamic groupId;
@dynamic groupOwnerMobile;
@dynamic type;
@dynamic userMobile;
@dynamic createTime;

@end

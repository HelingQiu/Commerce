//
//  GroupInfo+CoreDataProperties.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//
//

#import "GroupInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GroupInfo (CoreDataProperties)

+ (NSFetchRequest<GroupInfo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *groupAvatar;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *remark;
@property (nullable, nonatomic, copy) NSString *teamid;
@property (nullable, nonatomic, copy) NSString *tid;
@property (nullable, nonatomic, copy) NSString *groupId;
@property (nullable, nonatomic, copy) NSString *groupOwnerMobile;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *userMobile;
@property (nullable, nonatomic, copy) NSString *createTime;

@end

NS_ASSUME_NONNULL_END

//
//  FriendInfo+CoreDataProperties.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//
//

#import "FriendInfo+CoreDataProperties.h"

@implementation FriendInfo (CoreDataProperties)

+ (NSFetchRequest<FriendInfo *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"FriendInfo"];
}

@dynamic friendId;
@dynamic userMobile;
@dynamic userName;
@dynamic companyJobTitle;
@dynamic companyName;

@end

//
//  AppDelegate.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/21.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
/*
 *  清除整个数据库并重新初始化
 *  @return 成功/失败
 */
- (BOOL)cleanAndResetupDB;

@end


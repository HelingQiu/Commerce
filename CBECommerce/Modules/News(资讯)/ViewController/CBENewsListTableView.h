//
//  CBENewsListTableView.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"
NS_ASSUME_NONNULL_BEGIN

@protocol ListDelegate <NSObject>

- (void)didSelectItemWith:(id)model;

@end
@interface CBENewsListTableView : UIView<JXPagerViewListViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) BOOL isNeedFooter;
@property (nonatomic, assign) BOOL isNeedHeader;
@property (nonatomic, assign) BOOL isHeaderRefreshed;   //默认为YES
@property (nonatomic, strong) NSDictionary *categoryDict;
@property (nonatomic, weak) id<ListDelegate>delegate;
- (void)beginFirstRefresh;

@end

NS_ASSUME_NONNULL_END

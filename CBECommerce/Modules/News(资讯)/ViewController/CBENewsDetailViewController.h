//
//  CBENewsDetailViewController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseWebController.h"
#import "CBENewsListModel.h"

@interface CBENewsDetailViewController : CBEBaseWebController

@property (nonatomic, strong) CBENewsListModel *model;

@end

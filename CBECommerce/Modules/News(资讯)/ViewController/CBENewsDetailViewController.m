//
//  CBENewsDetailViewController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBENewsDetailViewController.h"
#import <UShareUI/UShareUI.h>
#import "CBENewsVM.h"
#import "CBELoginController.h"

@interface CBENewsDetailViewController ()

@property (nonatomic, strong) UIButton *collectButton;//收藏按钮
@property (nonatomic, strong) CBEBaseButton *zanButton;//点赞按钮
@property (nonatomic, assign) BOOL collect;//是否收藏
@property (nonatomic, assign) BOOL praise;//是否点赞
@property (nonatomic, assign) NSInteger praiseCount;//点赞数

@end

@implementation CBENewsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"详情"];
    
    [self addRightButton];
    
    self.webView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 44);
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:StringFormat(@"http://39.108.175.213/article/detail.html?id=%@&client=ios",self.model.newsListId)]]];
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 44, kSCREEN_WIDTH, 44 + KiPhoneBottomNoSafeAreaDistance)];
    [bottomView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bottomView];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 0.5)];
    line.backgroundColor = UIColorFromRGB(0xE6E6E6);
    [bottomView addSubview:line];
    
    CBEBaseButton *zanButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    zanButton.frame = CGRectMake(5, 0, 44, 44);
    [zanButton setImageViewRect:CGRectMake(10, 5, 24, 24)];
    [zanButton setImage:IMAGE_NAMED(@"news_icon_zan01") forState:UIControlStateNormal];
    [zanButton setImage:IMAGE_NAMED(@"news_icon_zan02") forState:UIControlStateSelected];
    [zanButton setTitleLabelRect:CGRectMake(0, 29, 44, 14)];
    [zanButton setTitle:@"0" forState:UIControlStateNormal];
    [zanButton.titleLabel setFont:kFont(10)];
    [zanButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [zanButton setTitleColor:kMainOrangeColor forState:UIControlStateNormal];
    [zanButton addTarget:self action:@selector(zanAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:zanButton];
    self.zanButton = zanButton;
    
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(51, 0, kSCREEN_WIDTH - 51 - 5, 44)];
    [rightLabel setText:@"该文章若您认为涉及侵权，联系我们646399123@qq.com"];
    [rightLabel setTextColor:kMainTextColor];
    [rightLabel setFont:kFont(12)];
    [bottomView addSubview:rightLabel];
    
    [self addReadCount];
    [self requsetDetailData];
}
//阅读数增加
- (void)addReadCount
{
    [CBENewsVM addNewsReadCountWith:self.model.newsListId andComplete:^(id data) {
        
    } fail:^(NSError *error) {
        
    }];
}
//获取详情数据
- (void)requsetDetailData
{
    [CBENewsVM requestNewsDetailWith:self.model.newsListId andComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSDictionary *result = [data objectForKey:@"data"];
            self.collect = [[result objectForKey:@"collect"] boolValue];
            self.praise = [[result objectForKey:@"praise"] boolValue];
            self.praiseCount = [[result objectForKey:@"praiseCount"] integerValue];
            BOOL isLogin = [CBEUserModel sharedInstance].isLogin;
            if (isLogin) {
                if (self.collect) {
                    self.collectButton.selected = YES;
                }
                if (self.praise) {
                    self.zanButton.selected = YES;
                }
            }
            [self.zanButton setTitle:StringFormat(@"%ld",(long)self.praiseCount) forState:UIControlStateNormal];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)zanAction:(CBEBaseButton *)sender
{
    BOOL isLogin = [CBEUserModel sharedInstance].isLogin;
    if (isLogin) {
        if (self.praise) {
            [CBENewsVM requestCancelWithRelationId:self.model.newsListId withActionType:3 complete:^(id data) {
                if ([[data objectForKey:@"success"] boolValue]) {
                    self.zanButton.selected = NO;
                    self.praise = NO;
                    self.praiseCount --;
                    [self.zanButton setTitle:StringFormat(@"%ld",self.praiseCount) forState:UIControlStateNormal];
                }else{
                    [CommonUtils showHUDWithMessage:@"取消点赞失败" autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils showHUDWithMessage:@"取消点赞失败" autoHide:YES];
            }];
        }else{
            [CBENewsVM requestAddWithRelationId:self.model.newsListId withActionType:3 complete:^(id data) {
                if ([[data objectForKey:@"success"] boolValue]) {
                    self.zanButton.selected = YES;
                    self.praise = YES;
                    self.praiseCount ++;
                    [self.zanButton setTitle:StringFormat(@"%ld",self.praiseCount) forState:UIControlStateNormal];
                }else{
                    [CommonUtils showHUDWithMessage:@"点赞失败" autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils showHUDWithMessage:@"点赞失败" autoHide:YES];
            }];
        }
    }else{
        CBELoginController *loginController = [[CBELoginController alloc] init];
        [self presentViewController:loginController animated:YES completion:nil];
    }
}

- (void)collectAction:(UIButton *)sender
{
    BOOL isLogin = [CBEUserModel sharedInstance].isLogin;
    if (isLogin) {
        if (self.collect) {
            [CBENewsVM requestCancelWithRelationId:self.model.newsListId withActionType:1 complete:^(id data) {
                if ([[data objectForKey:@"success"] boolValue]) {
                    self.collectButton.selected = NO;
                    self.collect = NO;
                    [CommonUtils showHUDWithMessage:@"取消收藏成功" autoHide:YES];
                }else{
                    [CommonUtils showHUDWithMessage:@"取消收藏失败" autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils showHUDWithMessage:@"取消收藏失败" autoHide:YES];
            }];
        }else{
            [CBENewsVM requestAddWithRelationId:self.model.newsListId withActionType:1 complete:^(id data) {
                if ([[data objectForKey:@"success"] boolValue]) {
                    self.collectButton.selected = YES;
                    self.collect = YES;
                    [CommonUtils showHUDWithMessage:@"收藏成功" autoHide:YES];
                }else{
                    [CommonUtils showHUDWithMessage:@"收藏失败" autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils showHUDWithMessage:@"收藏失败" autoHide:YES];
            }];
        }
    }else{
        CBELoginController *loginController = [[CBELoginController alloc] init];
        [self presentViewController:loginController animated:YES completion:nil];
    }
}

- (void)shareAction:(UIButton *)sender
{
    BOOL isLogin = [CBEUserModel sharedInstance].isLogin;
    if (!isLogin) {
        CBELoginController *loginController = [[CBELoginController alloc] init];
        [self presentViewController:loginController animated:YES completion:nil];
    }else{
        [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine)]];
        [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
            // 根据获取的platformType确定所选平台进行下一步操作
            [self shareWebPageToPlatformType:platformType];
        }];
    }
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:self.model.title descr:[self removeHTML:self.model.content] thumImage:IMAGE_NAMED(@"icon1024")];
    //设置网页地址
    shareObject.webpageUrl = StringFormat(@"http://39.108.175.213/article/detail.html?id=%@",self.model.newsListId);
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}

- (NSString *)removeHTML:(NSString *)html{
    
    NSArray *components = [html componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    NSMutableArray *componentsToKeep = [NSMutableArray array];
    
    for (int i = 0; i < [components count]; i = i + 2) {
        
        [componentsToKeep addObject:[components objectAtIndex:i]];
        
    }
    
    NSString *plainText = [componentsToKeep componentsJoinedByString:@""];
    return plainText;
}

- (void)addRightButton
{
    UIButton *collectButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [collectButton setImage:IMAGE_NAMED(@"news_icon_mark") forState:UIControlStateNormal];
    [collectButton setImage:IMAGE_NAMED(@"huodong_icon_mark") forState:UIControlStateSelected];
    [collectButton addTarget:self action:@selector(collectAction:) forControlEvents:UIControlEventTouchUpInside];
    self.collectButton = collectButton;
    
    UIButton *shareButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [shareButton setImage:IMAGE_NAMED(@"event_icon_share") forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
    
    collectButton.frame = CGRectMake(kSCREEN_WIDTH - 44 - 34, 10, 24, 24);
    shareButton.frame = CGRectMake(kSCREEN_WIDTH - 44, 10, 24, 24);
    [self.navigationItem.titleView addSubview:shareButton];
    [self.navigationItem.titleView addSubview:collectButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CBENewsController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBENewsController.h"
#import "CBENewsNavigationBar.h"
#import "JXPagerView.h"
#import "JXPagerListRefreshView.h"
#import "JXCategoryTitleView.h"
#import "CBENewsSearchController.h"
#import "CBENewsVM.h"
#import "CBEBannarModel.h"
#import "CBENewsListTableView.h"
#import "JXCategoryView.h"
#import "CBECycleViewCell.h"
#import "CBEWebController.h"
#import "CBENewsListModel.h"
#import "CBENewsDetailViewController.h"
#import "CBENewsFitterController.h"

@interface CBENewsController ()<JXPagerViewDelegate,UIScrollViewDelegate,ListDelegate,JXCategoryViewDelegate,CBECycleDelegate>

@property (nonatomic, strong) CBENewsNavigationBar *navigationBar;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) CBECycleViewCell *leftHeaderView;
@property (nonatomic, strong) CBECycleViewCell *rightHeaderView;

@property (nonatomic, strong) JXPagerView *leftPagerView;
@property (nonatomic, strong) JXCategoryTitleView *leftCategoryView;
@property (nonatomic, strong) NSMutableArray *leftVControllers;
@property (nonatomic, strong) NSMutableArray *leftTitles;

@property (nonatomic, strong) JXPagerView *rightPagerView;
@property (nonatomic, strong) JXCategoryTitleView *rightCategoryView;
@property (nonatomic, strong) NSMutableArray *rightVControllers;
@property (nonatomic, strong) NSMutableArray *rightTitles;

@property (nonatomic, strong) NSMutableArray *bannarArray;//轮播数据来源
@property (nonatomic, strong) NSArray *rightTypeArray;//全部按钮数据来源

@end

@implementation CBENewsController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}
- (void)setupNavibar {
    self.navigationBar = [[CBENewsNavigationBar alloc] init];
    [self.navigationBar setFrame:CGRectMake(0, 0, kSCREEN_WIDTH, KNavigationBarHeight)];
    [self.view addSubview:self.navigationBar];
    kWeakSelf
    self.navigationBar.newsBlock = ^{
        [weakSelf.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    };
    self.navigationBar.handsonBlock = ^{
        [weakSelf.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH, 0) animated:YES];
    };
    self.navigationBar.searchBlock = ^{
        CBENewsSearchController *searchController = [[CBENewsSearchController alloc] init];
        [weakSelf.navigationController pushViewController:searchController animated:YES];
    };
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavibar];
    [self.view addSubview:self.scrollView];
    self.leftPagerView = [self preferredLeftPagerView];
    [self.scrollView addSubview:self.leftPagerView];
    self.rightPagerView = [self preferredRightPagerView];
    [self.scrollView addSubview:self.rightPagerView];
    //扣边返回处理，下面的代码要加上
    [self.leftPagerView.listContainerView.collectionView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    [self.leftPagerView.mainTableView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.leftCategoryView.selectedIndex == 0);
    [self.rightPagerView.listContainerView.collectionView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    [self.rightPagerView.mainTableView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.rightCategoryView.selectedIndex == 0);
    
    [self requestLeftData];
    [self requestRightData];
}
#pragma mark - 网络请求
- (void)requestLeftData {
    [CBENewsVM requestRightIndexWithTopId:@"1" andComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *bannarList = [[data objectForKey:@"data"] objectForKey:@"banners"];
            [self.bannarArray removeAllObjects];
            NSArray *banerArray = [CBEBannarModel mj_objectArrayWithKeyValuesArray:bannarList];
            [self.bannarArray addObjectsFromArray:banerArray];
            
            [self.leftHeaderView setNewsBannarDataSource:self.bannarArray];
            [self.rightHeaderView setNewsBannarDataSource:self.bannarArray];
            
            NSArray *infoTypesList = [[data objectForKey:@"data"] objectForKey:@"infoTypes"];
            [self setLeftCategoryTitle:infoTypesList];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        NSLog(@"----%@",error);
    }];
}
- (void)setLeftCategoryTitle:(NSArray *)array {
    self.leftTitles = [NSMutableArray array];
    self.leftVControllers = [NSMutableArray arrayWithCapacity:self.leftTitles.count];
    for (NSDictionary *obj in array) {
        CBENewsListTableView *controller = [[CBENewsListTableView alloc] init];
        controller.categoryDict = obj;
        controller.delegate = self;
        controller.isNeedHeader = YES;
        controller.isNeedFooter = YES;
        controller.isHeaderRefreshed = NO;
        [self.leftVControllers addObject:controller];
        [self.leftTitles addObject:[obj objectForKey:@"name"]];
    }//WithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight - 44 - kHeightScale(160))
    self.leftCategoryView.titles = self.leftTitles;
    [self.leftCategoryView reloadData];
    [self.leftPagerView reloadData];
    [self.leftVControllers.firstObject beginFirstRefresh];
}
- (void)requestRightData {
    [CBENewsVM requestRightIndexWithTopId:@"2" andComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *infoTypesList = [[data objectForKey:@"data"] objectForKey:@"infoTypes"];
            [self setRightCategoryTitle:infoTypesList];
        }
    } fail:^(NSError *error) {
        
    }];
}
- (void)setRightCategoryTitle:(NSArray *)array {
    self.rightTitles = [NSMutableArray array];
    self.rightTypeArray = array;
    
    self.rightVControllers = [NSMutableArray arrayWithCapacity:self.rightTitles.count];
    for (NSDictionary *obj in array) {
        CBENewsListTableView *controller = [[CBENewsListTableView alloc] init];
        controller.categoryDict = obj;
        controller.delegate = self;
        controller.isNeedHeader = YES;
        controller.isNeedFooter = YES;
        controller.isHeaderRefreshed = NO;
        [self.rightVControllers addObject:controller];
        [self.rightTitles addObject:[obj objectForKey:@"name"]];
    }
    self.rightCategoryView.titles = self.rightTitles;
    [self.rightCategoryView reloadData];
    [self.rightPagerView reloadData];
    [self.rightVControllers.firstObject beginFirstRefresh];
}
#pragma mark -全部响应事件
- (void)fitterAction {
    CBENewsFitterController *fitterController = [[CBENewsFitterController alloc] init];
    fitterController.typeArray = self.rightTypeArray;
    [self.navigationController pushViewController:fitterController animated:YES];
}
#pragma mark--
- (JXPagerView *)preferredLeftPagerView {
    JXPagerView *pager = [[JXPagerListRefreshView alloc] initWithDelegate:self];
    pager.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight);
    pager.listContainerView.collectionView.backgroundColor = kMainDefaltGrayColor;
    pager.tag = 100;
    pager.backgroundColor = kMainDefaltGrayColor;
    pager.mainTableView.tableHeaderView = self.leftHeaderView;
    return pager;
}
- (JXPagerView *)preferredRightPagerView {
    JXPagerView *pager = [[JXPagerListRefreshView alloc] initWithDelegate:self];
    pager.frame = CGRectMake(kSCREEN_WIDTH, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight);
    pager.listContainerView.collectionView.backgroundColor = kMainDefaltGrayColor;
    pager.tag = 101;
    pager.backgroundColor = kMainDefaltGrayColor;
    pager.mainTableView.tableHeaderView = self.rightHeaderView;
    return pager;
}
#pragma mark - JXPagerViewDelegate
- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    if (pagerView == self.leftPagerView) {
        return self.leftHeaderView;
    }
    return self.rightHeaderView;
}
- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return kHeightScale(160);
}
- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 44;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    if (pagerView == self.rightPagerView) {
        return self.rightCategoryView;
    }
    return self.leftCategoryView;
}

- (NSArray<id<JXPagerViewListViewDelegate>> *)listViewsInPagerView:(JXPagerView *)pagerView {
    if (pagerView == self.rightPagerView) {
        return self.rightVControllers;
    }
    return self.leftVControllers;
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    if (categoryView == self.rightCategoryView) {
        [self.rightVControllers[index] beginFirstRefresh];
    }else{
        [self.leftVControllers[index] beginFirstRefresh];
    }
}
#pragma mark - 轮播图选中
- (void)cycleViewDidSelectedAtIndex:(NSInteger)index {
    //点击轮播图事件
    NSLog(@"selected index:%ld",index);
    CBEBannarModel *model = [self.bannarArray objectAtIndex:index];
    CBEWebController *webController = [[CBEWebController alloc] init];
    webController.urlString = model.bannerUrl;
    [webController setBarTitle:@"资讯详情"];
    [self.navigationController pushViewController:webController animated:YES];
}
#pragma mark - 新闻信息选中
- (void)didSelectItemWith:(CBENewsListModel *)model {
    CBENewsDetailViewController *detailController = [[CBENewsDetailViewController alloc] init];
    detailController.model = model;
    [self.navigationController pushViewController:detailController animated:YES];
}
#pragma mark - scrollViewdelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.scrollView) {
        CGFloat distance = scrollView.contentOffset.x;
        CGFloat width = scrollView.frame.size.width;
        NSInteger index = distance / width;
        [self.navigationBar moveLineAtIndex:index];
//        self.pageIndex = index;
    }
}
#pragma mark - getter
- (NSMutableArray *)bannarArray {
    if (!_bannarArray) {
        _bannarArray = [NSMutableArray array];
    }
    return _bannarArray;
}
- (CBECycleViewCell *)leftHeaderView {
    if (!_leftHeaderView) {
        _leftHeaderView = [[CBECycleViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        _leftHeaderView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kHeightScale(160));
        _leftHeaderView.delegate = self;
    }
    return _leftHeaderView;
}
- (CBECycleViewCell *)rightHeaderView {
    if (!_rightHeaderView) {
        _rightHeaderView = [[CBECycleViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        _rightHeaderView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kHeightScale(160));
        _rightHeaderView.delegate = self;
    }
    return _rightHeaderView;
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight)];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH * 2, 0);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.scrollEnabled = NO;
    }
    return _scrollView;
}
- (JXCategoryTitleView *)leftCategoryView {
    if (!_leftCategoryView) {
        _leftCategoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 44)];
        _leftCategoryView.titles = self.leftTitles;
        _leftCategoryView.backgroundColor = [UIColor whiteColor];
        _leftCategoryView.titleSelectedColor = kMainBlackColor;
        _leftCategoryView.titleColor = kMainBlackColor;
        _leftCategoryView.titleColorGradientEnabled = YES;
        _leftCategoryView.titleLabelZoomEnabled = YES;
        _leftCategoryView.titleFont = kFont(14);
        _leftCategoryView.delegate = self;
        
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorLineViewColor = kMainBlueColor;
        lineView.indicatorLineWidth = 30;
        _leftCategoryView.indicators = @[lineView];
        
        _leftCategoryView.contentScrollView = self.leftPagerView.listContainerView.collectionView;
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, kSCREEN_WIDTH, 0.5)];
        line.backgroundColor = UIColorFromRGB(0xE6E6E6);
        [_leftCategoryView addSubview:line];
    }
    return _leftCategoryView;
}
- (JXCategoryTitleView *)rightCategoryView {
    if (!_rightCategoryView) {
        _rightCategoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH - 30 - 82, 44)];
        _rightCategoryView.titles = self.rightTitles;
        _rightCategoryView.backgroundColor = [UIColor whiteColor];
        _rightCategoryView.titleSelectedColor = kMainBlackColor;
        _rightCategoryView.titleColor = kMainBlackColor;
        _rightCategoryView.titleColorGradientEnabled = YES;
        _rightCategoryView.titleLabelZoomEnabled = YES;
        _rightCategoryView.titleFont = kFont(14);
        _rightCategoryView.delegate = self;
        
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorLineViewColor = kMainBlueColor;
        lineView.indicatorLineWidth = 30;
        _rightCategoryView.indicators = @[lineView];
        
        _rightCategoryView.contentScrollView = self.rightPagerView.listContainerView.collectionView;
        _rightCategoryView.contentScrollView.frame = CGRectMake(0, 0, kSCREEN_WIDTH - 30 - 82, 44);
        
        CBEBaseButton *allBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
        [allBtn setFrame:CGRectMake(kSCREEN_WIDTH - 82, 0, 82, 44)];
        [allBtn setImageViewRect:CGRectMake(15, 13, 16, 16)];
        [allBtn setTitleLabelRect:CGRectMake(35, 10, 32, 21)];
        [allBtn setImage:IMAGE_NAMED(@"cv_icon_look") forState:UIControlStateNormal];
        [allBtn setTitle:@"全部" forState:UIControlStateNormal];
        [allBtn.titleLabel setFont:kFont(15)];
        [allBtn setTitleColor:kMainBlueColor forState:UIControlStateNormal];
        [allBtn setBackgroundColor:[UIColor whiteColor]];
        allBtn.layer.borderColor = kMainLineColor.CGColor;
        allBtn.layer.borderWidth = 0.5;
        [allBtn addTarget:self action:@selector(fitterAction) forControlEvents:UIControlEventTouchUpInside];
        [_rightCategoryView addSubview:allBtn];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, kSCREEN_WIDTH, 0.5)];
        line.backgroundColor = UIColorFromRGB(0xE6E6E6);
        [_rightCategoryView addSubview:line];
    }
    return _rightCategoryView;
}
@end

//
//  CBENewsCategoryController.m
//  CBECommerce
//
//  Created by don on 2018/10/23.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBENewsCategoryController.h"
#import "CBENoOrderView.h"
#import "CBENewsListCell.h"
#import "CBENewsDetailViewController.h"
#import "CBENewsVM.h"
#import "CBENewsListModel.h"

@interface CBENewsCategoryController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBENoOrderView *noDataView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation CBENewsCategoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setBarTitle:[self.typeBody objectForKey:@"name"]];
    [self.view addSubview:self.tableView];
    [self requestData];
}

- (void)requestData
{
    _dataSource = [NSMutableArray array];
    [CBENewsVM requestSearchNewsWithTypeId:[self.typeBody objectForKey:@"id"] andKeyword:@"" andComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *dataArray = [data objectForKey:@"data"];
            [dataArray enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CBENewsListModel *model = [CBENewsListModel mj_objectWithKeyValues:obj];
                [_dataSource addObject:model];
            }];
            if (_dataSource.count == 0) {
                _noDataView.hidden = NO;
            }else{
                _noDataView.hidden = YES;
            }
            [_tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGFLOAT_MIN)];
    headView.backgroundColor = [UIColor whiteColor];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBENewsListCell *cell = [CBENewsListCell cellForTableView:tableView];
    CBENewsListModel *model = [_dataSource objectAtIndex:indexPath.section];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBENewsListModel *model = [_dataSource objectAtIndex:indexPath.section];
    CBENewsDetailViewController *detailController = [[CBENewsDetailViewController alloc] init];
    detailController.model = model;
    [self.navigationController pushViewController:detailController animated:YES];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _noDataView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _tableView.width, _tableView.height)];
        _noDataView.hidden = YES;
        _noDataView.title = @"暂无数据";
        [_tableView addSubview:_noDataView];
    }
    return _tableView;
}

@end

//
//  CBENewsFitterController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBENewsFitterController.h"
#import "CBEScrollPositionView.h"
#import "CBENewsCollectionCell.h"
#import "CBECollectionHeadReusableView.h"
#import "CBENewsVM.h"
#import "CBENewsCategoryController.h"
#import "CBENoOrderView.h"

@interface CBENewsFitterController ()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,ScorllPositionDelegate>

@property (nonatomic) CBEScrollPositionView  *positionView;
@property (nonatomic) UIScrollView         *scroView;

@property (nonatomic, strong) NSArray *thirdArray;


@end

@implementation CBENewsFitterController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"全部"];
    self.automaticallyAdjustsScrollViewInsets=NO;
    CBEScrollPositionView *postionView = [[CBEScrollPositionView alloc] init];
    postionView.delegate = self;
    postionView.titlesArr = self.typeArray;
    postionView.contentScrollView = self.scroView;
    postionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:postionView];
    self.positionView = postionView;
    
    [self.view addSubview:self.scroView];
    [self addScroviewSubViews];
    
    NSDictionary *firstDict = [self.typeArray objectAtIndex:0];
    [self requestTypeInfosDataWith:[firstDict objectForKey:@"id"] withIndex:0];
}

- (void)requestTypeInfosDataWith:(NSString *)typeId withIndex:(NSInteger)index
{
    [CBENewsVM requestNewsType:typeId andComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            self.thirdArray = [data objectForKey:@"data"];
            UICollectionView *collectView = [self.scroView viewWithTag:1000+index];
            CBENoOrderView *nodataView = [collectView viewWithTag:2000+index];
            if (!self.thirdArray.count) {
                nodataView.hidden = NO;
            }else{
                nodataView.hidden = YES;
            }
            [collectView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)addScroviewSubViews{
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowLayout.headerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 58);
    flowLayout.footerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 0);
    
    for (int i = 0; i<self.positionView.titlesArr.count; i++) {
        UICollectionView *normCollectView = [[UICollectionView alloc] initWithFrame:CGRectMake(self.scroView.bounds.size.width*i, 0,self.scroView.bounds.size.width, self.scroView.bounds.size.height) collectionViewLayout:flowLayout];
        normCollectView.backgroundColor =  [UIColor whiteColor];;
        normCollectView.dataSource=self;
        normCollectView.delegate=self;
        normCollectView.tag = 1000+i;
        [self.scroView addSubview:normCollectView];
        [normCollectView registerNib:[UINib nibWithNibName:@"CBENewsCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"CBENewsCollectionCell"];
        [normCollectView registerNib:[UINib nibWithNibName:@"CBECollectionHeadReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CBECollectionHeadReusableView"];
        
        CBENoOrderView *noDataView = [[CBENoOrderView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, kSCREEN_HEIGHT - KNavigationBarHeight - 40)];
        noDataView.title = @"暂无数据";
        noDataView.hidden = YES;
        noDataView.tag = 2000+i;
        [normCollectView addSubview:noDataView];
        [normCollectView bringSubviewToFront:noDataView];
    }
}

#pragma mark -
- (void)didTitleClickAtIndex:(NSInteger)index
{
    NSDictionary *dict = [self.typeArray objectAtIndex:index];
    [self requestTypeInfosDataWith:[dict objectForKey:@"id"] withIndex:index];
}

#pragma mark - 定义section数量
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.thirdArray.count;
}

#pragma mark -- UICollectionViewDataSource
//定义每个section的cell个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    if (section == 0) {
//        return 5;
//    }
    NSArray *array = [[self.thirdArray objectAtIndex:section] objectForKey:@"infoTypeSuns"];
    return array.count;
}

#pragma mark - 定义每个Item 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((kSCREEN_WIDTH - 60)/3, 44);
}

//定义每个UICollectionView 纵向的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return kPadding15;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return kPadding15;
}

#pragma mark - 定义每个UICollectionView 的 margin
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, kPadding15, 0, kPadding15);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if (kind == UICollectionElementKindSectionHeader) {
        CBECollectionHeadReusableView *reusableView;
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CBECollectionHeadReusableView" forIndexPath:indexPath];
        NSDictionary *body = [self.thirdArray objectAtIndex:indexPath.section];
        [reusableView.titleLabel setText:[body objectForKey:@"name"]];
        return reusableView;
    }
    return nil;
}
#pragma mark - 每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBENewsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CBENewsCollectionCell" forIndexPath:indexPath];
    NSDictionary *thirdBody = [self.thirdArray objectAtIndex:indexPath.section];
    NSDictionary *forthBody = [[thirdBody objectForKey:@"infoTypeSuns"] objectAtIndex:indexPath.row];
    [cell.itemLabel setText:[forthBody objectForKey:@"name"]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *thirdBody = [self.thirdArray objectAtIndex:indexPath.section];
    NSDictionary *forthBody = [[thirdBody objectForKey:@"infoTypeSuns"] objectAtIndex:indexPath.row];
    CBENewsCategoryController *cateController = [[CBENewsCategoryController alloc] init];
    cateController.typeBody = forthBody;
    [self.navigationController pushViewController:cateController animated:YES];
}

-(UIScrollView *)scroView{
    if (!_scroView ) {
        _scroView = [[UIScrollView alloc]init];
        _scroView.bounces = NO;
        _scroView.delegate =self;
        _scroView.pagingEnabled=YES;
    }
    return _scroView;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger   pageIndex = scrollView.contentOffset.x/self.scroView.bounds.size.width;
    [self.positionView resetTitileViewState:pageIndex];
    NSDictionary *firstDict = [self.typeArray objectAtIndex:pageIndex];
    [self requestTypeInfosDataWith:[firstDict objectForKey:@"id"] withIndex:pageIndex];
    self.positionView.titleContentView.scroll=NO;
    
}
-(void)viewWillLayoutSubviews{
    if (@available(iOS 11.0, *)) {
        self.positionView.frame = CGRectMake(0,self.view.safeAreaInsets.top, self.view.bounds.size.width, 40);
    } else {
        self.positionView.frame = CGRectMake(0,0, self.view.bounds.size.width, 40);
    }
    self.scroView.frame = CGRectMake(0, self.positionView.frame.origin.y+self.positionView.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height-self.positionView.bounds.size.height-self.positionView.frame.origin.y);
    self.scroView.contentSize = CGSizeMake(self.view.bounds.size.width*self.positionView.titlesArr.count, 0);
    for (int  i = 0; i<self.scroView.subviews.count; i++) {
        UILabel *lab =self.scroView.subviews[i];
        lab.frame = CGRectMake(self.scroView.bounds.size.width*i, 0,self.scroView.bounds.size.width, self.scroView.bounds.size.height);
    }
}

@end

//
//  CBENewsSearchController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBENewsSearchController.h"
#import "LSSearchBarView.h"
#import "CBENewsListCell.h"
#import "CBENewsDetailViewController.h"
#import "CBENewsVM.h"

@interface CBENewsSearchController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) LSSearchBarView *searchView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation CBENewsSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _searchView = [[LSSearchBarView alloc] initWithAlignment:(SearchBarAlignmentAlignmentLeft) placeholder:@"搜索关键字" didClicked:nil];
    [self.navigationItem.titleView addSubview:_searchView];
    [self setBarButton:(BarButtonItemRight) action:@selector(cancelAction) title:@"取消" titleColor:kMainBlueColor];
    
    _searchView.textField.placeholder = @"输入搜索内容";
    self.textField = _searchView.textField;
    self.textField.delegate = self;
    self.textField.userInteractionEnabled = YES;
    [self.textField becomeFirstResponder];
    
    // 监控当前输入框的值
    [self.textField addTarget:self action:@selector(textFieldValueChangedAction:) forControlEvents:UIControlEventEditingChanged];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight)) style:(UITableViewStylePlain)];
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 20.f)];
    headView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headView;
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
        [self.tableView setSeparatorColor:[UIColor ls_colorWithHex:0xf0f2f5]];
    }
    self.extendedLayoutIncludesOpaqueBars = YES;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)textFieldValueChangedAction:(UITextField *)field
{
    NSString *keyword = field.text;
    [self requestDataWithKeyword:keyword];
}

- (void)requestDataWithKeyword:(NSString *)keyword
{
    _dataSource = [NSMutableArray array];
    [CBENewsVM requestSearchNewsWithTypeId:@"" andKeyword:keyword andComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *dataArray = [data objectForKey:@"data"];
            [dataArray enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CBENewsListModel *model = [CBENewsListModel mj_objectWithKeyValues:obj];
                [_dataSource addObject:model];
            }];
            [_tableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)cancelAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBENewsListCell *cell = [CBENewsListCell cellForTableView:tableView];
    CBENewsListModel *model = [_dataSource objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBENewsListModel *model = [_dataSource objectAtIndex:indexPath.row];
    CBENewsDetailViewController *detailController = [[CBENewsDetailViewController alloc] init];
    detailController.model = model;
    [self.navigationController pushViewController:detailController animated:YES];
}

@end

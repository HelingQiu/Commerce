//
//  CBENewsCategoryController.h
//  CBECommerce
//
//  Created by don on 2018/10/23.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBENewsCategoryController : CBEBaseController

@property (nonatomic, strong) NSDictionary *typeBody;

@end

NS_ASSUME_NONNULL_END

//
//  CBENewsFitterController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBENewsFitterController : CBEBaseController

@property (nonatomic, strong) NSArray *typeArray;

@end

NS_ASSUME_NONNULL_END

//
//  CBENewsVM.h
//  CBECommerce
//
//  Created by don on 2018/10/22.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBENewsVM : NSObject

+ (void)requestNewsIndexComplete:(CBENetWorkSucceed)successed
                            fail:(CBENetWorkFailure)failed;

+ (void)requestRightIndexWithTopId:(NSString *)topId
                       andComplete:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed;

+ (void)requestInformation:(NSString *)typeId
               andComplete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed;

+ (void)requestNewsType:(NSString *)typeId
            andComplete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed;

+ (void)requestSearchNewsWithTypeId:(NSString *)typeId
                         andKeyword:(NSString *)keyword
                        andComplete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed;

+ (void)addNewsReadCountWith:(NSString *)detailId
                 andComplete:(CBENetWorkSucceed)successed
                        fail:(CBENetWorkFailure)failed;
//资讯详情
+ (void)requestNewsDetailWith:(NSString *)newsId
                  andComplete:(CBENetWorkSucceed)successed
                         fail:(CBENetWorkFailure)failed;

//资讯ID或者活动ID 1资讯收藏,2活动收藏,3资讯点赞
+ (void)requestAddWithRelationId:(NSString *)relationId
                  withActionType:(NSInteger)actionType
                        complete:(CBENetWorkSucceed)successed
                            fail:(CBENetWorkFailure)failed;
//1取消资讯收藏,2取消活动收藏,3取消点赞
+ (void)requestCancelWithRelationId:(NSString *)relationId
                     withActionType:(NSInteger)actionType
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed;

//资讯详情数据
+ (void)requestNewsDetailDataWithId:(NSString *)newsId
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed;

@end

NS_ASSUME_NONNULL_END

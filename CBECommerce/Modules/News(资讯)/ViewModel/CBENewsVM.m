//
//  CBENewsVM.m
//  CBECommerce
//
//  Created by don on 2018/10/22.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBENewsVM.h"

@implementation CBENewsVM

+ (void)requestNewsIndexComplete:(CBENetWorkSucceed)successed
                            fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kInformationIndex parameter:nil timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestRightIndexWithTopId:(NSString *)topId
                       andComplete:(CBENetWorkSucceed)successed
                            fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"topId":topId};
//    [[CBENetworkManager sharedInstance] getRequestWithUrl:kTopLevelChange parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager GET:StringFormat(@"%@%@",Host,kTopLevelChange) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

+ (void)requestInformation:(NSString *)typeId
               andComplete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"typeId":typeId};
//    [[CBENetworkManager sharedInstance] postRequestWithUrl:kGetInfosByTypeId parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager GET:StringFormat(@"%@%@",Host,kGetInfosByTypeId) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

+ (void)requestNewsType:(NSString *)typeId
            andComplete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"typeId":typeId};
//    [[CBENetworkManager sharedInstance] postRequestWithUrl:kTwoTypeChange parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager GET:StringFormat(@"%@%@",Host,kTwoTypeChange) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

+ (void)requestSearchNewsWithTypeId:(NSString *)typeId
                         andKeyword:(NSString *)keyword
                        andComplete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (![CommonUtils isBlankString:StringFormat(@"%@",typeId)]) {
        [params setObject:typeId forKey:@"typeId"];
    }
    if (![CommonUtils isBlankString:keyword]) {
        [params setObject:keyword forKey:@"keyword"];
    }
    
//    [[CBENetworkManager sharedInstance] postRequestWithUrl:kSelectByKeyword parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager POST:StringFormat(@"%@%@",Host,kSelectByKeyword) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

+ (void)addNewsReadCountWith:(NSString *)detailId
                 andComplete:(CBENetWorkSucceed)successed
                        fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:detailId?:@"" forKey:@"id"];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kNewsAddReadCount parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//资讯详情数据
+ (void)requestNewsDetailWith:(NSString *)newsId
                  andComplete:(CBENetWorkSucceed)successed
                         fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:newsId?:@"" forKey:@"id"];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kNewsDetailApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestAddWithRelationId:(NSString *)relationId
                  withActionType:(NSInteger)actionType
                        complete:(CBENetWorkSucceed)successed
                            fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [params setObject:relationId forKey:@"relationId"];
    [params setObject:@(actionType) forKey:@"collectType"];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kCollectionAndDianzanApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestCancelWithRelationId:(NSString *)relationId
                     withActionType:(NSInteger)actionType
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [params setObject:relationId forKey:@"relationId"];
    [params setObject:@(actionType) forKey:@"collectType"];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kCancelCollectionAndDianzanApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//资讯详情数据
+ (void)requestNewsDetailDataWithId:(NSString *)newsId
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed
{
    
}


@end

//
//  CBENewsListModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/14.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBENewsListModel : NSObject

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *newsListId;
@property (nonatomic, copy) NSString *informationSource;
@property (nonatomic, assign) NSInteger informationType;
@property (nonatomic, copy) NSString *modifyTime;
@property (nonatomic, copy) NSString *picture;
@property (nonatomic, copy) NSString *readCount;
@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *createTimeStr;

@end

NS_ASSUME_NONNULL_END

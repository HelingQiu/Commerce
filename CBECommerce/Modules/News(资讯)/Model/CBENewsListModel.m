//
//  CBENewsListModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/14.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBENewsListModel.h"

@implementation CBENewsListModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"newsListId":@"id"};
}

@end

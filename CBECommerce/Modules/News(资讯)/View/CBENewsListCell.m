//
//  CBENewsListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/29.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBENewsListCell.h"
#import "CBENewsListModel.h"
#import "CBEAlignTopLabel.h"

@implementation CBENewsListCell
{
    UIImageView *_thumbView;
    CBEAlignTopLabel *_titleLabel;
    UILabel *_sourceLabel;
    UILabel *_readCountLabel;
    UILabel *_timeLabel;
}
+ (CBENewsListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"newsListCell";
    CBENewsListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBENewsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    _thumbView = [UIImageView new];
    [self.contentView addSubview:_thumbView];
    [_thumbView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(kPadding15);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(75);
    }];
    
    _titleLabel = [CBEAlignTopLabel new];
    [_titleLabel setFont:kFont(14)];
    [_titleLabel setTextColor:kMainBlackColor];
    [_titleLabel setNumberOfLines:2];
    [_titleLabel setText:@""];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_thumbView.mas_top);
        make.left.equalTo(self->_thumbView.mas_right).offset(kPadding15);
        make.right.equalTo(self.contentView.mas_right).offset(-kPadding15);
        make.height.mas_equalTo(44);
    }];
    
    _readCountLabel = [UILabel new];
    [_readCountLabel setFont:kFont(12)];
    [_readCountLabel setTextColor:kMainTextColor];
    [_readCountLabel setText:@"0阅读"];
    [self.contentView addSubview:_readCountLabel];
    [_readCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self->_thumbView.mas_bottom);
        make.left.equalTo (self->_thumbView.mas_right).offset(kPadding15);
        make.height.mas_equalTo(17);
    }];
    
    _timeLabel = [UILabel new];
    [_timeLabel setFont:kFont(12)];
    [_timeLabel setTextColor:kMainTextColor];
    [_timeLabel setText:@""];
    [self.contentView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_readCountLabel.mas_right).offset(kPadding10);
        make.bottom.equalTo(self->_thumbView.mas_bottom);
//        make.right.equalTo(self.contentView.mas_right).offset(-kPadding15);
        make.height.mas_equalTo(17);
    }];
}

- (void)refreshDataWith:(CBENewsListModel *)model
{
    [_titleLabel setText:model.title];
    [_titleLabel ls_setRowSpace:8];
    [_timeLabel setText:model.modifyTime];
    if ([CommonUtils isBlankString:model.modifyTime]) {
        [_timeLabel setText:model.createTimeStr];
    }
    [_readCountLabel setText:StringFormat(@"%@阅读",model.readCount)];
    [_thumbView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"news_default")];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

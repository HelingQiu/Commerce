//
//  CBECollectionHeadReusableView.h
//  LineAgeScrollView
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 com.sswang.www. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBECollectionHeadReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END

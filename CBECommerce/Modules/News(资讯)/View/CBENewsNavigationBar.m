//
//  CBENewsNavigationBar.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBENewsNavigationBar.h"

@implementation CBENewsNavigationBar

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    
    CGFloat orginY = KIsiPhoneX?44:20;
    
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(kSCREEN_WIDTH - 30 - kPadding15, orginY+7, 30, 30)];
    [searchButton setBackgroundImage:IMAGE_NAMED(@"home_icon_search") forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:searchButton];
    
    self.newsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.newsButton setFrame:CGRectMake(kSCREEN_WIDTH/2 - 60, orginY+2, 40, 40)];
    [self.newsButton setTitle:@"资讯" forState:UIControlStateNormal];
    [self.newsButton setTitleColor:kMainBlueColor forState:UIControlStateSelected];
    [self.newsButton setTitleColor:[UIColor ls_colorWithHexString:@"#92A1B3"] forState:UIControlStateNormal];
    [self.newsButton.titleLabel setFont:kBoldFont(17)];
    [self.newsButton addTarget:self action:@selector(newsAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.newsButton];
    self.newsButton.selected = YES;
    
    self.handsonButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.handsonButton setFrame:CGRectMake(kSCREEN_WIDTH/2 + 20, orginY+2, 40, 40)];
    [self.handsonButton setTitle:@"实操" forState:UIControlStateNormal];
    [self.handsonButton setTitleColor:kMainBlueColor forState:UIControlStateSelected];
    [self.handsonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.handsonButton.titleLabel setFont:kBoldFont(17)];
    [self.handsonButton addTarget:self action:@selector(handsonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.handsonButton];
}

- (void)searchAction
{
    if (self.searchBlock) {
        self.searchBlock();
    }
}

- (void)newsAction:(UIButton *)sender
{
    sender.selected = YES;
    _handsonButton.selected = NO;
    if (self.newsBlock) {
        self.newsBlock();
    }
}

- (void)handsonAction:(UIButton *)sender
{
    sender.selected = YES;
    _newsButton.selected = NO;
    if (self.handsonBlock) {
        self.handsonBlock();
    }
}

- (void)moveLineAtIndex:(NSInteger)index
{
    if (index == 1) {//从第1页移动到第0页
        _newsButton.selected = NO;
        _handsonButton.selected = YES;
    }else{//从第0页移动到第1页
        _newsButton.selected = YES;
        _handsonButton.selected = NO;
    }
}

@end

//
//  CBENewsCollectionCell.m
//  LineAgeScrollView
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 com.sswang.www. All rights reserved.
//

#import "CBENewsCollectionCell.h"

@implementation CBENewsCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.itemLabel.layer.borderWidth = 0.5;
    self.itemLabel.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)setItemSelect:(BOOL)selected {
    if (selected) {
        self.itemLabel.layer.borderColor = kMainOrangeColor.CGColor;
        self.itemLabel.backgroundColor = [UIColor ls_colorWithHex:0xFC6B3F andAlpha:0.12];
        self.itemLabel.textColor = kMainOrangeColor;
    }else{
        self.itemLabel.layer.borderColor = [UIColor clearColor].CGColor;
        self.itemLabel.backgroundColor = [UIColor ls_colorWithHex:0xF4F4F4 andAlpha:1];
        self.itemLabel.textColor = kMainBlackColor;
    }
}

@end

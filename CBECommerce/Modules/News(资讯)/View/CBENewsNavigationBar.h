//
//  CBENewsNavigationBar.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^NewsBlock)(void);
typedef void(^HandsonBlock)(void);
typedef void(^SearchBlock)(void);

@interface CBENewsNavigationBar : UIView

@property (nonatomic, strong) UIButton *newsButton;
@property (nonatomic, strong) UIButton *handsonButton;

@property (nonatomic, copy) NewsBlock newsBlock;
@property (nonatomic, copy) HandsonBlock handsonBlock;
@property (nonatomic, copy) SearchBlock searchBlock;

- (void)moveLineAtIndex:(NSInteger)index;

@end

//
//  CBEScrollPositionView.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CBEScollPositionTitleContentView;

@protocol ScorllPositionDelegate <NSObject>

- (void)didTitleClickAtIndex:(NSInteger)index;

@end
@interface CBEScrollPositionView : UIView

@property (nonatomic, weak) id<ScorllPositionDelegate>delegate;
@property (nonatomic) NSArray           *titlesArr;//标题数组
@property (nonatomic, assign) CGFloat    titleViewWidth;//子控件的宽度
@property (nonatomic) CBEScollPositionTitleContentView *titleContentView;//顶部title的容器
@property (nonatomic) UIScrollView      *contentScrollView;//传入自己需要联动的内容容器scrollview
/**
 *估计索引确定选中的标题
 */
- (void)resetTitileViewState:(NSInteger)index;

@end

@interface CBEScollPositionTitleContentView:UIView

@property (nonatomic) CGPoint         indexPoint;
@property (nonatomic, assign) CGFloat  scrollX;
@property (nonatomic, assign) BOOL     scroll;
@property (nonatomic, assign) BOOL     moveRight;
@property (nonatomic, assign) BOOL     moveLeft;
@property (nonatomic, assign) CGFloat  fixedWidth;

@end

NS_ASSUME_NONNULL_END

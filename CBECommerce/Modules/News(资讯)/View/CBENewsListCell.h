//
//  CBENewsListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/29.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

@interface CBENewsListCell : CBEBaseTableViewCell

+ (CBENewsListCell *)cellForTableView:(UITableView *)tableView;

@end

//
//  CBEEticketHeadView.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/26.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEEticketHeadView : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *applyLabel;
+ (CBEEticketHeadView *)createHeaderView;

@end

NS_ASSUME_NONNULL_END

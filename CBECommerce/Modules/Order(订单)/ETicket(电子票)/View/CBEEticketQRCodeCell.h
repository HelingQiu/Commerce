//
//  CBEEticketQRCodeCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEEticketQRCodeCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UIImageView *codeView;

+ (CBEEticketQRCodeCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

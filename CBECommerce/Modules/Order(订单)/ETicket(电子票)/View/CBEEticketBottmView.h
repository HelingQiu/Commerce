//
//  CBEEticketBottmView.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^MapBlock)(void);
typedef void(^ChatBlock)(void);
@interface CBEEticketBottmView : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *sponsorLabel;

@property (nonatomic, copy) MapBlock mapBlock;
@property (nonatomic, copy) ChatBlock chatBlock;
+ (CBEEticketBottmView *)createBottomView;

@end

NS_ASSUME_NONNULL_END

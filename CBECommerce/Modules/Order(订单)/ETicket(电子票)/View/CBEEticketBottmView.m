//
//  CBEEticketBottmView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEEticketBottmView.h"
#import "CBEETicketModel.h"

@implementation CBEEticketBottmView

+ (CBEEticketBottmView *)createBottomView
{
    CBEEticketBottmView *cell = [[NSBundle mainBundle] loadNibNamed:@"CBEEticketBottmView" owner:self options:nil].firstObject;
    return cell;
}

- (void)refreshDataWith:(CBEETicketModel *)model
{
    [self.titleLabel setText:model.title];
    __block NSString *timeString = @"时间：";
    NSArray *timeArray = model.timeList;
    [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *start = [obj objectForKey:@"startTime"];
        NSString *end = [obj objectForKey:@"endTime"];
        timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
        if (idx == timeArray.count - 1) {
            [self->_timeLabel setText:timeString];
        }
    }];
    [self.addressLabel setText:StringFormat(@"地址：%@",model.address?:@"")];
    [self.sponsorLabel setText:StringFormat(@"主办方：%@",model.sponsorName?:@"")];
}

- (IBAction)addAction:(UITapGestureRecognizer *)sender {
    if (self.mapBlock) {
        self.mapBlock();
    }
}
- (IBAction)addActionR:(UIButton *)sender {
    if (self.mapBlock) {
        self.mapBlock();
    }
}
- (IBAction)zbfAction:(UITapGestureRecognizer *)sender {
    if (self.chatBlock) {
        self.chatBlock();
    }
}
- (IBAction)zbfActionR:(UIButton *)sender {
    if (self.chatBlock) {
        self.chatBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEEticketNoPriceCodeCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEEticketNoPriceCodeCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *pLabel;
@property (weak, nonatomic) IBOutlet UIImageView *codeView;

@end

NS_ASSUME_NONNULL_END

//
//  CBEEticketHeadView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/26.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEEticketHeadView.h"

@implementation CBEEticketHeadView

+ (CBEEticketHeadView *)createHeaderView
{
    CBEEticketHeadView *cell = [[NSBundle mainBundle] loadNibNamed:@"CBEEticketHeadView" owner:self options:nil].firstObject;
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

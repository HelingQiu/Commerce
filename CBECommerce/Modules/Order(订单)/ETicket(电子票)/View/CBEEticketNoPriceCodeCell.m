//
//  CBEEticketNoPriceCodeCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEEticketNoPriceCodeCell.h"
#import "LSQRCodeManager.h"

@implementation CBEEticketNoPriceCodeCell

+ (CBEEticketNoPriceCodeCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEEticketNoPriceCodeCell";
    CBEEticketNoPriceCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEEticketNoPriceCodeCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(id)model
{
    UIImage *codeImage = [LSQRCodeManager generateQRCodeImageWithString:@"https://www.baidu.com" imageSize:200];
    [self.codeView setImage:codeImage];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

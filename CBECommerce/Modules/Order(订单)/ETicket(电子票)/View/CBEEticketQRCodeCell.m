//
//  CBEEticketQRCodeCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEEticketQRCodeCell.h"
#import "LSQRCodeManager.h"
#import "CBEOrderDetailTicketModel.h"

@implementation CBEEticketQRCodeCell

+ (CBEEticketQRCodeCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEEticketQRCodeCell";
    CBEEticketQRCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEEticketQRCodeCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEOrderDetailTicketModel *)model
{
    [self.codeView sd_setImageWithURL:[NSURL URLWithString:StringFormat(@"%@%@",model.fileUrl,model.fileName)] placeholderImage:IMAGE_NAMED(@"news_default")];
    [self.numLabel setText:model.applyTicketId];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

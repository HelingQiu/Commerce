//
//  CBEEticketController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/26.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEOrderDetailModel.h"
#import "CBEOrderDetailTicketModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, PageStatusType) {
    PageStatusTypeWait = 0,//待参与
    PageStatusTypeDone,//已完成
    PageStatusTypeCancel,//已取消
    PageStatusTypeRefund//退款
};
@interface CBEEticketController : CBEBaseController

@property (nonatomic, assign) PageStatusType pageType;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, assign) NSInteger index;

@end

NS_ASSUME_NONNULL_END

//
//  CBEEticketController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/26.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEEticketController.h"
#import "CBEEticketHeadView.h"
#import "CBEApplyCell.h"
#import "CBEEticketBottmView.h"
#import "CBEEticketQRCodeCell.h"
#import "CBEEticketNoPriceCodeCell.h"
#import "CBEOrderVM.h"
#import "CBEETicketModel.h"
#import "CBEMapViewController.h"
#import "CBEMessageChatController.h"

@interface CBEEticketController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBEETicketModel *ticketModel;

@end

@implementation CBEEticketController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.scrollView];
    [self requestATicketInfo];
}

- (void)requestATicketInfo
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEOrderVM getEticketInfoWithTicketId:self.orderId complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.ticketModel = [CBEETicketModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
            [self setBarTitle:StringFormat(@"我的电子票(1/%lu)",(unsigned long)self.ticketModel.applyTicketList.count)];
            [self.scrollView setContentSize:CGSizeMake(kSCREEN_WIDTH * self.ticketModel.applyTicketList.count, 0)];
            for (int i = 0; i<self.ticketModel.applyTicketList.count; i++) {
                UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(kPadding15 + kSCREEN_WIDTH * i, kPadding15, kSCREEN_WIDTH - 2*kPadding15, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kPadding15 - 50) style:UITableViewStyleGrouped];
                tableView.delegate = self;
                tableView.dataSource = self;
                tableView.backgroundColor = [UIColor whiteColor];
                tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                tableView.clipsToBounds = YES;
                tableView.layer.cornerRadius = 4;
                tableView.tag = i;
                [self.scrollView addSubview:tableView];
            }
            [self.scrollView setContentOffset:CGPointMake(self.index *kSCREEN_WIDTH, 0)];
            [self setBarTitle:StringFormat(@"我的电子票(%d/%lu)",self.index+1,(unsigned long)self.ticketModel.applyTicketList.count)];
            [self addBottomView];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
    }];
}

- (void)refundAction:(id)sender
{
    //申请退款
    [UIAlertView ls_alertWithCallBackBlock:^(NSInteger buttonIndex) {
        if (buttonIndex) {
            [self cancelOrRefund];
        }
    } title:@"申请退款" message:@"是否确认申请整个订单退款?" cancelButtonName:@"取消" otherButtonTitles:@"确认", nil];
}

- (void)cancelOrRefund
{
    [CommonUtils showHUDWithWaitingMessage:@"申请中"];
    [CBEOrderVM cancelOrrefundApplyWithOrderId:self.orderId complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"申请成功，等待审核" autoHide:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)saveTicket:(id)sender
{
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 0.0f); [self.view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext(); UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
    if (image) {
        [CommonUtils showHUDWithMessage:@"保存成功" autoHide:YES];
    }
}

- (void)addBottomView
{
    if (self.ticketModel.orderType == 1) {
        self.pageType = PageStatusTypeWait;
    }else if (self.ticketModel.orderType == 2) {
        self.pageType = PageStatusTypeWait;
    }else if (self.ticketModel.orderType == 3) {
        self.pageType = PageStatusTypeRefund;
    }else if (self.ticketModel.orderType == 4) {
        self.pageType = PageStatusTypeDone;
    }else{
        self.pageType = PageStatusTypeCancel;
    }
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 50, kSCREEN_WIDTH, 50)];
    [self.view addSubview:bottomView];
    
    switch (self.pageType) {
        case PageStatusTypeWait:
        {
            CBEBaseButton *leftBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setFrame:CGRectMake(47, 14, 100, 22)];
            [leftBtn setImageViewRect:CGRectMake(0, 0, 22, 22)];
            [leftBtn setTitleLabelRect:CGRectMake(32, 0, 68, 22)];
            [leftBtn setImage:IMAGE_NAMED(@"ticket_cancle") forState:UIControlStateNormal];
            [leftBtn setTitle:@"申请退款" forState:UIControlStateNormal];
            [leftBtn.titleLabel setFont:kFont(15)];
            [leftBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
            [leftBtn addTarget:self action:@selector(refundAction:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView addSubview:leftBtn];
            
            CBEBaseButton *rightBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
            [rightBtn setFrame:CGRectMake(kSCREEN_WIDTH-50-115, 14, 115, 22)];
            [rightBtn setImageViewRect:CGRectMake(0, 0, 22, 22)];
            [rightBtn setTitleLabelRect:CGRectMake(32, 0, 80, 22)];
            [rightBtn setImage:IMAGE_NAMED(@"ticket_save") forState:UIControlStateNormal];
            [rightBtn setTitle:@"保存电子票" forState:UIControlStateNormal];
            [rightBtn.titleLabel setFont:kFont(15)];
            [rightBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
            [rightBtn addTarget:self action:@selector(saveTicket:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView addSubview:rightBtn];
        }
            break;
        case PageStatusTypeDone:
        case PageStatusTypeCancel:
        {
            CBEBaseButton *rightBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
            [rightBtn setFrame:CGRectMake((kSCREEN_WIDTH-115)/2, 14, 115, 22)];
            [rightBtn setImageViewRect:CGRectMake(0, 0, 22, 22)];
            [rightBtn setTitleLabelRect:CGRectMake(32, 0, 80, 22)];
            [rightBtn setImage:IMAGE_NAMED(@"ticket_save") forState:UIControlStateNormal];
            [rightBtn setTitle:@"保存电子票" forState:UIControlStateNormal];
            [rightBtn.titleLabel setFont:kFont(15)];
            [rightBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
            [rightBtn addTarget:self action:@selector(saveTicket:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView addSubview:rightBtn];
        }
            break;
        case PageStatusTypeRefund:
        {
            CBEBaseButton *rightBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
            [rightBtn setFrame:CGRectMake((kSCREEN_WIDTH-250)/2, 14, 250, 22)];
            [rightBtn setTitleLabelRect:CGRectMake(0, 0, 250, 22)];
            [rightBtn setTitle:@"正在退款中（一般三到五个工作日）" forState:UIControlStateNormal];
            [rightBtn.titleLabel setFont:kFont(15)];
            [rightBtn setTitleColor:[UIColor ls_colorWithHexString:@"#FC6B3F"] forState:UIControlStateNormal];
            [bottomView addSubview:rightBtn];
        }
            break;
        default:
            break;
    }
}

#pragma mark --
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 92.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CBEEticketHeadView *headView = [CBEEticketHeadView createHeaderView];
    headView.frame = CGRectMake(0, 0, kSCREEN_WIDTH - kPadding15, 92);
    
    CBEOrderDetailTicketModel *model = [CBEOrderDetailTicketModel mj_objectWithKeyValues:[self.ticketModel.applyTicketList objectAtIndex:tableView.tag]];
    const float EPSINON = 0.00001;
    double money = model.price;
    if ((money >= - EPSINON) && (money <= EPSINON)){//免费票
        [headView.priceLabel setText:StringFormat(@"费用：免费")];
    }else{
        [headView.priceLabel setText:StringFormat(@"费用：%.2f",model.price)];
    }
    [headView.applyLabel setText:StringFormat(@"%@ %@",self.ticketModel.applyName?:@"",self.ticketModel.applyPhone?:@"")];
    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 170.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    //免费或者只有一个二维码不显示费用
    CBEEticketBottmView *footView = [CBEEticketBottmView createBottomView];
    footView.frame = CGRectMake(0, 0, kSCREEN_WIDTH - kPadding15, 170);
    [footView refreshDataWith:self.ticketModel];
    
    footView.mapBlock = ^{
        CBEMapViewController *mapController = [[CBEMapViewController alloc] init];
        mapController.latitude = self.ticketModel.latitude;
        mapController.longitude = self.ticketModel.longitude;
        mapController.address = self.ticketModel.address;
        [self.navigationController pushViewController:mapController animated:YES];
    };
    
    footView.chatBlock = ^{
        NIMSession *session = [NIMSession session:self.ticketModel.sponsorMobile type:NIMSessionTypeP2P];
        CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:session];
        [self.navigationController pushViewController:vc animated:YES];
    };
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 275.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEEticketQRCodeCell *cell = [CBEEticketQRCodeCell cellForTableView:tableView];
    CBEOrderDetailTicketModel *model = [CBEOrderDetailTicketModel mj_objectWithKeyValues:[self.ticketModel.applyTicketList objectAtIndex:tableView.tag]];
    [cell refreshDataWith:model];
    return cell;
}
#pragma mark -
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView) {
        CGFloat distance = scrollView.contentOffset.x;
        CGFloat width = scrollView.frame.size.width;
        NSInteger index = distance / width;
        [self setBarTitle:StringFormat(@"我的电子票(%lu/%lu)",index+1,(unsigned long)self.ticketModel.applyTicketList.count)];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

#pragma mark - getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(kPadding15, kPadding15, kSCREEN_WIDTH - 2*kPadding15, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kPadding15 - 50) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.clipsToBounds = YES;
        _tableView.layer.cornerRadius = 4;
    }
    return _tableView;
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 50)];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, 0);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
    }
    return _scrollView;
}

@end

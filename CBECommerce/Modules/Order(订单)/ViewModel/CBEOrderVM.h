//
//  CBEOrderVM.h
//  CBECommerce
//
//  Created by don on 2018/10/25.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEOrderVM : NSObject
//订单列表
+ (void)requestOrderListWith:(NSInteger)orderType
                    complete:(CBENetWorkSucceed)successed
                        fail:(CBENetWorkFailure)failed;
//订单详情
+ (void)requestOrderDetailWithDetailId:(NSString *)detailId
                              complete:(CBENetWorkSucceed)successed
                                  fail:(CBENetWorkFailure)failed;
//发起支付
+ (void)resqPayWithOrderId:(NSString *)orderId
             withOrderCode:(NSString *)orderCode
               withPayType:(NSString *)payType
                  complete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed;
//取消活动或者申请退款
+ (void)cancelOrrefundApplyWithOrderId:(NSString *)orderId
                              complete:(CBENetWorkSucceed)successed
                                  fail:(CBENetWorkFailure)failed;
//获取电子票信息
+ (void)getEticketInfoWithTicketId:(NSString *)orderId
                          complete:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed;

@end

NS_ASSUME_NONNULL_END

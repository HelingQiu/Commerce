//
//  CBEOrderVM.m
//  CBECommerce
//
//  Created by don on 2018/10/25.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEOrderVM.h"

@implementation CBEOrderVM

+ (void)requestOrderListWith:(NSInteger)orderType
                    complete:(CBENetWorkSucceed)successed
                        fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [params setObject:StringFormat(@"%ld",(long)orderType) forKey:@"orderType"];
//    [params setObject:@"1" forKey:@"userId"];
    NSLog(@"%@----====",[CBEUserModel sharedInstance].token);
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryMyOrderByUserId parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestOrderDetailWithDetailId:(NSString *)detailId
                              complete:(CBENetWorkSucceed)successed
                                  fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [params setObject:detailId forKey:@"id"];
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryOrderInfoById parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//调起支付
+ (void)resqPayWithOrderId:(NSString *)orderId
             withOrderCode:(NSString *)orderCode
               withPayType:(NSString *)payType
                  complete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:orderCode forKey:@"orderCode"];
    [params setObject:orderId forKey:@"id"];
    [params setObject:payType forKey:@"applyAccountType"];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kCreateOrder parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//取消活动或者申请退款
+ (void)cancelOrrefundApplyWithOrderId:(NSString *)orderId
                              complete:(CBENetWorkSucceed)successed
                                  fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:orderId forKey:@"id"];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kActivityRefund parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//获取电子票信息
+ (void)getEticketInfoWithTicketId:(NSString *)orderId
                          complete:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:orderId forKey:@"id"];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kGetEticketApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end

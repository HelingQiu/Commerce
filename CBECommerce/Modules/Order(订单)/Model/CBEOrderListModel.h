//
//  CBEOrderListModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/28.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEOrderListModel : NSObject

@property (nonatomic, copy) NSString *activityId;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, strong) NSArray *applyTicketList;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *orderId;//id
@property (nonatomic, copy) NSString *picture;
@property (nonatomic, assign) NSInteger refundStatus;
@property (nonatomic, copy) NSString *regionName;
@property (nonatomic, copy) NSString *sponsorMobile;
@property (nonatomic, copy) NSString *sponsorName;
@property (nonatomic, strong) NSArray *timeList;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) double totalPrice;
@property (nonatomic, assign) NSInteger orderType;//订单状态

@end

NS_ASSUME_NONNULL_END

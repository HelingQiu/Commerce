//
//  CBEOrderDetailTicketModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/10.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEOrderDetailTicketModel : NSObject

@property (nonatomic, copy) NSString *activityId;//活动ID
@property (nonatomic, copy) NSString *billId;//订单ID
@property (nonatomic, copy) NSString *fileName;//二维码名称
@property (nonatomic, copy) NSString *fileUrl;//二维码地址
@property (nonatomic, copy) NSString *applyTicketId;//票单ID
@property (nonatomic, copy) NSString *name;//票名称
@property (nonatomic, assign) double price;//票价
@property (nonatomic, copy) NSString *ticketId;//对应活动票的ID
@property (nonatomic, assign) NSInteger ticketStatus;//状态，0未启用（待付款状态），1正常，2，已用，3取消


@end

NS_ASSUME_NONNULL_END

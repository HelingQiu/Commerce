//
//  CBEETicketModel.h
//  CBECommerce
//
//  Created by don on 2018/11/11.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEETicketModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *activityId;
@property (nonatomic, copy) NSString *applyPhone;
@property (nonatomic, copy) NSString *billId;
@property (nonatomic, copy) NSString *regionName;//片区名称
@property (nonatomic, copy) NSString *sponsorName;//主办方名称
@property (nonatomic, copy) NSString *sponsorMobile;//主办方电话
@property (nonatomic, copy) NSString *picture;
@property (nonatomic, strong) NSArray *timeList;//活动时间-根据订单取的
@property (nonatomic, copy) NSString *title;//活动标题
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *applyName;
@property (nonatomic, assign) NSInteger orderType;
@property (nonatomic, strong) NSArray *applyTicketList;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;


@end

NS_ASSUME_NONNULL_END

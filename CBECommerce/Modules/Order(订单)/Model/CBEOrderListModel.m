//
//  CBEOrderListModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/28.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEOrderListModel.h"

@implementation CBEOrderListModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"orderId":@"id"};
}

@end

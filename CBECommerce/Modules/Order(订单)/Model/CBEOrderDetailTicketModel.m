//
//  CBEOrderDetailTicketModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/10.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEOrderDetailTicketModel.h"

@implementation CBEOrderDetailTicketModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"applyTicketId":@"id"};
}

@end

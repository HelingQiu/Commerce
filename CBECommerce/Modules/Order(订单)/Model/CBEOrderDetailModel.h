//
//  CBEOrderDetailModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/9.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEOrderDetailModel : NSObject

@property (nonatomic, copy) NSString *activityId;//活动ID
@property (nonatomic, copy) NSString *applyName;//报名用户名
@property (nonatomic, copy) NSString *applyPhone;//报名号码
@property (nonatomic, strong) NSArray *applyTicketList;//电子票列表
@property (nonatomic, copy) NSString *createTime;//创建时间
@property (nonatomic, copy) NSString *orderCode;//订单编号
@property (nonatomic, assign) NSInteger orderType;//订单状态
@property (nonatomic, assign) double paymentPrice;//支付金额
@property (nonatomic, copy) NSString *picture;//海报
@property (nonatomic, copy) NSString *sponsorName;//发布机构
@property (nonatomic, copy) NSString *sponsorMobile;//主办方电话
@property (nonatomic, strong) NSArray *timeList;//活动时间
@property (nonatomic, copy) NSString *title;//活动标题
@property (nonatomic, assign) double totalPrice;//票总价

@property (nonatomic, copy) NSString *address;//活动地址
@property (nonatomic, copy) NSString *name;//票名
@property (nonatomic, copy) NSString *price;//票价
@property (nonatomic, copy) NSString *activityUserId;//主办方userId

@end

NS_ASSUME_NONNULL_END

//
//  CBEWaitPayController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CBEWaitPayController : CBEBaseController

@property (nonatomic, strong) CBEOrderListModel *orderModel;

@end

NS_ASSUME_NONNULL_END

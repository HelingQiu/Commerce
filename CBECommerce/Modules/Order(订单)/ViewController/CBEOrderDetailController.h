//
//  CBEOrderDetailController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, OrderStatusPageType) {
    OrderStatusPageTypeWaitJoin = 0,//待参与
    OrderStatusPageTypeComplete,    //已完成
    OrderStatusPageTypeRefund,      //退款
    OrderStatusPageTypeCancel,       //已取消  //待付款是独立页面
    OrderStatusPageTypeWaitPay
};
@interface CBEOrderDetailController : CBEBaseController

@property (nonatomic, assign) OrderStatusPageType pageType;

@property (nonatomic, strong) CBEOrderListModel *orderModel;

@end

NS_ASSUME_NONNULL_END

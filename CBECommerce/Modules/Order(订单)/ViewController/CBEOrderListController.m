//
//  CBEOrderListController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderListController.h"
#import "CBEOrderTopView.h"
#import "CBENoOrderView.h"
#import "CBEOrderListCell.h"
#import "CBEWaitPayController.h"
#import "CBEOrderDetailController.h"
#import "CBEEticketController.h"
#import "CBEOrderVM.h"
#import "CBEOrderListModel.h"
#import "UIAlertController+Blocks.h"

@interface CBEOrderListController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property (nonatomic, assign) NSInteger pageIndex;

@property (nonatomic, strong) CBEOrderTopView *topView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UITableView *firstTableView;
@property (nonatomic, strong) CBENoOrderView *firstNoView;
@property (nonatomic, strong) NSMutableArray *firstArray;

@property (nonatomic, strong) UITableView *secondTableView;
@property (nonatomic, strong) CBENoOrderView *secondNoView;
@property (nonatomic, strong) NSMutableArray *secondArray;

@property (nonatomic, strong) UITableView *thirdTableView;
@property (nonatomic, strong) CBENoOrderView *thirdNoView;
@property (nonatomic, strong) NSMutableArray *thirdArray;

@property (nonatomic, strong) UITableView *forthTableView;
@property (nonatomic, strong) CBENoOrderView *forthNoView;
@property (nonatomic, strong) NSMutableArray *forthArray;

@property (nonatomic, strong) UITableView *fifthTableView;
@property (nonatomic, strong) CBENoOrderView *fifthNoView;
@property (nonatomic, strong) NSMutableArray *fifthArray;

@end

@implementation CBEOrderListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBar];
    [self.view addSubview:self.topView];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.firstTableView];
    [self.scrollView addSubview:self.secondTableView];
    [self.scrollView addSubview:self.thirdTableView];
    [self.scrollView addSubview:self.forthTableView];
    [self.scrollView addSubview:self.fifthTableView];
    self.firstArray = [NSMutableArray array];
    self.secondArray = [NSMutableArray array];
    self.thirdArray = [NSMutableArray array];
    self.forthArray = [NSMutableArray array];
    self.fifthArray = [NSMutableArray array];
    
    [self otherPageComehere];
}

- (void)setNavBar
{
    [self setBarTitle:@"我的订单"];
    self.pageIndex = 0;
}

- (void)requestDataListWithType:(NSInteger)type
{
    [CBEOrderVM requestOrderListWith:type complete:^(id data) {
        [self.firstTableView.mj_header endRefreshing];
        [self.secondTableView.mj_header endRefreshing];
        [self.thirdTableView.mj_header endRefreshing];
        [self.forthTableView.mj_header endRefreshing];
        [self.fifthTableView.mj_header endRefreshing];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *dataArray = [data objectForKey:@"data"];
            if (type == -1) {
                [self.firstArray removeAllObjects];
                
            }
            if (type == 1) {
                [self.secondArray removeAllObjects];
                
            }
            if (type == 2) {
                [self.thirdArray removeAllObjects];
                
            }
            if (type == 3) {
                [self.forthArray removeAllObjects];
                
            }
            if (type == 4) {
                [self.fifthArray removeAllObjects];
                
            }
            [dataArray enumerateObjectsUsingBlock:^(NSDictionary  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CBEOrderListModel *model = [CBEOrderListModel mj_objectWithKeyValues:obj];
                if (type == -1) {
                    [self.firstArray addObject:model];
                }else if (type == 1) {
                    [self.secondArray addObject:model];
                }else if (type == 2) {
                    [self.thirdArray addObject:model];
                }else if (type == 3) {
                    [self.forthArray addObject:model];
                }else if (type == 4) {
                    [self.fifthArray addObject:model];
                }
            }];
            if (type == -1) {
                [self.firstTableView reloadData];
                if (!self.firstArray.count) {
                    self.firstNoView.hidden = NO;
                }else{
                    self.firstNoView.hidden = YES;
                }
            }else if (type == 1) {
                [self.secondTableView reloadData];
                if (!self.secondArray.count) {
                    self.secondNoView.hidden = NO;
                }else{
                    self.secondNoView.hidden = YES;
                }
            }else if (type == 2) {
                [self.thirdTableView reloadData];
                if (!self.thirdArray.count) {
                    self.thirdNoView.hidden = NO;
                }else{
                    self.thirdNoView.hidden = YES;
                }
            }else if (type == 3) {
                [self.forthTableView reloadData];
                if (!self.forthArray.count) {
                    self.forthNoView.hidden = NO;
                }else{
                    self.forthNoView.hidden = YES;
                }
            }else if (type == 4) {
                [self.fifthTableView reloadData];
                if (!self.fifthArray.count) {
                    self.fifthNoView.hidden = NO;
                }else{
                    self.fifthNoView.hidden = YES;
                }
            }
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [self.firstTableView.mj_header endRefreshing];
        [self.secondTableView.mj_header endRefreshing];
        [self.thirdTableView.mj_header endRefreshing];
        [self.forthTableView.mj_header endRefreshing];
        [self.fifthTableView.mj_header endRefreshing];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)otherPageComehere
{
    if (self.pageType == OrderPageTypeAll) {
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [self.firstTableView.mj_header beginRefreshing];
    }else if (self.pageType == OrderPageTypeWaitPay){
        [self.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH, 0) animated:YES];
        [self.secondTableView.mj_header beginRefreshing];
    }else if (self.pageType == OrderPageTypeWaitJoin) {
        [self.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH*2, 0) animated:YES];
        [self.thirdTableView.mj_header beginRefreshing];
    }else if (self.pageType == OrderPageTypeRefund) {
        [self.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH*3, 0) animated:YES];
        [self.forthTableView.mj_header beginRefreshing];
    }else if (self.pageType == OrderPageTypeComplete) {
        [self.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH*4, 0) animated:YES];
        [self.fifthTableView.mj_header beginRefreshing];
    }
}

- (CBEOrderTopView *)topView
{
    if (!_topView) {
        _topView = [[CBEOrderTopView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 50)];
        kWeakSelf;
        _topView.block = ^(NSInteger index) {
            [weakSelf.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH * index, 0) animated:YES];
            if (index == 0) {
                if (!weakSelf.firstArray.count) {
                    [weakSelf.firstTableView.mj_header beginRefreshing];
                }
            }else if (index == 1) {
                if (!weakSelf.secondArray.count) {
                    [weakSelf.secondTableView.mj_header beginRefreshing];
                }
            }else if (index == 2) {
                if (!weakSelf.thirdArray.count) {
                    [weakSelf.thirdTableView.mj_header beginRefreshing];
                }
            }else if (index == 3) {
                if (!weakSelf.forthArray.count) {
                    [weakSelf.forthTableView.mj_header beginRefreshing];
                }
            }else if (index == 4) {
                if (!weakSelf.fifthArray.count) {
                    [weakSelf.fifthTableView.mj_header beginRefreshing];
                }
            }
        };
    }
    return _topView;
}

#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.firstTableView) {
        return self.firstArray.count;
    }
    if (tableView == self.secondTableView) {
        return self.secondArray.count;
    }
    if (tableView == self.thirdTableView) {
        return self.thirdArray.count;
    }
    if (tableView == self.forthTableView) {
        return self.forthArray.count;
    }
    return self.fifthArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.firstTableView) {
        CBEOrderListModel *model = [self.firstArray objectAtIndex:indexPath.section];
        if (model.orderType == 1 || model.orderType == 2) {
            return 261.f;
        }
        return 221.f;
    }else if (tableView == self.secondTableView || tableView == self.thirdTableView) {
        return 261.f;
    }
    return 221.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEOrderListCell *cell = [CBEOrderListCell cellForTableView:tableView];
    if (tableView == self.firstTableView) {
        CBEOrderListModel *model = [self.firstArray objectAtIndex:indexPath.section];
        [cell refreshDataWith:model];
        cell.rightBlock = ^{
            if (model.orderType == 1) {//去付款
                CBEWaitPayController *payController = [[CBEWaitPayController alloc] init];
                payController.orderModel = model;
                [self.navigationController pushViewController:payController animated:YES];
            }else if (model.orderType == 2) {//待参与
                if (model.totalPrice > 0.0000001) {
                    //申请退款
                    [UIAlertController showAlertInViewController:self
                                                       withTitle:@"申请退款"
                                                         message:@"是否确认申请退款?"
                                               cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:@"确认"
                                               otherButtonTitles:nil
                                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){

                                                            if (buttonIndex == controller.destructiveButtonIndex) {
                                                                [self cancelOrRefundWith:model withTable:tableView];
                                                            }
                                                        }];
                }else{
                    //取消活动
                    [UIAlertController showAlertInViewController:self
                                                       withTitle:@"取消活动"
                                                         message:@"是否确认取消活动?"
                                               cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:@"确认"
                                               otherButtonTitles:nil
                                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){

                                                            if (buttonIndex == controller.destructiveButtonIndex) {
                                                                [self cancelOrRefundWith:model withTable:tableView];
                                                            }
                                                        }];
                }
            }
        };
    }else if (tableView == self.secondTableView) {
        CBEOrderListModel *model = [self.secondArray objectAtIndex:indexPath.section];
        [cell refreshDataWith:model];
        cell.rightBlock = ^{
            CBEWaitPayController *payController = [[CBEWaitPayController alloc] init];
            payController.orderModel = model;
            [self.navigationController pushViewController:payController animated:YES];
        };
    }else if (tableView == self.thirdTableView) {
        CBEOrderListModel *model = [self.thirdArray objectAtIndex:indexPath.section];
        [cell refreshDataWith:model];
        cell.rightBlock = ^{
            if (model.orderType == 1) {//去付款
                CBEWaitPayController *payController = [[CBEWaitPayController alloc] init];
                payController.orderModel = model;
                [self.navigationController pushViewController:payController animated:YES];
            }else if (model.orderType == 2) {//待参与
                if (model.totalPrice > 0.0000001) {
                    //申请退款
                    [UIAlertController showAlertInViewController:self
                                                       withTitle:@"申请退款"
                                                         message:@"是否确认申请退款?"
                                               cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:@"确认"
                                               otherButtonTitles:nil
                                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                            
                                                            if (buttonIndex == controller.destructiveButtonIndex) {
                                                                [self cancelOrRefundWith:model withTable:tableView];
                                                            }
                                                        }];
                }else{
                    //取消活动
                    [UIAlertController showAlertInViewController:self
                                                       withTitle:@"取消活动"
                                                         message:@"是否确认取消活动?"
                                               cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:@"确认"
                                               otherButtonTitles:nil
                                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                            
                                                            if (buttonIndex == controller.destructiveButtonIndex) {
                                                                [self cancelOrRefundWith:model withTable:tableView];
                                                            }
                                                        }];
                }
            }
        };
    }else if (tableView == self.forthTableView) {
        CBEOrderListModel *model = [self.forthArray objectAtIndex:indexPath.section];
        [cell refreshDataWith:model];
    }else{
        CBEOrderListModel *model = [self.fifthArray objectAtIndex:indexPath.section];
        [cell refreshDataWith:model];
    }
    
    return cell;
}

- (void)cancelOrRefundWith:(CBEOrderListModel *)model withTable:(UITableView *)tableView
{
    [CommonUtils showHUDWithWaitingMessage:@"申请中"];
    [CBEOrderVM cancelOrrefundApplyWithOrderId:model.orderId complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"申请成功，等待审核" autoHide:YES];
            if (tableView == self.firstTableView) {
                [self.firstTableView.mj_header beginRefreshing];
            }else if (tableView == self.secondTableView) {
                [self.secondTableView.mj_header beginRefreshing];
            }else if (tableView == self.thirdTableView) {
                [self.thirdTableView.mj_header beginRefreshing];
            }else if (tableView == self.forthTableView) {
                [self.forthTableView.mj_header beginRefreshing];
            }else{
                [self.fifthTableView.mj_header beginRefreshing];
            }
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEOrderListModel *model;
    OrderStatusPageType type = 0;
    if (tableView == _firstTableView) {
        model = [self.firstArray objectAtIndex:indexPath.section];
        if (model.orderType == 1) {
            type = OrderStatusPageTypeWaitPay;
        }else if (model.orderType == 2) {
            type = OrderStatusPageTypeWaitJoin;
        }else if (model.orderType == 3) {
            type = OrderStatusPageTypeRefund;
        }else if (model.orderType == 4) {
            type = OrderStatusPageTypeComplete;
        }else{
            type = OrderStatusPageTypeCancel;
        }
    }else if (tableView == _secondTableView) {
        model = [self.secondArray objectAtIndex:indexPath.section];
        type = OrderStatusPageTypeWaitPay;
    }else if (tableView == _thirdTableView) {
        model = [self.thirdArray objectAtIndex:indexPath.section];
        type = OrderStatusPageTypeWaitJoin;
    }else if (tableView == _forthTableView) {
        model = [self.forthArray objectAtIndex:indexPath.section];
        type = OrderStatusPageTypeRefund;
    }else{
        model = [self.fifthArray objectAtIndex:indexPath.section];
        type = OrderStatusPageTypeComplete;
    }
    if (type != OrderStatusPageTypeWaitPay) {
        CBEOrderDetailController *detailController = [[CBEOrderDetailController alloc] init];
        detailController.orderModel = model;
        detailController.pageType = type;
        [self.navigationController pushViewController:detailController animated:YES];
    }else{
        
        CBEWaitPayController *payController = [[CBEWaitPayController alloc] init];
        payController.orderModel = model;
        [self.navigationController pushViewController:payController animated:YES];
    }
    
}
#pragma mark - ScrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView) {
        CGFloat distance = scrollView.contentOffset.x;
        CGFloat width = scrollView.frame.size.width;
        NSInteger index = distance / width;
        if (_pageIndex != index) {
            [self.topView moveLineAtIndex:index];
            if (index == 0) {
                if (!self.firstArray.count) {
                    [self.firstTableView.mj_header beginRefreshing];
                }
            }else if (index == 1) {
                if (!self.secondArray.count) {
                    [self.secondTableView.mj_header beginRefreshing];
                }
            }else if (index == 2) {
                if (!self.thirdArray.count) {
                    [self.thirdTableView.mj_header beginRefreshing];
                }
            }else if (index == 3) {
                if (!self.forthArray.count) {
                    [self.forthTableView.mj_header beginRefreshing];
                }
            }else if (index == 4) {
                if (!self.fifthArray.count) {
                    [self.fifthTableView.mj_header beginRefreshing];
                }
            }
        }
        _pageIndex = index;
    }
}

#pragma mark - Prvite
- (UITableView *)firstTableView
{
    if (!_firstTableView) {
        _firstTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 50) style:UITableViewStyleGrouped];
        _firstTableView.delegate = self;
        _firstTableView.dataSource = self;
        _firstTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _firstTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requestDataListWithType:-1];
        }];
        
        _firstNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _firstTableView.width, _firstTableView.height)];
        _firstNoView.hidden = YES;
        _firstNoView.title = @"还没有相关订单";
        [_firstTableView addSubview:_firstNoView];
    }
    return _firstTableView;
}

- (UITableView *)secondTableView
{
    if (!_secondTableView) {
        _secondTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 50) style:UITableViewStyleGrouped];
        _secondTableView.delegate = self;
        _secondTableView.dataSource = self;
        _secondTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _secondTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requestDataListWithType:1];
        }];
        
        _secondNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _secondTableView.width, _secondTableView.height)];
        _secondNoView.hidden = YES;
        _secondNoView.title = @"还没有相关订单";
        [_secondTableView addSubview:_secondNoView];
    }
    return _secondTableView;
}

- (UITableView *)thirdTableView
{
    if (!_thirdTableView) {
        _thirdTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH*2, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 50) style:UITableViewStyleGrouped];
        _thirdTableView.delegate = self;
        _thirdTableView.dataSource = self;
        _thirdTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _thirdTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requestDataListWithType:2];
        }];
        
        _thirdNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _thirdTableView.width, _thirdTableView.height)];
        _thirdNoView.hidden = YES;
        _thirdNoView.title = @"还没有相关订单";
        [_thirdTableView addSubview:_thirdNoView];
    }
    return _thirdTableView;
}

- (UITableView *)forthTableView
{
    if (!_forthTableView) {
        _forthTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH*3, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 50) style:UITableViewStyleGrouped];
        _forthTableView.delegate = self;
        _forthTableView.dataSource = self;
        _forthTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _forthTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requestDataListWithType:3];
        }];
        
        _forthNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _forthTableView.width, _forthTableView.height)];
        _forthNoView.hidden = YES;
        _forthNoView.title = @"还没有相关订单";
        [_forthTableView addSubview:_forthNoView];
    }
    return _forthTableView;
}

- (UITableView *)fifthTableView
{
    if (!_fifthTableView) {
        _fifthTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH*4, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - 50) style:UITableViewStyleGrouped];
        _fifthTableView.delegate = self;
        _fifthTableView.dataSource = self;
        _fifthTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _fifthTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requestDataListWithType:4];
        }];
        
        _fifthNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _fifthTableView.width, _fifthTableView.height)];
        _fifthNoView.hidden = YES;
        _fifthNoView.title = @"还没有相关订单";
        [_fifthTableView addSubview:_fifthNoView];
    }
    return _fifthTableView;
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance)];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH * 5, 0);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
    }
    return _scrollView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

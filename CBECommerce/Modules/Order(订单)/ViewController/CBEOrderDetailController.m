//
//  CBEOrderDetailController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderDetailController.h"
#import "CBEApplyCell.h"
#import "CBEOrderDetailCell.h"
#import "CBEOrderPriceCell.h"
#import "CBEOrderInfoCell.h"
#import "CBEDetailBottomCell.h"
#import "CBEETicketCell.h"
#import "CBEEticketController.h"
#import "CBEOrderVM.h"
#import "CBEOrderDetailModel.h"
#import "CBEOrderDetailTicketModel.h"
#import "CBEMessageChatController.h"
#import "CBEApplyerInfoController.h"

static CGFloat kBottomHeight = 50.f;

@interface CBEOrderDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBEDetailBottomCell *bottomView;
@property (nonatomic, strong) CBEOrderDetailModel *detailModel;
@property (nonatomic, strong) NSArray *cateArray;

@end

@implementation CBEOrderDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBar];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self requestOrderDetailData];
}
- (void)setNavBar
{
    [self setBarTitle:@"订单详情"];
}

//获取详情
- (void)requestOrderDetailData
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEOrderVM requestOrderDetailWithDetailId:self.orderModel.orderId complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.detailModel = [CBEOrderDetailModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
            self.cateArray = [self filtterTicketData];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CBEApplyCell *cell = [CBEApplyCell cellForTableView:tableView];
        [cell.applyInfoLabel setText:StringFormat(@"%@(%@)",self.detailModel.applyName?:@"",self.detailModel.applyPhone?:@"")];
        return cell;
    }else if (indexPath.section == 1) {
        CBEOrderDetailCell *cell = [CBEOrderDetailCell cellForTableView:tableView];
        [cell refreshDataWith:self.detailModel];
        return cell;
    }else if (indexPath.section == 2) {
        CBEETicketCell *cell = [CBEETicketCell cellForTableView:tableView];
        NSArray *ticketArray = self.detailModel.applyTicketList;
        CBEOrderDetailTicketModel *ticketModel = [CBEOrderDetailTicketModel mj_objectWithKeyValues:[ticketArray objectAtIndex:indexPath.row]];
        [cell refreshDataWith:[self.cateArray objectAtIndex:indexPath.row]];
        if (self.orderModel.orderType == 1) {
            [cell.statusLabel setText:@"待付款"];
        }else if (self.orderModel.orderType == 2) {
            [cell.statusLabel setText:@"待参与"];
        }else if (self.orderModel.orderType == 3) {
            [cell.statusLabel setText:@"退款"];
        }else if (self.orderModel.orderType == 4) {
            [cell.statusLabel setText:@"已完成"];
        }else{
            [cell.statusLabel setText:@"已取消"];
        }
        cell.ticketBlock = ^{
            CBEEticketController *ticketController = [[CBEEticketController alloc] init];
            ticketController.pageType = PageStatusTypeRefund;
            ticketController.orderId = ticketModel.billId;
            NSInteger index = 0;
            if (indexPath.row > 0) {
                for (int i = 0; i < indexPath.row; i++) {
                    NSArray *array = [self.cateArray objectAtIndex:i];
                    index += array.count;
                }
            }
            ticketController.index = index;
            [self.navigationController pushViewController:ticketController animated:YES];
        };
        return cell;
    }else if (indexPath.section == 3){
        CBEOrderPriceCell *cell = [CBEOrderPriceCell cellForTableView:tableView];
        [cell refreshDataWith:self.detailModel];
        
        return cell;
    }
    CBEOrderInfoCell *cell = [CBEOrderInfoCell cellForTableView:tableView];
    [cell refreshDataWith:self.detailModel];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        NSArray *ticketArray = self.detailModel.applyTicketList;
        return self.cateArray.count;;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44.f;
    }else if (indexPath.section == 1) {
        return 145.f;
    }else if (indexPath.section == 2) {
        return 120.f;
    }else if (indexPath.section == 3) {
        return 75.f;
    }
    return 75.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return CGFLOAT_MIN;
    }
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CBEApplyerInfoController *applyController = [[CBEApplyerInfoController alloc] init];
        applyController.applyName = self.detailModel.applyName;
        applyController.applyPhone = self.detailModel.applyPhone;
        [self.navigationController pushViewController:applyController animated:YES];
    }
}
#pragma mark - private
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (CBEDetailBottomCell *)bottomView
{
    if (!_bottomView) {
        _bottomView = [CBEDetailBottomCell createBottomView];
        _bottomView.frame = CGRectMake(0, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight, kSCREEN_WIDTH, kBottomHeight);
        
        kWeakSelf
        _bottomView.leftBlock = ^{
            [weakSelf gotoChat];
        };
        _bottomView.rightBlock = ^{
            [weakSelf callAction];
        };
    }
    return _bottomView;
}

- (void)gotoChat
{
    NIMSession *session = [NIMSession session:self.detailModel.activityUserId  type:NIMSessionTypeP2P];
    CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:session];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)callAction
{
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",self.detailModel.sponsorMobile];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
    [self.view addSubview:callWebview];
}

- (NSArray *)filtterTicketData {
    NSArray *array1 = self.detailModel.applyTicketList;
    NSMutableArray *array = [NSMutableArray arrayWithArray:array1];
    
    NSMutableArray *dataMutablearray = [@[] mutableCopy];
    for (int i = 0; i < array.count; i ++) {
        
        NSDictionary *body = array[i];
        long ticketId = [[body objectForKey:@"ticketId"] longValue];
        NSMutableArray *tempArray = [@[] mutableCopy];
        
        [tempArray addObject:body];
        
        for (int j = i+1; j < array.count; j ++) {
            
            NSDictionary *jBody = array[j];
            long jTicketId = [[jBody objectForKey:@"ticketId"] longValue];
            if(ticketId == jTicketId){
                
                [tempArray addObject:jBody];
                
                [array removeObjectAtIndex:j];
                j -= 1;
            }
        }
        [dataMutablearray addObject:tempArray];
    }
    return dataMutablearray;
}

@end

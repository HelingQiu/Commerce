//
//  CBEWaitPayController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEWaitPayController.h"
#import "CBEApplyCell.h"
#import "CBEOrderDetailCell.h"
#import "CBEETicketCell.h"
#import "CBEPayTypeCell.h"
#import "CBEOrderBottomCell.h"
#import "CBEApplyerInfoController.h"
#import "CBEApplySuccessController.h"
#import "CBEOrderVM.h"
#import <AlipaySDK/AlipaySDK.h>
#import "CBEOrderDetailModel.h"
#import "CBEOrderDetailTicketModel.h"
#import "WXApi.h"

static CGFloat kBottomHeight = 50.f;

@interface CBEWaitPayController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) BOOL isWeiXinPay;
@property (nonatomic, strong) CBEOrderBottomCell *bottomView;
@property (nonatomic, strong) CBEOrderDetailModel *detailModel;
@property (nonatomic, strong) NSArray *cateTicketArray;

@end

@implementation CBEWaitPayController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBar];
    self.isWeiXinPay = YES;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self requestOrderDetailData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccessed:) name:kPaySuccessed object:nil];
}

- (void)setNavBar
{
    [self setBarTitle:@"订单详情"];
    self.isWeiXinPay = YES;
}
//获取详情
- (void)requestOrderDetailData
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEOrderVM requestOrderDetailWithDetailId:self.orderModel.orderId complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.detailModel = [CBEOrderDetailModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
            [self.bottomView.payCountLabel setText:StringFormat(@"¥%.2f",_detailModel.paymentPrice)];
            self.cateTicketArray = [self filtterTicketData];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
//发起支付
- (void)createOrder
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    NSString *paytype = @"1";
    if (self.isWeiXinPay) {
        paytype = @"2";
    }
    [CBEOrderVM resqPayWithOrderId:self.orderModel.orderId withOrderCode:self.detailModel.orderCode?:@"" withPayType:paytype complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            if (!self.isWeiXinPay) {
                NSString *payString = [data objectForKey:@"data"];
                NSString *appScheme = @"kuajinghelp";
                [[AlipaySDK defaultService] payOrder:payString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
                    NSLog(@"reslut = %@",resultDic);
                }];
            }else{
                NSDictionary *result = [data objectForKey:@"data"];
                /** appid */
                NSString *appid = result[@"appid"];
                /** 商家向财付通申请的商家id */
                NSString *partnerId = result[@"partnerid"];
                /** 预支付订单 */
                NSString *prepayId = result[@"prepayid"];
                /** 随机串，防重发 */
                NSString *nonceStr = result[@"noncestr"];
                /** 时间戳，防重发 */
                NSString *timeStamp = result[@"timestamp"];
                /** 商家根据财付通文档填写的数据和签名 */
                NSString *package = result[@"packageValue"];
                /** 商家根据微信开放平台文档对数据做的签名 */
                NSString *sign  = result[@"sign"];
                
                PayReq *req             = [[PayReq alloc] init];
                req.partnerId           = partnerId;
                req.prepayId            = prepayId;
                req.nonceStr            = nonceStr;
                req.timeStamp           = timeStamp.intValue;
                req.package             = package;
                req.sign                = sign;
                [WXApi sendReq:req];
            }
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)paySuccessed:(NSNotification *)note
{
    NSInteger success = [[note object] integerValue];
    if (success == 1) {
        CBEApplySuccessController *successController = [[CBEApplySuccessController alloc] initWithNibName:@"CBEApplySuccessController" bundle:nil];
        successController.orderId = self.orderModel.orderId;
        successController.activityId = self.detailModel.activityId;
        [self.navigationController pushViewController:successController animated:YES];
    }else{
        [CommonUtils showHUDWithMessage:@"支付失败" autoHide:YES];
    }
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CBEApplyCell *cell = [CBEApplyCell cellForTableView:tableView];
        [cell.applyInfoLabel setText:StringFormat(@"%@(%@)",self.detailModel.applyName?:@"暂无",self.detailModel.applyPhone?:@"暂无")];
        return cell;
    }else if (indexPath.section == 1) {
        CBEOrderDetailCell *cell = [CBEOrderDetailCell cellForTableView:tableView];
        [cell refreshDataWith:self.detailModel];
        return cell;
    }else if (indexPath.section == 2) {
        CBEETicketCell *cell = [CBEETicketCell cellForTableView:tableView];
        cell.seeBtn.hidden = YES;
//        NSArray *ticketArray = [self filtterTicketData];//self.detailModel.applyTicketList;
        NSArray *subTicketArray = [self.cateTicketArray objectAtIndex:indexPath.row];
        [cell refreshDataWith:subTicketArray];
        
        return cell;
    }
    CBEPayTypeCell *cell = [CBEPayTypeCell cellForTableView:tableView];
    if (indexPath.row == 0) {
        cell.isWeixinPay = YES;
        if (self.isWeiXinPay == YES) {
            cell.selectBtn.selected = YES;
        }else{
            cell.selectBtn.selected = NO;
        }
    }else{
        cell.isWeixinPay = NO;
        if (self.isWeiXinPay == YES) {
            cell.selectBtn.selected = NO;
        }else{
            cell.selectBtn.selected = YES;
        }
    }
    kWeakSelf
    cell.block = ^(BOOL isWeixinPay) {
        weakSelf.isWeiXinPay = isWeixinPay;
        NSLog(@"=============:%d",isWeixinPay);
        
        if (indexPath.row == 0) {
            CBEPayTypeCell *unSelectCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:3]];
            unSelectCell.selectBtn.selected = NO;
        }else{
            CBEPayTypeCell *unSelectCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
            unSelectCell.selectBtn.selected = NO;
        }
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CBEApplyerInfoController *applyController = [[CBEApplyerInfoController alloc] init];
        applyController.applyName = self.detailModel.applyName;
        applyController.applyPhone = self.detailModel.applyPhone;
        [self.navigationController pushViewController:applyController animated:YES];
    }
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {//电子票
        NSArray *ticketArray = [self filtterTicketData];//self.detailModel.applyTicketList;
        return self.cateTicketArray.count;
    }
    if (section == 3) {//支付方式
        return 2;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44.f;
    }else if (indexPath.section == 1) {
        return 145.f;
    }else if (indexPath.section == 2) {
        return 120.f;
    }
    return 50.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return CGFLOAT_MIN;
    }
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

#pragma mark - private
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (CBEOrderBottomCell *)bottomView
{
    if (!_bottomView) {
        _bottomView = [CBEOrderBottomCell createBottomView];
        _bottomView.frame = CGRectMake(0, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight, kSCREEN_WIDTH, kBottomHeight);
        _bottomView.orderType = OrderActionTypePay;
        _bottomView.backgroundColor = [UIColor whiteColor];
        
        kWeakSelf
        _bottomView.actionBlock = ^(OrderActionType type) {
            //调起支付 支付完成跳转报名成功
            
            [weakSelf createOrder];
            
//            CBEApplySuccessController *successController = [[CBEApplySuccessController alloc] initWithNibName:@"CBEApplySuccessController" bundle:nil];
//            [weakSelf.navigationController pushViewController:successController animated:YES];
        };
    }
    return _bottomView;
}

- (NSArray *)filtterTicketData {
    NSArray *array1 = self.detailModel.applyTicketList;
    NSMutableArray *array = [NSMutableArray arrayWithArray:array1];
    
    NSMutableArray *dataMutablearray = [@[] mutableCopy];
    for (int i = 0; i < array.count; i ++) {
        
        NSDictionary *body = array[i];
        long ticketId = [[body objectForKey:@"ticketId"] longValue];
        NSMutableArray *tempArray = [@[] mutableCopy];
        
        [tempArray addObject:body];
        
        for (int j = i+1; j < array.count; j ++) {
            
            NSDictionary *jBody = array[j];
            long jTicketId = [[jBody objectForKey:@"ticketId"] longValue];
            if(ticketId == jTicketId){
                
                [tempArray addObject:jBody];
                
                [array removeObjectAtIndex:j];
                j -= 1;
            }
        }
        [dataMutablearray addObject:tempArray];
    }
    return dataMutablearray;
}

@end

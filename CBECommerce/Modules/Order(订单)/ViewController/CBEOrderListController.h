//
//  CBEOrderListController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, OrderPageType) {
    OrderPageTypeWaitJoin = 0,//待参与
    OrderPageTypeComplete,    //已完成
    OrderPageTypeRefund,      //退款
    OrderPageTypeWaitPay,     //待付款
    OrderPageTypeAll
};
@interface CBEOrderListController : CBEBaseController

@property (nonatomic, assign) OrderPageType pageType;

@end

NS_ASSUME_NONNULL_END

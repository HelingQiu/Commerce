//
//  CBENoOrderView.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBENoOrderView : UIView

/**
 初始化，默认是全屏大小，图标在上，文字在下，整个居中
 
 @return 无数据视图
 */
- (instancetype)initWithDefaultView;

/**
 无数据图标名称，默认是默认图片的名字
 */
@property (nonatomic,strong) NSString *imageName;

/**
 无数据提示文字，默认是“无数据”
 */
@property (nonatomic,strong) NSString *title;

@end

NS_ASSUME_NONNULL_END

//
//  CBENoOrderView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBENoOrderView.h"

// 提示文字的高度
static CGFloat KTitleLabelHeight = 44.f;

@interface CBENoOrderView()

@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) UILabel *titleLabel;

@end

@implementation CBENoOrderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubViews];
    }
    return self;
}

- (instancetype)initWithDefaultView {
    self = [super initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
    if (self) {
        [self addSubViews];
    }
    return self;
}

- (void)addSubViews {
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.imageView];
    CGFloat imageHeight = self.imageView.image.size.height;
    CGFloat offsetY = (imageHeight + KTitleLabelHeight)*0.5 - imageHeight;
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerX.equalTo(self);
         make.centerY.equalTo(self).mas_offset(offsetY);
     }];
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerX.equalTo(self);
         make.height.mas_equalTo(KTitleLabelHeight);
         make.top.equalTo(self.imageView.mas_bottom).offset(-35);
     }];
}


- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        
        UIImage *image = [UIImage imageNamed:@"img_noorder"];
        CGFloat imageViewOriginY = (self.frame.size.height-image.size.height-KTitleLabelHeight)/2.0;
        CGFloat imageViewOriginX = (self.frame.size.width-image.size.width)/2.0;
        
        _imageView.frame = CGRectMake(imageViewOriginX, imageViewOriginY, image.size.width, image.size.height);
        _imageView.image = image;
        
    }
    return _imageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.frame = CGRectMake(0, self.imageView.frame.origin.y+self.imageView.frame.size.height/2.0, self.frame.size.width, KTitleLabelHeight);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = kFont(16);
        _titleLabel.textColor = kMainSubTextColor;
        _titleLabel.text = @"暂无数据";
        
    }
    return _titleLabel;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    // 更换title
    self.titleLabel.text = _title;
}

- (void)setImageName:(NSString *)imageName {
    _imageName = imageName;
    
    // 更换image
    self.imageView.image = [UIImage imageNamed:_imageName];
    
    CGFloat imageHeight = self.imageView.image.size.height;
    CGFloat offsetY = (self.frame.size.height - imageHeight - KTitleLabelHeight)*0.5;
    [self.imageView mas_updateConstraints:^(MASConstraintMaker *make)
     {
         make.centerY.equalTo(self).mas_offset(offsetY);
     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

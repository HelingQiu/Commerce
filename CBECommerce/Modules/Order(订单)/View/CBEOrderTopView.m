//
//  CBEOrderTopView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderTopView.h"

@implementation CBEOrderTopView
{
    UIView *_moveLine;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    
    
    CGFloat width = kSCREEN_WIDTH/5.0;
    UIButton *firstBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [firstBtn setFrame:CGRectMake(0, 0, width, 50)];
    [firstBtn setTitle:@"全部" forState:UIControlStateNormal];
    [firstBtn setTitleColor:kMainBlackColor forState:UIControlStateSelected];
    [firstBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
    [firstBtn.titleLabel setFont:kFont(15)];
    [firstBtn addTarget:self action:@selector(firstAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:firstBtn];
//    firstBtn.selected = YES;
    
    UIButton *secondBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [secondBtn setFrame:CGRectMake(width, 0, width, 50)];
    [secondBtn setTitle:@"待付款" forState:UIControlStateNormal];
    [secondBtn setTitleColor:kMainBlackColor forState:UIControlStateSelected];
    [secondBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
    [secondBtn.titleLabel setFont:kFont(15)];
    [secondBtn addTarget:self action:@selector(secondAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:secondBtn];
    
    UIButton *thirdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [thirdBtn setFrame:CGRectMake(width*2, 0, width, 50)];
    [thirdBtn setTitle:@"待参与" forState:UIControlStateNormal];
    [thirdBtn setTitleColor:kMainBlackColor forState:UIControlStateSelected];
    [thirdBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
    [thirdBtn.titleLabel setFont:kFont(15)];
    [thirdBtn addTarget:self action:@selector(thirdAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:thirdBtn];
    
    UIButton *forthBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [forthBtn setFrame:CGRectMake(width*3, 0, width, 50)];
    [forthBtn setTitle:@"退款" forState:UIControlStateNormal];
    [forthBtn setTitleColor:kMainBlackColor forState:UIControlStateSelected];
    [forthBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
    [forthBtn.titleLabel setFont:kFont(15)];
    [forthBtn addTarget:self action:@selector(forthAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:forthBtn];
    
    UIButton *fifthBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [fifthBtn setFrame:CGRectMake(width*4, 0, width, 50)];
    [fifthBtn setTitle:@"已完成" forState:UIControlStateNormal];
    [fifthBtn setTitleColor:kMainBlackColor forState:UIControlStateSelected];
    [fifthBtn setTitleColor:kMainBlackColor forState:UIControlStateNormal];
    [fifthBtn.titleLabel setFont:kFont(15)];
    [fifthBtn addTarget:self action:@selector(fifthAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:fifthBtn];
    
    _moveLine = [[UIView alloc] initWithFrame:CGRectMake(width/2 - 12, 39, 24, 3)];
    [_moveLine setBackgroundColor:kMainBlueColor];
    _moveLine.clipsToBounds = YES;
    _moveLine.layer.cornerRadius = 2;
    [self addSubview:_moveLine];
}

- (void)firstAction:(UIButton *)sender
{
    if (self.block) {
        self.block(0);
    }
    [self moveLineAtIndex:0];
}

- (void)secondAction:(UIButton *)sender
{
    if (self.block) {
        self.block(1);
    }
    [self moveLineAtIndex:1];
}

- (void)thirdAction:(UIButton *)sender
{
    [self moveLineAtIndex:2];
    if (self.block) {
        self.block(2);
    }
}

- (void)forthAction:(UIButton *)sender
{
    [self moveLineAtIndex:3];
    if (self.block) {
        self.block(3);
    }
}

- (void)fifthAction:(UIButton *)sender
{
    [self moveLineAtIndex:4];
    if (self.block) {
        self.block(4);
    }
}

- (void)moveLineAtIndex:(NSInteger)index
{
    CGFloat width = kSCREEN_WIDTH/5.0;
    CGFloat orginX = (width/2 - 12) + width * index;
    [UIView animateWithDuration:0.3 animations:^{
        self->_moveLine.mj_x = orginX;
    }];
}

@end

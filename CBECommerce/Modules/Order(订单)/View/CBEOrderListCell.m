//
//  CBEOrderListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderListCell.h"
#import "CBEOrderListModel.h"

@implementation CBEOrderListCell
{
    CBEOrderListModel *_listModel;
}
+ (CBEOrderListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"orderListCell";
    CBEOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEOrderListCell" owner:self options:nil].firstObject;
    }
    return cell;
}
- (IBAction)rightAction:(UIButton *)sender {
    if (self.rightBlock) {
        self.rightBlock();
    }
}
- (IBAction)leftAction:(UIButton *)sender {
}

- (void)refreshDataWith:(CBEOrderListModel *)model {
    _listModel = model;
    if (model.orderType == 1) {
        [_statusLabel setText:@"待付款"];
        self.leftBtn.hidden = YES;
        self.rightBtn.hidden = NO;
        [self.rightBtn setTitle:@"去付款" forState:UIControlStateNormal];
    }else if (model.orderType == 2) {
        [_statusLabel setText:@"待参与"];
        self.leftBtn.hidden = YES;
        self.rightBtn.hidden = NO;
        if (model.totalPrice > 0.0000001) {
            [self.rightBtn setTitle:@"申请退款" forState:UIControlStateNormal];
        }else{
            [self.rightBtn setTitle:@"取消活动" forState:UIControlStateNormal];
        }
    }else if (model.orderType == 3) {
        if (model.refundStatus == 0) {
            [_statusLabel setText:@"退款中"];
            self.leftBtn.hidden = YES;
            self.rightBtn.hidden = YES;
        }else if (model.refundStatus == 1) {
            [_statusLabel setText:@"已退款"];
            self.leftBtn.hidden = YES;
            self.rightBtn.hidden = YES;
        }
    }else if (model.orderType == 4) {
        [_statusLabel setText:@"已完成"];
        self.leftBtn.hidden = YES;
        self.rightBtn.hidden = YES;
    }else{
        [_statusLabel setText:@"已取消"];
        self.leftBtn.hidden = YES;
        self.rightBtn.hidden = YES;
    }
    if (model.orderType != 1 && model.orderType != 2) {
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.totalLabel.mas_bottom).offset(10);
            make.left.right.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(0);
        }];
        [_rightBtn updateConstraints];
    }else{
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.totalLabel.mas_bottom).offset(10);
            make.left.right.equalTo(self.contentView);
            make.height.mas_equalTo(1);
            make.bottom.equalTo(self.rightBtn.mas_top).offset(-4);
        }];
        [_rightBtn updateConstraints];
    }
    [self.titLabel setText:StringFormat(@"发布机构：%@",model.sponsorName?:@"")];
    [self.headView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"news_default")];
    [self.rightLabel setText:model.title];
    __block NSString *timeString = @"";
    NSArray *timeArray = model.timeList;
    [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *start = [obj objectForKey:@"startTime"];
        NSString *end = [obj objectForKey:@"endTime"];
        timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
        if (idx == timeArray.count - 1) {
            [self->_timeLabel setText:StringFormat(@"%@",timeString)];
        }
    }];
    [self.timeLabel setText:timeString];
    
    [self.addLabel setText:StringFormat(@"地址：%@",model.address)];
    
    __block NSString *priceString = @"价格：¥";
    NSArray *ticketArray = model.applyTicketList;
    [ticketArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *price = StringFormat(@"%@",[obj objectForKey:@"price"]);
        NSString *count = StringFormat(@"%@",[obj objectForKey:@"count"]);
        priceString = StringFormat(@"%@%@*%@ ",priceString,price,count);
        if (idx == ticketArray.count - 1) {
            [self->_priceLabel setText:StringFormat(@"%@",priceString)];
        }
    }];
    
    [self.totalLabel setText:StringFormat(@"合计：%.2f",model.totalPrice)];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEOrderInfoCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderInfoCell.h"
#import "CBEOrderDetailModel.h"

@implementation CBEOrderInfoCell

+ (CBEOrderInfoCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"orderInfoCell";
    CBEOrderInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEOrderInfoCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEOrderDetailModel *)model
{
    [self.orderCodeLabel setText:StringFormat(@"订单编号：%@",model.orderCode?:@"")];
    [self.createTimeLabel setText:StringFormat(@"创建时间：%@",model.createTime?:@"")];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

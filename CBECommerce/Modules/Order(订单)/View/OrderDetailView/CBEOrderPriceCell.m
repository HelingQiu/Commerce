//
//  CBEOrderPriceCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderPriceCell.h"
#import "CBEOrderDetailModel.h"

@implementation CBEOrderPriceCell

+ (CBEOrderPriceCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"orderPriceCell";
    CBEOrderPriceCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEOrderPriceCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEOrderDetailModel *)model
{
    [self.totalPrice setText:StringFormat(@"%.2f", model.totalPrice)];
    [self.payPrice setText:StringFormat(@"%.2f", model.paymentPrice)];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEOrderDetailCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEOrderDetailCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

+ (CBEOrderDetailCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

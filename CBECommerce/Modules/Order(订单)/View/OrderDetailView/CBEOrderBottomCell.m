//
//  CBEOrderBottomCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderBottomCell.h"

@implementation CBEOrderBottomCell

+ (CBEOrderBottomCell *)createBottomView
{

    CBEOrderBottomCell *cell = [[NSBundle mainBundle] loadNibNamed:@"CBEOrderBottomCell" owner:self options:nil].firstObject;
    return cell;
}

- (void)setOrderType:(OrderActionType)orderType
{
    if (orderType == OrderActionTypeApply) {
        [self.actionBtn setBackgroundColor:kMainBlueColor];
        [self.actionBtn setTitle:@"下一步" forState:UIControlStateNormal];
    }
}

- (void)refreshPayCount:(CGFloat)money
{
    [self.payCountLabel setText:StringFormat(@"¥%.2f",money)];
}

- (IBAction)btnClick:(UIButton *)sender {
    if (self.actionBlock) {
        self.actionBlock(self.orderType);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEOrderDetailCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEOrderDetailCell.h"
#import "CBEOrderDetailModel.h"

@implementation CBEOrderDetailCell

+ (CBEOrderDetailCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"orderDetailCell";
    CBEOrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEOrderDetailCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEOrderDetailModel *)model
{
    [self.topLabel setText:StringFormat(@"发布机构：%@",model.sponsorName?:@"")];
    [self.headView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"news_default")];
    [self.titleLabel setText:model.title];
    __block NSString *timeString = @"";
    NSArray *timeArray = model.timeList;
    [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *start = [obj objectForKey:@"startTime"];
        NSString *end = [obj objectForKey:@"endTime"];
        timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
        if (idx == timeArray.count - 1) {
            [self->_timeLabel setText:timeString];
        }
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

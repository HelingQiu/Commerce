//
//  CBEDetailBottomCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEDetailBottomCell.h"

@implementation CBEDetailBottomCell

+ (CBEDetailBottomCell *)createBottomView
{
    
    CBEDetailBottomCell *cell = [[NSBundle mainBundle] loadNibNamed:@"CBEDetailBottomCell" owner:self options:nil].firstObject;
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)leftAction:(id)sender {
    if (self.leftBlock) {
        self.leftBlock();
    }
}
- (IBAction)rightAction:(id)sender {
    if (self.rightBlock) {
        self.rightBlock();
    }
}

@end

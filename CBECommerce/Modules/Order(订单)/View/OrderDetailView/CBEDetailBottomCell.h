//
//  CBEDetailBottomCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^LeftBlock)(void);
typedef void(^RightBlock)(void);
@interface CBEDetailBottomCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

@property (copy, nonatomic) LeftBlock leftBlock;
@property (copy, nonatomic) RightBlock rightBlock;

+ (CBEDetailBottomCell *)createBottomView;

@end

NS_ASSUME_NONNULL_END

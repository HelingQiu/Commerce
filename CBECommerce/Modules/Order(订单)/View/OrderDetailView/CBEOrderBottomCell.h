//
//  CBEOrderBottomCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, OrderActionType) {
    OrderActionTypeApply = 0,//立即报名按钮
    OrderActionTypePay,      //立即支付按钮
};

typedef void(^OrderActionBlock)(OrderActionType type);

@interface CBEOrderBottomCell : CBEBaseTableViewCell

@property (nonatomic, assign) OrderActionType orderType;
@property (nonatomic, copy) OrderActionBlock actionBlock;
@property (weak, nonatomic) IBOutlet UILabel *payCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;

+ (CBEOrderBottomCell *)createBottomView;

- (void)refreshPayCount:(CGFloat)money;

@end

NS_ASSUME_NONNULL_END

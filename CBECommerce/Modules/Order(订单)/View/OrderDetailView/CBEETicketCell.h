//
//  CBEETicketCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"
#import "CBEOrderDetailTicketModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^TicketActionBlock)(void);
@interface CBEETicketCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *seeBtn;

@property (copy, nonatomic) TicketActionBlock ticketBlock;

+ (CBEETicketCell *)cellForTableView:(UITableView *)tableView;
- (void)refreshModelDataWith:(CBEOrderDetailTicketModel *)model;

@end

NS_ASSUME_NONNULL_END

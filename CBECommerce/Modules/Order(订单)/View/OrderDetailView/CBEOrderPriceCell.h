//
//  CBEOrderPriceCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEOrderPriceCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *totalPrice;
@property (weak, nonatomic) IBOutlet UILabel *payPrice;

+ (CBEOrderPriceCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

//
//  CBEETicketCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEETicketCell.h"
#import "CBEOrderDetailTicketModel.h"

@implementation CBEETicketCell

+ (CBEETicketCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEETicketCell";
    CBEETicketCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEETicketCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (IBAction)seeAction:(UIButton *)sender {
    if (self.ticketBlock) {
        self.ticketBlock();
    }
}

- (void)refreshDataWith:(NSArray *)subTicketArray
{
    CBEOrderDetailTicketModel *model = [CBEOrderDetailTicketModel mj_objectWithKeyValues:[subTicketArray objectAtIndex:0]];
    [self.priceLabel setText:StringFormat(@"%ld * %.2f",subTicketArray.count,model.price)];
    [self.nameLabel setText:model.name];
    if (model.ticketStatus == 0) {
        [self.statusLabel setText:@"待付款"];
    }else if (model.ticketStatus == 1) {
        [self.statusLabel setText:@"正常"];
    }else if (model.ticketStatus == 2) {
        [self.statusLabel setText:@"已用"];
    }else{
        [self.statusLabel setText:@"已取消"];
    }
}
- (void)refreshModelDataWith:(CBEOrderDetailTicketModel *)model {
    [self.priceLabel setText:StringFormat(@"1 * %.2f",model.price)];
    [self.nameLabel setText:model.name];
    if (model.ticketStatus == 0) {
        [self.statusLabel setText:@"待付款"];
    }else if (model.ticketStatus == 1) {
        [self.statusLabel setText:@"正常"];
    }else if (model.ticketStatus == 2) {
        [self.statusLabel setText:@"已用"];
    }else{
        [self.statusLabel setText:@"已取消"];
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

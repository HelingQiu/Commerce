//
//  CBEPayTypeCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEPayTypeCell.h"

@implementation CBEPayTypeCell

+ (CBEPayTypeCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"payTypeCell";
    CBEPayTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEPayTypeCell" owner:self options:nil].firstObject;
        [cell.selectBtn setTitle:nil forState:UIControlStateSelected];
    }
    return cell;
}

- (IBAction)btnAction:(UIButton *)sender {
    sender.selected = YES;
    if (self.block) {
        self.block(_isWeixinPay);
    }
}

- (void)setIsWeixinPay:(BOOL)isWeixinPay
{
    _isWeixinPay = isWeixinPay;
    if (isWeixinPay) {
        [self.payImageView setImage:IMAGE_NAMED(@"pay_weixin")];
        [self.payNameLabel setText:@"微信"];
    }else{
        [self.payImageView setImage:IMAGE_NAMED(@"pay_alipay")];
        [self.payNameLabel setText:@"支付宝"];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

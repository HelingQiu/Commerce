//
//  CBEPayTypeCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^SelectActionBlock)(BOOL isWeixinPay);
@interface CBEPayTypeCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *payImageView;
@property (weak, nonatomic) IBOutlet UILabel *payNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@property (nonatomic, assign) BOOL isWeixinPay;
@property (nonatomic, copy) SelectActionBlock block;

+ (CBEPayTypeCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

//
//  CBEOrderInfoCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEOrderInfoCell : CBEBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel;

+ (CBEOrderInfoCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

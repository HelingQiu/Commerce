//
//  CBEOrderTopView.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^BtnActionBlock)(NSInteger index);
@interface CBEOrderTopView : UIView

@property (nonatomic, copy) BtnActionBlock block;
- (void)moveLineAtIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END

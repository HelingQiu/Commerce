//
//  CBEPositionController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEPositionController.h"
#import "CBEPwTextField.h"
#import <IQKeyboardManager.h>
#import "CBEEditSubmitModel.h"
#import "CBELeftRightLabelCell.h"
#import "CBEConnectVM.h"

@interface CBEPositionController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBEPwTextField *textField;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation CBEPositionController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [IQKeyboardManager sharedManager].enable = NO;
    [self setBarTitle:@"职位名称"];
    [self setBarButton:BarButtonItemRight action:@selector(doneAction) title:@"完成"];
    [self setupUI];
}
- (void)doneAction {
    if ([CommonUtils isBlankString:self.textField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入职位名称" autoHide:YES];
        return;
    }
    [self.view endEditing:YES];
    [CBEEditSubmitModel sharedCBEEditSubmitModel].position = self.textField.text;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupUI {
    
    self.textField = [[CBEPwTextField alloc] initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 44)];
    self.textField.placeholder = @"请输入职位名称";
    self.textField.text = [CBEEditSubmitModel sharedCBEEditSubmitModel].position;
    self.textField.font = kFont(15);
    self.textField.textColor = kMainBlackColor;
    self.textField.clipsToBounds = YES;
    self.textField.layer.borderWidth = 0.5;
    self.textField.layer.borderColor = kMainLineColor.CGColor;
    self.textField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.textField];
    [self.textField addTarget:self action:@selector(textFieldValueChangedAction:) forControlEvents:UIControlEventEditingChanged];
    [self.textField becomeFirstResponder];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 54, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - 54) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = kMainDefaltGrayColor;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
}
- (void)textFieldValueChangedAction:(UITextField *)field {
    NSString *keyword = field.text;
    [self getPositionDataWithKeyword:keyword];
}
- (void)getPositionDataWithKeyword:(NSString *)keyword {
    [CBEConnectVM getIndustryWith:@"2" keyword:keyword complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            self.dataSource = [data objectForKey:@"data"];
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
    }];
    
}
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CBELeftRightLabelCell *cell = [CBELeftRightLabelCell cellForTableView:tableView];
    cell.rightLabel.hidden = YES;
    cell.arrowView.hidden = YES;
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.row];
    NSMutableAttributedString *attriStr = [[NSMutableAttributedString alloc] initWithString:[body objectForKey:@"name"]];
    NSRange range = [[body objectForKey:@"name"] rangeOfString:self.textField.text];
    [attriStr addAttribute:NSForegroundColorAttributeName value:kMainBlueColor range:range];
    cell.titleLabel.attributedText = attriStr;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.row];
    self.textField.text = [body objectForKey:@"name"];
    [self getPositionDataWithKeyword:[body objectForKey:@"name"]];
}
@end

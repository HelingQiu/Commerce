//
//  CBEIndustryController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEIndustryController.h"
#import "CBEConnectVM.h"
#import "CBEIndustryCell.h"
#import "CBEEditSubmitModel.h"

@interface CBEIndustryController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSIndexPath *selectIndex;

@end

@implementation CBEIndustryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.view addSubview:self.tableView];
    self.selectIndex = [NSIndexPath indexPathForRow:0 inSection:0];
    if (self.pageType == IndustryPageType_Industry) {
        [self setBarTitle:@"行业"];
        [self requestDataIndustry];
    }else if (self.pageType == IndustryPageType_Direct) {
        [self setBarTitle:@"行业方向"];
        [self requestDataDirect];
    }
    
}
- (void)backButtonAction {
    NSDictionary *body = [self.dataSource objectAtIndex:self.selectIndex.section];
    if (self.pageType == IndustryPageType_Industry) {
        [CBEEditSubmitModel sharedCBEEditSubmitModel].industry = [body objectForKey:@"name"];
    }else if (self.pageType == IndustryPageType_Direct) {
        [CBEEditSubmitModel sharedCBEEditSubmitModel].industryDirection = [body objectForKey:@"name"];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)requestDataIndustry {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEConnectVM getIndustryWith:@"0" keyword:nil complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.dataSource = [data objectForKey:@"data"];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
- (void)requestDataDirect {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEConnectVM getIndustryWith:@"1" keyword:nil complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.dataSource = [data objectForKey:@"data"];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
#pragma mark --
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10.f)];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CBEIndustryCell *cell = [CBEIndustryCell cellForTableView:tableView];
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.section];
    cell.leftLabel.text = [body objectForKey:@"name"];
    if (indexPath == self.selectIndex) {
        cell.selectView.hidden = NO;
    }else{
        cell.selectView.hidden = YES;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //当前cell row
    NSInteger newRow = [indexPath section];
    //记录上一次cell row
    NSInteger oldRow = (self.selectIndex != nil) ? [self.selectIndex section] : -1;
    if (newRow != oldRow) {
        //选中cell
        CBEIndustryCell *newcell = [tableView cellForRowAtIndexPath:indexPath];
        newcell.selectView.hidden = NO;
        //取消上一次选中cell
        CBEIndustryCell *oldCell = [tableView cellForRowAtIndexPath:self.selectIndex];
        oldCell.selectView.hidden = YES;
    }
    self.selectIndex = indexPath;
}
#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = kMainDefaltGrayColor;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

@end

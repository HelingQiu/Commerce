//
//  CBEMobileController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMobileController.h"
#import "CBEEditSubmitModel.h"
#import "CBEPwTextField.h"
#import <IQKeyboardManager.h>

@interface CBEMobileController ()
@property (nonatomic, strong) CBEPwTextField *textField;
@end

@implementation CBEMobileController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [IQKeyboardManager sharedManager].enable = NO;
    [self setBarTitle:@"联系方式"];
    [self setBarButton:BarButtonItemRight action:@selector(doneAction) title:@"完成"];
    [self setupUI];
}
- (void)doneAction {
    if (![CommonUtils isMobileNumber:self.textField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入正确的联系方式" autoHide:YES];
        return;
    }
    [self.view endEditing:YES];
    [CBEEditSubmitModel sharedCBEEditSubmitModel].mobile = self.textField.text;
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isMobileHidden = NO;
    }else{
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isMobileHidden = YES;
    }
}
- (void)setupUI {
    
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 150, 44)];
    tipLabel.font = kFont(13);
    tipLabel.textColor = kMainTextColor;
    tipLabel.text = @"联系方式对他人是否可见";
    [self.view addSubview:tipLabel];
    
    UIButton *seeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    seeBtn.frame = CGRectMake(kSCREEN_WIDTH - kPadding15 - 18, 13, 18, 18);
    [seeBtn setImage:IMAGE_NAMED(@"cv_icon_unlook") forState:UIControlStateNormal];
    [seeBtn setImage:IMAGE_NAMED(@"cv_icon_look") forState:UIControlStateSelected];
    [seeBtn addTarget:self action:@selector(setAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:seeBtn];
    
    self.textField = [[CBEPwTextField alloc] initWithFrame:CGRectMake(0, 44, kSCREEN_WIDTH, 44)];
    self.textField.placeholder = @"请输入联系方式";
    self.textField.font = kFont(15);
    self.textField.textColor = kMainBlackColor;
    self.textField.clipsToBounds = YES;
    self.textField.layer.borderWidth = 0.5;
    self.textField.layer.borderColor = kMainLineColor.CGColor;
    self.textField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.textField];
    [self.textField becomeFirstResponder];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

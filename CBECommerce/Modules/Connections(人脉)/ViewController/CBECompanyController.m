//
//  CBECompanyController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBECompanyController.h"
#import "CBEEditSubmitModel.h"
#import "CBEPwTextField.h"
#import <IQKeyboardManager.h>
#import "CBEPositionController.h"

@interface CBECompanyController ()

@property (nonatomic, strong) CBEPwTextField *textField;

@end

@implementation CBECompanyController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [IQKeyboardManager sharedManager].enable = NO;
    [self setBarTitle:@"公司名称"];
    [self setBarButton:BarButtonItemRight action:@selector(nextStep) title:@"完成"];
    [self setupUI];
}
- (void)nextStep {
    if ([CommonUtils isBlankString:self.textField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入公司名" autoHide:YES];
        return;
    }
    [self.view endEditing:YES];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [CBEEditSubmitModel sharedCBEEditSubmitModel].company = self.textField.text;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isCompanyHidden = NO;
    }else{
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isCompanyHidden = YES;
    }
}
- (void)setupUI {
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 150, 44)];
    tipLabel.font = kFont(13);
    tipLabel.textColor = kMainTextColor;
    tipLabel.text = @"公司对他人是否可见";
    [self.view addSubview:tipLabel];
    
    UIButton *seeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    seeBtn.frame = CGRectMake(kSCREEN_WIDTH - kPadding15 - 18, 13, 18, 18);
    [seeBtn setImage:IMAGE_NAMED(@"cv_icon_unlook") forState:UIControlStateNormal];
    [seeBtn setImage:IMAGE_NAMED(@"cv_icon_look") forState:UIControlStateSelected];
    [seeBtn addTarget:self action:@selector(setAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:seeBtn];
    seeBtn.selected = [CBEEditSubmitModel sharedCBEEditSubmitModel].isCompanyHidden;
    
    self.textField = [[CBEPwTextField alloc] initWithFrame:CGRectMake(0, 44, kSCREEN_WIDTH, 44)];
    self.textField.placeholder = @"请输入公司名称";
    self.textField.text = [CBEEditSubmitModel sharedCBEEditSubmitModel].company;
    self.textField.font = kFont(15);
    self.textField.textColor = kMainBlackColor;
    self.textField.clipsToBounds = YES;
    self.textField.layer.borderWidth = 0.5;
    self.textField.layer.borderColor = kMainLineColor.CGColor;
    self.textField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.textField];
    [self.textField becomeFirstResponder];
}
@end

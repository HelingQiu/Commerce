//
//  CBECardController.m
//  CBECommerce
//
//  Created by Rainer on 2019/2/20.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBECardController.h"
#import "CBEConnectVM.h"
#import "CBEEditCardHeadCell.h"
#import "CBECardCell.h"
#import <UIButton+WebCache.h>
#import "CBELeftRightLabelCell.h"
#import "CBEMessageVM.h"

@interface CBECardController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSDictionary *resultDict;

@end

@implementation CBECardController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavBar];
    [self.view addSubview:self.tableView];
    [self getUserInfo];
}

- (void)setupNavBar {
    self.view.backgroundColor = kMainDefaltGrayColor;
    [self.navigationItem.titleView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            [obj removeFromSuperview];
        }
    }];
    [self setBarButton:BarButtonItemLeft action:@selector(backAction) imageName:@"login_close"];
    [self setBarTitle:StringFormat(@"%@的名片信息",self.model.userName)];
}
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)getUserInfo {
    NSDictionary *params = @{@"userId":self.model.userId};
    [CBEConnectVM getUserInfoWith:params complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSDictionary *result = [data objectForKey:@"data"];
            self.resultDict = result;
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}
- (void)addFriend {
    [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
    [CBEMessageVM addFriendWithMobile:self.model.userId Complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
    }];
}
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 150;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        CBEEditCardHeadCell *cell = [CBEEditCardHeadCell cellForTableView:tableView];
        [cell.headBtn sd_setImageWithURL:[NSURL URLWithString:[_resultDict objectForKey:@"headPhoto"]] forState:UIControlStateNormal placeholderImage:IMAGE_NAMED(@"cv_icon_user")];
        cell.nameField.text = self.model.userName?:@"暂无";
        cell.inputBtn.hidden = YES;
        
        return cell;
    }
    CBECardCell *cell = [CBECardCell cellForTableView:tableView];
    if (indexPath.row == 1) {
        [cell.leftLab setText:@"真实姓名"];
        if ([[_resultDict objectForKey:@"realNameVisible"] isEqualToString:@"y"]) {
            [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"realName"]?:@"暂无")];
        }else{
            [cell.rightLab setText:@"******"];
        }
    }else if (indexPath.row == 2) {
        [cell.leftLab setText:@"性别"];
        if ([[_resultDict objectForKey:@"sex"] integerValue] == 2) {
            [cell.rightLab setText:@"女"];
        }else{
            [cell.rightLab setText:@"男"];
        }
    }else if (indexPath.row == 3) {
        [cell.leftLab setText:@"公司"];
        if ([[_resultDict objectForKey:@"CompanyOrJobTitleVisible"] isEqualToString:@"y"]) {
            [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"companyName"]?:@"暂无")];
        }else{
            [cell.rightLab setText:@"******"];
        }
    }else if (indexPath.row == 4) {
        [cell.leftLab setText:@"职位"];
        [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"companyJobTitle"]?:@"暂无")];
    }else if (indexPath.row == 5) {
        [cell.leftLab setText:@"毕业学校"];
        [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"schoolName"]?:@"暂无")];
    }else if (indexPath.row == 6) {
        [cell.leftLab setText:@"籍贯"];
        [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"nativePlace"]?:@"暂无")];
    }else if (indexPath.row == 7){
        [cell.leftLab setText:@"所在行业"];
        [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"industry"]?:@"暂无")];
    }else if (indexPath.row == 8){
        [cell.leftLab setText:@"行业方向"];
        [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"industryDirection"]?:@"暂无")];
    }else if (indexPath.row == 9){
        [cell.leftLab setText:@"出生年月日"];
        [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"birthday"]?:@"暂无")];
    }else if (indexPath.row == 10){
        [cell.leftLab setText:@"工作地区"];
        [cell.rightLab setText:StringFormat(@"%@",[_resultDict objectForKey:@"workingLocation"]?:@"暂无")];
    }else if (indexPath.row == 11){
        CBELeftRightLabelCell *cell = [CBELeftRightLabelCell cellForTableView:tableView];
        [cell.titleLabel setText:@"联系方式"];
        if ([[_resultDict objectForKey:@"mobileVisible"] isEqualToString:@"y"]) {
            
            [cell.rightLabel setText:StringFormat(@"%@",[_resultDict objectForKey:@"mobile"]?:@"暂无")];
        }else{
            NSString *mobile = [self numberSuitScanf:[_resultDict objectForKey:@"companyName"]];
            [cell.rightLabel setText:mobile?:@"暂无"];
        }
        return cell;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 11) {
        if ([[_resultDict objectForKey:@"mobileVisible"] isEqualToString:@"y"]) {
            NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",[_resultDict objectForKey:@"mobile"]];
            UIWebView * callWebview = [[UIWebView alloc] init];
            [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
            [self.view addSubview:callWebview];
        }
    }
}

- (NSString *)numberSuitScanf:(NSString *)number {
    //首先验证是不是手机号码
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|7[06-8])\\\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    BOOL isOk = [regextestmobile evaluateWithObject:number];
    if (isOk) {
        //如果是手机号码的话
        NSString *numberString = [number stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
        return numberString;
    }
    return number;
}
#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = kMainDefaltGrayColor;
        _tableView.separatorColor = kMainLineColor;
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 104)];
        view.backgroundColor = kMainDefaltGrayColor;
        
        UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(33, 30, kSCREEN_WIDTH - 66, 44)];
        [addButton setTitle:@"加好友" forState:UIControlStateNormal];
        addButton.titleLabel.font = kFont(16);
        addButton.layer.cornerRadius = 4;
        [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addButton setBackgroundColor:kMainBlueColor];
        [addButton addTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:addButton];
        _tableView.tableFooterView = view;
    }
    return _tableView;
}
@end

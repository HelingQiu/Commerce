//
//  CBEEditCardController.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/27.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEEditCardController.h"
#import "CBELeftRightLabelCell.h"
#import "CBEEditCardHeadCell.h"
#import "CBEEditGenderCell.h"
#import "CBEEditSubmitModel.h"
#import "CBEMineVM.h"
#import <QiniuSDK.h>
#import "CBECompanyController.h"
#import <UIButton+WebCache.h>
#import "CZHAddressPickerView.h"
#import "CBEIndustryController.h"
#import "ChooseDatePickerView.h"
#import "CBEMobileController.h"
#import "CBEConnectVM.h"
#import "CBEPositionController.h"
#import "CBESchoolController.h"
#import "CBERealNameController.h"

@interface CBEEditCardController ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ChooseDatePickerViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSDate *selectDate;

@end

@implementation CBEEditCardController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavBar];
    [self.view addSubview:self.tableView];
    [self getUserInfo];
    [CBEEditSubmitModel sharedCBEEditSubmitModel].sex = 1;
}
- (void)setupNavBar {
    self.view.backgroundColor = kMainDefaltGrayColor;
    [self.navigationItem.titleView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            [obj removeFromSuperview];
        }
    }];
    [self setBarButton:BarButtonItemLeft action:@selector(backAction) imageName:@"login_close"];
    [self setBarTitle:@"编辑名片信息"];
}
- (void)getUserInfo {
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId};
    [CBEConnectVM getUserInfoWith:params complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSDictionary *result = [data objectForKey:@"data"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].sex = [[result objectForKey:@"sex"] integerValue];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].headUrl = [result objectForKey:@"headPhoto"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].nickName = [result objectForKey:@"userName"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].realName = [result objectForKey:@"realName"];
            if ([[result objectForKey:@"realNameVisible"] isEqualToString:@"y"]) {
                [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden = NO;
            }else{
                [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden = YES;
            }
            [CBEEditSubmitModel sharedCBEEditSubmitModel].company = [result objectForKey:@"companyName"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].position = [result objectForKey:@"companyJobTitle"];
            if ([[result objectForKey:@"CompanyOrJobTitleVisible"] isEqualToString:@"y"]) {
                [CBEEditSubmitModel sharedCBEEditSubmitModel].isCompanyHidden = NO;
            }else{
                [CBEEditSubmitModel sharedCBEEditSubmitModel].isCompanyHidden = YES;
            }
            [CBEEditSubmitModel sharedCBEEditSubmitModel].school = [result objectForKey:@"schoolName"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].nativePlace = [result objectForKey:@"nativePlace"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].industry = [result objectForKey:@"industry"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].industryDirection = [result objectForKey:@"industryDirection"];
            
            [CBEEditSubmitModel sharedCBEEditSubmitModel].birthday = [result objectForKey:@"birthday"];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].mobile = [result objectForKey:@"mobile"];
            if ([[result objectForKey:@"mobileVisible"] isEqualToString:@"y"]) {
                [CBEEditSubmitModel sharedCBEEditSubmitModel].isMobileHidden = NO;
            }else{
                [CBEEditSubmitModel sharedCBEEditSubmitModel].isMobileHidden = YES;
            }
            [CBEEditSubmitModel sharedCBEEditSubmitModel].address = [result objectForKey:@"workingLocation"];
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)saveCard {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].headUrl]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].headUrl forKey:@"headPhoto"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].realName]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].realName forKey:@"realName"];
        if ([CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden) {
            [params setObject:@"n" forKey:@"realNameVisible"];
        }else{
            [params setObject:@"y" forKey:@"realNameVisible"];
        }
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].nickName]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].nickName forKey:@"userName"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].mobile]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].mobile forKey:@"mobile"];
        if ([CBEEditSubmitModel sharedCBEEditSubmitModel].isMobileHidden) {
            [params setObject:@"n" forKey:@"mobileVisible"];
        }else{
            [params setObject:@"y" forKey:@"mobileVisible"];
        }
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].birthday]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].birthday forKey:@"birthday"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].industry]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].industry forKey:@"industry"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].industryDirection]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].industryDirection forKey:@"industryDirection"];
    }
    if ([CBEEditSubmitModel sharedCBEEditSubmitModel].sex == 1 || [CBEEditSubmitModel sharedCBEEditSubmitModel].sex == 2) {
        [params setObject:@([CBEEditSubmitModel sharedCBEEditSubmitModel].sex) forKey:@"sex"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].company]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].company forKey:@"companyName"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].nativePlace]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].nativePlace forKey:@"nativePlace"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].position]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].position forKey:@"companyJobTitle"];
        if ([CBEEditSubmitModel sharedCBEEditSubmitModel].isCompanyHidden) {
            [params setObject:@"n" forKey:@"companyOrJobTitleVisible"];
        }else{
            [params setObject:@"y" forKey:@"companyOrJobTitleVisible"];
        }
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].address]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].address forKey:@"workingLocation"];
    }
    if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].schoolId]) {
        [params setObject:[CBEEditSubmitModel sharedCBEEditSubmitModel].schoolId forKey:@"schooldId"];
    }
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEConnectVM submitEditUserInfo:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"保存成功" autoHide:YES];
            [[CBEEditSubmitModel sharedCBEEditSubmitModel] clearData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 150;
    }
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        CBEEditCardHeadCell *cell = [CBEEditCardHeadCell cellForTableView:tableView];
        cell.headBlock = ^{
            [self actionSheetShow];
        };
        if ([CBEEditSubmitModel sharedCBEEditSubmitModel].headImage != nil) {
            [cell.headBtn setImage:[CBEEditSubmitModel sharedCBEEditSubmitModel].headImage forState:UIControlStateNormal];
        }else if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].headUrl]) {
            [cell.headBtn sd_setImageWithURL:[NSURL URLWithString:[CBEEditSubmitModel sharedCBEEditSubmitModel].headUrl] forState:UIControlStateNormal];
        }
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].nickName]) {
            cell.nameField.text = [CBEEditSubmitModel sharedCBEEditSubmitModel].nickName;
        }
        cell.seeBtn.selected = [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden;
        
        return cell;
    }
    CBELeftRightLabelCell *cell = [CBELeftRightLabelCell cellForTableView:tableView];
    if (indexPath.row == 1) {
        [cell.titleLabel setText:@"真实姓名"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].realName]) {
            [cell.rightLabel setText:StringFormat(@"%@",[CBEEditSubmitModel sharedCBEEditSubmitModel].realName)];
        }else{
            [cell.rightLabel setText:@"请输入真实姓名"];
        }
    }else if (indexPath.row == 2) {
        CBEEditGenderCell *cell = [CBEEditGenderCell cellForTableView:tableView];
        if ([CBEEditSubmitModel sharedCBEEditSubmitModel].sex == 1) {
            cell.maleBtn.selected = YES;
            cell.maleBtn.backgroundColor = kMainBlueColor;
            cell.femaleBtn.selected = NO;
            cell.femaleBtn.backgroundColor = [UIColor whiteColor];
        }else{
            cell.maleBtn.selected = NO;
            cell.maleBtn.backgroundColor = [UIColor whiteColor];
            cell.femaleBtn.selected = YES;
            cell.femaleBtn.backgroundColor = kMainBlueColor;
        }
        return cell;
    }else if (indexPath.row == 3) {
        [cell.titleLabel setText:@"公司"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].company]) {
            [cell.rightLabel setText:StringFormat(@"%@",[CBEEditSubmitModel sharedCBEEditSubmitModel].company)];
        }else{
            [cell.rightLabel setText:@"请输入公司名称"];
        }
    }else if (indexPath.row == 4) {
        [cell.titleLabel setText:@"职位"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].position]) {
            [cell.rightLabel setText:StringFormat(@"%@",[CBEEditSubmitModel sharedCBEEditSubmitModel].position)];
        }else{
            [cell.rightLabel setText:@"请输入职位"];
        }
    }else if (indexPath.row == 5) {
        [cell.titleLabel setText:@"毕业学校"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].school]) {
            [cell.rightLabel setText:StringFormat(@"%@",[CBEEditSubmitModel sharedCBEEditSubmitModel].school)];
        }else{
            [cell.rightLabel setText:@"请输入学校名称"];
        }
    }else if (indexPath.row == 6) {
        [cell.titleLabel setText:@"籍贯"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].nativePlace]) {
            [cell.rightLabel setText:[CBEEditSubmitModel sharedCBEEditSubmitModel].nativePlace];
        }else{
            [cell.rightLabel setText:@"请选择籍贯"];
        }
    }else if (indexPath.row == 7){
        [cell.titleLabel setText:@"所在行业"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].industry]) {
            [cell.rightLabel setText:[CBEEditSubmitModel sharedCBEEditSubmitModel].industry];
        }else{
            [cell.rightLabel setText:@"请选择行业"];
        }
    }else if (indexPath.row == 8){
        [cell.titleLabel setText:@"行业方向"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].industryDirection]) {
            [cell.rightLabel setText:[CBEEditSubmitModel sharedCBEEditSubmitModel].industryDirection];
        }else{
            [cell.rightLabel setText:@"请选择行业方向"];
        }
    }else if (indexPath.row == 9){
        [cell.titleLabel setText:@"出生年月日"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].birthday]) {
            [cell.rightLabel setText:[CBEEditSubmitModel sharedCBEEditSubmitModel].birthday];
        }else{
            [cell.rightLabel setText:@"请选择生日时间"];
        }
    }else if (indexPath.row == 10){
        [cell.titleLabel setText:@"工作地区"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].address]) {
            [cell.rightLabel setText:[CBEEditSubmitModel sharedCBEEditSubmitModel].address];
        }else{
            [cell.rightLabel setText:@"请选择工作地区"];
        }
    }else if (indexPath.row == 11){
        [cell.titleLabel setText:@"联系方式"];
        if (![CommonUtils isBlankString:[CBEEditSubmitModel sharedCBEEditSubmitModel].mobile]) {
            [cell.rightLabel setText:[CBEEditSubmitModel sharedCBEEditSubmitModel].mobile];
        }else{
            [cell.rightLabel setText:@"请输入联系方式"];
        }
    }
    
    return cell;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 104;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 104)];
//    view.backgroundColor = kMainDefaltGrayColor;
//
//    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(33, 30, kSCREEN_WIDTH - 66, 44)];
//    [addButton setTitle:@"保存" forState:UIControlStateNormal];
//    addButton.titleLabel.font = kFont(16);
//    addButton.layer.cornerRadius = 4;
//    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [addButton setBackgroundColor:kMainBlueColor];
//    [addButton addTarget:self action:@selector(saveCard) forControlEvents:UIControlEventTouchUpInside];
//    [view addSubview:addButton];
//
//    return view;
//}
// 设置距离左右各15的距离
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, kPadding15, 0, kPadding15)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        //真实姓名
        CBERealNameController *realNameVC = [[CBERealNameController alloc] init];
        [self.navigationController pushViewController:realNameVC animated:YES];
    }else if (indexPath.row == 3) {
        //公司
        CBECompanyController *companyController = [[CBECompanyController alloc] init];
        [self.navigationController pushViewController:companyController animated:YES];
    }else if (indexPath.row == 4) {
        //职位
        CBEPositionController *positionController = [[CBEPositionController alloc] init];
        [self.navigationController pushViewController:positionController animated:YES];
    }else if (indexPath.row == 5) {
        //学校
        CBESchoolController *schoolVC = [[CBESchoolController alloc] init];
        [self.navigationController pushViewController:schoolVC animated:YES];
    }else if (indexPath.row == 6) {
        //籍贯
        [CZHAddressPickerView cityPickerViewWithCityBlock:^(NSString *province, NSString *city) {
            CBELeftRightLabelCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            NSString *nativePlace = StringFormat(@"%@%@",province,city);
            [cell.rightLabel setText:nativePlace];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].nativePlace = nativePlace;
        }];
    }else if (indexPath.row == 7) {
        //行业
        CBEIndustryController *industryController = [[CBEIndustryController alloc] init];
        industryController.pageType = IndustryPageType_Industry;
        [self.navigationController pushViewController:industryController animated:YES];
    }else if (indexPath.row == 8) {
        //行业方向
        CBEIndustryController *industryController = [[CBEIndustryController alloc] init];
        industryController.pageType = IndustryPageType_Direct;
        [self.navigationController pushViewController:industryController animated:YES];
    }else if (indexPath.row == 9) {
        //生日
        [self datePickerShow];
    }else if (indexPath.row == 10) {
        //工作地区
        [CZHAddressPickerView cityPickerViewWithCityBlock:^(NSString *province, NSString *city) {
            CBELeftRightLabelCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            NSString *nativePlace = StringFormat(@"%@%@",province,city);
            [cell.rightLabel setText:nativePlace];
            [CBEEditSubmitModel sharedCBEEditSubmitModel].address = nativePlace;
        }];
    }else if (indexPath.row == 11) {
        //电话号码
        CBEMobileController *mobileController = [[CBEMobileController alloc] init];
        [self.navigationController pushViewController:mobileController animated:YES];
    }
}
#pragma mark - DatePicker
- (void)datePickerShow {
    ChooseDatePickerView *chooseDataPicker = [[ChooseDatePickerView alloc] initWithFrame:self.view.bounds];
    chooseDataPicker.delegate = self;
    if (!self.selectDate) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString:@"2000-01-01"];
        self.selectDate = date;
    }
    chooseDataPicker.data = self.selectDate;
    [chooseDataPicker show];
}
- (void)finishSelectDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    CBELeftRightLabelCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
    [cell.rightLabel setText:[formatter stringFromDate:date]];
    self.selectDate = date;
    [CBEEditSubmitModel sharedCBEEditSubmitModel].birthday = [formatter stringFromDate:date];
}
#pragma mark - 相机相关
- (void)actionSheetShow {
    [UIActionSheet ls_actionSheetWithCallBackBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {// 判断系统是否支持相机
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1001;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相机权限" autoHide:YES];
            }
        }else if (buttonIndex == 1){// 判断系统是否支持相册
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1001;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相册权限" autoHide:YES];
            }
        }
    } showInView:self.view title:@"设置头像" destructiveButtonTitle:@"相机" cancelButtonName:@"取消" otherButtonTitles:@"相册", nil];
}
#pragma mark - 相机相册
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage]; //通过key值获取到图片
    CBEEditCardHeadCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.headBtn setImage:image forState:UIControlStateNormal];
    [CBEEditSubmitModel sharedCBEEditSubmitModel].headImage = image;
    //上传图片到服务器--在这里进行图片上传的网络请求
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM getQiniuTokenComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSString *token = [data objectForKey:@"data"];
            QNUploadManager *upManager = [[QNUploadManager alloc] init];
            NSData *imageData = UIImagePNGRepresentation(image);
            NSString *uuid = [CommonUtils getUUID];
            [upManager putData:imageData key:uuid token:token
                      complete: ^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                          NSLog(@"%@", info);
                          NSLog(@"%@", resp);
                          //成功后调后台接口上传
                          [CommonUtils hideHUD];
                          if (info.statusCode == 200) {
                              [CBEEditSubmitModel sharedCBEEditSubmitModel].headUrl = uuid;
                          }else{
                              [CommonUtils hideHUD];
                          }
                      } option:nil];
        }else{
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

//当用户取消选择的时候，调用该方法
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{}];
}
#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = kMainDefaltGrayColor;
        _tableView.separatorColor = kMainLineColor;
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 104)];
        view.backgroundColor = kMainDefaltGrayColor;
        
        UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(33, 30, kSCREEN_WIDTH - 66, 44)];
        [addButton setTitle:@"保存" forState:UIControlStateNormal];
        addButton.titleLabel.font = kFont(16);
        addButton.layer.cornerRadius = 4;
        [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addButton setBackgroundColor:kMainBlueColor];
        [addButton addTarget:self action:@selector(saveCard) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:addButton];
        _tableView.tableFooterView = view;
    }
    return _tableView;
}

@end

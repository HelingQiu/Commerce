//
//  CBESearchItemController.m
//  CBECommerce
//
//  Created by don on 2018/11/27.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBESearchItemController.h"

@interface CBESearchItemController ()

@end

@implementation CBESearchItemController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavBar];
    [self setupUI];
}
- (void)setupNavBar {
    switch (self.itemType) {
            case SearchItemTypeCompany:
                [self setBarTitle:@"公司"];
            break;
            
        default:
            break;
    }
}
- (void)setupUI {
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 44)];
    [self.view addSubview:textField];
}

@end

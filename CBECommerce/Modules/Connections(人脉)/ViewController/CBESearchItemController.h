//
//  CBESearchItemController.h
//  CBECommerce
//
//  Created by don on 2018/11/27.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, SearchItemType) {//搜索项
    SearchItemTypeCompany = 0,//公司
    SearchItemTypePosition = 1,//职位
    SearchItemTypeSchool = 2,//学校
    SearchItemTypeProfession = 3,//专业
    SearchItemTypeEducation = 4 //学历
};
@interface CBESearchItemController : CBEBaseController

@property (nonatomic, assign) SearchItemType itemType;

@end

NS_ASSUME_NONNULL_END

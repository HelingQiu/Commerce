//
//  CBEMyResumeController.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/25.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMyResumeController.h"
#import "CBEMicroTopView.h"
#import "CBEEditWorkExperienceController.h"
#import "CBEConnectionsCell.h"
#import "CBEEditEducationController.h"

@interface CBEMyResumeController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation CBEMyResumeController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)setupUI {
    CBEMicroTopView *topView = [CBEMicroTopView createView];
    topView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, KNavigationBarHeight + 143);
    [self.view addSubview:topView];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setFrame:CGRectMake(0, kStatusHeight, KItemWidth, 44)];
    UIImage *image = [[UIImage imageNamed:@"arrow_back"] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
    [leftButton setImage:image forState:(UIControlStateNormal)];
    [leftButton addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:leftButton];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(KItemWidth, kStatusHeight, kSCREEN_WIDTH-KItemWidth*2, 44);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"我的微简历";
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:17.f];
    [self.view addSubview:label];
     
    [self.view addSubview:self.tableView];
}

- (void)leftAction {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - tableviewdelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CBEConnectionsCell *cell = [CBEConnectionsCell cellForTableView:tableView];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 44)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, kSCREEN_WIDTH - 2*kPadding15, 44)];
    [label setTextColor:kMainBlackColor];
    [label setFont:kFont(16)];
    if (section == 0) {
        [label setText:@"工作经历"];
    }else{
        [label setText:@"教育经历"];
    }
    [view addSubview:label];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(kPadding15, 43.5, kSCREEN_WIDTH - 2*kPadding15, 0.5)];
    [line setBackgroundColor:kMainLineColor];
    [view addSubview:line];
    
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 54;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 54)];
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 44)];
    if (section == 0) {
        [addButton setTitle:@"添加工作经历" forState:UIControlStateNormal];
    }else{
        [addButton setTitle:@"添加教育经历" forState:UIControlStateNormal];
    }
    addButton.titleLabel.font = kFont(15);
    [addButton setTitleColor:kMainBlueColor forState:UIControlStateNormal];
    [addButton setImage:[UIImage imageNamed:@"cv_icon_plus"] forState:UIControlStateNormal];
    [addButton ls_setImagePosition:LSImagePositionLeft spacing:10];
    [addButton setBackgroundColor:[UIColor whiteColor]];
    [addButton addTarget:self action:@selector(addCard:) forControlEvents:UIControlEventTouchUpInside];
    addButton.tag = section;
    [view addSubview:addButton];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 0.5)];
    [line setBackgroundColor:kMainLineColor];
    [view addSubview:line];
    
    return view;
}
- (void)addCard:(UIButton *)sender {
    NSInteger index = sender.tag;
    if (index == 0) {
        //工作经历
        CBEEditWorkExperienceController *workController = [[CBEEditWorkExperienceController alloc] init];
        [self.navigationController pushViewController:workController animated:YES];
    }else{
        //教育经历
        CBEEditEducationController *educationController = [[CBEEditEducationController alloc] init];
        [self.navigationController pushViewController:educationController animated:YES];
    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, KNavigationBarHeight + 143, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - 143) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}
@end





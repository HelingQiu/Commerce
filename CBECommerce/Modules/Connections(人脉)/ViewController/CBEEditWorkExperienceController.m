//
//  CBEEditWorkExperienceController.m
//  CBECommerce
//
//  Created by don on 2018/11/27.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEEditWorkExperienceController.h"
#import "CBELeftRightLabelCell.h"

@interface CBEEditWorkExperienceController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation CBEEditWorkExperienceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavBar];
    [self.view addSubview:self.tableView];
}
- (void)setupNavBar
{
    [self.navigationItem.titleView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            [obj removeFromSuperview];
        }
    }];
    [self setBarButton:BarButtonItemLeft action:@selector(backAction) imageName:@"login_close"];
    [self setBarTitle:@"编辑工作经历"];
    [self setBarButton:BarButtonItemRight action:@selector(deleteAction) imageName:@"im_icon_delete"];
}
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)deleteAction {
    
}
#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 44;
    }
    return 200;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CBELeftRightLabelCell *cell = [CBELeftRightLabelCell cellForTableView:tableView];
    if (indexPath.section ==0) {
        if (indexPath.row == 0) {
            [cell.titleLabel setText:@"公司"];
        }else if (indexPath.row == 1) {
            [cell.titleLabel setText:@"职位"];
        }else if (indexPath.row == 2) {
            [cell.titleLabel setText:@"开始时间"];
        }else{
            [cell.titleLabel setText:@"结束时间"];
        }
        [cell.rightLabel setText:@"必填项"];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, kSCREEN_WIDTH - 2*kPadding15, 30)];
    [label setTextColor:kMainTextColor];
    [label setFont:kFont(14)];
    if (section == 0) {
        [label setText:@"我的工作经历"];
    }else{
        [label setText:@"经历描述"];
    }
    [view addSubview:label];
    
    return view;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

@end

//
//  CBERealNameController.m
//  CBECommerce
//
//  Created by Rainer on 2019/2/20.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBERealNameController.h"
#import "CBEEditSubmitModel.h"
#import "CBEPwTextField.h"
#import <IQKeyboardManager.h>

@interface CBERealNameController ()

@property (nonatomic, strong) CBEPwTextField *textField;

@end

@implementation CBERealNameController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [IQKeyboardManager sharedManager].enable = NO;
    [self setBarTitle:@"真实姓名"];
    [self setBarButton:BarButtonItemRight action:@selector(nextStep) title:@"完成"];
    [self setupUI];
}
- (void)setupUI {
    
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 150, 44)];
    tipLabel.font = kFont(13);
    tipLabel.textColor = kMainTextColor;
    tipLabel.text = @"真实姓名对他人是否可见";
    [self.view addSubview:tipLabel];
    
    UIButton *seeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    seeBtn.frame = CGRectMake(kSCREEN_WIDTH - kPadding15 - 18, 13, 18, 18);
    [seeBtn setImage:IMAGE_NAMED(@"cv_icon_unlook") forState:UIControlStateNormal];
    [seeBtn setImage:IMAGE_NAMED(@"cv_icon_look") forState:UIControlStateSelected];
    [seeBtn addTarget:self action:@selector(setAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:seeBtn];
    seeBtn.selected = [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden;
    
    self.textField = [[CBEPwTextField alloc] initWithFrame:CGRectMake(0, 44, kSCREEN_WIDTH, 44)];
    self.textField.placeholder = @"请输入真实姓名";
    self.textField.text = [CBEEditSubmitModel sharedCBEEditSubmitModel].realName;
    self.textField.font = kFont(15);
    self.textField.textColor = kMainBlackColor;
    self.textField.clipsToBounds = YES;
    self.textField.layer.borderWidth = 0.5;
    self.textField.layer.borderColor = kMainLineColor.CGColor;
    self.textField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.textField];
    [self.textField becomeFirstResponder];
}
- (void)nextStep {
    if ([CommonUtils isBlankString:self.textField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入真实姓名" autoHide:YES];
        return;
    }
    [self.view endEditing:YES];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [CBEEditSubmitModel sharedCBEEditSubmitModel].realName = self.textField.text;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden = NO;
    }else{
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden = YES;
    }
}
@end

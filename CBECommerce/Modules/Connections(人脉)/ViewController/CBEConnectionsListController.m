//
//  CBEConnectionsListController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEConnectionsListController.h"
#import "CBEMicroInfoCell.h"
#import "CBEConnectionsCell.h"
#import "CBENoOrderView.h"
#import "CBEEditCardController.h"
#import "CBEConnectVM.h"
#import "CBEConnectListModel.h"
#import "CBEMessageVM.h"
#import "CBEEditSubmitModel.h"
#import "CBEFriendsSearchController.h"
#define kTopHeight 168
#import "UIScrollView+EmptyDataSet.h"
#import "CBECardController.h"

typedef NS_ENUM(NSUInteger, kQueryType) {
    kQueryType_First = 1,
    kQueryType_Second = 2,
    kQueryType_Third = 3,
    kQueryType_Forth = 4,
    kQueryType_Fifth = 5
};
@interface CBEConnectionsListController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, assign) NSInteger pageIndex;

@property (nonatomic, strong) CBEMicroInfoCell *topView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UITableView *firstTableView;
@property (nonatomic, strong) CBENoOrderView *firstNoView;
@property (nonatomic, strong) NSMutableArray *firstArray;
@property (nonatomic, assign) NSInteger firstCurrentNum;

@property (nonatomic, strong) UITableView *secondTableView;
@property (nonatomic, strong) CBENoOrderView *secondNoView;
@property (nonatomic, strong) NSMutableArray *secondArray;
@property (nonatomic, assign) NSInteger secondCurrentNum;

@property (nonatomic, strong) UITableView *thirdTableView;
@property (nonatomic, strong) CBENoOrderView *thirdNoView;
@property (nonatomic, strong) NSMutableArray *thirdArray;
@property (nonatomic, assign) NSInteger thirdCurrentNum;

@property (nonatomic, strong) UITableView *forthTableView;
@property (nonatomic, strong) CBENoOrderView *forthNoView;
@property (nonatomic, strong) NSMutableArray *forthArray;
@property (nonatomic, assign) NSInteger forthCurrentNum;

@property (nonatomic, strong) UITableView *fifthTableView;
@property (nonatomic, strong) CBENoOrderView *fifthNoView;
@property (nonatomic, strong) NSMutableArray *fifthArray;
@property (nonatomic, assign) NSInteger fifthCurrentNum;

@end

@implementation CBEConnectionsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBar];
    [self addTopView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[CBEEditSubmitModel sharedCBEEditSubmitModel] clearData];
}
- (void)setNavBar
{
    [self setBarTitle:@"人脉"];
    [self setBarButton:BarButtonItemRight action:@selector(searchAction) imageName:@"home_icon_search"];
    self.backBtn.hidden = YES;
}

- (void)addTopView
{
    _topView = [CBEMicroInfoCell createView];
    _topView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kTopHeight);
    [self.view addSubview:_topView];
    [_topView refreshDataWith:nil];
    kWeakSelf
    _topView.microBlock = ^{
        CBEEditCardController *resumeController = [[CBEEditCardController alloc] init];
        [weakSelf.navigationController pushViewController:resumeController animated:YES];
    };
    _topView.btnBlock = ^(NSInteger index) {
        [weakSelf.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH * index, 0) animated:YES];
        if (index == 0) {
            if (!weakSelf.firstArray.count) {
                [weakSelf.firstTableView.mj_header beginRefreshing];
            }
        }else if (index == 1) {
            if (!weakSelf.secondArray.count) {
                [weakSelf.secondTableView.mj_header beginRefreshing];
            }
        }else if (index == 2) {
            if (!weakSelf.thirdArray.count) {
                [weakSelf.thirdTableView.mj_header beginRefreshing];
            }
        }else if (index == 3) {
            if (!weakSelf.forthArray.count) {
                [weakSelf.forthTableView.mj_header beginRefreshing];
            }
        }else if (index == 4) {
            if (!weakSelf.fifthArray.count) {
                [weakSelf.fifthTableView.mj_header beginRefreshing];
            }
        }
    };
    
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.firstTableView];
    [self.scrollView addSubview:self.secondTableView];
    [self.scrollView addSubview:self.thirdTableView];
    [self.scrollView addSubview:self.forthTableView];
    [self.scrollView addSubview:self.fifthTableView];
    
    [self.firstTableView.mj_header beginRefreshing];
}
- (void)loadUserInfoWith:(NSInteger)pageNum andType:(kQueryType)type {
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"pageSize":@"10",
                             @"pageNum":@(pageNum),
                             @"queryType":@(type)
                             
                             };//@"queryBeginTime":@"2018-01-01",
    //@"queryEndTime":@"2020-01-01"
    [CBEConnectVM queryUserListWith:params complete:^(id data) {
        [self endRefreshWith:type];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSInteger totalNum = [[data objectForKey:@"totalPages"] integerValue];
            if (totalNum <= pageNum) {
                [self endRefreshNoDataWith:type];
            }
            if (pageNum == 1) {
                [self parseDataArrayWith:type withArray:[CBEConnectListModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]]];
            }else{
                [self parseMoreDataWith:type withArray:[CBEConnectListModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]]];
            }
            
            [self tableReloadDataWith:type];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            [self parseShowNoDataView:type];
        }
    } fail:^(NSError *error) {
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
- (void)tableReloadDataWith:(kQueryType)type {
    if (type == kQueryType_First) {
        [self.firstTableView reloadData];
    }else if (type == kQueryType_Second) {
        [self.secondTableView reloadData];
    }else if (type == kQueryType_Third) {
        [self.thirdTableView reloadData];
    }else if (type == kQueryType_Forth) {
        [self.forthTableView reloadData];
    }else if (type == kQueryType_Fifth) {
        [self.fifthTableView reloadData];
    }
}
- (void)parseDataArrayWith:(kQueryType)type withArray:(NSArray *)array {
    if (type == kQueryType_First) {
        self.firstArray = [array mutableCopy];
    }else if (type == kQueryType_Second) {
        self.secondArray = [array mutableCopy];
    }else if (type == kQueryType_Third) {
        self.thirdArray = [array mutableCopy];
    }else if (type == kQueryType_Forth) {
        self.forthArray = [array mutableCopy];
    }else if (type == kQueryType_Fifth) {
        self.fifthArray = [array mutableCopy];
    }
    [self parseShowNoDataView:type];
}
- (void)parseShowNoDataView:(kQueryType)type {
    if (type == kQueryType_First) {
        if (self.firstArray.count) {
            self.firstNoView.hidden = YES;
        }else{
            self.firstNoView.hidden = NO;
        }
    }else if (type == kQueryType_Second) {
        if (self.secondArray.count) {
            self.secondNoView.hidden = YES;
        }else{
            self.secondNoView.hidden = NO;
        }
    }else if (type == kQueryType_Fifth) {
        if (self.thirdArray.count) {
            self.thirdNoView.hidden = YES;
        }else{
            self.thirdNoView.hidden = NO;
        }
    }else if (type == kQueryType_Forth) {
        if (self.forthArray.count) {
            self.forthNoView.hidden = YES;
        }else{
            self.forthNoView.hidden = NO;
        }
    }else if (type == kQueryType_Third) {
        if (self.fifthArray.count) {
            self.fifthNoView.hidden = YES;
        }else{
            self.fifthNoView.hidden = NO;
        }
    }
}
- (void)parseMoreDataWith:(kQueryType)type withArray:(NSArray *)array {
    if (type == kQueryType_First) {
        [self.firstArray addObjectsFromArray:array];
    }else if (type == kQueryType_Second) {
        [self.secondArray addObjectsFromArray:array];
    }else if (type == kQueryType_Fifth) {
        [self.thirdArray addObjectsFromArray:array];
    }else if (type == kQueryType_Forth) {
        [self.forthArray addObjectsFromArray:array];
    }else if (type == kQueryType_Third) {
        [self.fifthArray addObjectsFromArray:array];
    }
}
- (void)endRefreshWith:(kQueryType)type {
    if (type == kQueryType_First) {
        [self.firstTableView.mj_header endRefreshing];
        [self.firstTableView.mj_footer endRefreshing];
    }else if (type == kQueryType_Second) {
        [self.secondTableView.mj_header endRefreshing];
        [self.secondTableView.mj_footer endRefreshing];
    }else if (type == kQueryType_Fifth) {
        [self.thirdTableView.mj_header endRefreshing];
        [self.thirdTableView.mj_footer endRefreshing];
    }else if (type == kQueryType_Forth) {
        [self.forthTableView.mj_header endRefreshing];
        [self.forthTableView.mj_footer endRefreshing];
    }else if (type == kQueryType_Third) {
        [self.fifthTableView.mj_header endRefreshing];
        [self.fifthTableView.mj_footer endRefreshing];
    }
}
- (void)endRefreshNoDataWith:(kQueryType)type {
    if (type == kQueryType_First) {
        [self.firstTableView.mj_footer endRefreshingWithNoMoreData];
    }else if (type == kQueryType_Second) {
        [self.secondTableView.mj_footer endRefreshingWithNoMoreData];
    }else if (type == kQueryType_Fifth) {
        [self.thirdTableView.mj_footer endRefreshingWithNoMoreData];
    }else if (type == kQueryType_Forth) {
        [self.forthTableView.mj_footer endRefreshingWithNoMoreData];
    }else if (type == kQueryType_Third) {
        [self.fifthTableView.mj_footer endRefreshingWithNoMoreData];
    }
}
- (void)searchAction {
    CBEFriendsSearchController *searchController = [[CBEFriendsSearchController alloc] init];
    [self.navigationController pushViewController:searchController animated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.firstTableView) {
        return self.firstArray.count;
    }else if (tableView == self.secondTableView) {
        return self.secondArray.count;
    }else if (tableView == self.thirdTableView) {
        return self.thirdArray.count;
    }else if (tableView == self.forthTableView) {
        return self.forthArray.count;
    }
    return self.fifthArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEConnectionsCell *cell = [CBEConnectionsCell cellForTableView:tableView];
    if (tableView == self.firstTableView) {
        CBEConnectListModel *model = [self.firstArray objectAtIndex:indexPath.row];
        [cell refreshDataWith:model];
        cell.addBlock = ^{
            //点击添加好友触发事件
            [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
            [CBEMessageVM addFriendWithMobile:model.userId Complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                    [self.firstArray removeObject:model];
                    [tableView reloadData];
                }else{
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
            }];
        };
    }else if (tableView == self.secondTableView) {
        CBEConnectListModel *model = [self.secondArray objectAtIndex:indexPath.row];
        [cell refreshDataWith:model];
        cell.addBlock = ^{
            //点击添加好友触发事件
            [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
            [CBEMessageVM addFriendWithMobile:model.userId Complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                    [self.secondArray removeObject:model];
                    [tableView reloadData];
                }else{
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
            }];
        };
    }else if (tableView == self.thirdTableView) {
        CBEConnectListModel *model = [self.thirdArray objectAtIndex:indexPath.row];
        [cell refreshDataWith:model];
        cell.addBlock = ^{
            //点击添加好友触发事件
            [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
            [CBEMessageVM addFriendWithMobile:model.userId Complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                    [self.thirdArray removeObject:model];
                    [tableView reloadData];
                }else{
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
            }];
        };
    }else if (tableView == self.forthTableView) {
        CBEConnectListModel *model = [self.forthArray objectAtIndex:indexPath.row];
        [cell refreshDataWith:model];
        cell.addBlock = ^{
            //点击添加好友触发事件
            [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
            [CBEMessageVM addFriendWithMobile:model.userId Complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                    [self.forthArray removeObject:model];
                    [tableView reloadData];
                }else{
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
            }];
        };
    }else{
        CBEConnectListModel *model = [self.fifthArray objectAtIndex:indexPath.row];
        [cell refreshDataWith:model];
        cell.addBlock = ^{
            //点击添加好友触发事件
            [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
            [CBEMessageVM addFriendWithMobile:model.userId Complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                    [self.forthArray removeObject:model];
                    [tableView reloadData];
                }else{
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
            }];
        };
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CBEConnectListModel *model;
    if (tableView == self.firstTableView) {
        model = [self.firstArray objectAtIndex:indexPath.row];
    }else if (tableView == self.secondTableView) {
        model = [self.secondArray objectAtIndex:indexPath.row];
    }else if (tableView == self.thirdTableView) {
        model = [self.thirdArray objectAtIndex:indexPath.row];
    }else if (tableView == self.forthTableView) {
        model = [self.forthArray objectAtIndex:indexPath.row];
    }else{
        model = [self.fifthArray objectAtIndex:indexPath.row];
    }
    CBECardController *cardVC = [[CBECardController alloc] init];
    cardVC.model = model;
    [self.navigationController pushViewController:cardVC animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView) {
        CGFloat distance = scrollView.contentOffset.x;
        CGFloat width = scrollView.frame.size.width;
        NSInteger index = distance / width;
        [_topView moveToPageIndex:index];
        if (index == 0) {
            if (!self.firstArray.count) {
                [self.firstTableView.mj_header beginRefreshing];
            }
        }else if (index == 1) {
            if (!self.secondArray.count) {
                [self.secondTableView.mj_header beginRefreshing];
            }
        }else if (index == 2) {
            if (!self.thirdArray.count) {
                [self.thirdTableView.mj_header beginRefreshing];
            }
        }else if (index == 3) {
            if (!self.forthArray.count) {
                [self.forthTableView.mj_header beginRefreshing];
            }
        }else if (index == 4) {
            if (!self.fifthArray.count) {
                [self.fifthTableView.mj_header beginRefreshing];
            }
        }
    }
}
#pragma mark - Prvite
- (UITableView *)firstTableView {
    if (!_firstTableView) {
        _firstTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight - kTopHeight) style:UITableViewStyleGrouped];
        _firstTableView.delegate = self;
        _firstTableView.dataSource = self;
        _firstTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _firstTableView.backgroundColor = kMainDefaltGrayColor;
        _firstTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.firstCurrentNum = 1;
            [self loadUserInfoWith:self.firstCurrentNum andType:kQueryType_First];
        }];
        _firstTableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            self.firstCurrentNum ++;
            [self loadUserInfoWith:self.firstCurrentNum andType:kQueryType_First];
        }];
        
        _firstNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _firstTableView.width, _firstTableView.height)];
        _firstNoView.hidden = YES;
        _firstNoView.imageName = @"img_nonews";
        _firstNoView.title = @"暂无信息";
        [_firstTableView addSubview:_firstNoView];
    }
    return _firstTableView;
}
- (NSMutableArray *)firstArray {
    if (!_firstArray) {
        _firstArray = [NSMutableArray array];
    }
    return _firstArray;
}
- (UITableView *)secondTableView {
    if (!_secondTableView) {
        _secondTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight - kTopHeight) style:UITableViewStyleGrouped];
        _secondTableView.delegate = self;
        _secondTableView.dataSource = self;
        _secondTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _secondTableView.backgroundColor = kMainDefaltGrayColor;
        
        _secondNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _secondTableView.width, _secondTableView.height)];
        _secondNoView.hidden = YES;
        _secondNoView.imageName = @"img_nonews";
        _secondNoView.title = @"暂无同行推荐";
        [_secondTableView addSubview:_secondNoView];
        
        _secondTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.secondCurrentNum = 1;
            [self loadUserInfoWith:self.secondCurrentNum andType:kQueryType_Second];
        }];
        _secondTableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            self.secondCurrentNum ++;
            [self loadUserInfoWith:self.secondCurrentNum andType:kQueryType_Second];
        }];
    }
    return _secondTableView;
}
- (NSMutableArray *)secondArray {
    if (!_secondArray) {
        _secondArray = [NSMutableArray array];
    }
    return _secondArray;
}
- (UITableView *)thirdTableView {
    if (!_thirdTableView) {
        _thirdTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH*2, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight - kTopHeight) style:UITableViewStyleGrouped];
        _thirdTableView.delegate = self;
        _thirdTableView.dataSource = self;
        _thirdTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _thirdTableView.backgroundColor = kMainDefaltGrayColor;
        _thirdTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.thirdCurrentNum = 1;
            [self loadUserInfoWith:self.thirdCurrentNum andType:kQueryType_Fifth];
        }];
        _thirdTableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            self.thirdCurrentNum ++;
            [self loadUserInfoWith:self.thirdCurrentNum andType:kQueryType_Fifth];
        }];
        _thirdNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _thirdTableView.width, _thirdTableView.height)];
        _thirdNoView.hidden = YES;
        _thirdNoView.imageName = @"img_nonews";
        _thirdNoView.title = @"暂无校友推荐";
        [_thirdTableView addSubview:_thirdNoView];
    }
    return _thirdTableView;
}
- (NSMutableArray *)thirdArray {
    if (!_thirdArray) {
        _thirdArray = [NSMutableArray array];
    }
    return _thirdArray;
}
- (UITableView *)forthTableView {
    if (!_forthTableView) {
        _forthTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH*3, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight - kTopHeight) style:UITableViewStyleGrouped];
        _forthTableView.delegate = self;
        _forthTableView.dataSource = self;
        _forthTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _forthTableView.backgroundColor = kMainDefaltGrayColor;
        _forthTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.forthCurrentNum = 1;
            [self loadUserInfoWith:self.forthCurrentNum andType:kQueryType_Forth];
        }];
        _forthTableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            self.forthCurrentNum ++;
            [self loadUserInfoWith:self.forthCurrentNum andType:kQueryType_Forth];
        }];
        
        _forthNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _forthTableView.width, _forthTableView.height)];
        _forthNoView.hidden = YES;
        _forthNoView.imageName = @"img_nonews";
        _forthNoView.title = @"暂无老乡推荐";
        [_forthTableView addSubview:_forthNoView];
    }
    return _forthTableView;
}
- (NSMutableArray *)forthArray {
    if (!_forthArray) {
        _forthArray = [NSMutableArray array];
    }
    return _forthArray;
}

- (UITableView *)fifthTableView {
    if (!_fifthTableView) {
        _fifthTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH*4, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight - kTopHeight) style:UITableViewStyleGrouped];
        _fifthTableView.delegate = self;
        _fifthTableView.dataSource = self;
        _fifthTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _fifthTableView.backgroundColor = kMainDefaltGrayColor;
        _fifthTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.fifthCurrentNum = 1;
            [self loadUserInfoWith:self.fifthCurrentNum andType:kQueryType_Third];
        }];
        _fifthTableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            self.fifthCurrentNum ++;
            [self loadUserInfoWith:self.fifthCurrentNum andType:kQueryType_Third];
        }];
        
        _fifthNoView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _fifthTableView.width, _fifthTableView.height)];
        _fifthNoView.hidden = YES;
        _fifthNoView.imageName = @"img_nonews";
        _fifthNoView.title = @"暂无同天生日推荐";
        [_fifthTableView addSubview:_fifthNoView];
    }
    return _fifthTableView;
}
- (NSMutableArray *)fifthArray {
    if (!_fifthArray) {
        _fifthArray = [NSMutableArray array];
    }
    return _fifthArray;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight - kTopHeight)];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH * 5, 0);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
    }
    return _scrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

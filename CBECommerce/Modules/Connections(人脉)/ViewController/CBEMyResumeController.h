//
//  CBEMyResumeController.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/25.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEMyResumeController : CBEBaseController

@end

NS_ASSUME_NONNULL_END

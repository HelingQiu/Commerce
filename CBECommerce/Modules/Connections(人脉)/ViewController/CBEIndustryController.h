//
//  CBEIndustryController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, IndustryPageType) {
    IndustryPageType_Industry = 0,//行业
    IndustryPageType_Direct//行业方向
};
@interface CBEIndustryController : CBEBaseController

@property (nonatomic, assign) IndustryPageType pageType;

@end

NS_ASSUME_NONNULL_END

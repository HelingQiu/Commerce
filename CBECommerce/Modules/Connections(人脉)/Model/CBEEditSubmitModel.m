//
//  CBEEditSubmitModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEEditSubmitModel.h"

@implementation CBEEditSubmitModel

singleton_implementation(CBEEditSubmitModel)

- (void)clearData {
    self.headUrl = nil;
    self.headImage = nil;
    self.nickName = nil;
    self.sex = 0;
    self.company = nil;
    self.position = nil;
    self.nativePlace = nil;
    self.industry = nil;
    self.industryDirection = nil;
    self.birthday = nil;
    self.address = nil;
    self.mobile = nil;
}
@end

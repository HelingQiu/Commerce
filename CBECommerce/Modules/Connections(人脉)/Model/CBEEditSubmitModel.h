//
//  CBEEditSubmitModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBEEditSubmitModel : NSObject

singleton_interface(CBEEditSubmitModel);
@property (nonatomic, copy) NSString *headUrl;//头像url
@property (nonatomic, strong) UIImage *headImage;//头像
@property (nonatomic, copy) NSString *nickName;//昵称
@property (nonatomic, copy) NSString *realName;//真实姓名
@property (nonatomic, assign) BOOL isNameHidden;//名字是否可见
@property (nonatomic, assign) NSInteger sex;//性别 1=男 2=女
@property (nonatomic, copy) NSString *company;//公司名
@property (nonatomic, assign) BOOL isCompanyHidden;//公司是否可见
@property (nonatomic, copy) NSString *position;//职位
@property (nonatomic, copy) NSString *school;//毕业学校
@property (nonatomic, copy) NSString *schoolId;//学校id
@property (nonatomic, copy) NSString *nativePlace;//籍贯
@property (nonatomic, copy) NSString *industry;//行业
@property (nonatomic, copy) NSString *industryDirection;//行业方向
@property (nonatomic, copy) NSString *birthday;//生日
@property (nonatomic, copy) NSString *address;//地区
@property (nonatomic, assign) BOOL isMobileHidden;//联系方式是否可见
@property (nonatomic, copy) NSString *mobile;//联系方式

- (void)clearData;

@end

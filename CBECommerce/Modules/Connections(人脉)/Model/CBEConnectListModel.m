//
//  CBEConnectListModel.m
//  CBECommerce
//
//  Created by don on 2018/11/21.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEConnectListModel.h"

@implementation CBEConnectListModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"uid":@"id"};
}

@end

//
//  CBEConnectListModel.h
//  CBECommerce
//
//  Created by don on 2018/11/21.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEConnectListModel : NSObject

@property (nonatomic, copy) NSString *companyJobTitle;//职位
@property (nonatomic, copy) NSString *companyName;//公司名称
@property (nonatomic, copy) NSString *uid;//
@property (nonatomic, copy) NSString *userMobile;//用户号码
@property (nonatomic, copy) NSString *showTag;//显示标签
@property (nonatomic, copy) NSString *industry;//行业方向
@property (nonatomic, copy) NSString *headPhoto;//用户头像
@property (nonatomic, copy) NSString *userName;//用户名称
@property (nonatomic, assign) NSInteger sex;//性别
@property (nonatomic, copy) NSString *userId;//用户ID

@end

NS_ASSUME_NONNULL_END

//
//  CBECardCell.m
//  CBECommerce
//
//  Created by Rainer on 2019/2/20.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBECardCell.h"

@implementation CBECardCell

+ (CBECardCell *)cellForTableView:(UITableView *)tableView {
    static NSString *identify = @"CBECardCell";
    CBECardCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBECardCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

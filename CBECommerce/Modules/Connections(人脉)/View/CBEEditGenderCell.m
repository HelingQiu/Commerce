//
//  CBEEditGenderCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEEditGenderCell.h"
#import "CBEEditSubmitModel.h"

@implementation CBEEditGenderCell

+ (CBEEditGenderCell *)cellForTableView:(UITableView *)tableView {
    static NSString *identify = @"CBEEditGenderCell";
    CBEEditGenderCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEEditGenderCell" owner:self options:nil].firstObject;
    }
    cell.maleBtn.selected = YES;
    cell.maleBtn.backgroundColor = kMainBlueColor;
    return cell;
}
- (IBAction)maleAction:(UIButton *)sender {
    self.maleBtn.selected = YES;
    self.maleBtn.backgroundColor = kMainBlueColor;
    self.femaleBtn.selected = NO;
    self.femaleBtn.backgroundColor = [UIColor whiteColor];
    [CBEEditSubmitModel sharedCBEEditSubmitModel].sex = 1;
}
- (IBAction)femaleAction:(UIButton *)sender {
    self.maleBtn.selected = NO;
    self.maleBtn.backgroundColor = [UIColor whiteColor];
    self.femaleBtn.selected = YES;
    self.femaleBtn.backgroundColor = kMainBlueColor;
    [CBEEditSubmitModel sharedCBEEditSubmitModel].sex = 2;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

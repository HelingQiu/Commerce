//
//  CBEConnectionsCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"
#import "CBEFriendModel.h"

typedef void(^AddFriendBlock)(void);
@interface CBEConnectionsCell : CBEBaseTableViewCell

@property (nonatomic, copy) AddFriendBlock addBlock;
+ (CBEConnectionsCell *)cellForTableView:(UITableView *)tableView;
- (void)refreshUserDataWith:(CBEFriendModel *)model;

@end

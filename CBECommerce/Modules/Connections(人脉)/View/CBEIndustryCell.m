//
//  CBEIndustryCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEIndustryCell.h"

@implementation CBEIndustryCell

+ (CBEIndustryCell *)cellForTableView:(UITableView *)tableView {
    static NSString *identify = @"CBEIndustryCell";
    CBEIndustryCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEIndustryCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

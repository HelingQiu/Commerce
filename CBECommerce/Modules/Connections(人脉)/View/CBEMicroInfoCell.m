//
//  CBEMicroInfoCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMicroInfoCell.h"

@implementation CBEMicroInfoCell

+ (CBEMicroInfoCell *)createView
{
    CBEMicroInfoCell *cell = [[NSBundle mainBundle] loadNibNamed:@"CBEMicroInfoCell" owner:self options:nil].firstObject;
    return cell;
}

- (IBAction)topAction:(UITapGestureRecognizer *)sender {
    if (self.microBlock) {
        self.microBlock();
    }
}

- (IBAction)editAction:(UIButton *)sender {
    
}
- (IBAction)arrowAction:(UIButton *)sender {
}
- (IBAction)firstAction:(UIButton *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat orginX = (kSCREEN_WIDTH/5.0 - 30)/2;
        self.blueLine.mj_x = orginX;
    }];
    if (self.btnBlock) {
        self.btnBlock(0);
    }
}
- (IBAction)secondAction:(UIButton *)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat orginX = (kSCREEN_WIDTH/5.0 - 30)/2 + kSCREEN_WIDTH/5.0;
        self.blueLine.mj_x = orginX;
    }];
    if (self.btnBlock) {
        self.btnBlock(1);
    }
}
- (IBAction)thirdAction:(UIButton *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat orginX = (kSCREEN_WIDTH/5.0 - 30)/2 + kSCREEN_WIDTH/5.0*2;
        self.blueLine.mj_x = orginX;
    }];
    if (self.btnBlock) {
        self.btnBlock(2);
    }
}
- (IBAction)forthAction:(UIButton *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat orginX = (kSCREEN_WIDTH/5.0 - 30)/2 + kSCREEN_WIDTH/5.0*3;
        self.blueLine.mj_x = orginX;
    }];
    if (self.btnBlock) {
        self.btnBlock(3);
    }
}
- (IBAction)fifthAction:(UIButton *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat orginX = (kSCREEN_WIDTH/5.0 - 30)/2 + kSCREEN_WIDTH/5.0*4;
        self.blueLine.mj_x = orginX;
    }];
    if (self.btnBlock) {
        self.btnBlock(4);
    }
}

- (void)refreshDataWith:(id)model
{
    [_headView sd_setImageWithURL:[NSURL URLWithString:[[CBEUserModel sharedInstance].userInfo.headPhoto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"cv_icon_user")];
    [_nameLabel setText:[CBEUserModel sharedInstance].userInfo.userName?:[CBEUserModel sharedInstance].userInfo.mobile];
    NSString *company = StringFormat(@"%@%@",[CBEUserModel sharedInstance].userInfo.companyName?:@"",[CBEUserModel sharedInstance].userInfo.companyJobTitle?:@"")?:@"暂无";
    [_companyLabel setText:company];
}

- (void)moveToPageIndex:(NSInteger)index
{
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat orginX = (kSCREEN_WIDTH/5.0 - 30)/2 + kSCREEN_WIDTH/5.0*index;
        self.blueLine.mj_x = orginX;
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CGFloat orginX = (kSCREEN_WIDTH/5.0 - 30)/2;
    self.blueLine = [[UIView alloc] initWithFrame:CGRectMake(orginX, 37, 30, 3)];
    self.blueLine.clipsToBounds = YES;
    self.blueLine.layer.cornerRadius = 2.0;
    self.blueLine.backgroundColor = kMainBlueColor;
    [self.bottomView addSubview:self.blueLine];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEEditCardHeadCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/28.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^HeadBlock)(void);
@interface CBEEditCardHeadCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *headBtn;
@property (weak, nonatomic) IBOutlet UIButton *seeBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIButton *inputBtn;
@property (copy, nonatomic) HeadBlock headBlock;
+ (CBEEditCardHeadCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

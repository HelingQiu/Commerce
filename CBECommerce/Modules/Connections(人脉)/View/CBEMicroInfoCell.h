//
//  CBEMicroInfoCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^MicroBlock)(void);
typedef void(^BtnActionBlock)(NSInteger index);
@interface CBEMicroInfoCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *thirdBtn;
@property (weak, nonatomic) IBOutlet UIButton *forthBtn;
@property (weak, nonatomic) IBOutlet UIButton *fifthBtn;

@property (strong, nonatomic) UIView *blueLine;
@property (strong, nonatomic) MicroBlock microBlock;
@property (copy, nonatomic) BtnActionBlock btnBlock;

+ (CBEMicroInfoCell *)createView;
- (void)moveToPageIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END

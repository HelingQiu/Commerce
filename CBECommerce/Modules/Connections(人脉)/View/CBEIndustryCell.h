//
//  CBEIndustryCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEIndustryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectView;

+ (CBEIndustryCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

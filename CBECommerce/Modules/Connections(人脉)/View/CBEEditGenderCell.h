//
//  CBEEditGenderCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEEditGenderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *maleBtn;
@property (weak, nonatomic) IBOutlet UIButton *femaleBtn;
+ (CBEEditGenderCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

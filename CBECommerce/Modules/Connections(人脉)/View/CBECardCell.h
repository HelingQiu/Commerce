//
//  CBECardCell.h
//  CBECommerce
//
//  Created by Rainer on 2019/2/20.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBECardCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLab;
@property (weak, nonatomic) IBOutlet UILabel *rightLab;

+ (CBECardCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

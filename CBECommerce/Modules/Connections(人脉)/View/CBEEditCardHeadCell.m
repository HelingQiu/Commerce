//
//  CBEEditCardHeadCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/28.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEEditCardHeadCell.h"
#import "CBEEditSubmitModel.h"

@implementation CBEEditCardHeadCell

+ (CBEEditCardHeadCell *)cellForTableView:(UITableView *)tableView {
    static NSString *identify = @"CBEEditCardHeadCell";
    CBEEditCardHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEEditCardHeadCell" owner:self options:nil].firstObject;
    }
    return cell;
}
- (IBAction)headAction:(UIButton *)sender {
    if (self.headBlock) {
        self.headBlock();
    }
}
- (IBAction)seeAction:(UIButton *)sender {
    sender.selected = !sender.selected;//选中则对外不隐藏
    if (sender.selected) {
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden = NO;
    }else{
        [CBEEditSubmitModel sharedCBEEditSubmitModel].isNameHidden = YES;
    }
}
- (IBAction)textChange:(UITextField *)sender {
    [CBEEditSubmitModel sharedCBEEditSubmitModel].nickName = sender.text;
}
- (IBAction)inputAction:(UIButton *)sender {
    sender.selected = !sender.selected;//选中则可编辑
    if (sender.selected) {
        self.nameField.enabled = YES;
    }else{
        self.nameField.enabled = NO;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

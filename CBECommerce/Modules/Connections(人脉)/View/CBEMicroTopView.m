//
//  CBEMicroTopView.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/25.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMicroTopView.h"

@implementation CBEMicroTopView

+ (CBEMicroTopView *)createView
{
    CBEMicroTopView *cell = [[NSBundle mainBundle] loadNibNamed:@"CBEMicroTopView" owner:self options:nil].firstObject;
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

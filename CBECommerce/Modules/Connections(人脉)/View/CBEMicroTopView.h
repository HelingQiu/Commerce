//
//  CBEMicroTopView.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/25.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEMicroTopView : UITableViewCell

+ (CBEMicroTopView *)createView;

@end

NS_ASSUME_NONNULL_END

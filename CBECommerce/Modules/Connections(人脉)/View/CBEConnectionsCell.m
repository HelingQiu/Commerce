//
//  CBEConnectionsCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/1.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEConnectionsCell.h"
#import "CBEConnectListModel.h"

@implementation CBEConnectionsCell
{
    UIImageView *_headView;
    UILabel *_nameLabel;
    UILabel *_positionLabel;
    UILabel *_tagLabel;
    UIButton *_addFriendButton;
}
+ (CBEConnectionsCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"connectionsCell";
    CBEConnectionsCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEConnectionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    _headView = [UIImageView new];
    [_headView sd_setImageWithURL:[NSURL URLWithString:@"http://images.liqucn.com/img/h1/h962/img201709131554350_info300X300.jpg"]];
    _headView.clipsToBounds = YES;
    _headView.layer.cornerRadius = 24;
    [self.contentView addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(kPadding15);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.height.mas_equalTo(48);
    }];
    
    _nameLabel = [UILabel new];
    [_nameLabel setFont:kFont(16)];
    [_nameLabel setTextColor:kMainBlackColor];
    [_nameLabel setText:@"迪丽热巴"];
    [self.contentView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_top);
        make.left.equalTo(self->_headView.mas_right).offset(kPadding10);
        make.height.mas_equalTo(18);
    }];
    
    _tagLabel = [UILabel new];
    [_tagLabel setFont:kFont(9)];
    [_tagLabel setTextColor:kMainOrangeColor];
    [_tagLabel setTextAlignment:NSTextAlignmentCenter];
    [_tagLabel setClipsToBounds:YES];
    [_tagLabel.layer setCornerRadius:2];
    [_tagLabel.layer setBorderColor:kMainOrangeColor.CGColor];
    [_tagLabel.layer setBorderWidth:0.5];
    [_tagLabel setBackgroundColor:[UIColor ls_colorWithHex:0xFE9C37 andAlpha:0.12]];
    [_tagLabel setText:@"11个共同好友"];
    [self.contentView addSubview:_tagLabel];
    [_tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_nameLabel.mas_top);
        make.left.equalTo(self->_nameLabel.mas_right).offset(kPadding10);
        make.height.mas_equalTo(kHeightScale(18));
    }];
    
    _addFriendButton = [UIButton new];
    [_addFriendButton setTitle:@"加好友" forState:UIControlStateNormal];
    [_addFriendButton setBackgroundColor:kMainBlueColor];
    [_addFriendButton.titleLabel setFont:kFont(16)];
    [_addFriendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_addFriendButton setClipsToBounds:YES];
    [_addFriendButton.layer setCornerRadius:12];
    [_addFriendButton addTarget:self action:@selector(addFriendAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_addFriendButton];
    [_addFriendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-kPadding15);
        make.centerY.equalTo(self->_headView.mas_centerY);
        make.width.mas_equalTo(72);
        make.height.mas_equalTo(24);
    }];
    
    _positionLabel = [UILabel new];
    [_positionLabel setFont:kFont(12)];
    [_positionLabel setTextColor:kMainTextColor];
    [_positionLabel setText:@"深圳市引导美科技有限公司iOS开发工程师"];
    [self.contentView addSubview:_positionLabel];
    [_positionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self->_headView.mas_bottom).offset(-kPadding10/2.0);
        make.left.equalTo(self->_headView.mas_right).offset(kPadding10);
        make.right.equalTo(self.contentView.mas_right).offset(-kPadding15-kPadding10 - 72);
        make.height.mas_equalTo(14);
    }];
    
}
- (void)refreshDataWith:(CBEConnectListModel *)model {
    [_headView sd_setImageWithURL:[NSURL URLWithString:[model.headPhoto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"cv_icon_user")];
    [_nameLabel setText:model.userName?:model.userMobile];
    CGFloat width = [CommonUtils widthForString:model.showTag Font:kFont(9) andWidth:kSCREEN_WIDTH];
    [_tagLabel setText:model.showTag];
    [_tagLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width + 10);
    }];
    [_positionLabel setText:StringFormat(@"%@%@",model.companyName?:@"",model.companyJobTitle?:@"")];
}
- (void)refreshUserDataWith:(CBEFriendModel *)model {
    [_headView sd_setImageWithURL:[NSURL URLWithString:[model.headPhoto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"cv_icon_user")];
    [_nameLabel setText:model.userName?:model.userMobile];
    [_tagLabel setText:@""];
    [_positionLabel setText:StringFormat(@"%@%@",model.companyName?:@"",model.companyJobTitle?:@"")];
}
- (void)addFriendAction {
    if (self.addBlock) {
        self.addBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

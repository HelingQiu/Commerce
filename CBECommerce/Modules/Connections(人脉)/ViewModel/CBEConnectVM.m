//
//  CBEConnectVM.m
//  CBECommerce
//
//  Created by don on 2018/11/21.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEConnectVM.h"

@implementation CBEConnectVM

+ (void)getIndustryWith:(NSString *)parterId
                keyword:(NSString *)keyword
            complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:parterId forKey:@"parentId"];
    if (![CommonUtils isBlankString:keyword]) {
        [params setObject:keyword forKey:@"keywordQuery"];
    }
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kGetIndustryOrDirectionOrJobTitle parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)submitEditUserInfo:(NSDictionary *)params
                  complete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kEditUserInfo parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)queryUserListWith:(NSDictionary *)params
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryUserList parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//检索学校信息
+ (void)querySchoolWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kSearchSchoolApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//获取名片信息
+ (void)getUserInfoWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed {
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
//    
//    [[CBENetworkManager sharedInstance] getRequestWithUrl:kGetUserInfoApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager GET:StringFormat(@"%@%@",Host,kGetUserInfoApi) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}
@end

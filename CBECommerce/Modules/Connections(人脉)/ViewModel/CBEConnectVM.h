//
//  CBEConnectVM.h
//  CBECommerce
//
//  Created by don on 2018/11/21.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBEConnectVM : NSObject
//人脉首页
+ (void)queryUserListWith:(NSDictionary *)params
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed;
//行业 行业方向 职位  0,1,2
+ (void)getIndustryWith:(NSString *)parterId
                keyword:(NSString *)keyword
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed;
//编辑名片
+ (void)submitEditUserInfo:(NSDictionary *)params
                  complete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed;
//检索学校信息
+ (void)querySchoolWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed;
//获取名片信息
+ (void)getUserInfoWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed;

@end


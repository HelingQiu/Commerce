//
//  CBEMessageVM.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/8.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEMessageVM : NSObject
//创建群聊
+ (void)createGroupWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed;
//我的好友
+ (void)getMyFriendsWith:(NSDictionary *)params
                complete:(CBENetWorkSucceed)successed
                    fail:(CBENetWorkFailure)failed;
//我的群聊
+ (void)getMyGroupsComplete:(CBENetWorkSucceed)successed
                       fail:(CBENetWorkFailure)failed;

//代办列表
+ (void)todoListWith:(NSDictionary *)params
            complete:(CBENetWorkSucceed)successed
                fail:(CBENetWorkFailure)failed;

//推荐群和好友
+ (void)recommendGroupAndFriendsComplete:(CBENetWorkSucceed)successed
                                    fail:(CBENetWorkFailure)failed;

//添加好友
+ (void)addFriendWithMobile:(NSString *)mobile
                   Complete:(CBENetWorkSucceed)successed
                       fail:(CBENetWorkFailure)failed;

//获取群组成员
+ (void)getGroupMemberWithGroupId:(NSString *)groupId
                         Complete:(CBENetWorkSucceed)successed
                             fail:(CBENetWorkFailure)failed;

//修改群名称
+ (void)editGroupNameWith:(NSString *)groupName
              withGroupId:(NSString *)groupId
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed;

//退出或者解散群聊
+ (void)leaveGroupWith:(NSString *)groupId
              complete:(CBENetWorkSucceed)successed
                  fail:(CBENetWorkFailure)failed;

//添加群成员
+ (void)addGroupListWith:(NSDictionary *)params
                complete:(CBENetWorkSucceed)successed
                    fail:(CBENetWorkFailure)failed;

//删除群成员
+ (void)kickGroupMemberWith:(NSDictionary *)params
                   complete:(CBENetWorkSucceed)successed
                       fail:(CBENetWorkFailure)failed;

//同意或者拒绝添加好友请求
+ (void)applyFriendWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed;

//搜索添加好友
+ (void)searchFirendsWith:(NSDictionary *)params
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed;

@end

NS_ASSUME_NONNULL_END

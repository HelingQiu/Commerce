//
//  NTESSessionUtil.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/14.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
// 最近会话本地扩展标记类型
typedef NS_ENUM(NSInteger, NTESRecentSessionMarkType){
    // @ 标记
    NTESRecentSessionMarkTypeAt,
    // 置顶标记
    NTESRecentSessionMarkTypeTop,
};
@interface NTESSessionUtil : NSObject

+ (void)addRecentSessionMark:(NIMSession *)session type:(NTESRecentSessionMarkType)type;

+ (void)removeRecentSessionMark:(NIMSession *)session type:(NTESRecentSessionMarkType)type;

+ (BOOL)recentSessionIsMark:(NIMRecentSession *)recent type:(NTESRecentSessionMarkType)type;

@end

NS_ASSUME_NONNULL_END

//
//  CBESessionConfig.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/22.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NIMKit.h>
#import "NIMSessionConfig.h"
NS_ASSUME_NONNULL_BEGIN

@interface CBESessionConfig : NSObject<NIMSessionConfig>

- (NSArray<NIMMediaItem *> *)mediaItems;
    
@end

NS_ASSUME_NONNULL_END

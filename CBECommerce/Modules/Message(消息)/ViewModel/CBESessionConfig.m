//
//  CBESessionConfig.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/22.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBESessionConfig.h"

@implementation CBESessionConfig

- (NSArray<NIMMediaItem *> *)mediaItems {
    NIMMediaItem *pic =       [NIMMediaItem item:@"onTapMediaItemPicture:"
                                     normalImage:[UIImage imageNamed:@"im_icon_gallery"]
                                   selectedImage:[UIImage imageNamed:@"im_icon_gallery"]
                                           title:@"相册"];
    
    NIMMediaItem *shoot =     [NIMMediaItem item:@"onTapMediaItemShoot:"
                                     normalImage:[UIImage imageNamed:@"im_icon_camera"]
                                   selectedImage:[UIImage imageNamed:@"im_icon_camera"]
                                           title:@"拍摄"];
    
    NIMMediaItem *location =  [NIMMediaItem item:@"onTapMediaItemLocation:"
                                     normalImage:[UIImage imageNamed:@"im_icon_location"]
                                   selectedImage:[UIImage imageNamed:@"im_icon_location"]
                                           title:@"位置"];
    NSArray *items = @[pic,shoot,location];
    return items;
}

@end

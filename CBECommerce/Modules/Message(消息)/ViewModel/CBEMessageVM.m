//
//  CBEMessageVM.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/8.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMessageVM.h"

@implementation CBEMessageVM

//创建群聊
+ (void)createGroupWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kCreateGroup parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//我的好友
+ (void)getMyFriendsWith:(NSDictionary *)params
                complete:(CBENetWorkSucceed)successed
                    fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kMyFriendApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//我的群聊
+ (void)getMyGroupsComplete:(CBENetWorkSucceed)successed
                       fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kMyGroupApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//代办列表
+ (void)todoListWith:(NSDictionary *)params
            complete:(CBENetWorkSucceed)successed
                fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kTodoListApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//推荐群和好友
+ (void)recommendGroupAndFriendsComplete:(CBENetWorkSucceed)successed
                                    fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kLogRecommend parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//添加好友
+ (void)addFriendWithMobile:(NSString *)mobile
                   Complete:(CBENetWorkSucceed)successed
                       fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"friendUserId":mobile,
                             @"msg":@"添加好友",
                             @"userId":[CBEUserModel sharedInstance].userInfo.userId
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kAddFriendApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//获取群组成员
+ (void)getGroupMemberWithGroupId:(NSString *)groupId
                         Complete:(CBENetWorkSucceed)successed
                             fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"groupId":groupId};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kQueryGroupUser parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//修改群名称
+ (void)editGroupNameWith:(NSString *)groupName
              withGroupId:(NSString *)groupId
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"groupId":groupId,
                             @"name":groupName
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kEditGroupName parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//退出或者解散群聊
+ (void)leaveGroupWith:(NSString *)groupId
              complete:(CBENetWorkSucceed)successed
                  fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"groupId":groupId};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kLeaveGroup parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//添加群成员
+ (void)addGroupListWith:(NSDictionary *)params
                complete:(CBENetWorkSucceed)successed
                    fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kAddGroupUserList parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//删除群成员
+ (void)kickGroupMemberWith:(NSDictionary *)params
                   complete:(CBENetWorkSucceed)successed
                       fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kKickGroupMember parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//同意或者拒绝添加好友请求
+ (void)applyFriendWith:(NSDictionary *)params
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kApplyFriendApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//搜索添加好友
+ (void)searchFirendsWith:(NSDictionary *)params
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kSearchFriendsApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end

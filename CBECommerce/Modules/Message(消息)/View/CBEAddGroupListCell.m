//
//  CBEAddGroupListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAddGroupListCell.h"
#import "CBEGroupModel.h"

@implementation CBEAddGroupListCell

+ (CBEAddGroupListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEAddGroupListCell";
    CBEAddGroupListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEAddGroupListCell" owner:self options:nil].firstObject;
    }
    return cell;
}
- (IBAction)addAction:(UIButton *)sender {
}
- (void)refreshDataWith:(CBEGroupModel *)model {
    [self.groupNameLabel setText:model.name];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

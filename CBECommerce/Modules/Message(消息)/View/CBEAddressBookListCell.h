//
//  CBEAddressBookListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/30.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEAddressBookListCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *posComLabel;
+ (CBEAddressBookListCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

//
//  CBEApplyAddCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/13.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^IntergerBlock)(NSInteger index);
@interface CBEApplyAddCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (copy, nonatomic) IntergerBlock block;
+ (CBEApplyAddCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

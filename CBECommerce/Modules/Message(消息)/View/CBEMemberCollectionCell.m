//
//  CBEMemberCollectionCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/16.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMemberCollectionCell.h"

@implementation CBEMemberCollectionCell
{
    UIImageView *_headView;
    UILabel *_nameLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}
- (void)setupUI
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.clipsToBounds = YES;
    self.contentView.layer.cornerRadius = 2;
    
    _headView = [UIImageView new];
    [_headView setClipsToBounds:YES];
    [_headView.layer setCornerRadius:25];
    [self.contentView addSubview:_headView];
    
    _nameLabel = [UILabel new];
    [_nameLabel setFont:kFont(14)];
    [_nameLabel setTextColor:kMainBlackColor];
    [_nameLabel setTextAlignment:NSTextAlignmentCenter];
    [_nameLabel setLineBreakMode:NSLineBreakByTruncatingMiddle];
    [_nameLabel setText:@""];
    [self.contentView addSubview:_nameLabel];
}
- (void)refreshDataWith:(CBEGroupMemberModel *)model {
    [_headView sd_setImageWithURL:[NSURL URLWithString:model.headPhoto] placeholderImage:IMAGE_NAMED(@"im_icon_grouptalk")];
    [_nameLabel setText:model.userName?:model.userMobile];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(0);
        make.width.height.mas_equalTo(48);
        make.centerX.equalTo(self.contentView.mas_centerX);
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_bottom).offset(6);
        make.left.equalTo(self.contentView.mas_left).offset(2);
        make.right.equalTo(self.contentView.mas_right).offset(-2);
        make.height.mas_equalTo(20);
    }];
}
@end

//
//  CBEAddGroupListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEAddGroupListCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
+ (CBEAddGroupListCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

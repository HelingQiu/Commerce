//
//  CBEMemberCollectionCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/16.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBEGroupModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CBEMemberCollectionCell : UICollectionViewCell

- (void)refreshDataWith:(CBEGroupMemberModel *)model;

@end

NS_ASSUME_NONNULL_END

//
//  CBEApplyAddCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/13.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEApplyAddCell.h"
#import "CBETodoModel.h"

@implementation CBEApplyAddCell

+ (CBEApplyAddCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEApplyAddCell";
    CBEApplyAddCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEApplyAddCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (IBAction)agreeAction:(UIButton *)sender {
    if (self.block) {
        self.block(1);
    }
}
- (IBAction)refuseAction:(UIButton *)sender {
    if (self.block) {
        self.block(0);
    }
}
- (void)refreshDataWith:(CBETodoModel *)model {
    [self.headView sd_setImageWithURL:[NSURL URLWithString:[model.headPhoto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"im_icon_grouptalk")];
    [self.nameLabel setText:model.userName?:model.mobile];
    [self.contentLabel setText:StringFormat(@"%@%@",model.companyName?:@"",model.companyJobTitle?:@"")];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

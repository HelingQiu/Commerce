//
//  CBEMessageListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/29.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NIMAvatarImageView;
@class NIMRecentSession;
@class NIMBadgeView;

NS_ASSUME_NONNULL_BEGIN

@interface CBEMessageListCell : UITableViewCell

@property (nonatomic,strong) NIMAvatarImageView *avatarImageView;

@property (nonatomic,strong) UILabel *nameLabel;

@property (nonatomic,strong) UILabel *posiLabel;

@property (nonatomic,strong) UILabel *messageLabel;

@property (nonatomic,strong) UILabel *timeLabel;

@property (nonatomic,strong) NIMBadgeView *badgeView;

- (void)refresh:(NIMRecentSession*)recent;

@end

NS_ASSUME_NONNULL_END

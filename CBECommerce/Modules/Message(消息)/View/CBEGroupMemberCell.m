//
//  CBEGroupMemberCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/16.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEGroupMemberCell.h"
#import "CBEMemberCollectionCell.h"

@implementation CBEGroupMemberCell
{
    NSArray *_memberArray;
    UICollectionView *_collectionView;
    CBEBaseButton *_moreButton;
}
+ (CBEGroupMemberCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEGroupMemberCell";
    CBEGroupMemberCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEGroupMemberCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    UILabel *labTitle = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 200, 44)];
    [labTitle setText:@"群成员"];
    [labTitle setFont:kFont(14)];
    [labTitle setTextColor:kMainBlackColor];
    [self.contentView addSubview:labTitle];
    
    CBEBaseButton *moreButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    [moreButton setFrame:CGRectMake(kSCREEN_WIDTH - 64, 10, 54, 24)];
    [moreButton setTitleLabelRect:CGRectMake(0, 0, 30, 24)];
    [moreButton setImageViewRect:CGRectMake(30, 0, 24, 24)];
    [moreButton setTitle:@"更多" forState:UIControlStateNormal];
    [moreButton setTitleColor:kMainBlackColor forState:UIControlStateNormal];
    [moreButton.titleLabel setFont:kFont(14)];
    moreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [moreButton setImage:IMAGE_NAMED(@"arrow_right") forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreButton];
    _moreButton = moreButton;
    
    CGRect collectionViewFrame= CGRectMake(0, 44, kSCREEN_WIDTH, 90);
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    flowLayout.minimumInteritemSpacing = 1;
    flowLayout.itemSize = CGSizeMake(375/5, 90);
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:flowLayout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:collectionView];
    _collectionView = collectionView;
    [collectionView registerClass:[CBEMemberCollectionCell class] forCellWithReuseIdentifier:@"memberCollectionCell"];
}
- (void)moreAction {
    if (self.moreBlock) {
        self.moreBlock();
    }
}
- (void)refreshDataWith:(NSArray *)memberList {
    _memberArray = memberList;
    [_moreButton setTitle:StringFormat(@"%lu人",(unsigned long)memberList.count) forState:UIControlStateNormal];
    [_collectionView reloadData];
}
#pragma mark -delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _memberArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBEMemberCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"memberCollectionCell" forIndexPath:indexPath];
    CBEGroupMemberModel *model = [_memberArray objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //选中数据模型
    id model = @"";
    if (self.mayknowSelectedBlock) {
        self.mayknowSelectedBlock(model);
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

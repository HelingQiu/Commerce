//
//  CBEMemberListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/17.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^DeleteBlock)(void);
@interface CBEMemberListCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupOwner;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (copy, nonatomic) DeleteBlock deleteBlock;
+ (CBEMemberListCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

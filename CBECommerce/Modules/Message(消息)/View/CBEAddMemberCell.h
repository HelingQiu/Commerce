//
//  CBEAddMemberCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEAddMemberCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;

+ (CBEAddMemberCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

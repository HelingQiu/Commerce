//
//  CBEAddressBookListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/30.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAddressBookListCell.h"
#import "CBEFriendModel.h"

@implementation CBEAddressBookListCell

+ (CBEAddressBookListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEAddressBookListCell";
    CBEAddressBookListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEAddressBookListCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEFriendModel *)model {
    [self.headView sd_setImageWithURL:[NSURL URLWithString:[model.headPhoto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"im_icon_grouptalk")];
    [self.nameLabel setText:model.userName?:model.userMobile];
    NSString *position = StringFormat(@"%@%@",model.companyName?:@"",model.companyJobTitle?:@"")?:@"暂无";
    [self.posComLabel setText:position];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

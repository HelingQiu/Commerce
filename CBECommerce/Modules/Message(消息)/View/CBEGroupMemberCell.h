//
//  CBEGroupMemberCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/16.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"
#import "CBEGroupModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^MoreBlock)(void);
typedef void(^ItemSelectBlock)(id model);

@interface CBEGroupMemberCell : CBEBaseTableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, copy) MoreBlock moreBlock;
@property (nonatomic, copy) ItemSelectBlock mayknowSelectedBlock;

+ (CBEGroupMemberCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

//
//  CBEMemberListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/17.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMemberListCell.h"
#import "CBEGroupModel.h"

@implementation CBEMemberListCell

+ (CBEMemberListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEMemberListCell";
    CBEMemberListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEMemberListCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (IBAction)deleteClick:(UIButton *)sender {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}
- (void)refreshDataWith:(CBEGroupMemberModel *)model {
    [self.headView sd_setImageWithURL:[NSURL URLWithString:model.headPhoto] placeholderImage:IMAGE_NAMED(@"im_icon_grouptalk")];
    [_nameLabel setText:model.userName?:model.userMobile];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

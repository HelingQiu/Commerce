//
//  CBEAddMemberCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAddMemberCell.h"
#import "CBEFriendModel.h"

@implementation CBEAddMemberCell

+ (CBEAddMemberCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEAddMemberCell";
    CBEAddMemberCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEAddMemberCell" owner:self options:nil].firstObject;
    }
    return cell;
}
- (void)refreshDataWith:(CBEFriendModel *)model {
    [self.headView sd_setImageWithURL:[NSURL URLWithString:[model.headPhoto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"im_icon_grouptalk")];
    [self.nameLabel setText:model.userName?:model.userMobile];
    [self.companyLabel setText:StringFormat(@"%@%@",model.companyName?:@"",model.companyJobTitle?:@"")];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

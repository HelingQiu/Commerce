//
//  CBEMemberSearchController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/22.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMemberSearchController.h"
#import "LSSearchBarView.h"
#import "ChineseToPinyin.h"
#import "CBEMemberListCell.h"
#import "CBEGroupModel.h"
#import "CBEMessageVM.h"

@interface CBEMemberSearchController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) LSSearchBarView *searchView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) CBEGroupMemberModel *ownerModel;

@end

@implementation CBEMemberSearchController

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.ownerModel = [self.memberList firstObject];
    
    _searchView = [[LSSearchBarView alloc] initWithAlignment:(SearchBarAlignmentAlignmentLeft) placeholder:@"搜索用户名" didClicked:nil];
    [self.navigationItem.titleView addSubview:_searchView];
    [self setBarButton:(BarButtonItemRight) action:@selector(cancelAction) title:@"取消" titleColor:kMainBlueColor];
    
    _searchView.textField.placeholder = @"搜索用户名";
    self.textField = _searchView.textField;
    self.textField.delegate = self;
    self.textField.userInteractionEnabled = YES;
    [self.textField becomeFirstResponder];
    
    // 监控当前输入框的值
    [self.textField addTarget:self action:@selector(textFieldValueChangedAction:) forControlEvents:UIControlEventEditingChanged];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight)) style:(UITableViewStylePlain)];
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.f)];
    headView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headView;
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
        [self.tableView setSeparatorColor:[UIColor ls_colorWithHex:0xf0f2f5]];
    }
    self.extendedLayoutIncludesOpaqueBars = YES;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}
- (void)textFieldValueChangedAction:(UITextField *)field
{
    NSString *keyword = field.text;
    [self searchFriendsWithKeyword:keyword];
}

- (void)searchFriendsWithKeyword:(NSString *)keyword
{
    //需要事先情况存放搜索结果的数组
    [self.dataArray removeAllObjects];
    for (int i = 0; i < self.memberList.count; i++) {
        CBEGroupMemberModel *model = self.memberList[i];
        NSString *lowStr = [ChineseToPinyin pinyinFromChineseString:keyword withSpace:NO ];
        NSString *allStr =[ChineseToPinyin pinyinFromChineseString:model.userName withSpace:NO];
        BOOL isHas = [allStr isEqualToString:lowStr];
        BOOL isPY = false;
        if (lowStr || lowStr.length != 0) {
            isPY = [allStr hasPrefix:lowStr];
        }
        if (isHas) {
            //这种情况是精确查找。
            [self.dataArray addObject:model];//讲搜索后的数据添加到数组中
        }else{
            if (isPY && !isHas) {
                //迷糊查找
                [self.dataArray addObject:model];
            }
        }
        if (i == self.memberList.count - 1) {
            [self.tableView reloadData];
        }
    }
}
- (void)cancelAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEMemberListCell *cell = [CBEMemberListCell cellForTableView:tableView];
    CBEGroupMemberModel *model = [self.dataArray objectAtIndex:indexPath.row];
    
    cell.groupOwner.hidden = YES;
    if ([model.userId isEqualToString:self.ownerModel.userId]) {
        cell.groupOwner.hidden = NO;
        cell.deleteBtn.hidden = YES;
    }else{
        if (self.ownerModel && [self.ownerModel.userId isEqualToString:[CBEUserModel sharedInstance].userInfo.userId]) { //群主可以删除群成员
            cell.deleteBtn.hidden = NO;
            cell.deleteBlock = ^{
                [self deleteMember:model];
            };
        }else{
            cell.groupOwner.hidden = YES;
            cell.deleteBtn.hidden = YES;
        }
    }
    
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)deleteMember:(CBEGroupMemberModel *)model {
    NSDictionary *params = @{@"groupId":self.groupId,
                             @"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"deleteUserId":model.userId
                             };
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM kickGroupMemberWith:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"踢出群聊成功" autoHide:YES];
            [self.dataArray removeObject:model];
            [self setBarTitle:StringFormat(@"群成员(%lu)",(unsigned long)self.memberList.count)];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"踢出群成员失败，请重试" autoHide:YES];
    }];
}

@end

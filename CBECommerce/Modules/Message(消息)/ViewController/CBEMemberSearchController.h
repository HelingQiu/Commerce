//
//  CBEMemberSearchController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/22.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEMemberSearchController : CBEBaseController

@property (nonatomic, strong) NSArray *memberList;
@property (nonatomic, copy) NSString *groupId;

@end

NS_ASSUME_NONNULL_END

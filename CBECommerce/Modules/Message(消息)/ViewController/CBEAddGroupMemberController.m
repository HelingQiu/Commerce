//
//  CBEAddGroupMemberController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAddGroupMemberController.h"
#import "CBEAddMemberCell.h"
#import "CBEMessageVM.h"
#import "CBEMessageChatController.h"
#import "CBEFriendModel.h"
#import "CBEGroupModel.h"

@interface CBEAddGroupMemberController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *letterArray;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSMutableArray *selectArray;
@property (nonatomic, strong) NSMutableArray *selectNameArray;

@end

@implementation CBEAddGroupMemberController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"添加群成员"];
    [self setBarButton:BarButtonItemRight action:@selector(nextStep) title:@"确定"];
    self.letterArray = @[];
    [self.view addSubview:self.tableView];
    [self getMyFriends];
}
- (void)getMyFriends {
    NSDictionary *params = @{@"pageNum":@"1",
                             @"pageSize":@"10"
                             };
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM getMyFriendsWith:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray<CBEFriendModel *> *dataArray = [CBEFriendModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]];
            if (self.pageForm == PageFrom_GroupSetting) {
                //过滤已经在群内的好友
                NSMutableArray *array = [NSMutableArray array];
                //找到arr1中有,arr2中没有的数据
                [dataArray enumerateObjectsUsingBlock:^(CBEFriendModel *friend, NSUInteger idx, BOOL * _Nonnull stop) {
                    __block BOOL isHave = NO;
                    [self.inMemberList enumerateObjectsUsingBlock:^(CBEGroupMemberModel *member, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([friend.userMobile isEqual:member.userMobile]) {
                            isHave = YES;
                            *stop = YES;
                        }
                    }];
                    if (!isHave) {
                        [array addObject:friend];
                    }
                }];
                NSDictionary *dict = [self sortObjectsAccordingToInitialWith:array SortKey:@"userName"];
                self.dataSource = [dict objectForKey:@"array"];
                self.letterArray = [dict objectForKey:@"char"];
                [self.tableView reloadData];
            }else{
                NSDictionary *dict = [self sortObjectsAccordingToInitialWith:dataArray SortKey:@"userName"];
                self.dataSource = [dict objectForKey:@"array"];
                self.letterArray = [dict objectForKey:@"char"];
                [self.tableView reloadData];
            }
        }else{
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)nextStep {
    if (self.selectArray.count == 0) {
        [CommonUtils showHUDWithMessage:@"至少选择一个群成员" autoHide:YES];
        return;
    }
    __block NSString *userIdsString = @"";
    __block NSString *groupName = @"";
    [self.selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == self.selectArray.count - 1) {
            userIdsString = [userIdsString stringByAppendingString:StringFormat(@"%@",obj)];
        }else{
            userIdsString = [userIdsString stringByAppendingString:StringFormat(@"%@,",obj)];
        }
    }];
    [self.selectNameArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == self.selectArray.count - 1) {
            groupName = [groupName stringByAppendingString:StringFormat(@"%@",obj)];
        }else{
            groupName = [groupName stringByAppendingString:StringFormat(@"%@、",obj)];
        }
    }];
    if (self.pageForm == PageFrom_GroupSetting) {
        //向群里添加好友
        NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                                 @"groupId":self.groupId,
                                 @"userIds":userIdsString
                                 };
        [CommonUtils showHUDWithWaitingMessage:nil];
        [CBEMessageVM addGroupListWith:params complete:^(id data) {
            [CommonUtils hideHUD];
            if ([[data objectForKey:@"success"] boolValue]) {
                [CommonUtils showHUDWithMessage:@"添加成功" autoHide:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:kAddGroupMemberNotification object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
        }];
    }else{
        //请求接口创建群聊 创建成功后进入群聊页面
        NSDictionary *params = @{@"name":groupName,
                                 @"userId":[CBEUserModel sharedInstance].userInfo.userId,
                                 @"type":@"0",
                                 @"joinmode":@"0",
                                 @"remark":@"群公告",
                                 @"tagsName":self.groupTags,
                                 @"msg":@"创建群聊",
                                 @"userIds":userIdsString
                                 };
        [CommonUtils showHUDWithWaitingMessage:nil];
        [CBEMessageVM createGroupWith:params complete:^(id data) {
            [CommonUtils hideHUD];
            if ([[data objectForKey:@"success"] boolValue]) {
                NSDictionary *dataDict = [data objectForKey:@"data"];
                NSString *teamId = StringFormat(@"%@",[dataDict objectForKey:@"groupId"]);
                NIMSession *session = [NIMSession session:teamId type:NIMSessionTypeTeam];
                CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:session];
                [vc setBarTitle:[data objectForKey:@"name"]];
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
        }];
    }
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.letterArray objectAtIndex:section];
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.letterArray;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 32)];
    [headView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, kSCREEN_WIDTH - kPadding15*2, 32)];
    [nameLabel setText:[self.letterArray objectAtIndex:section]];
    [nameLabel setFont:kFont(14)];
    [nameLabel setTextColor:kMainTextColor];
    [headView addSubview:nameLabel];
    
    return headView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CBEAddMemberCell *cell = [CBEAddMemberCell cellForTableView:tableView];
    CBEFriendModel *model = [self.dataSource[indexPath.section] objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CBEAddMemberCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectBtn.selected = !cell.selectBtn.selected;
    CBEFriendModel *model = [self.dataSource[indexPath.section] objectAtIndex:indexPath.row];
    BOOL isIn = [self.selectArray containsObject:model.userId];
    BOOL isNameIn = [self.selectNameArray containsObject:model.userName];
    if (cell.selectBtn.selected) {
        if (!isIn) {
            [self.selectArray addObject:model.userId];
        }
        if (!isNameIn) {
            [self.selectNameArray addObject:model.userName];
        }
    }else{
        if (isIn) {
            [self.selectArray removeObject:model.userId];
        }
        if (isNameIn) {
            [self.selectNameArray addObject:model.userName];
        }
    }
}
#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionIndexColor = kMainOrangeColor;
    }
    return _tableView;
}
- (NSMutableArray *)selectArray {
    if (!_selectArray) {
        _selectArray = [NSMutableArray array];
    }
    return _selectArray;
}
- (NSMutableArray *)selectNameArray {
    if (!_selectNameArray) {
        _selectNameArray = [NSMutableArray array];
    }
    return _selectNameArray;
}
#pragma -- private
// 按首字母分组排序数组
- (NSDictionary *)sortObjectsAccordingToInitialWith:(NSArray *)willSortArr SortKey:(NSString *)sortkey {
    // 初始化UILocalizedIndexedCollation
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    //得出collation索引的数量，这里是27个（26个字母和1个#）
    NSInteger sectionTitlesCount = [[collation sectionTitles] count];
    //初始化一个数组newSectionsArray用来存放最终的数据，我们最终要得到的数据模型应该形如@[@[以A开头的数据数组], @[以B开头的数据数组], @[以C开头的数据数组], ... @[以#(其它)开头的数据数组]]
    NSMutableArray *newSectionsArray = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
    
    //初始化27个空数组加入newSectionsArray
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [newSectionsArray addObject:array];
    }
    
    NSLog(@"newSectionsArray %@ %@",newSectionsArray,collation.sectionTitles);
    
    NSMutableArray *firstChar = [NSMutableArray arrayWithCapacity:10];
    
    //将每个名字分到某个section下
    for (id Model in willSortArr) {
        //获取name属性的值所在的位置，比如"林丹"，首字母是L，在A~Z中排第11（第一位是0），sectionNumber就为11
        NSInteger sectionNumber = [collation sectionForObject:Model collationStringSelector:NSSelectorFromString(sortkey)];
        
        //把name为“林丹”的p加入newSectionsArray中的第11个数组中去
        NSMutableArray *sectionNames = newSectionsArray[sectionNumber];
        [sectionNames addObject:Model];
        
        //拿出每名字的首字母
        NSString * str= collation.sectionTitles[sectionNumber];
        [firstChar addObject:str];
        NSLog(@"sectionNumbersectionNumber %ld %@",sectionNumber,str);
    }
    
    //返回首字母排好序的数据
    NSArray *firstCharResult = [self SortFirstChar:firstChar];
    
    
    NSLog(@"firstCharResult== %@",firstCharResult);
    
    //对每个section中的数组按照name属性排序
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *personArrayForSection = newSectionsArray[index];
//        NSArray *sortedPersonArrayForSection = [collation sortedArrayFromArray:personArrayForSection collationStringSelector:NSSelectorFromString(sortkey)];
        newSectionsArray[index] = personArrayForSection;
    }
    
    //删除空的数组
    NSMutableArray *finalArr = [NSMutableArray new];
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        if (((NSMutableArray *)(newSectionsArray[index])).count != 0) {
            [finalArr addObject:newSectionsArray[index]];
        }
    }
    return @{@"array":finalArr,
             @"char":firstCharResult};
}

- (NSArray *)SortFirstChar:(NSArray *)firstChararry{
    //数组去重复
    
    NSMutableArray *noRepeat = [[NSMutableArray alloc]initWithCapacity:8];
    
    NSMutableSet *set = [[NSMutableSet alloc]initWithArray:firstChararry];
    
    [set enumerateObjectsUsingBlock:^(id obj , BOOL *stop){
        
        
        [noRepeat addObject:obj];
        
    }];
    
    //字母排序
    NSArray *resultkArrSort1 = [noRepeat sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    //把”#“放在最后一位
    NSMutableArray *resultkArrSort2 = [[NSMutableArray alloc]initWithArray:resultkArrSort1];
    if ([resultkArrSort2 containsObject:@"#"]) {
        
        [resultkArrSort2 removeObject:@"#"];
        [resultkArrSort2 addObject:@"#"];
    }
    
    return resultkArrSort2;
}

@end

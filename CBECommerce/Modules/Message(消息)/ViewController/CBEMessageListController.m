//
//  CBEMessageListController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/16.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMessageListController.h"
#import "NIMSessionViewController.h"
#import "NIMSessionListCell.h"
#import "UIView+NIM.h"
#import "NIMAvatarImageView.h"
#import "NIMKitUtil.h"
#import "NIMKit.h"
#import "LSSearchBarView.h"
#import "CBEMessageListCell.h"
#import "CBEAddressBookController.h"
#import "CBEPopView.h"
#import "CBEAddFriendsListController.h"
#import "CBEMessageChatController.h"
#import "CBERelevancyTagController.h"
#import "CBEWaitHandleController.h"
#import "NTESSessionUtil.h"
#import "CBEMessageVM.h"
#import "CBETodoModel.h"
#import "CBESearchConversionController.h"
#import "NIMBadgeView.h"

@interface CBEMessageListController ()

@property (nonatomic, strong) NSMutableArray *todoArray;

@end

@implementation CBEMessageListController

- (void)dealloc{
    [[NIMSDK sharedSDK].conversationManager removeDelegate:self];
    [[NIMSDK sharedSDK].loginManager removeDelegate:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    [self doLogin];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.delegate         = self;
    self.tableView.dataSource       = self;
    self.tableView.separatorColor   = kMainLineColor;
    self.tableView.tableFooterView  = [[UIView alloc] init];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _recentSessions = [[NIMSDK sharedSDK].conversationManager.allRecentSessions mutableCopy];
    if (!self.recentSessions.count) {
        _recentSessions = [NSMutableArray array];
    }else{
        _recentSessions = [self customSortRecents:_recentSessions];
    }
    
    [[NIMSDK sharedSDK].conversationManager addDelegate:self];
    [[NIMSDK sharedSDK].loginManager addDelegate:self];
    
    extern NSString *const NIMKitTeamInfoHasUpdatedNotification;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTeamInfoHasUpdatedNotification:) name:NIMKitTeamInfoHasUpdatedNotification object:nil];
    
    extern NSString *const NIMKitTeamMembersHasUpdatedNotification;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTeamMembersHasUpdatedNotification:) name:NIMKitTeamMembersHasUpdatedNotification object:nil];
    
    extern NSString *const NIMKitUserInfoHasUpdatedNotification;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUserInfoHasUpdatedNotification:) name:NIMKitUserInfoHasUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTopRecentRefresh) name:kSessionTopNotification object:nil];
    
    NSArray *allTeams = [[NIMSDK sharedSDK].teamManager allMyTeams];
    NSLog(@"%@",allTeams);
}
- (void)requestTodoData {
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId};
    [CBEMessageVM todoListWith:params complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *todoList = [data objectForKey:@"data"];
            self.todoArray = [[CBETodoModel mj_objectArrayWithKeyValuesArray:todoList] mutableCopy];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    } fail:^(NSError *error) {
    }];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self doLogin];
    [self requestTodoData];
}

- (void)doLogin {
    if ([CBEUserModel sharedInstance].isLogin) {
        [[[NIMSDK sharedSDK] loginManager] autoLogin:[CBEUserModel sharedInstance].userInfo.userId token:[CBEUserModel sharedInstance].userInfo.yxToken];
    }else{
        [[[NIMSDK sharedSDK] loginManager] login:[CBEUserModel sharedInstance].userInfo.userId
                                           token:[CBEUserModel sharedInstance].userInfo.yxToken
                                      completion:^(NSError *error) {
                                          if (error == nil){
                                              self->_recentSessions = [[NIMSDK sharedSDK].conversationManager.allRecentSessions mutableCopy];
                                              if (!self.recentSessions.count) {
                                                  self->_recentSessions = [NSMutableArray array];
                                              }else{
                                                  self->_recentSessions = [self customSortRecents:self->_recentSessions];
                                              }
                                              [[NIMSDK sharedSDK].conversationManager addDelegate:self];
                                              [[NIMSDK sharedSDK].loginManager addDelegate:self];
                                          }else{
                                              NSString *toast = [NSString stringWithFormat:@"登录失败 code: %zd",error.code];
                                              NSLog(@"%@",toast);
                                          }
                                      }];
    }
}
- (void)onAutoLoginFailed:(NSError *)error {
    NSLog(@"%@",error);
}
- (void)setNavBar {
    [self setBarTitle:@"消息"];
    self.backBtn.hidden = YES;
    [self setBarButton:BarButtonItemLeft action:@selector(addressBookAction) imageName:@"im_icon_tongxl"];
    [self setBarButton:BarButtonItemRight action:@selector(addFriendsAction) imageName:@"im_icon_plus"];
}
//通讯录列表
- (void)addressBookAction {
    CBEAddressBookController *addressController = [[CBEAddressBookController alloc] init];
    [self.navigationController pushViewController:addressController animated:YES];
}
//添加好友
- (void)addFriendsAction {
    [CBEPopView addPopViewSelectWithWindowFrame:CGRectMake(kSCREEN_WIDTH-150, KNavigationBarHeight + 40, 150, 100) selectData:@[@"发起群聊",@"添加好友"] images:@[@"im_icon_plusgroup",@"im_icon_pluspeople"] action:^(NSInteger index) {
        NSLog(@"选择%ld",index);
        if (index == 1) {
            CBEAddFriendsListController *addController = [[CBEAddFriendsListController alloc] init];
            [self.navigationController pushViewController:addController animated:YES];
            [CBEPopView hiden];
        }else{
            CBERelevancyTagController *groupController = [[CBERelevancyTagController alloc] init];
            [self.navigationController pushViewController:groupController animated:YES];
        }
    } animated:YES];
}
#pragma mark - 置顶
- (void)onTopRecentAtIndexPath:(NIMRecentSession *)recent
                   atIndexPath:(NSIndexPath *)indexPath
                         isTop:(BOOL)isTop
{
    if (isTop)
    {
        [NTESSessionUtil removeRecentSessionMark:recent.session type:NTESRecentSessionMarkTypeTop];
    }
    else
    {
        [NTESSessionUtil addRecentSessionMark:recent.session type:NTESRecentSessionMarkTypeTop];
    }
    self.recentSessions = [self customSortRecents:self.recentSessions];
    [self.tableView reloadData];
}
- (void)onTopRecentRefresh
{
    self.recentSessions = [self customSortRecents:self.recentSessions];
    [self.tableView reloadData];
}

- (void)refresh {
    if (!self.recentSessions.count) {
//        self.tableView.hidden = YES;
    }else{
        self.tableView.hidden = NO;
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row > 0) {
        NIMRecentSession *recentSession = self.recentSessions[indexPath.row - 1];
        [self onSelectedRecent:recentSession atIndexPath:indexPath];
    }else if (indexPath.row == 0){
        CBEWaitHandleController *waitController = [[CBEWaitHandleController alloc] init];
        [self.navigationController pushViewController:waitController animated:YES];
    }else{
//        NIMSession *session = [NIMSession session:@"18938903336" type:NIMSessionTypeP2P];
//        CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:session];
//        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.f;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recentSessions.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 50)];
    [headView setBackgroundColor:[UIColor whiteColor]];
    
    LSSearchBarView *searchView = [[LSSearchBarView alloc] initWithFrame:CGRectMake(15, 9, kSCREEN_WIDTH - 30, 32) placeholder:@"搜索" didClicked:^{
        CBESearchConversionController *searchController = [[CBESearchConversionController alloc] init];
        [self.navigationController pushViewController:searchController animated:YES];
    }];
    searchView.layer.cornerRadius = 16;
    [headView addSubview:searchView];
    return headView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellId = @"cellId";
    CBEMessageListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[CBEMessageListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell.avatarImageView addTarget:self action:@selector(onTouchAvatar:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (indexPath.row == 0) {
        cell.avatarImageView.image = IMAGE_NAMED(@"im_icon_daichuli");
        cell.nameLabel.text = @"待处理消息";
        [cell.nameLabel sizeToFit];
        cell.messageLabel.textColor = kMainBlueColor;
        if (self.todoArray.count) {
            [cell.messageLabel setText:StringFormat(@"%lu请求待处理",(unsigned long)self.todoArray.count)];
        }else{
            [cell.messageLabel setText:@"暂无待办请求"];
            cell.badgeView.hidden = YES;
        }
        [cell.messageLabel sizeToFit];
    }else{
        cell.messageLabel.textColor = [UIColor lightGrayColor];
        NIMRecentSession *recent = self.recentSessions[indexPath.row - 1];
        cell.nameLabel.text = [self nameForRecentSession:recent];
        [cell.avatarImageView setAvatarBySession:recent.session];
        if (recent.session.sessionType == NIMSessionTypeTeam) {
            [cell.avatarImageView setImage:IMAGE_NAMED(@"im_icon_group1")];
        }
        [cell.nameLabel sizeToFit];
        cell.messageLabel.attributedText  = [self contentForRecentSession:recent];
        [cell.messageLabel sizeToFit];
        cell.timeLabel.text = [self timestampDescriptionForRecentSession:recent];
        [cell.timeLabel sizeToFit];
        [cell refresh:recent];
    }
    
    return cell;
}

#pragma mark - NIMConversationManagerDelegate
- (void)didAddRecentSession:(NIMRecentSession *)recentSession
           totalUnreadCount:(NSInteger)totalUnreadCount {
    [self.recentSessions addObject:recentSession];
    [self sort];
    _recentSessions = [self customSortRecents:_recentSessions];
    [self refresh];
}

- (void)didUpdateRecentSession:(NIMRecentSession *)recentSession
              totalUnreadCount:(NSInteger)totalUnreadCount {
    for (NIMRecentSession *recent in self.recentSessions) {
        if ([recentSession.session.sessionId isEqualToString:recent.session.sessionId]) {
            [self.recentSessions removeObject:recent];
            break;
        }
    }
    NSInteger insert = [self findInsertPlace:recentSession];
    [self.recentSessions insertObject:recentSession atIndex:insert];
    _recentSessions = [self customSortRecents:_recentSessions];
    [self refresh];
}

- (void)didRemoveRecentSession:(NIMRecentSession *)recentSession
              totalUnreadCount:(NSInteger)totalUnreadCount {
    //清理本地数据
    NSInteger index = [self.recentSessions indexOfObject:recentSession];
    [self.recentSessions removeObjectAtIndex:index];
    
    //如果删除本地会话后就不允许漫游当前会话，则需要进行一次删除服务器会话的操作
    if (self.autoRemoveRemoteSession)
    {
        [[NIMSDK sharedSDK].conversationManager deleteRemoteSessions:@[recentSession.session]
                                                          completion:nil];
    }
    _recentSessions = [self customSortRecents:_recentSessions];
    [self refresh];
}

- (void)messagesDeletedInSession:(NIMSession *)session {
    _recentSessions = [[NIMSDK sharedSDK].conversationManager.allRecentSessions mutableCopy];
    _recentSessions = [self customSortRecents:_recentSessions];
    [self refresh];
}

- (void)allMessagesDeleted {
    _recentSessions = [[NIMSDK sharedSDK].conversationManager.allRecentSessions mutableCopy];
    _recentSessions = [self customSortRecents:_recentSessions];
    [self refresh];
}

- (void)allMessagesRead {
    _recentSessions = [[NIMSDK sharedSDK].conversationManager.allRecentSessions mutableCopy];
    _recentSessions = [self customSortRecents:_recentSessions];
    [self refresh];
}

//- (NSMutableArray *)customSortRecents:(NSMutableArray *)recentSessions {
//    if (<#condition#>) {
//        <#statements#>
//    }
//    return self.recentSessions;
//}
- (NSMutableArray *)customSortRecents:(NSMutableArray *)recentSessions
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:recentSessions];
    [array sortUsingComparator:^NSComparisonResult(NIMRecentSession *obj1, NIMRecentSession *obj2) {
        NSInteger score1 = [NTESSessionUtil recentSessionIsMark:obj1 type:NTESRecentSessionMarkTypeTop]? 10 : 0;
        NSInteger score2 = [NTESSessionUtil recentSessionIsMark:obj2 type:NTESRecentSessionMarkTypeTop]? 10 : 0;
        if (obj1.lastMessage.timestamp > obj2.lastMessage.timestamp)
        {
            score1 += 1;
        }
        else if (obj1.lastMessage.timestamp < obj2.lastMessage.timestamp)
        {
            score2 += 1;
        }
        if (score1 == score2)
        {
            return NSOrderedSame;
        }
        return score1 > score2? NSOrderedAscending : NSOrderedDescending;
    }];
    return array;
}
#pragma mark - NIMLoginManagerDelegate
- (void)onLogin:(NIMLoginStep)step {
    if (step == NIMLoginStepSyncOK) {
        [self refresh];
    }
}

#pragma mark - Override
- (void)onSelectedAvatar:(NIMRecentSession *)recentSession
             atIndexPath:(NSIndexPath *)indexPath {
    CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:recentSession.session];
    [self.navigationController pushViewController:vc animated:YES];
};

- (void)onSelectedRecent:(NIMRecentSession *)recentSession atIndexPath:(NSIndexPath *)indexPath {
    CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:recentSession.session];
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSString *)nameForRecentSession:(NIMRecentSession *)recent {
    if (recent.session.sessionType == NIMSessionTypeP2P) {
        NSArray *userList = [FriendInfo MR_findByAttribute:@"userId" withValue:recent.session.sessionId];
        if (userList.count) {
            FriendInfo *model = [userList firstObject];
            return model.userName?:model.userMobile;
        }
        return [NIMKitUtil showNick:recent.session.sessionId inSession:recent.session];
    }else{
        NIMTeam *team = [[NIMSDK sharedSDK].teamManager teamById:recent.session.sessionId];
        return team.teamName;
    }
}

- (NSAttributedString *)contentForRecentSession:(NIMRecentSession *)recent {
    NSString *content = [self messageContent:recent.lastMessage];
    return [[NSAttributedString alloc] initWithString:content ?: @""];
}

- (NSString *)timestampDescriptionForRecentSession:(NIMRecentSession *)recent {
    return [NIMKitUtil showTime:recent.lastMessage.timestamp showDetail:NO];
}

#pragma mark - Misc
- (NSInteger)findInsertPlace:(NIMRecentSession *)recentSession {
    __block NSUInteger matchIdx = 0;
    __block BOOL find = NO;
    [self.recentSessions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NIMRecentSession *item = obj;
        if (item.lastMessage.timestamp <= recentSession.lastMessage.timestamp) {
            *stop = YES;
            find  = YES;
            matchIdx = idx;
        }
    }];
    if (find) {
        return matchIdx;
    }else{
        return self.recentSessions.count;
    }
}

- (void)sort {
    [self.recentSessions sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NIMRecentSession *item1 = obj1;
        NIMRecentSession *item2 = obj2;
        if (item1.lastMessage.timestamp < item2.lastMessage.timestamp) {
            return NSOrderedDescending;
        }
        if (item1.lastMessage.timestamp > item2.lastMessage.timestamp) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];
}

- (void)onTouchAvatar:(id)sender {
    UIView *view = [sender superview];
    while (![view isKindOfClass:[UITableViewCell class]]) {
        view = view.superview;
    }
    UITableViewCell *cell  = (UITableViewCell *)view;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NIMRecentSession *recent = self.recentSessions[indexPath.row];
    [self onSelectedAvatar:recent atIndexPath:indexPath];
}

#pragma mark - Private
- (NSString *)messageContent:(NIMMessage*)lastMessage {
    NSString *text = @"";
    switch (lastMessage.messageType) {
        case NIMMessageTypeText:
            text = lastMessage.text;
            break;
        case NIMMessageTypeAudio:
            text = @"[语音]";
            break;
        case NIMMessageTypeImage:
            text = @"[图片]";
            break;
        case NIMMessageTypeVideo:
            text = @"[视频]";
            break;
        case NIMMessageTypeLocation:
            text = @"[位置]";
            break;
        case NIMMessageTypeNotification:{
            return [self notificationMessageContent:lastMessage];
        }
        case NIMMessageTypeFile:
            text = @"[文件]";
            break;
        case NIMMessageTypeTip:
            text = lastMessage.text;
            break;
        case NIMMessageTypeRobot:
            text = [self robotMessageContent:lastMessage];
            break;
        default:
            text = @"[未知消息]";
    }
    if (lastMessage.session.sessionType == NIMSessionTypeP2P || lastMessage.messageType == NIMMessageTypeTip) {
        return text;
    }else{
        NSString *from = lastMessage.from;
        if (lastMessage.messageType == NIMMessageTypeRobot) {
            NIMRobotObject *object = (NIMRobotObject *)lastMessage.messageObject;
            if (object.isFromRobot) {
                from = object.robotId;
            }
        }
        NSString *nickName = [NIMKitUtil showNick:from inSession:lastMessage.session];
        return nickName.length ? [nickName stringByAppendingFormat:@" : %@",text] : @"";
    }
}

- (NSString *)notificationMessageContent:(NIMMessage *)lastMessage {
    NIMNotificationObject *object = lastMessage.messageObject;
    if (object.notificationType == NIMNotificationTypeNetCall) {
        NIMNetCallNotificationContent *content = (NIMNetCallNotificationContent *)object.content;
        if (content.callType == NIMNetCallTypeAudio) {
            return @"[网络通话]";
        }
        return @"[视频聊天]";
    }
    if (object.notificationType == NIMNotificationTypeTeam) {
        NIMTeam *team = [[NIMSDK sharedSDK].teamManager teamById:lastMessage.session.sessionId];
        if (team.type == NIMTeamTypeNormal) {
            return @"[讨论组信息更新]";
        }else{
            return @"[群信息更新]";
        }
    }
    return @"[未知消息]";
}

- (NSString *)robotMessageContent:(NIMMessage *)lastMessage {
    NIMRobotObject *object = lastMessage.messageObject;
    if (object.isFromRobot){
        return @"[机器人消息]";
    }else{
        return lastMessage.text;
    }
}

#pragma mark - Notification
- (void)onUserInfoHasUpdatedNotification:(NSNotification *)notification{
    [self refresh];
}

- (void)onTeamInfoHasUpdatedNotification:(NSNotification *)notification{
    [self refresh];
}

- (void)onTeamMembersHasUpdatedNotification:(NSNotification *)notification{
    [self refresh];
}

@end

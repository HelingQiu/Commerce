//
//  CBEMyGroupController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMyGroupController.h"
#import "CBEAddGroupListCell.h"
#import "CBEMessageVM.h"
#import "CBEGroupModel.h"
#import "CBEMessageChatController.h"

@interface CBEMyGroupController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *groupArray;

@end

@implementation CBEMyGroupController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"我的群聊"];
    [self.view addSubview:self.tableView];
    [self getMyGroups];
}
- (void)getMyGroups {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM getMyGroupsComplete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray<CBEGroupModel *> *dataArray = [CBEGroupModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]];
            self.groupArray = dataArray;
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
#pragma mark --
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEAddGroupListCell *cell = [CBEAddGroupListCell cellForTableView:tableView];
    CBEGroupModel *model = [self.groupArray objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    [cell.groupNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(cell.contentView.mas_right).offset(-kPadding15);
    }];
    cell.addBtn.hidden = YES;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CBEGroupModel *model = [self.groupArray objectAtIndex:indexPath.row];
    NIMSession *session = [NIMSession session:model.groupId type:NIMSessionTypeTeam];
    CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:session];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionIndexColor = kMainOrangeColor;
    }
    return _tableView;
}

@end

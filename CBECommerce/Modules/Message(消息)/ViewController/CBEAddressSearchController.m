//
//  CBEAddressSearchController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/18.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAddressSearchController.h"
#import "LSSearchBarView.h"
#import "CBEAddressBookListCell.h"
#import "CBEFriendModel.h"
#import "ChineseToPinyin.h"
#import "CBEMessageChatController.h"

@interface CBEAddressSearchController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) LSSearchBarView *searchView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation CBEAddressSearchController

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _searchView = [[LSSearchBarView alloc] initWithAlignment:(SearchBarAlignmentAlignmentLeft) placeholder:@"搜索用户名" didClicked:nil];
    [self.navigationItem.titleView addSubview:_searchView];
    [self setBarButton:(BarButtonItemRight) action:@selector(cancelAction) title:@"取消" titleColor:kMainBlueColor];
    
    _searchView.textField.placeholder = @"搜索用户名";
    self.textField = _searchView.textField;
    self.textField.delegate = self;
    self.textField.userInteractionEnabled = YES;
    [self.textField becomeFirstResponder];
    
    // 监控当前输入框的值
    [self.textField addTarget:self action:@selector(textFieldValueChangedAction:) forControlEvents:UIControlEventEditingChanged];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight)) style:(UITableViewStylePlain)];
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.f)];
    headView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headView;
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
        [self.tableView setSeparatorColor:[UIColor ls_colorWithHex:0xf0f2f5]];
    }
    self.extendedLayoutIncludesOpaqueBars = YES;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}
- (void)textFieldValueChangedAction:(UITextField *)field {
    NSString *keyword = field.text;
    [self searchFriendsWithKeyword:keyword];
}

- (void)searchFriendsWithKeyword:(NSString *)keyword {
    //需要事先情况存放搜索结果的数组
    [self.dataArray removeAllObjects];
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[c] %@",keyword];
//    self.dataArray = [NSMutableArray arrayWithArray:[self.friendsList filteredArrayUsingPredicate:predicate]];
    
    
    for (int i = 0; i < self.friendsList.count; i++) {
        CBEFriendModel *model = self.friendsList[i];
        NSString *lowStr = [ChineseToPinyin pinyinFromChineseString:keyword withSpace:NO ];
        NSString *allStr =[ChineseToPinyin pinyinFromChineseString:model.userName withSpace:NO];
        BOOL isHas = [allStr isEqualToString:lowStr];
        BOOL isPY = false;
        if ( lowStr || lowStr.length != 0) {
            isPY = [allStr hasPrefix:lowStr];
        }
        if (isHas) {
            //这种情况是精确查找。
            [self.dataArray addObject:model];//讲搜索后的数据添加到数组中
        }else{
            if (isPY && !isHas) {
                //迷糊查找
                [self.dataArray addObject:model];
            }else{//包含数字
                if ([model.userName containsString:keyword]) {
                    [self.dataArray addObject:model];
                }
            }
        }
        if (i == self.friendsList.count - 1) {
            [self.tableView reloadData];
        }
    }
}

- (void)cancelAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CBEAddressBookListCell *cell = [CBEAddressBookListCell cellForTableView:tableView];
    CBEFriendModel *model = [self.dataArray objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CBEFriendModel *model = [self.dataArray objectAtIndex:indexPath.row];
    NIMSession *session = [NIMSession session:model.userId type:NIMSessionTypeP2P];
    CBEMessageChatController *messageVC = [[CBEMessageChatController alloc] initWithSession:session];
    [self.navigationController pushViewController:messageVC animated:YES];
}

@end

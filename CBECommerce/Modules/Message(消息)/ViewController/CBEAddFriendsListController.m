//
//  CBEAddFriendsListController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAddFriendsListController.h"
#import "LSSearchBarView.h"
#import "CBEConnectionsCell.h"
#import "CBEAddGroupListCell.h"
#import "CBEMessageVM.h"
#import "CBEFriendModel.h"
#import "CBEGroupModel.h"
#import "CBEFriendsSearchController.h"

@interface CBEAddFriendsListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *userArray;
@property (nonatomic, strong) NSArray *groupArray;

@end

@implementation CBEAddFriendsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"添加好友"];
    [self.view addSubview:self.tableView];
    [self requstRecommentData];
}
- (void)requstRecommentData {
    [CBEMessageVM recommendGroupAndFriendsComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *userList = [[data objectForKey:@"data"] objectForKey:@"userList"];
            self.userArray = [[CBEFriendModel mj_objectArrayWithKeyValuesArray:userList] mutableCopy];
            
            NSArray *groupList = [[data objectForKey:@"data"] objectForKey:@"groupList"];
            self.groupArray = [CBEGroupModel mj_objectArrayWithKeyValuesArray:groupList];
            
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return self.userArray.count;
    }else if (section == 2){
        return self.groupArray.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }else if (section == 1) {
        if (!self.userArray.count) {
            return 0;
        }
        return 40;
    }else {
        if (!self.groupArray.count) {
            return 0;
        }
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1 || section == 2) {
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 40)];
        [headView setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, kSCREEN_WIDTH - kPadding15*2, 40)];
        if (section == 1) {
            [nameLabel setText:@"你可能认识这些人"];
        }else{
            [nameLabel setText:@"你可能想加入这些群"];
        }
        [nameLabel setFont:kFont(14)];
        [nameLabel setTextColor:kMainTextColor];
        [headView addSubview:nameLabel];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(kPadding15, 39.5, kSCREEN_WIDTH - kPadding15 * 2, 0.5)];
        line.backgroundColor = kMainLineColor;
        [headView addSubview:line];
        if (section == 1) {
            if (!self.userArray.count) {
                return nil;
            }
        }else{
            if (!self.groupArray.count) {
                return nil;
            }
        }
        return headView;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 50.f;
    }else if (indexPath.section == 1) {
        return 80.f;
    }
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        LSSearchBarView *searchView = [[LSSearchBarView alloc] initWithFrame:CGRectMake(15, 9, kSCREEN_WIDTH - 30, 32) placeholder:@"搜索用户姓名、微信名、公司名称、职位" didClicked:^{
            CBEFriendsSearchController *searchController = [[CBEFriendsSearchController alloc] init];
            [self.navigationController pushViewController:searchController animated:YES];
        }];
        searchView.layer.cornerRadius = 16;
        [cell addSubview:searchView];
        
        return cell;
    }
    if (indexPath.section == 1) {
        CBEConnectionsCell *cell = [CBEConnectionsCell cellForTableView:tableView];
        CBEFriendModel *model = [self.userArray objectAtIndex:indexPath.row];
        [cell refreshUserDataWith:model];
        cell.addBlock = ^{
            //点击添加好友触发事件
            [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
            [CBEMessageVM addFriendWithMobile:model.friendId Complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                    [self.userArray removeObject:model];
                    [tableView reloadData];
                }else{
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
            }];
        };
        return cell;
    }
    CBEAddGroupListCell *cell = [CBEAddGroupListCell cellForTableView:tableView];
    CBEGroupModel *model = [self.groupArray objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}
#pragma mark - getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionIndexColor = kMainOrangeColor;
    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

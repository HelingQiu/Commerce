//
//  CBEPersonSettingController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/14.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEPersonSettingController.h"
#import "NTESSessionUtil.h"

@interface CBEPersonSettingController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NIMSession *session;

@end

@implementation CBEPersonSettingController

- (instancetype)initWithSession:(NIMSession *)session {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _session = session;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"聊天详情"];
    [self.view addSubview:self.tableView];
}

#pragma mark --
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10.f)];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self reuseSwitchCell:indexPath];
}
- (UITableViewCell *)reuseSwitchCell:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = kFont(15);
    cell.textLabel.textColor = UIColorFromRGB(0x222222);
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(kSCREEN_WIDTH - 40 - 18, 10, 40, 24);
    [btn setImage:IMAGE_NAMED(@"switch_close") forState:UIControlStateNormal];
    [btn setImage:IMAGE_NAMED(@"switch_open") forState:UIControlStateSelected];
    
    if (indexPath.row == 0) {
        NIMRecentSession *recent = [[NIMSDK sharedSDK].conversationManager recentSessionBySession:self.session];
        BOOL isTop = [NTESSessionUtil recentSessionIsMark:recent type:NTESRecentSessionMarkTypeTop];
        btn.selected = isTop;
        [cell.textLabel setText:@"置顶该会话"];
        [btn addTarget:self action:@selector(firstAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:btn];
    }else if (indexPath.row == 1) {
        BOOL isInBlackList = [[NIMSDK sharedSDK].userManager isUserInBlackList:_session.sessionId];
        btn.selected = isInBlackList;
        [cell.textLabel setText:@"加入黑名单"];
        [btn addTarget:self action:@selector(secondAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:btn];
    }else if (indexPath.row == 2){
        BOOL needNotify    = [[NIMSDK sharedSDK].userManager notifyForNewMsg:_session.sessionId];
        btn.selected = needNotify;
        [cell.textLabel setText:@"消息免打扰"];
        [btn addTarget:self action:@selector(thirdAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:btn];
    }else{
        [cell.textLabel setText:@"文件列表"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}
- (void)firstAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    NIMRecentSession *recent = [[NIMSDK sharedSDK].conversationManager recentSessionBySession:_session];
    if (sender.selected) {
        if (!recent) {
            [[NIMSDK sharedSDK].conversationManager addEmptyRecentSessionBySession:_session];
        }
        [NTESSessionUtil addRecentSessionMark:_session type:NTESRecentSessionMarkTypeTop];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSessionTopNotification object:nil];
    } else {
        if (recent) {
            [NTESSessionUtil removeRecentSessionMark:_session type:NTESRecentSessionMarkTypeTop];
            [[NSNotificationCenter defaultCenter] postNotificationName:kSessionTopNotification object:nil];
        } else {}
    }
}
- (void)secondAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [CommonUtils showHUDWithWaitingMessage:nil];
        [[NIMSDK sharedSDK].userManager addToBlackList:_session.sessionId completion:^(NSError *error) {
            [CommonUtils hideHUD];
            if (!error) {
                [CommonUtils showHUDWithMessage:@"拉黑成功" autoHide:YES];
            }else{
                [CommonUtils showHUDWithMessage:@"拉黑失败" autoHide:YES];
                sender.selected = !sender.selected;
            }
        }];
    }else{
        [CommonUtils showHUDWithWaitingMessage:nil];
        [[NIMSDK sharedSDK].userManager removeFromBlackBlackList:_session.sessionId completion:^(NSError *error) {
            [CommonUtils hideHUD];
            if (!error) {
                [CommonUtils showHUDWithMessage:@"移除黑名单成功" autoHide:YES];
            }else{
                [CommonUtils showHUDWithMessage:@"移除黑名单失败" autoHide:YES];
                sender.selected = !sender.selected;
            }
        }];
    }
}

- (void)thirdAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    [CommonUtils showHUDWithWaitingMessage:nil];
    [[NIMSDK sharedSDK].userManager updateNotifyState:sender.selected forUser:_session.sessionId completion:^(NSError *error) {
        [CommonUtils hideHUD];
        if (error) {
            [CommonUtils showHUDWithMessage:@"设置失败" autoHide:YES];
            sender.selected = !sender.selected;
        }
    }];
}
#pragma mark - getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

@end

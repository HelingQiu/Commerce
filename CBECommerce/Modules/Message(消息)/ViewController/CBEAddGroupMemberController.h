//
//  CBEAddGroupMemberController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, PageFrom) {
    PageFrom_Create,//创建群
    PageFrom_GroupSetting //群成员列表
};
@interface CBEAddGroupMemberController : CBEBaseController

@property (nonatomic, copy) NSString *groupTags;//群标签

@property (nonatomic, assign) PageFrom pageForm;//
@property (nonatomic, copy) NSString *groupId;//群成员列表进入时的群id
@property (nonatomic, strong) NSArray *inMemberList;//已经在群内的成员

@end

NS_ASSUME_NONNULL_END

//
//  CBECreateGroupController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBECreateGroupController.h"
#import "CBEPwTextField.h"
#import "CBERelevancyTagController.h"

@interface CBECreateGroupController ()

@property (nonatomic, strong) CBEPwTextField *textField;

@end

@implementation CBECreateGroupController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"发起群聊"];
    [self setupUI];
    [self setBarButton:BarButtonItemRight action:@selector(nextStep) title:@"下一步"];
}
- (void)setupUI {
    UIImageView *backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 250 * kSCREEN_WIDTH/375.0)];
    backImageView.image = IMAGE_NAMED(@"im_img");
    [self.view addSubview:backImageView];
    
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, backImageView.bottom - 34, kSCREEN_WIDTH, 22)];
    tipLabel.font = kFont(16);
    tipLabel.textColor = kMainTextColor;
    tipLabel.text = @"起个好名字，吸引更多小伙伴加入！";
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [backImageView addSubview:tipLabel];
    
    self.textField = [[CBEPwTextField alloc] initWithFrame:CGRectMake(0, backImageView.bottom + 10, kSCREEN_WIDTH, 44)];
    self.textField.placeholder = @"群名称 (2-15个字）";
    self.textField.font = kFont(15);
    self.textField.textColor = kMainBlackColor;
    self.textField.clipsToBounds = YES;
    self.textField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.textField];
}
- (void)nextStep {
    if (self.textField.text.length > 15 || self.textField.text.length < 2) {
        [CommonUtils showHUDWithMessage:@"请输入2-15字的群聊名称" autoHide:YES];
        return;
    }
    CBERelevancyTagController *tagController = [[CBERelevancyTagController alloc] init];
    [self.navigationController pushViewController:tagController animated:YES];
}
@end

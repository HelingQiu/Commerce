//
//  CBESearchConversionController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/22.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBESearchConversionController : CBEBaseController

@end

NS_ASSUME_NONNULL_END

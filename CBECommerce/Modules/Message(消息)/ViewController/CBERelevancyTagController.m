//
//  CBERelevancyTagController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBERelevancyTagController.h"
#import "CBENewsCollectionCell.h"
#import "CBEAddGroupMemberController.h"
#import "CBEHomeVM.h"

@interface CBERelevancyTagController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectView;
@property (nonatomic, strong) NSArray *tagArray;
@property (nonatomic, strong) NSMutableArray *selectArray;

@end

@implementation CBERelevancyTagController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setBarTitle:@"关联标签"];
    [self setBarButton:BarButtonItemRight action:@selector(nextStep) title:@"下一步"];
    [self setupUI];
    
    [self requestTagData];
}
- (void)setupUI {
    self.view.backgroundColor = [UIColor whiteColor];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowLayout.headerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 0);
    flowLayout.footerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 0);
    
    UICollectionView *normCollectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kPadding15,kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight - kPadding15) collectionViewLayout:flowLayout];
    normCollectView.backgroundColor =  [UIColor whiteColor];;
    normCollectView.dataSource = self;
    normCollectView.delegate = self;
    [self.view addSubview:normCollectView];
    self.collectView = normCollectView;
    [normCollectView registerNib:[UINib nibWithNibName:@"CBENewsCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"CBENewsCollectionCell"];
}

- (void)requestTagData
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEHomeVM requestActivityTagDataComplete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *resultArray = [data objectForKey:@"data"];
            self.tagArray = [resultArray copy];
            [self.collectView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)nextStep {
    if (self.selectArray.count == 0) {
        [CommonUtils showHUDWithMessage:@"至少选择一个标签" autoHide:YES];
        return;
    }
    __block NSString *tagsString = @"";
    [self.selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == self.selectArray.count - 1) {
            tagsString = [tagsString stringByAppendingString:StringFormat(@"%@",obj)];
        }else{
            tagsString = [tagsString stringByAppendingString:StringFormat(@"%@,",obj)];
        }
    }];
    CBEAddGroupMemberController *addMemberController = [[CBEAddGroupMemberController alloc] init];
    addMemberController.groupTags = tagsString;
    [self.navigationController pushViewController:addMemberController animated:YES];
}
#pragma mark --
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.tagArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kSCREEN_WIDTH - 60)/3, 44);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return kPadding15;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return kPadding15;
}
#pragma mark - 定义每个UICollectionView 的 margin
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, kPadding15, 0, kPadding15);
}
#pragma mark - 每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CBENewsCollectionCell" forIndexPath:indexPath];
    NSDictionary *body = [self.tagArray objectAtIndex:indexPath.row];
    [cell.itemLabel setText:[body objectForKey:@"name"]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = (CBENewsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.isSelected == YES) {
        [cell setItemSelect:NO];
        cell.isSelected = NO;
    }else {
        [cell setItemSelect:YES];
        cell.isSelected = YES;
    }
    NSDictionary *body = [self.tagArray objectAtIndex:indexPath.row];
    NSString *tag = [body objectForKey:@"name"];
    BOOL isIn = [self.selectArray containsObject:tag];
    if (cell.isSelected) {
        if (!isIn) {
            [self.selectArray addObject:tag];
        }
    }else{
        if (isIn) {
            [self.selectArray removeObject:tag];
        }
    }
}
- (NSMutableArray *)selectArray {
    if (!_selectArray) {
        _selectArray = [NSMutableArray array];
    }
    return _selectArray;
}

@end

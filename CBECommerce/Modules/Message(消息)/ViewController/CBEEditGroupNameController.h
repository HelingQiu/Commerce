//
//  CBEEditGroupNameController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/16.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEEditGroupNameController : CBEBaseController

@property (nonatomic, copy) NSString *groupId;

@end

NS_ASSUME_NONNULL_END

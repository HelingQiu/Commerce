//
//  CBEEditGroupNameController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/16.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEEditGroupNameController.h"
#import <IQKeyboardManager.h>
#import "CBEPwTextField.h"
#import "CBEMessageVM.h"

@interface CBEEditGroupNameController ()

@property (nonatomic, strong) CBEPwTextField *textField;

@end

@implementation CBEEditGroupNameController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"修改群名称"];
    [IQKeyboardManager sharedManager].enable = NO;
    [self setBarButton:BarButtonItemRight action:@selector(nextStep) title:@"保存"];
    [self setupUI];
}
- (void)nextStep {
    if (self.textField.text.length < 2 || self.textField.text.length > 15) {
        [CommonUtils showHUDWithMessage:@"请输入2-15字的群名称" autoHide:YES];
        return;
    }
    [self.view endEditing:YES];
    [CommonUtils showHUDWithWaitingMessage:@"保存中..."];
    [CBEMessageVM editGroupNameWith:self.textField.text withGroupId:self.groupId complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"保存成功" autoHide:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:kEditGroupNameSuccessNotification object:self.textField.text];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
- (void)setupUI {
    self.textField = [[CBEPwTextField alloc] initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 44)];
    self.textField.placeholder = @"修改群名称";
    self.textField.font = kFont(15);
    self.textField.textColor = kMainBlackColor;
    self.textField.clipsToBounds = YES;
    self.textField.layer.borderWidth = 0.5;
    self.textField.layer.borderColor = kMainLineColor.CGColor;
    self.textField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.textField];
    [self.textField becomeFirstResponder];
}

@end

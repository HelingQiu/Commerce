//
//  CBEAddressSearchController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/18.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEAddressSearchController : CBEBaseController

@property (nonatomic, strong) NSArray *friendsList;

@end

NS_ASSUME_NONNULL_END

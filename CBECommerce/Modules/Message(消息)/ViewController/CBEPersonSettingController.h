//
//  CBEPersonSettingController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/14.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEPersonSettingController : CBEBaseController

- (instancetype)initWithSession:(NIMSession *)session;

@end

NS_ASSUME_NONNULL_END

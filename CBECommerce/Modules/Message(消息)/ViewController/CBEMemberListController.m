//
//  CBEMemberListController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/17.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMemberListController.h"
#import "LSSearchBarView.h"
#import "CBEMemberListCell.h"
#import "CBEGroupModel.h"
#import "NIMKitTitleView.h"
#import "CBEAddGroupMemberController.h"
#import "CBEMessageVM.h"
#import "CBEMemberSearchController.h"

@interface CBEMemberListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBEGroupMemberModel *ownerModel;

@end

@implementation CBEMemberListController
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:StringFormat(@"群成员(%lu)",(unsigned long)self.memberList.count)];
    [self.view addSubview:self.tableView];
    [self initHeaderView];
    
    self.ownerModel = [self.memberList firstObject];
    if (self.ownerModel && [self.ownerModel.userId isEqualToString:[CBEUserModel sharedInstance].userInfo.userId]) { //群主可以添加群成员
        NIMKitTitleView *titleView = (NIMKitTitleView *)self.navigationItem.titleView;
        [titleView addSubview:[self addBarButton:BarButtonItemRight action:@selector(addMember) title:@"添加"]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMemberList) name:kAddGroupMemberNotification object:nil];
    }
}
- (void)refreshMemberList {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM getGroupMemberWithGroupId:self.groupId Complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.memberList = [CBEGroupMemberModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]];
            [self setBarTitle:StringFormat(@"群成员(%lu)",(unsigned long)self.memberList.count)];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"获取群成员失败，请重试" autoHide:YES];
    }];
}
- (void)addMember {
    CBEAddGroupMemberController *addController = [[CBEAddGroupMemberController alloc] init];
    addController.pageForm = PageFrom_GroupSetting;
    addController.groupId = self.groupId;
    addController.inMemberList = self.memberList;
    [self.navigationController pushViewController:addController animated:YES];
}
- (void)initHeaderView {
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 50)];
    [headView setBackgroundColor:[UIColor whiteColor]];
    
    LSSearchBarView *searchView = [[LSSearchBarView alloc] initWithFrame:CGRectMake(15, 9, kSCREEN_WIDTH - 30, 32) placeholder:@"搜索" didClicked:^{
        CBEMemberSearchController *searchController = [[CBEMemberSearchController alloc] init];
        searchController.memberList = self.memberList;
        searchController.groupId = self.groupId;
        [self.navigationController pushViewController:searchController animated:YES];
    }];
    searchView.layer.cornerRadius = 16;
    [headView addSubview:searchView];
    [self.tableView setTableHeaderView:headView];
}
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.memberList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CBEMemberListCell *cell = [CBEMemberListCell cellForTableView:tableView];
    CBEGroupMemberModel *model = [self.memberList objectAtIndex:indexPath.row];
    if (indexPath.row == 0) {
        cell.deleteBtn.hidden = YES;
        cell.groupOwner.hidden = NO;
    }else{
        cell.groupOwner.hidden = YES;
        if (self.ownerModel && [self.ownerModel.userId isEqualToString:[CBEUserModel sharedInstance].userInfo.userId]) { //群主可以删除群成员
            cell.deleteBtn.hidden = NO;
            cell.deleteBlock = ^{
                [self deleteMember:model];
            };
        }else{
            cell.deleteBtn.hidden = YES;
        }
    }
    [cell refreshDataWith:model];
    
    return cell;
}
- (void)deleteMember:(CBEGroupMemberModel *)model {
    NSDictionary *params = @{@"groupId":self.groupId,
                             @"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"deleteUserId":model.userId
                             };
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM kickGroupMemberWith:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"踢出群聊成功" autoHide:YES];
            [self.memberList removeObject:model];
            [self setBarTitle:StringFormat(@"群成员(%lu)",(unsigned long)self.memberList.count)];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"踢出群成员失败，请重试" autoHide:YES];
    }];
}
#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = kMainLineColor;
    }
    return _tableView;
}
- (NSMutableArray *)memberList {
    if (!_memberList) {
        _memberList = [NSMutableArray array];
    }
    return _memberList;
}
@end

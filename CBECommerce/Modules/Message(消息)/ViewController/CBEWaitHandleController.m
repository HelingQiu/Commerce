//
//  CBEWaitHandleController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/13.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEWaitHandleController.h"
#import "CBEConnectionsCell.h"
#import "CBEAddGroupListCell.h"
#import "CBEApplyAddCell.h"
#import "CBEMessageVM.h"
#import "CBETodoModel.h"
#import "CBEFriendModel.h"
#import "CBEGroupModel.h"

@interface CBEWaitHandleController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *toDoArray;
@property (nonatomic, strong) NSMutableArray *userArray;
@property (nonatomic, strong) NSArray *groupArray;

@end

@implementation CBEWaitHandleController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"待处理事项"];
    [self.view addSubview:self.tableView];
    [self requestTodoData];
    [self requstRecommentData];
}
- (void)requestTodoData {
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId};
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM todoListWith:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *todoList = [data objectForKey:@"data"];
            self.toDoArray = [[CBETodoModel mj_objectArrayWithKeyValuesArray:todoList] mutableCopy];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
- (void)requstRecommentData {
    [CBEMessageVM recommendGroupAndFriendsComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *userList = [[data objectForKey:@"data"] objectForKey:@"userList"];
            self.userArray = [[CBEFriendModel mj_objectArrayWithKeyValuesArray:userList] mutableCopy];
            
            NSArray *groupList = [[data objectForKey:@"data"] objectForKey:@"groupList"];
            self.groupArray = [CBEGroupModel mj_objectArrayWithKeyValuesArray:groupList];
            
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.toDoArray.count;
    }else if (section == 1) {
        return self.userArray.count;
    }
    return self.groupArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }else if (section == 1) {
        if (!self.userArray.count) {
            return 0;
        }
        return 40;
    }else {
        if (!self.groupArray.count) {
            return 0;
        }
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10.f)];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1 || section == 2) {
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 40)];
        [headView setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, kSCREEN_WIDTH - kPadding15*2, 40)];
        if (section == 1) {
            [nameLabel setText:@"你可能认识这些人"];
        }else{
            [nameLabel setText:@"你可能想加入这些群"];
        }
        [nameLabel setFont:kFont(14)];
        [nameLabel setTextColor:kMainTextColor];
        [headView addSubview:nameLabel];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(kPadding15, 39.5, kSCREEN_WIDTH - kPadding15 * 2, 0.5)];
        line.backgroundColor = kMainLineColor;
        [headView addSubview:line];
        if (section == 1) {
            if (!self.userArray.count) {
                return nil;
            }
        }else{
            if (!self.groupArray.count) {
                return nil;
            }
        }
        return headView;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 68.f;
    }else if (indexPath.section == 1) {
        return 80.f;
    }
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CBEApplyAddCell *cell = [CBEApplyAddCell cellForTableView:tableView];
        CBETodoModel *model = [self.toDoArray objectAtIndex:indexPath.row];
        [cell refreshDataWith:model];
        cell.block = ^(NSInteger index) {
            [self applyFriend:model andIndex:index];
        };
        return cell;
    }
    if (indexPath.section == 1) {
        CBEConnectionsCell *cell = [CBEConnectionsCell cellForTableView:tableView];
        CBEFriendModel *model = [self.userArray objectAtIndex:indexPath.row];
        [cell refreshUserDataWith:model];
        cell.addBlock = ^{
            //点击添加好友触发事件
            [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
            [CBEMessageVM addFriendWithMobile:model.friendId Complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                    [self.userArray removeObject:model];
                    [tableView reloadData];
                }else{
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
            }];
        };
        return cell;
    }
    CBEAddGroupListCell *cell = [CBEAddGroupListCell cellForTableView:tableView];
    CBEGroupModel *model = [self.groupArray objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}
- (void)applyFriend:(CBETodoModel *)model andIndex:(NSInteger)index {
    //index == 1 同意 0拒绝
    NSDictionary *params = @{@"applyId":model.applyId,
                             @"status":(index == 1)?@"2":@"3"
                             };
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM applyFriendWith:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:(index == 1)?@"同意申请成功":@"拒绝申请成功" autoHide:YES];
            [self.toDoArray removeObject:model];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:(index == 1)?@"同意申请失败，请重试":@"拒绝申请失败，请重试" autoHide:YES];
    }];
}
#pragma mark - getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionIndexColor = kMainOrangeColor;
    }
    return _tableView;
}

@end

//
//  CBEGroupSettingController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/15.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEGroupSettingController.h"
#import "CBELeftRightLabelCell.h"
#import "CBEGroupMemberCell.h"
#import "NTESSessionUtil.h"
#import "CBEMessageVM.h"
#import "CBEGroupModel.h"
#import "CBEEditGroupNameController.h"
#import "CBEMemberListController.h"

@interface CBEGroupSettingController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NIMSession *session;
@property (nonatomic, strong) NIMTeam *team;
@property (nonatomic, strong) NSArray *memberArray;
@property (nonatomic, strong) CBEGroupMemberModel *createModel;
@property (nonatomic, strong) UILabel *logOut;

@end

@implementation CBEGroupSettingController

- (instancetype)initWithSession:(NIMSession *)session {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _session = session;
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"群设置"];
    self.team = [[[NIMSDK sharedSDK] teamManager] teamById:self.session.sessionId];
    [self.view addSubview:self.tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshGroupName:) name:kEditGroupNameSuccessNotification object:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestMemberListData];
}
- (void)requestMemberListData {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM getGroupMemberWithGroupId:self.team.teamId Complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.memberArray = [CBEGroupMemberModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]];
            self.createModel = [self.memberArray firstObject];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"获取群成员失败，请重试" autoHide:YES];
    }];
}
- (void)refreshGroupName:(NSNotification *)note {
    NSString *groupName = [note object];
    self.team.teamName = groupName;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma mark --
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else if (section == 1) {
        return 1;
    }else if (section == 2) {
        return 3;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return 134.f;
    }
    return 44.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10.f)];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, CGFLOAT_MIN)];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        CBEGroupMemberCell *cell = [CBEGroupMemberCell cellForTableView:tableView];
        [cell refreshDataWith:self.memberArray];
        cell.moreBlock = ^{
            CBEMemberListController *memberController = [[CBEMemberListController alloc] init];
            memberController.memberList = [self.memberArray copy];
            memberController.groupId = self.team.teamId;
            [self.navigationController pushViewController:memberController animated:YES];
        };
        return cell;
    }
    if (indexPath.section == 3) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"outCell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"outCell"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.logOut) {
            [self.logOut removeFromSuperview];
        }
        self.logOut = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 44)];
        if (self.createModel && [self.createModel.userId isEqualToString:[CBEUserModel sharedInstance].userInfo.userId]) {
            [self.logOut setText:@"解散群聊"];
        }else{
            [self.logOut setText:@"退出群聊"];
        }
        [self.logOut setFont:kFont(15)];
        [self.logOut setTextColor:kMainOrangeColor];
        [self.logOut setTextAlignment:NSTextAlignmentCenter];
        [cell addSubview:self.logOut];
        return cell;
    }
    CBELeftRightLabelCell *cell = [CBELeftRightLabelCell cellForTableView:tableView];
    [cell.rightLabel setTextColor:kMainBlackColor];
    cell.arrowView.hidden = YES;
    cell.line.hidden = YES;
    if (indexPath.section == 0) {
        cell.arrowView.hidden = NO;
        [cell.titleLabel setText:@"群名称"];
        [cell.rightLabel setText:self.team.teamName];
    }else if (indexPath.section == 2){
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(kSCREEN_WIDTH - 40 - 18, 10, 40, 24);
        [btn setImage:IMAGE_NAMED(@"switch_close") forState:UIControlStateNormal];
        [btn setImage:IMAGE_NAMED(@"switch_open") forState:UIControlStateSelected];
        if (indexPath.row == 0) {
            [cell.titleLabel setText:@"创建者"];
            [cell.rightLabel setText:self.createModel?self.createModel.userName:@"暂无信息"];
            cell.line.hidden = NO;
            [cell.rightLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(cell.contentView.mas_right).offset(-15);
            }];
        }else if (indexPath.row == 1) {
            [cell.titleLabel setText:@"置顶该群"];
            [cell.rightLabel setText:@""];
            cell.line.hidden = NO;
            NIMRecentSession *recent = [[NIMSDK sharedSDK].conversationManager recentSessionBySession:self.session];
            BOOL isTop = [NTESSessionUtil recentSessionIsMark:recent type:NTESRecentSessionMarkTypeTop];
            btn.selected = isTop;
            [btn addTarget:self action:@selector(firstAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
        }else if (indexPath.row == 2) {
            [cell.titleLabel setText:@"群消息免打扰"];
            [cell.rightLabel setText:@""];
            NIMTeamNotifyState state = [[NIMSDK sharedSDK].teamManager notifyStateForNewMsg:self.team.teamId];
            if (state == NIMTeamNotifyStateNone) {
                btn.selected = NO;
            }else{
                btn.selected = YES;
            }
            [btn addTarget:self action:@selector(secondAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
        }else{
            cell.arrowView.hidden = NO;
            [cell.titleLabel setText:@"群文件"];
            [cell.rightLabel setText:@""];
        }
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CBEEditGroupNameController *nameController = [[CBEEditGroupNameController alloc] init];
        nameController.groupId = self.team.teamId;
        [self.navigationController pushViewController:nameController animated:YES];
    }
    if (indexPath.section == 3) {
        [self leaveGroup];
    }
}
- (void)leaveGroup {
    if (self.createModel && [self.createModel.userId isEqualToString:[CBEUserModel sharedInstance].userInfo.userId]) {
        //解散群
        [UIAlertView ls_alertWithTitle:nil message:@"确认解散群聊?" cancelButtonName:@"取消" actionButtonName:@"确认" CallBackBlock:^(NSInteger buttonIndex) {
            if (buttonIndex) {
                [CommonUtils showHUDWithWaitingMessage:nil];
                [CBEMessageVM leaveGroupWith:self.team.teamId complete:^(id data) {
                    [CommonUtils hideHUD];
                    if ([[data objectForKey:@"success"] boolValue]) {
                        [[NIMSDK sharedSDK].teamManager dismissTeam:self.team.teamId completion:^(NSError * _Nullable error) {
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }];
                    }else{
                        [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                    }
                } fail:^(NSError *error) {
                    [CommonUtils hideHUD];
                    [CommonUtils showHUDWithMessage:@"解散失败" autoHide:YES];
                }];
                
            }
        }];
    }else{
        [UIAlertView ls_alertWithTitle:nil message:@"确认退出群聊?" cancelButtonName:@"取消" actionButtonName:@"确认" CallBackBlock:^(NSInteger buttonIndex) {
            if (buttonIndex) {
                [CommonUtils showHUDWithWaitingMessage:nil];
                [CBEMessageVM leaveGroupWith:self.team.teamId complete:^(id data) {
                    [CommonUtils hideHUD];
                    if ([[data objectForKey:@"success"] boolValue]) {
                        [[NIMSDK sharedSDK].teamManager quitTeam:self.team.teamId completion:^(NSError * _Nullable error) {
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }];
                    }else{
                        [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                    }
                } fail:^(NSError *error) {
                    [CommonUtils hideHUD];
                    [CommonUtils showHUDWithMessage:@"退出失败" autoHide:YES];
                }];
                
            }
        }];
    }
}
- (void)firstAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    NIMRecentSession *recent = [[NIMSDK sharedSDK].conversationManager recentSessionBySession:_session];
    if (sender.selected) {
        if (!recent) {
            [[NIMSDK sharedSDK].conversationManager addEmptyRecentSessionBySession:_session];
        }
        [NTESSessionUtil addRecentSessionMark:_session type:NTESRecentSessionMarkTypeTop];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSessionTopNotification object:nil];
    } else {
        if (recent) {
            [NTESSessionUtil removeRecentSessionMark:_session type:NTESRecentSessionMarkTypeTop];
            [[NSNotificationCenter defaultCenter] postNotificationName:kSessionTopNotification object:nil];
        } else {}
    }
}
- (void)secondAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [CommonUtils showHUDWithWaitingMessage:nil];
        [[NIMSDK sharedSDK].teamManager updateNotifyState:NIMTeamNotifyStateNone inTeam:self.team.teamId completion:^(NSError * _Nullable error) {
            [CommonUtils hideHUD];
            if (error) {
                [CommonUtils showHUDWithMessage:@"设置失败" autoHide:YES];
                sender.selected = !sender.selected;
            }
        }];
    }else{
        [CommonUtils showHUDWithWaitingMessage:nil];
        [[NIMSDK sharedSDK].teamManager updateNotifyState:NIMTeamNotifyStateAll inTeam:self.team.teamId completion:^(NSError * _Nullable error) {
            [CommonUtils hideHUD];
            if (error) {
                [CommonUtils showHUDWithMessage:@"设置失败" autoHide:YES];
                sender.selected = !sender.selected;
            }
        }];
    }
}
#pragma mark - getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

@end

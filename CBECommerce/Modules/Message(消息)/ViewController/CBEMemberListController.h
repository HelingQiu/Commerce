//
//  CBEMemberListController.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/17.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEMemberListController : CBEBaseController

@property (nonatomic, strong) NSMutableArray *memberList;
@property (nonatomic, copy) NSString *groupId;

@end

NS_ASSUME_NONNULL_END

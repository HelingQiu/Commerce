//
//  CBEAddressBookController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/28.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEAddressBookController.h"
#import "LSSearchBarView.h"
#import "CBEAddressBookListCell.h"
#import "CBEMessageVM.h"
#import "CBEMyGroupController.h"
#import "CBEFriendModel.h"
#import "CBEMessageChatController.h"
#import "CBEAddressSearchController.h"

@interface CBEAddressBookController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *letterArray;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSArray *dataArray;//搜索的数据源

@end

@implementation CBEAddressBookController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setBarTitle:@"通讯录"];
    self.letterArray = @[];
    [self.view addSubview:self.tableView];
    [self getMyFriends];
}
- (void)getMyFriends {
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId
                             };
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMessageVM getMyFriendsWith:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray<CBEFriendModel *> *dataArray = [CBEFriendModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]];
            
            [dataArray enumerateObjectsUsingBlock:^(CBEFriendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                FriendInfo *model = [FriendInfo MR_createEntity];
                model.userName = obj.userName;
                model.userMobile = obj.userMobile;
                model.friendId = obj.friendId;
                model.userId = obj.userId;
                model.companyName = obj.companyName;
                model.companyJobTitle = obj.companyJobTitle;
            }];
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
            self.dataArray = dataArray;
            NSDictionary *dict = [self sortObjectsAccordingToInitialWith:dataArray SortKey:@"userName"];
            self.dataSource = [dict objectForKey:@"array"];
            self.letterArray = [dict objectForKey:@"char"];
            [self.tableView reloadData];
        }else{
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    return [self.dataSource[section-1] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section != 0) {
        return [self.letterArray objectAtIndex:section-1];
    }
    return nil;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.letterArray;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 50;
    }
    return 32;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.5;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 0.5)];
    footView.backgroundColor = [UIColor whiteColor];
    if (section != 0) {
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(15, 0, kSCREEN_WIDTH-30, 0.5)];
        line.backgroundColor = UIColorFromRGB(0xE6E6E6);
        [footView addSubview:line];
    }
    return footView;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 50)];
        [headView setBackgroundColor:[UIColor whiteColor]];
        
        LSSearchBarView *searchView = [[LSSearchBarView alloc] initWithFrame:CGRectMake(15, 9, kSCREEN_WIDTH - 30, 32) placeholder:@"搜索用户姓名" didClicked:^{
            CBEAddressSearchController *searchController = [[CBEAddressSearchController alloc] init];
            searchController.friendsList = self.dataArray;
            [self.navigationController pushViewController:searchController animated:YES];
        }];
        searchView.layer.cornerRadius = 16;
        [headView addSubview:searchView];
        return headView;
    }
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 32)];
    [headView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, kSCREEN_WIDTH - kPadding15*2, 32)];
    [nameLabel setText:[self.letterArray objectAtIndex:section-1]];
    [nameLabel setFont:kFont(14)];
    [nameLabel setTextColor:kMainTextColor];
    [headView addSubview:nameLabel];
    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        [cell.imageView setImage:IMAGE_NAMED(@"im_icon_group1")];
        [cell.textLabel setText:@"我的群聊"];
        [cell.textLabel setFont:kFont(16)];
        
        return cell;
    }
    CBEAddressBookListCell *cell = [CBEAddressBookListCell cellForTableView:tableView];
    CBEFriendModel *model = [self.dataSource[indexPath.section-1] objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        CBEMyGroupController *groupController = [[CBEMyGroupController alloc] init];
        [self.navigationController pushViewController:groupController animated:YES];
    }else{
        CBEFriendModel *model = [self.dataSource[indexPath.section-1] objectAtIndex:indexPath.row];
        NIMSession *session = [NIMSession session:model.userId type:NIMSessionTypeP2P];
        CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:session];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionIndexColor = kMainOrangeColor;
    }
    return _tableView;
}
// 按首字母分组排序数组
- (NSDictionary *)sortObjectsAccordingToInitialWith:(NSArray *)willSortArr SortKey:(NSString *)sortkey {
    // 初始化UILocalizedIndexedCollation
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    //得出collation索引的数量，这里是27个（26个字母和1个#）
    NSInteger sectionTitlesCount = [[collation sectionTitles] count];
    //初始化一个数组newSectionsArray用来存放最终的数据，我们最终要得到的数据模型应该形如@[@[以A开头的数据数组], @[以B开头的数据数组], @[以C开头的数据数组], ... @[以#(其它)开头的数据数组]]
    NSMutableArray *newSectionsArray = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
    
    //初始化27个空数组加入newSectionsArray
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [newSectionsArray addObject:array];
    }
    
    NSLog(@"newSectionsArray %@ %@",newSectionsArray,collation.sectionTitles);
    
    NSMutableArray *firstChar = [NSMutableArray arrayWithCapacity:10];
    
    //将每个名字分到某个section下
    for (id Model in willSortArr) {
        //获取name属性的值所在的位置，比如"林丹"，首字母是L，在A~Z中排第11（第一位是0），sectionNumber就为11
        NSInteger sectionNumber = [collation sectionForObject:Model collationStringSelector:NSSelectorFromString(sortkey)];
        
        //把name为“林丹”的p加入newSectionsArray中的第11个数组中去
        NSMutableArray *sectionNames = newSectionsArray[sectionNumber];
        [sectionNames addObject:Model];
        
        //拿出每名字的首字母
        NSString * str= collation.sectionTitles[sectionNumber];
        [firstChar addObject:str];
        NSLog(@"sectionNumbersectionNumber %ld %@",sectionNumber,str);
    }
    
    //返回首字母排好序的数据
    NSArray *firstCharResult = [self SortFirstChar:firstChar];
    
    
    NSLog(@"firstCharResult== %@",firstCharResult);
    
    //对每个section中的数组按照name属性排序
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *personArrayForSection = newSectionsArray[index];
//        NSArray *sortedPersonArrayForSection = [collation sortedArrayFromArray:personArrayForSection collationStringSelector:NSSelectorFromString(sortkey)];
        newSectionsArray[index] = personArrayForSection;
    }
    
    //删除空的数组
    NSMutableArray *finalArr = [NSMutableArray new];
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        if (((NSMutableArray *)(newSectionsArray[index])).count != 0) {
            [finalArr addObject:newSectionsArray[index]];
        }
    }
    return @{@"array":finalArr,
             @"char":firstCharResult};
}

- (NSArray *)SortFirstChar:(NSArray *)firstChararry{
    //数组去重复
    
    NSMutableArray *noRepeat = [[NSMutableArray alloc]initWithCapacity:8];
    
    NSMutableSet *set = [[NSMutableSet alloc]initWithArray:firstChararry];
    
    [set enumerateObjectsUsingBlock:^(id obj , BOOL *stop){
        
        
        [noRepeat addObject:obj];
        
    }];
    
    //字母排序
    NSArray *resultkArrSort1 = [noRepeat sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    //把”#“放在最后一位
    NSMutableArray *resultkArrSort2 = [[NSMutableArray alloc]initWithArray:resultkArrSort1];
    if ([resultkArrSort2 containsObject:@"#"]) {
        
        [resultkArrSort2 removeObject:@"#"];
        [resultkArrSort2 addObject:@"#"];
    }
    
    return resultkArrSort2;
}

@end

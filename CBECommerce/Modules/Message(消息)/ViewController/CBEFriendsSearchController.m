//
//  CBEFriendsSearchController.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/18.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEFriendsSearchController.h"
#import "LSSearchBarView.h"
#import "CBEConnectionsCell.h"
#import "CBEFriendModel.h"
#import "CBEMessageVM.h"

@interface CBEFriendsSearchController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) LSSearchBarView *searchView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation CBEFriendsSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _searchView = [[LSSearchBarView alloc] initWithAlignment:(SearchBarAlignmentAlignmentLeft) placeholder:@"搜索用户姓名、微信名、公司名称、职位" didClicked:nil];
    [self.navigationItem.titleView addSubview:_searchView];
    [self setBarButton:(BarButtonItemRight) action:@selector(cancelAction) title:@"取消" titleColor:kMainBlueColor];
    
    _searchView.textField.placeholder = @"搜索用户姓名、微信名、公司名称、职位";
    self.textField = _searchView.textField;
    self.textField.delegate = self;
    self.textField.userInteractionEnabled = YES;
    [self.textField becomeFirstResponder];
    
    // 监控当前输入框的值
    [self.textField addTarget:self action:@selector(textFieldValueChangedAction:) forControlEvents:UIControlEventEditingChanged];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight)) style:(UITableViewStylePlain)];
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.f)];
    headView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headView;
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
        [self.tableView setSeparatorColor:[UIColor ls_colorWithHex:0xf0f2f5]];
    }
    self.extendedLayoutIncludesOpaqueBars = YES;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)textFieldValueChangedAction:(UITextField *)field
{
    NSString *keyword = field.text;
    [self searchFriendsWithKeyword:keyword];
}

- (void)searchFriendsWithKeyword:(NSString *)keyword
{
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"keywordQuery":keyword
                             };
    [CBEMessageVM searchFirendsWith:params complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *array = [data objectForKey:@"data"];
            self.dataArray = [[CBEFriendModel mj_objectArrayWithKeyValuesArray:array] mutableCopy];
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)cancelAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEConnectionsCell *cell = [CBEConnectionsCell cellForTableView:tableView];
    CBEFriendModel *model = [self.dataArray objectAtIndex:indexPath.row];
    [cell refreshUserDataWith:model];
    cell.addBlock = ^{
        //点击添加好友触发事件
        [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
        [CBEMessageVM addFriendWithMobile:model.friendId Complete:^(id data) {
            [CommonUtils hideHUD];
            if ([[data objectForKey:@"success"] boolValue]) {
                [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
//                [self.dataArray removeObject:model];
                [tableView reloadData];
            }else{
                [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
        }];
    };
    return cell;
}

@end

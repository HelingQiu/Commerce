//
//  CBEFriendModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEFriendModel : NSObject

@property (nullable, nonatomic, copy) NSString *friendId;//好友id
@property (nullable, nonatomic, copy) NSString *userMobile;//用户手机号
@property (nullable, nonatomic, copy) NSString *userName;//用户名
@property (nullable, nonatomic, copy) NSString *companyJobTitle;//职位名称
@property (nullable, nonatomic, copy) NSString *companyName;//公司名称
@property (nullable, nonatomic, copy) NSString *industry;//行业
@property (nullable, nonatomic, copy) NSString *headPhoto;//头像
@property (nullable, nonatomic, copy) NSString *showTag;//标签
@property (nullable, nonatomic, copy) NSString *userId;//用户id

@end

NS_ASSUME_NONNULL_END

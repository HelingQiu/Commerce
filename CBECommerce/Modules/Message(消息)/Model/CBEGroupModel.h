//
//  CBEGroupModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/12.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CBEGroupMemberModel;
@interface CBEGroupModel : NSObject

@property (nullable, nonatomic, copy) NSString *groupAvatar;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *remark;
@property (nullable, nonatomic, copy) NSString *teamid;
@property (nullable, nonatomic, copy) NSString *tid;
@property (nullable, nonatomic, copy) NSString *groupId;
@property (nullable, nonatomic, copy) NSString *groupOwnerMobile;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *userMobile;
@property (nullable, nonatomic, copy) NSString *createTime;
@property (nullable, nonatomic, copy) NSString *tagsName;//标签

@end
@interface CBEGroupMemberModel : NSObject

@property (nullable, nonatomic, copy) NSString *userId;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *groupId;
@property (nullable, nonatomic, copy) NSString *userMobile;
@property (nullable, nonatomic, copy) NSString *headPhoto;
@property (nullable, nonatomic, copy) NSString *memberType;
@property (nullable, nonatomic, copy) NSString *memberId;

@end
NS_ASSUME_NONNULL_END

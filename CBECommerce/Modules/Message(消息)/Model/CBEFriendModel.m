//
//  CBEFriendModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/12/11.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEFriendModel.h"

@implementation CBEFriendModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"friendId":@"id"};
}

@end

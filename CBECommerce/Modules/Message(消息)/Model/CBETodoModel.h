//
//  CBETodoModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/12/16.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBETodoModel : NSObject

@property (nullable, nonatomic, copy) NSString *applyId;
@property (nullable, nonatomic, copy) NSString *mobile;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyJobTitle;
@property (nullable, nonatomic, copy) NSString *companyName;
@property (nullable, nonatomic, copy) NSString *msg;
@property (nullable, nonatomic, copy) NSString *headPhoto;

@end

NS_ASSUME_NONNULL_END

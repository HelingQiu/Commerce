//
//  CBEMapViewController.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/15.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEMapViewController : CBEBaseController

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, copy) NSString *address;

@end

NS_ASSUME_NONNULL_END

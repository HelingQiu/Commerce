//
//  CBEActivityDetailController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEActivityModel.h"

@interface CBEActivityDetailController : CBEBaseController

@property (nonatomic, strong) CBEActivityModel *detailModel;

@end

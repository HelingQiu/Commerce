//
//  CBEActivityDetailController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEActivityDetailController.h"
#import "CBEActivityDetailFirstCell.h"
#import "CBEActivityDetailSecondCell.h"
#import "CBEActivityToolBar.h"
#import "CBEQRCodeView.h"
#import "CBEApplySureView.h"
#import "CBEApplyerEditController.h"
#import "CBEActivityDetailModel.h"
#import "CBEOrderListController.h"
#import "CBEActivityVM.h"
#import "CBENewsVM.h"
#import "CBEMessageChatController.h"
#import <UShareUI/UShareUI.h>
#import "CBEMapViewController.h"

@interface CBEActivityDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBEActivityToolBar *toolBar;
@property (nonatomic, strong) UIButton *eticketButton;
@property (nonatomic, strong) CBEQRCodeView *codeView;
@property (nonatomic, strong) CBEApplySureView *applyView;
@property (nonatomic, strong) CBEBackAlphaBlackView *alphaView;

@property (nonatomic, strong) CBEActivityDetailModel *dModel;

@property (nonatomic, assign) CGFloat fitHeight;
@property (nonatomic, assign) CGFloat timeHeight;

@end

@implementation CBEActivityDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBar];
    [self setCodeImage];
    self.timeHeight = 50.f;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.toolBar];
    [self.view addSubview:self.eticketButton];
    [self requestDetailData];
}

- (void)requestDetailData
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEActivityVM requestActivityDetailWithId:self.detailModel.actId?:@"" complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSDictionary *result = [data objectForKey:@"data"];
            self.dModel = [CBEActivityDetailModel mj_objectWithKeyValues:result];
            if (self.dModel.timeList.count > 1) {
                self.timeHeight = 24*self.dModel.timeList.count;
            }
            [self.tableView reloadData];
            if (self.dModel.isCollect) {
                self.toolBar.collectBtn.selected = YES;
            }
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)setNavBar
{
    [self setBarTitle:@"活动详情"];
    [self setBarButton:BarButtonItemRight action:@selector(shareAction) imageName:@"event_icon_share"];
}

- (void)setCodeImage
{
    self.codeView = [[CBEQRCodeView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 250)];
    self.codeView.hidden = YES;
    [self.view addSubview:self.codeView];
}

- (void)addTicketView
{
    self.alphaView = [[CBEBackAlphaBlackView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT)];
    [self.alphaView setUserInteractionEnabled:YES];
    [self.navigationController.view addSubview:self.alphaView];
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelApply)];
    [self.alphaView addGestureRecognizer:recognizer];
    
    _applyView = [[CBEApplySureView alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 100 + KiPhoneBottomNoSafeAreaDistance + 36 * self.dModel.timeList.count + 50*self.dModel.ticketList.count)];
    _applyView.payType = self.detailModel.payType;
    _applyView.timeArray = self.dModel.timeList;
    _applyView.priceArray = self.dModel.ticketList;
    [_applyView refreshConstraints];
    [self.navigationController.view addSubview:_applyView];
    __weak CBEApplySureView *weakView = _applyView;
    kWeakSelf
    _applyView.block = ^{
        [weakSelf.alphaView removeFromSuperview];
        [UIView animateWithDuration:0.3 animations:^{
            weakView.mj_y = kSCREEN_HEIGHT;
        }completion:^(BOOL finished) {
            [weakView removeFromSuperview];
        }];
    };
    
    //选完票之后下一步
    _applyView.nextBlock = ^(NSMutableDictionary * _Nonnull body,NSString *payMoney) {
        [weakSelf.alphaView removeFromSuperview];
        weakView.mj_y = kSCREEN_HEIGHT;
        [weakView removeFromSuperview];
        
        CBEApplyerEditController *editController = [[CBEApplyerEditController alloc] init];
        editController.detailModel = weakSelf.dModel;
        editController.applyParams = body;
        editController.payMoney = payMoney;
        [weakSelf.navigationController pushViewController:editController animated:YES];
    };
}
- (void)cancelApply {
    [self.alphaView removeFromSuperview];
    [UIView animateWithDuration:0.3 animations:^{
        _applyView.mj_y = kSCREEN_HEIGHT;
    }completion:^(BOOL finished) {
        [_applyView removeFromSuperview];
    }];
}
- (void)shareAction
{
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine)]];
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        // 根据获取的platformType确定所选平台进行下一步操作
        [self shareWebPageToPlatformType:platformType];
    }];
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    UIImage *image = [[[UIImage alloc] init] ls_cutLongPictureScrollViewFrom:self.tableView];
    NSLog(@"%@",image);
    self.codeView.hidden = NO;
    UIImage *bImage = [[[UIImage alloc] init] ls_makeImageWithView:self.codeView withSize:CGSizeMake(kSCREEN_WIDTH, 250)];
    
    UIImage *newImage = [UIImage ls_megreImageWithArry:@[image,bImage] andWithEdgeInset:UIEdgeInsetsZero andlineSpace:0];
    self.codeView.hidden = YES;
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //创建图片对象
    UMShareImageObject *shareObject = [UMShareImageObject shareObjectWithTitle:@"活动" descr:@"活动" thumImage:newImage];
    shareObject.shareImage = newImage;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}

- (void)ticketAction
{
    CBEOrderListController *orderListController = [[CBEOrderListController alloc] init];
    orderListController.pageType = OrderPageTypeAll;
    [self.navigationController pushViewController:orderListController animated:YES];
}

- (void)collectEvent
{
    if (self.dModel.isCollect) {
        [CBENewsVM requestCancelWithRelationId:self.detailModel.actId withActionType:2 complete:^(id data) {
            if ([[data objectForKey:@"success"] boolValue]) {
                self.toolBar.collectBtn.selected = NO;
                self.dModel.isCollect = NO;
                [CommonUtils showHUDWithMessage:@"取消收藏成功" autoHide:YES];
            }else{
                [CommonUtils showHUDWithMessage:@"取消收藏失败" autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils showHUDWithMessage:@"取消收藏失败" autoHide:YES];
        }];
    }else{
        [CBENewsVM requestAddWithRelationId:self.detailModel.actId withActionType:2 complete:^(id data) {
            if ([[data objectForKey:@"success"] boolValue]) {
                self.dModel.isCollect = YES;
                self.toolBar.collectBtn.selected = YES;
                [CommonUtils showHUDWithMessage:@"收藏成功" autoHide:YES];
            }else{
                [CommonUtils showHUDWithMessage:@"收藏失败" autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils showHUDWithMessage:@"收藏失败" autoHide:YES];
        }];
    }
}

- (void)gotoChat
{
    NIMSession *session = [NIMSession session:self.dModel.userId type:NIMSessionTypeP2P];
    CBEMessageChatController *vc = [[CBEMessageChatController alloc] initWithSession:session];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return self.fitHeight;
    }
    return 228+(kSCREEN_WIDTH - 2*kPadding15)*(180/345.0) + self.timeHeight;//50
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
    }
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        CBEActivityDetailSecondCell *cell= [CBEActivityDetailSecondCell cellForTableView:tableView];
        [cell refreshDataWith:self.dModel];
        kWeakSelf
        cell.heightBlock = ^(CGFloat kHeight) {
            weakSelf.fitHeight = kHeight;
            NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:1];
            [tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
        };
        return cell;
    }
    CBEActivityDetailFirstCell *cell = [CBEActivityDetailFirstCell cellForTableView:tableView];
    [cell refreshDataWith:self.dModel];
    
    cell.mapBlock = ^{
        CBEMapViewController *mapController = [[CBEMapViewController alloc] init];
        mapController.latitude = self.dModel.latitude;
        mapController.longitude = self.dModel.longitude;
        mapController.address = self.dModel.address;
        [self.navigationController pushViewController:mapController animated:YES];
    };
    return cell;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (CBEActivityToolBar *)toolBar
{
    if (!_toolBar) {
        _toolBar = [[CBEActivityToolBar alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight, kSCREEN_WIDTH, 49)];
        kWeakSelf
        if (self.detailModel.activeStatus > 0) {
            _toolBar.signBtn.enabled = NO;
            [_toolBar.signBtn setTitle:@"无法报名" forState:UIControlStateNormal];
        }
        _toolBar.block = ^(ActionType type) {
          //处理点击事件
            if (type == ActionType_Sign) {
                [weakSelf addTicketView];
                [UIView animateWithDuration:0.3 animations:^{
                    weakSelf.applyView.mj_y = kSCREEN_HEIGHT - (100 + KiPhoneBottomNoSafeAreaDistance + 36 * self.dModel.timeList.count + 50*self.dModel.ticketList.count);
                }];
            }else if (type == ActionType_Chat) {
                //去聊天页面
                [weakSelf gotoChat];
            }else{//收藏
                [weakSelf collectEvent];
            }
        };
    }
    return _toolBar;
}

- (UIButton *)eticketButton
{
    if (!_eticketButton) {
        _eticketButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _eticketButton.frame = CGRectMake(kSCREEN_WIDTH - 46 - kPadding15, kSCREEN_HEIGHT -KNavigationBarHeight - kTabbarHeight - kPadding15 - 46, 46, 46);
        [_eticketButton setBackgroundImage:IMAGE_NAMED(@"event_icon_ticket") forState:UIControlStateNormal];
        [_eticketButton setClipsToBounds:YES];
        [_eticketButton.layer setCornerRadius:25];
        [_eticketButton addTarget:self action:@selector(ticketAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _eticketButton;
}

@end

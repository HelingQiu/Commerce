//
//  CBEActivityVM.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEActivityVM : NSObject

+ (void)requestActivityDetailWithId:(NSString *)activityId
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed;
@end

NS_ASSUME_NONNULL_END

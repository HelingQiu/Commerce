//
//  CBEActivityVM.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEActivityVM.h"

@implementation CBEActivityVM

+ (void)requestActivityDetailWithId:(NSString *)activityId
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"id":activityId,
                             @"userId":[CBEUserModel sharedInstance].userInfo.userId
                             };
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryActivityById parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end

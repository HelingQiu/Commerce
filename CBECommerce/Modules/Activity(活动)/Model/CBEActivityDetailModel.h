//
//  CBEActivityDetailModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEActivityDetailModel : NSObject

@property (nonatomic, copy) NSString *activityId;//活动id
@property (nonatomic, copy) NSString *actiivtyTypeName;
@property (nonatomic, assign) NSInteger activeStatus;
@property (nonatomic, copy) NSString *activityTypeId;
@property (nonatomic, strong) NSArray *applyItemList;
@property (nonatomic, assign) NSInteger approveStatus;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) NSInteger payType;//付费类型（1，只交定金，2交全款）
@property (nonatomic, copy) NSString *cityName;//城市
@property (nonatomic, copy) NSString *cityCode;//城市代码
@property (nonatomic, copy) NSString *regionCode;//区域代码
@property (nonatomic, copy) NSString *regionName;//区域

@property (nonatomic, copy) NSString *picture;
@property (nonatomic, assign) NSInteger collectCount;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) NSInteger readCount;
@property (nonatomic, copy) NSString *showPriceRange;
@property (nonatomic, strong) NSArray *telationTagList;
@property (nonatomic, assign) NSInteger ticketCount;
@property (nonatomic, strong) NSArray *ticketList;
@property (nonatomic, strong) NSArray *timeList;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *userId;//活动创建人id
@property (nonatomic, assign) NSInteger currentStatus;//1报名中，2进行中
@property (nonatomic, copy) NSString *serviceTel;//主办方电话，聊天id
@property (nonatomic, assign) BOOL isCollect;//是否收藏

@end

NS_ASSUME_NONNULL_END

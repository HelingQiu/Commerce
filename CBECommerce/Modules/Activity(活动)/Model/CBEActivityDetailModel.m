//
//  CBEActivityDetailModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEActivityDetailModel.h"

@implementation CBEActivityDetailModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"activityId":@"id"};
}

@end

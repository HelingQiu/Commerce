//
//  CBEActivityDetailSecondCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

typedef void(^FitHeightBlock)(CGFloat kHeight);
@interface CBEActivityDetailSecondCell : CBEBaseTableViewCell

@property (nonatomic, copy) FitHeightBlock heightBlock;
+ (CBEActivityDetailSecondCell *)cellForTableView:(UITableView *)tableView;

@end

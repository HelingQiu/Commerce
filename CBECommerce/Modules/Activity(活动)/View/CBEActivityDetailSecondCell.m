//
//  CBEActivityDetailSecondCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEActivityDetailSecondCell.h"
#import "CBEActivityDetailModel.h"

@implementation CBEActivityDetailSecondCell
{
    UIImageView *_headView;
    UILabel *_titleLabel;
    UILabel *_detailLabel;
    UIWebView *_detailView;
}

+ (CBEActivityDetailSecondCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"activityDetailSecondCell";
    CBEActivityDetailSecondCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEActivityDetailSecondCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
//    _headView = [UIImageView new];
//    [_headView sd_setImageWithURL:[NSURL URLWithString:@"https://www.showself.com/yule/uploadfile/2017/0310/20170310045159177.jpg"] placeholderImage:IMAGE_NAMED(@"event_default")];
//    [self.contentView addSubview:_headView];
//    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.contentView.mas_left);
//        make.right.equalTo(self.contentView.mas_right);
//        make.top.equalTo(self.contentView.mas_top);
//        make.width.mas_equalTo(kSCREEN_WIDTH);
//        make.height.mas_equalTo(kHeightScale(156));
//    }];
    
//    _titleLabel = [UILabel new];
//    [_titleLabel setFont:kFont(16)];
//    [_titleLabel setTextColor:kMainBlackColor];
//    [_titleLabel setNumberOfLines:0];
//    [_titleLabel setText:@"会议概要"];
//    [self.contentView addSubview:_titleLabel];
//    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self->_headView.mas_bottom).offset(kPadding15);
//        make.left.equalTo(self->_headView.mas_left).offset(kPadding15);
//        make.right.equalTo(self->_headView.mas_right).offset(-kPadding15);
//        make.height.mas_equalTo(22);
//    }];
    
//    _detailLabel = [UILabel new];
//    [_detailLabel setFont:kFont(14)];
//    [_detailLabel setTextColor:kMainBlackColor];
//    [_detailLabel setNumberOfLines:0];
//    [_detailLabel setText:@""];
//    [self.contentView addSubview:_detailLabel];
//    [_detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self->_titleLabel.mas_bottom).offset(kPadding10);
//        make.left.equalTo(self->_headView.mas_left).offset(kPadding15);
//        make.right.equalTo(self->_headView.mas_right).offset(-kPadding15);
//        make.height.mas_equalTo(1);
//    }];
    
    _detailView = [UIWebView new];
    _detailView.scrollView.scrollEnabled = NO;
    [self.contentView addSubview:_detailView];
    [_detailView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [_detailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left).offset(kPadding15);
        make.right.equalTo(self.contentView.mas_right).offset(-kPadding15);
        make.height.mas_equalTo(300);
    }];
}

- (void)refreshDataWith:(CBEActivityDetailModel *)model
{
    NSString *htmlString = [NSString stringWithFormat:@"<html> \n" "<head> \n" "<style type=\"text/css\"> \n"
                            "body {font-size:15px;}\n"
                            "</style> \n" "</head> \n" "<body>" "<script type='text/javascript'>"
                            "window.onload = function(){\n"
                            "var $img = document.getElementsByTagName('img');\n"
                            "for(var p in  $img){\n"
                            " $img[p].style.width = '100%%';\n"
                            "$img[p].style.height ='auto'\n"
                            "}\n"
                            "}"
                            "</script>%@" "</body>" "</html>", model.content];
    [_detailView loadHTMLString:htmlString baseURL:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentSize"]) {
        CGSize fittingSize = [_detailView sizeThatFits:CGSizeZero];
        [_detailView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top);
            make.left.equalTo(self.contentView.mas_left).offset(kPadding15);
            make.right.equalTo(self.contentView.mas_right).offset(-kPadding15);
            make.height.mas_equalTo(fittingSize.height);
        }];
        
        if (self.heightBlock) {
            self.heightBlock(fittingSize.height);
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

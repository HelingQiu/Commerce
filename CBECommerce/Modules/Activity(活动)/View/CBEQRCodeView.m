//
//  CBEQRCodeView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEQRCodeView.h"
#import "LSQRCodeManager.h"

@implementation CBEQRCodeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    [self setBackgroundColor:[UIColor whiteColor]];
    
    UIImageView *codeView = [UIImageView new];
    [self addSubview:codeView];
    UIImage *codeImage = [LSQRCodeManager generateQRCodeImageWithString:@"https://www.baidu.com" imageSize:160];
    [codeView setImage:codeImage];
    
    [codeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(20);
        make.width.height.mas_equalTo(160);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    UILabel *label = [UILabel new];
    [self addSubview:label];
    label.font = kFont(14);
    label.textColor = kMainTextColor;
    label.numberOfLines = 2;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"想要了解更多出口跨境行业干货资讯长按二维码下载跨境帮APP";
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(codeView.mas_bottom).offset(10);
        make.width.mas_equalTo(224);
        make.height.mas_equalTo(40);
        make.centerX.equalTo(self.mas_centerX);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

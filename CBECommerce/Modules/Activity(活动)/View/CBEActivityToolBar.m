//
//  CBEActivityToolBar.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEActivityToolBar.h"

@implementation CBEActivityToolBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    
    CBEBaseButton *chatBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    chatBtn.frame = CGRectMake(30, 0, 42, 50);
    [chatBtn setImageViewRect:CGRectMake(9, 8, 24, 24)];
    [chatBtn setTitleLabelRect:CGRectMake(0, 33, 42, 14)];
    [chatBtn setImage:IMAGE_NAMED(@"huodong_icon_im") forState:UIControlStateNormal];
    [chatBtn setTitle:@"在线咨询" forState:UIControlStateNormal];
    [chatBtn.titleLabel setFont:kFont(10)];
    [chatBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [chatBtn setTitleColor:[UIColor ls_colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    [chatBtn addTarget:self action:@selector(chatAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:chatBtn];
    
    _collectBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    _collectBtn.frame = CGRectMake(100, 0, 40, 50);
    [_collectBtn setImageViewRect:CGRectMake(8, 8, 24, 24)];
    [_collectBtn setTitleLabelRect:CGRectMake(0, 33, 40, 14)];
    [_collectBtn setImage:IMAGE_NAMED(@"news_icon_mark") forState:UIControlStateNormal];
    [_collectBtn setImage:IMAGE_NAMED(@"huodong_icon_mark") forState:UIControlStateSelected];
    [_collectBtn setTitle:@"收藏" forState:UIControlStateNormal];
    [_collectBtn.titleLabel setFont:kFont(10)];
    [_collectBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_collectBtn setTitleColor:[UIColor ls_colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    [_collectBtn addTarget:self action:@selector(collectAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_collectBtn];
    
    _signBtn = [[CBEButton alloc] initWithFrame:CGRectZero title:@"立即报名" titleColor:[UIColor whiteColor] titleFontSize:16.0 backgroundColor:kMainBlueColor didClicked:^{
        //点击
        if (self.block) {
            self.block(ActionType_Sign);
        }
    }];
    _signBtn.layer.cornerRadius = 4;
    [self addSubview:_signBtn];
    [_signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_collectBtn.mas_right).offset(30);
        make.right.equalTo(self.mas_right).offset(-kPadding15);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
}

- (void)chatAction
{
    if (self.block) {
        self.block(ActionType_Chat);
    }
}

- (void)collectAction
{
    if (self.block) {
        self.block(ActionType_Collect);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

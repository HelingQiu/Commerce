//
//  CBEACCommonCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEACCommonCell : UIView

@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, assign) BOOL showArrow;
@property (nonatomic, copy) NSString *textStr;

@end

NS_ASSUME_NONNULL_END

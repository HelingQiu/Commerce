//
//  CBEActivityDetailFirstCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEActivityDetailFirstCell.h"
#import "CBEACCommonCell.h"
#import "CBEActivityDetailModel.h"

@implementation CBEActivityDetailFirstCell
{
    UIImageView *_headView;
    UIImageView *_statusView;
    UILabel *_statusLabel;
    UILabel *_titleLabel;
    CBEACCommonCell *_addcell;
    CBEACCommonCell *_timecell;
    CBEACCommonCell *_pricecell;
    CBEACCommonCell *_countcell;
    
}
+ (CBEActivityDetailFirstCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"activityDetailFirstCell";
    CBEActivityDetailFirstCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEActivityDetailFirstCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    _headView = [UIImageView new];
    [self.contentView addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(kPadding15);
        make.right.equalTo(self.contentView.mas_right).offset(-kPadding15);
        make.top.equalTo(self.contentView.mas_top).offset(kPadding15);
        make.width.mas_equalTo(kSCREEN_WIDTH - 2*kPadding15);
        make.height.mas_equalTo((kSCREEN_WIDTH - 2*kPadding15)*(180/345.0));
    }];
    
    _statusView = [UIImageView new];
    [_statusView setImage:IMAGE_NAMED(@"label_jinxingzhong")];
    [self.contentView addSubview:_statusView];
    [_statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_top).offset(kPadding10);
        make.left.equalTo(self.contentView.mas_left).offset(kPadding10);
        make.width.mas_equalTo(48);
        make.height.mas_equalTo(20);
    }];
    _statusLabel = [UILabel new];
    [_statusLabel setTextColor:[UIColor whiteColor]];
    [_statusLabel setFont:kFont(12)];
    [_statusLabel setTextAlignment:NSTextAlignmentCenter];
    [self.contentView addSubview:_statusLabel];
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_top).offset(kPadding10);
        make.left.equalTo(self.contentView.mas_left).offset(kPadding10);
        make.width.mas_equalTo(48);
        make.height.mas_equalTo(16);
    }];
    
    _titleLabel = [UILabel new];
    [_titleLabel setFont:kFont(16)];
    [_titleLabel setTextColor:kMainBlackColor];
    [_titleLabel setNumberOfLines:2];
    [_titleLabel setText:@""];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_bottom).offset(kPadding15);
        make.left.equalTo(self->_headView.mas_left);
        make.right.equalTo(self->_headView.mas_right);
        make.height.mas_equalTo(44);
    }];
    
    _timecell = [[CBEACCommonCell alloc] init];
    [self.contentView addSubview:_timecell];
    [_timecell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(9);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(50);
    }];
    _timecell.showArrow = NO;
    _timecell.textStr = @"";
    _timecell.imageName = @"event_icon_time";
    
    _addcell = [[CBEACCommonCell alloc] init];
    [self.contentView addSubview:_addcell];
    [_addcell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_timecell.mas_bottom);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(50);
    }];
    _addcell.showArrow = YES;
    _addcell.textStr = @"";
    _addcell.imageName = @"event_icon_address";
    
    UITapGestureRecognizer *addRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addAction)];
    [_addcell addGestureRecognizer:addRecognizer];
    
    _pricecell = [[CBEACCommonCell alloc] init];
    [self.contentView addSubview:_pricecell];
    [_pricecell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_addcell.mas_bottom);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(50);
    }];
    _pricecell.showArrow = NO;
    _pricecell.textStr = @"";
    _pricecell.imageName = @"event_icon_price";
    
    UITapGestureRecognizer *priceRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(priceAction)];
    [_pricecell addGestureRecognizer:priceRecognizer];
    
    _countcell = [[CBEACCommonCell alloc] init];
    [self.contentView addSubview:_countcell];
    [_countcell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_pricecell.mas_bottom);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(50);
    }];
    _countcell.showArrow = NO;
    _countcell.textStr = @"";
    _countcell.imageName = @"event_icon_num";
}

- (void)refreshDataWith:(CBEActivityDetailModel *)model
{
    [_headView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"event_default")];
    [_titleLabel setText:model.title];
    if (model.activeStatus == 0) {
        [_statusLabel setText:@"报名中"];
        [_statusView setImage:IMAGE_NAMED(@"label_baomingzhong")];
    }else if (model.activeStatus == 1){
        [_statusLabel setText:@"进行中"];
        [_statusView setImage:IMAGE_NAMED(@"label_jinxingzhong")];
    }else{
        _statusView.hidden = YES;
        _statusLabel.hidden = YES;
    }
    
    __block NSString *timeString = @"";
    NSArray *timeArray = model.timeList;
    if (timeArray.count > 1) {
        [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[CBEACCommonCell class]]) {
                [obj removeFromSuperview];
            }
        }];
        CGFloat originY = 9;
        for (int i = 0; i<timeArray.count; i++) {
            CBEACCommonCell *cell = [[CBEACCommonCell alloc] init];
            [self.contentView addSubview:cell];
            [cell mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self->_titleLabel.mas_bottom).offset(originY);
                make.left.right.equalTo(self.contentView);
                make.height.mas_equalTo(24);
            }];
            cell.showArrow = NO;
            NSString *start = [timeArray[i] objectForKey:@"startTime"];
            NSString *end = [timeArray[i] objectForKey:@"endTime"];
            cell.textStr = StringFormat(@"%@ %@",start,end);
            cell.imageName = @"event_icon_time";
            if (i != 0) {
                cell.imageName = @"";
            }
            originY += 24;
        }
        _addcell = [[CBEACCommonCell alloc] init];
        [self.contentView addSubview:_addcell];
        [_addcell mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self->_titleLabel.mas_bottom).offset(originY);
            make.left.right.equalTo(self.contentView);
            make.height.mas_equalTo(50);
        }];
        _addcell.showArrow = YES;
        _addcell.textStr = @"";
        _addcell.imageName = @"event_icon_address";
        
        UITapGestureRecognizer *addRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addAction)];
        [_addcell addGestureRecognizer:addRecognizer];
        
        _pricecell = [[CBEACCommonCell alloc] init];
        [self.contentView addSubview:_pricecell];
        [_pricecell mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self->_addcell.mas_bottom);
            make.left.right.equalTo(self.contentView);
            make.height.mas_equalTo(50);
        }];
        _pricecell.showArrow = NO;
        _pricecell.textStr = @"";
        _pricecell.imageName = @"event_icon_price";
        
        UITapGestureRecognizer *priceRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(priceAction)];
        [_pricecell addGestureRecognizer:priceRecognizer];
        
        _countcell = [[CBEACCommonCell alloc] init];
        [self.contentView addSubview:_countcell];
        [_countcell mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self->_pricecell.mas_bottom);
            make.left.right.equalTo(self.contentView);
            make.height.mas_equalTo(50);
        }];
        _countcell.showArrow = NO;
        _countcell.textStr = @"";
        _countcell.imageName = @"event_icon_num";
        
    }else{
        [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *start = [obj objectForKey:@"startTime"];
            NSString *end = [obj objectForKey:@"endTime"];
            timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
            if (idx == timeArray.count - 1) {
                [self->_timecell setTextStr:timeString];
            }
        }];
    }
    
    [_addcell setTextStr:StringFormat(@"%@%@%@",model.cityName?:@"",model.regionName?:@"",model.address?:@"")];
    [_pricecell setTextStr:model.showPriceRange];
    [_countcell setTextStr:StringFormat(@"已报名人数%ld，不限名额",model.ticketCount)];
}

- (void)addAction
{
    NSLog(@"addr action");
    if (self.mapBlock) {
        self.mapBlock();
    }
}

- (void)priceAction
{
    NSLog(@"price action");
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEActivityDetailFirstCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

typedef void(^MapBlock)(void);
@interface CBEActivityDetailFirstCell : CBEBaseTableViewCell

+ (CBEActivityDetailFirstCell *)cellForTableView:(UITableView *)tableView;
@property (nonatomic, copy) MapBlock mapBlock;

@end

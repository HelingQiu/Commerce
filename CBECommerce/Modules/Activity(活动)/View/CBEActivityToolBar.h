//
//  CBEActivityToolBar.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/23.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, ActionType) {
    ActionType_Chat = 0,
    ActionType_Collect,
    ActionType_Sign,
};
typedef void(^ChatActionBlock)(ActionType type);

@interface CBEActivityToolBar : UIView

@property (nonatomic, strong) CBEBaseButton *collectBtn;
@property (nonatomic, copy) ChatActionBlock block;
@property (nonatomic, strong) CBEButton *signBtn;//报名按钮

@end

NS_ASSUME_NONNULL_END

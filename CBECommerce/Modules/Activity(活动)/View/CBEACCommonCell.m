//
//  CBEACCommonCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEACCommonCell.h"

@implementation CBEACCommonCell
{
    UIImageView *_leftView;
    UILabel *_textLabel;
    UIImageView *_arrowView;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.userInteractionEnabled = YES;
    
    _leftView = [UIImageView new];
    [self addSubview:_leftView];
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(24);
        make.left.equalTo(self.mas_left).offset(kPadding15);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _textLabel = [UILabel new];
    [_textLabel setFont:kFont(14)];
    [_textLabel setTextColor:kMainTextColor];
    [_textLabel setText:@""];
    [self addSubview:_textLabel];
    [_textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self->_leftView.mas_right).offset(10);
        make.right.equalTo(self.mas_right).offset(-kPadding10-25);
        make.height.mas_equalTo(17);
    }];
    
    _arrowView = [UIImageView new];
    [self addSubview:_arrowView];
    [_arrowView setImage:IMAGE_NAMED(@"arrow_right")];
    [_arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.width.height.mas_equalTo(20);
        make.right.equalTo(self.mas_right).offset(-kPadding10);
    }];
}

- (void)setImageName:(NSString *)imageName
{
    [_leftView setImage:IMAGE_NAMED(imageName)];
}

- (void)setShowArrow:(BOOL)showArrow
{
    if (!showArrow) {
        _arrowView.hidden = YES;
    }
}

- (void)setTextStr:(NSString *)textStr
{
    [_textLabel setText:textStr];
}


@end

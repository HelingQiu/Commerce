//
//  CBEApplyPriceCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/25.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"
#import <PPNumberButton.h>
NS_ASSUME_NONNULL_BEGIN

@interface CBEApplyPriceCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet PPNumberButton *plusBtn;

+ (CBEApplyPriceCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

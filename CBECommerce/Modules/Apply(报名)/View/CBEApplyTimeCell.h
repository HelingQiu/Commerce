//
//  CBEApplyTimeCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/8.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEApplyTimeCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

+ (CBEApplyTimeCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

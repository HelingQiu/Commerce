//
//  CBEApplySureView.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^CloseActionBlock)(void);
typedef void(^NextActionBlock)(NSMutableDictionary *body,NSString *payMoney);

@interface CBEApplySureView : UIView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) NSInteger payType;//付费类型（1，只交定金，2交全款）
@property (nonatomic, strong) NSArray *timeArray;
@property (nonatomic, strong) NSArray *priceArray;
@property (nonatomic, copy) CloseActionBlock block;
@property (nonatomic, copy) NextActionBlock nextBlock;

@property (nonatomic, strong) NSIndexPath *selectIndexPath;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) CGFloat money;

- (void)refreshConstraints;

@end

NS_ASSUME_NONNULL_END

//
//  CBEApplyInfoCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEApplyInfoCell : CBEBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

+ (CBEApplyInfoCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

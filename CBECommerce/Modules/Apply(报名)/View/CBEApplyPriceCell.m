//
//  CBEApplyPriceCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/25.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEApplyPriceCell.h"

@implementation CBEApplyPriceCell

+ (CBEApplyPriceCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"applyPriceCell";
    CBEApplyPriceCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEApplyPriceCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(NSDictionary *)model
{
    NSString *price = StringFormat(@"%@：%@元",[model objectForKey:@"name"],[model objectForKey:@"price"]);
    [self.priceLabel setText:price];
    NSString *countStr = StringFormat(@"(剩余%@)",[model objectForKey:@"count"]);
    if ([[model objectForKey:@"count"] integerValue] == -1) {
        countStr = @"(不限票数)";
//        [self.plusBtn setMaxValue:100];
    }else{
        [self.plusBtn setMaxValue:[[model objectForKey:@"count"] integerValue]];
    }
    [self.countLabel setText:countStr];
    [self.plusBtn setMinValue:0];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEApplyTimeCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/8.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEApplyTimeCell.h"

@implementation CBEApplyTimeCell

+ (CBEApplyTimeCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEApplyTimeCell";
    CBEApplyTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEApplyTimeCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (IBAction)selectAction:(UIButton *)sender {
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

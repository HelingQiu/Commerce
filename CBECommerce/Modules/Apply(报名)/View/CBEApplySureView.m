//
//  CBEApplySureView.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEApplySureView.h"
#import "CBEOrderBottomCell.h"
#import "CBEApplyPriceCell.h"
#import "CBEApplyTimeCell.h"

@implementation CBEApplySureView
{
    CBEOrderBottomCell *_bottomView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    UILabel *topLabel = [UILabel new];
    [self addSubview:topLabel];
    [topLabel setText:@"请选择票的类型及数量"];
    [topLabel setTextColor:kMainSubTextColor];
    [topLabel setFont:kFont(14)];
    [topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(kPadding15);
        make.top.equalTo(self.mas_top).offset(kPadding15);
        make.width.mas_equalTo(160);
        make.height.mas_equalTo(22);
    }];
    
    UIButton *closeBtn = [UIButton new];
    [self addSubview:closeBtn];
    [closeBtn setBackgroundImage:IMAGE_NAMED(@"order_icon_close") forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topLabel.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-kPadding15);
        make.width.height.mas_equalTo(24);
    }];
    
    _bottomView = [CBEOrderBottomCell createBottomView];
    _bottomView.frame = CGRectMake(0, self.height - KiPhoneBottomNoSafeAreaDistance - 50, self.width, 50);
    _bottomView.orderType = OrderActionTypeApply;
    [self addSubview:_bottomView];
    
    _money = 0.0;
    [_bottomView refreshPayCount:_money];
    kWeakSelf
    _bottomView.actionBlock = ^(OrderActionType type) {
        //立即报名
        [weakSelf endEditing:YES];
        if (weakSelf.nextBlock) {
            NSMutableDictionary *body = [NSMutableDictionary dictionary];
            NSString *timeId = [[weakSelf.timeArray objectAtIndex:weakSelf.selectIndexPath.row] objectForKey:@"id"];
            
            NSMutableArray *applyTicketList = [NSMutableArray array];
            [weakSelf.priceArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CBEApplyPriceCell *cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:1]];
                NSDictionary *ticketDict = [weakSelf.priceArray objectAtIndex:idx];
                if (cell.plusBtn.currentNumber > 0) {
                    NSDictionary *dict = @{@"ticketId":[ticketDict objectForKey:@"id"]?:@"",
                                           @"name":[ticketDict objectForKey:@"name"]?:@"",
                                           @"price":[ticketDict objectForKey:@"price"]?:@"",
                                           @"activityId":[ticketDict objectForKey:@"activityId"]?:@"", @"count":@(cell.plusBtn.currentNumber)
                                           };
                    [applyTicketList addObject:dict];
                }
            }];
            [body setObject:timeId forKey:@"timeId"];//报名时间id
            [body setObject:applyTicketList forKey:@"applyTicketList"];//报名票
            if (applyTicketList.count != 0) {
                weakSelf.nextBlock(body,StringFormat(@"%.2f",weakSelf.money));
            }else{
                [CommonUtils showHUDWithMessage:@"至少购买一张票才可报名" autoHide:YES];
            }
        }
    };
    
//    self.timeArray = @[@""];
//    self.priceArray = @[@""];
    _selectIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tableView];
    _tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(50);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self->_bottomView.mas_top);
    }];
}



- (void)refreshConstraints
{
    _bottomView.frame = CGRectMake(0, self.height - KiPhoneBottomNoSafeAreaDistance - 50, self.width, 50);
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(50);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self->_bottomView.mas_top);
    }];
}

- (void)closeAction
{
    if (self.block) {
        self.block();
    }
}

#pragma mark ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.timeArray.count;
    }
    return self.priceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 36.f;
    }
    return 50.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CBEApplyTimeCell *cell = [CBEApplyTimeCell cellForTableView:tableView];
        NSDictionary *body = [self.timeArray objectAtIndex:indexPath.row];
        NSString *start = [body objectForKey:@"startTime"];
        NSString *end = [body objectForKey:@"endTime"];
        NSString *timeString = StringFormat(@"%@至%@",start,end);
        [cell.timeLabel setText:timeString];
        if (_selectIndexPath == indexPath) {
            cell.selectBtn.selected = YES;
        }else{
            cell.selectBtn.selected = NO;
        }
        return cell;
    }
    CBEApplyPriceCell *cell = [CBEApplyPriceCell cellForTableView:tableView];
    NSDictionary *body = [self.priceArray objectAtIndex:indexPath.row];
    [cell refreshDataWith:body];
    NSString *price = StringFormat(@"%@：%@%@元",[body objectForKey:@"name"],(self.payType == 1)?@"定金":@"全款",(self.payType == 1)?[body objectForKey:@"deposit"]:[body objectForKey:@"price"]);
    [cell.priceLabel setText:price];
    cell.plusBtn.resultBlock = ^(PPNumberButton *ppBtn, CGFloat number, BOOL increaseStatus) {
        [self getTotalPrice];
        //显示剩余的票
        NSInteger count = [[body objectForKey:@"count"] integerValue];
        if (count != -1) {//不是无限票数时
            NSString *countStr = StringFormat(@"(剩余%.0f)",count-number);
            [cell.countLabel setText:countStr];
        }
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (_selectIndexPath) {
            CBEApplyTimeCell *cell = [tableView cellForRowAtIndexPath:_selectIndexPath];
            cell.selectBtn.selected = NO;
        }
        _selectIndexPath = indexPath;
        CBEApplyTimeCell *cell = [tableView cellForRowAtIndexPath:_selectIndexPath];
        cell.selectBtn.selected = YES;
    }
}

- (void)getTotalPrice
{
    _money = 0.0;
    [self.priceArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CBEApplyPriceCell *cell = [self->_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:1]];
        CGFloat nowMoney = (self.payType == 1)?[[obj objectForKey:@"deposit"] floatValue]:[[obj objectForKey:@"price"] floatValue] * cell.plusBtn.currentNumber;
        self->_money += nowMoney;
        if (idx == self.priceArray.count - 1) {
            [self->_bottomView refreshPayCount:self->_money];
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}


@end

//
//  CBEApplyInfoCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEApplyInfoCell.h"

@implementation CBEApplyInfoCell

+ (CBEApplyInfoCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"applyInfoCell";
    CBEApplyInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEApplyInfoCell" owner:self options:nil].firstObject;
    }
    cell.contentLabel.adjustsFontSizeToFitWidth = YES;
    return cell;
}
- (void)refreshDataWith:(NSDictionary *)model {
    [self.topLabel setText:[model objectForKey:@"title"]];
    [self.contentLabel setText:[model objectForKey:@"value"]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

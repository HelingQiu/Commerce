//
//  CBEApplySuccessController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEApplySuccessController : CBEBaseController

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *activityId;

@end

NS_ASSUME_NONNULL_END

//
//  CBEApplyerInfoController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEApplyerInfoController.h"
#import "CBEApplyInfoCell.h"

@interface CBEApplyerInfoController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation CBEApplyerInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBar];
    [self.view addSubview:self.tableView];
    [self loadData];
}
- (void)setNavBar
{
    [self setBarTitle:@"报名人信息"];
}
- (void)loadData
{
    self.dataSource = @[@{@"title":@"姓名",@"value":self.applyName?:@""},
                        @{@"title":@"手机",@"value":self.applyPhone?:@""}];
    [self.tableView reloadData];
}
#pragma mark - tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEApplyInfoCell *cell = [CBEApplyInfoCell cellForTableView:tableView];
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.row];
    [cell refreshDataWith:body];
    return cell;
}
#pragma mark - private
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = kMainDefaltGrayColor;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CBEApplyerInfoController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/24.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEApplyerInfoController : CBEBaseController

@property (nonatomic, copy) NSString *applyName;
@property (nonatomic, copy) NSString *applyPhone;

@end

NS_ASSUME_NONNULL_END

//
//  CBEApplyerEditController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEApplyerEditController.h"
#import "CBEOrderBottomCell.h"
#import "CBEPwTextField.h"
#import "CBEWaitPayController.h"
#import "CBEApplyVM.h"
#import "CBEApplySuccessController.h"

static CGFloat kBottomHeight = 50.f;

@interface CBEApplyerEditController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) CBEOrderBottomCell *bottomView;

@property (nonatomic, strong) NSMutableArray *editArray;

@end

@implementation CBEApplyerEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"活动"];
    self.view.backgroundColor = [UIColor whiteColor];
//    self.applyParams = [NSMutableDictionary dictionary];
    [self setupUI];
    [self setEditArrayData];
}

- (void)setupUI
{
    UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, kSCREEN_WIDTH - 30, 22)];
    topLabel.font = kFont(16);
    topLabel.text = @"请填写您的联系信息";
    topLabel.textColor = kMainTextColor;
    [self.view addSubview:topLabel];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
}

- (void)setEditArrayData
{
    self.editArray = [NSMutableArray array];
    kWeakSelf
    [self.detailModel.applyItemList enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[obj objectForKey:@"itemName"]?:@"" forKey:@"itemName"];
        [dict setObject:@"" forKey:@"itemValue"];
        [dict setObject:[obj objectForKey:@"itemProp"]?:@"" forKey:@"itemProp"];
        [weakSelf.editArray addObject:dict];
    }];
}

- (void)applySubmit
{
    //判断必填项是否已填
    __block BOOL isCanSubmit = YES;
    
    [self.detailModel.applyItemList enumerateObjectsUsingBlock:^(NSDictionary *body, NSUInteger idx, BOOL * _Nonnull stop) {
        NSInteger itemType = [[body objectForKey:@"itemType"] integerValue];
        
        NSString *itemValue = [[self.editArray objectAtIndex:idx] objectForKey:@"itemValue"];
        if (itemType == 1 && [CommonUtils isBlankString:itemValue]) {
            [CommonUtils showHUDWithMessage:StringFormat(@"请输入%@",[body objectForKey:@"itemName"]) autoHide:YES];
            isCanSubmit = NO;
        }
    }];
    if (!isCanSubmit) {
        return;
    }
    [self.applyParams setObject:self.editArray forKey:@"applyItemList"];//报名对象
    [self.applyParams setObject:self.detailModel.userId forKey:@"activityUserId"];//活动创建人id
    [self.applyParams setObject:self.detailModel.activityId forKey:@"activityId"];//活动ID
    [self.applyParams setObject:self.detailModel.title forKey:@"activityName"];//活动名称
    [self.applyParams setObject:[CBEUserModel sharedInstance].userInfo.userId forKey:@"userId"];//用户ID
    
    [CommonUtils showHUDWithWaitingMessage:@"正在报名"];
    [CBEApplyVM activityApply:self.applyParams complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            const float EPSINON = 0.00001;
            float money = [self.payMoney floatValue];
            if ((money >= - EPSINON) && (money <= EPSINON)){//免费票
                //报名成功页面
                CBEApplySuccessController *successController = [[CBEApplySuccessController alloc] initWithNibName:@"CBEApplySuccessController" bundle:nil];
                successController.orderId = [data objectForKey:@"data"];
                successController.activityId = self.detailModel.activityId;
                [self.navigationController pushViewController:successController animated:YES];
            }else{
                CBEOrderListModel *listModel = [[CBEOrderListModel alloc] init];
                listModel.orderId = [data objectForKey:@"data"];
                CBEWaitPayController *payController = [[CBEWaitPayController alloc] init];
                payController.orderModel = listModel;
                [self.navigationController pushViewController:payController animated:YES];
            }
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
        
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
    
    
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.detailModel.applyItemList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10.f)];
    footView.backgroundColor = [UIColor whiteColor];
    return footView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"editCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *body = [self.detailModel.applyItemList objectAtIndex:indexPath.section];
    NSString *itemName = [body objectForKey:@"itemName"];
    NSInteger itemType = [[body objectForKey:@"itemType"] integerValue];
    if (itemType == 1) {
        [itemName stringByAppendingString:@"(必填)"];
    }
    
    CBEPwTextField *field = [[CBEPwTextField alloc] initWithFrame:CGRectMake(20, 0, kSCREEN_WIDTH - 40, 44)];
    [field addTarget:self action:@selector(textEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    field.delegate = self;
    field.backgroundColor = [UIColor ls_colorWithHexString:@"F5F5F5"];
    field.placeholder = itemName;
    field.clipsToBounds = YES;
    field.layer.cornerRadius = 4;
    field.tag = indexPath.section;
    [cell addSubview:field];
    
    NSString *fieldValue = [[self.editArray objectAtIndex:indexPath.section] objectForKey:@"itemValue"];
    if (![CommonUtils isBlankString:fieldValue]) {
        field.text = fieldValue;
    }
    
    return cell;
}

- (void)textEditingChanged:(CBEPwTextField *)field
{
    NSInteger row = field.tag;
    NSMutableDictionary *body = [self.editArray objectAtIndex:row];
    [body setObject:field.text forKey:@"itemValue"];
    [self.editArray replaceObjectAtIndex:row withObject:body];
}

#pragma mark - private
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 52, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight -52) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (CBEOrderBottomCell *)bottomView
{
    if (!_bottomView) {
        _bottomView = [CBEOrderBottomCell createBottomView];
        _bottomView.frame = CGRectMake(0, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight, kSCREEN_WIDTH, kBottomHeight);
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.orderType = OrderActionTypeApply;
        _bottomView.payCountLabel.text = StringFormat(@"¥%@",self.payMoney);
        [_bottomView.actionBtn setTitle:@"立即报名" forState:UIControlStateNormal];
        
        kWeakSelf
        _bottomView.actionBlock = ^(OrderActionType type) {
            [weakSelf applySubmit];
        };
    }
    return _bottomView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CBEApplyerEditController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEActivityDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CBEApplyerEditController : CBEBaseController

@property (nonatomic, strong) CBEActivityDetailModel *detailModel;

@property (nonatomic, strong) NSMutableDictionary *applyParams;
@property (nonatomic, copy) NSString *payMoney;

@end

NS_ASSUME_NONNULL_END

//
//  CBEApplySuccessController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEApplySuccessController.h"
#import "CBEBaseNavigationController.h"
#import "CBEEticketController.h"
#import <UMShare/UMShare.h>

@interface CBEApplySuccessController ()

@end

@implementation CBEApplySuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"报名成功"];
    [self.backBtn setImage:IMAGE_NAMED(@"login_close") forState:UIControlStateNormal];
}
- (IBAction)seeAction:(id)sender {
    
    CBEEticketController *ticketController = [[CBEEticketController alloc] init];
    ticketController.orderId = self.orderId;
    ticketController.index = 0;
    [self.navigationController pushViewController:ticketController animated:YES];
}
- (IBAction)wxShare:(id)sender {
    [self shareWebPageToPlatformType:UMSocialPlatformType_WechatSession];
}

- (IBAction)timeLineShare:(id)sender {
    [self shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine];
}

- (IBAction)qqShare:(id)sender {
    [self shareWebPageToPlatformType:UMSocialPlatformType_QQ];
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:@"活动分享" descr:@"跨境帮活动分享" thumImage:[UIImage imageNamed:@"AppIcon"]];
    //设置网页地址
    shareObject.webpageUrl = StringFormat(@"http://39.108.175.213/activity/detail.html?id=%@",self.activityId);
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}

- (void)backButtonAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

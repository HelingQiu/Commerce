//
//  CBEApplyVM.h
//  CBECommerce
//
//  Created by don on 2018/11/11.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEApplyVM : NSObject

//报名提交
+ (void)activityApply:(NSDictionary *)params
             complete:(CBENetWorkSucceed)successed
                 fail:(CBENetWorkFailure)failed;

@end

NS_ASSUME_NONNULL_END

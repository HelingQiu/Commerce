//
//  CBEApplyVM.m
//  CBECommerce
//
//  Created by don on 2018/11/11.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEApplyVM.h"

@implementation CBEApplyVM

//报名提交
+ (void)activityApply:(NSDictionary *)params
             complete:(CBENetWorkSucceed)successed
                 fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
//
//    [[CBENetworkManager sharedInstance] getRequestWithUrl:kActivityApply parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager POST:StringFormat(@"%@%@",Host,kActivityApply) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

@end

//
//  CBEAIndustyDictrectController.m
//  CBECommerce
//
//  Created by Rainer on 2019/2/23.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBEAIndustyDictrectController.h"
#import "CBENewsCollectionCell.h"
#import "CBEConnectVM.h"
#import "CBEBaseTabBarController.h"
#import "CBELoginVM.h"

@interface CBEAIndustyDictrectController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, copy) NSString *selectDirect;

@end

@implementation CBEAIndustyDictrectController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self requestDataDirect];
}

- (void)setupUI {
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 94, kSCREEN_WIDTH, 25)];
    lab1.text = @"行业方向";
    lab1.textColor = UIColorFromRGB(0x4A4A4A);
    lab1.textAlignment = NSTextAlignmentCenter;
    lab1.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:lab1];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowLayout.headerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 0);
    flowLayout.footerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 110);
    
    UICollectionView *normCollectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, lab1.bottom + 30,kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight - (lab1.bottom + 30)) collectionViewLayout:flowLayout];
    normCollectView.backgroundColor =  [UIColor whiteColor];;
    normCollectView.dataSource = self;
    normCollectView.delegate = self;
    [self.view addSubview:normCollectView];
    self.collectView = normCollectView;
    [normCollectView registerNib:[UINib nibWithNibName:@"CBENewsCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"CBENewsCollectionCell"];
    [normCollectView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"CBEFooterReusableView"];
}
- (void)requestDataDirect {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEConnectVM getIndustryWith:@"1" keyword:nil complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.dataSource = [data objectForKey:@"data"];
            [self.collectView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
- (void)nextStep {
    if ([CommonUtils isBlankString:self.selectDirect]) {
        [CommonUtils showHUDWithMessage:@"请选择行业方向" autoHide:YES];
        return;
    }
    //登录
    [CommonUtils showHUDWithWaitingMessage:nil];
    NSDictionary *params = @{@"industry":self.industry,
                             @"industryDirection":self.selectDirect
                             };
    [CBELoginVM updateIndustryWith:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"设置成功" autoHide:YES];
            [self presentHomePage];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请稍后重试" autoHide:YES];
    }];
}
- (void)presentHomePage {
    CBEBaseTabBarController *tabBar = [[CBEBaseTabBarController alloc] init];
    [[UIApplication sharedApplication] keyWindow].rootViewController = tabBar;
}
#pragma mark --
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kSCREEN_WIDTH - 23 * 2 - kPadding15 * 2)/3, 44);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 23;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 23;
}
#pragma mark - 定义每个UICollectionView 的 margin
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, kPadding15, 0, kPadding15);
}
#pragma mark - 每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CBENewsCollectionCell" forIndexPath:indexPath];
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.row];
    [cell.itemLabel setText:[body objectForKey:@"name"]];
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"CBEFooterReusableView" forIndexPath:indexPath];
        
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.frame = CGRectMake(33, 110 - 50, kSCREEN_WIDTH - 66, 50);
        nextBtn.backgroundColor = kMainBlueColor;
        nextBtn.layer.cornerRadius = 4;
        [nextBtn setTitle:@"完成" forState:UIControlStateNormal];
        [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [nextBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [nextBtn addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:nextBtn];
        return footView;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = (CBENewsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setItemSelect:YES];
    cell.isSelected = YES;
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.row];
    NSString *tag = [body objectForKey:@"name"];
    self.selectDirect = tag;
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = (CBENewsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setItemSelect:NO];
    cell.isSelected = NO;
    NSLog(@"1第%ld区，1第%ld个",(long)indexPath.section,(long)indexPath.row);
    self.selectDirect = nil;
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell =  (CBENewsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setItemSelect:NO];
    cell.isSelected = NO;
}


@end

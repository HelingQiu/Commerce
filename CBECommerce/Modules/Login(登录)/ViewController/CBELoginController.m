//
//  CBELoginController.m
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/7/30.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBELoginController.h"
#import "CBEBaseTabBarController.h"
#import "CBEButton.h"
#import "CBEMobileLoginController.h"
#import <UMShare/UMShare.h>
#import "CBELoginVM.h"
#import "CBEMobileBindController.h"
#import "CBEAIndustryController.h"

@interface CBELoginController ()

@property(nonatomic,strong)UIImageView *bgImageView;//背景图
@property(nonatomic,strong)UIImageView *logoView;
@property(nonatomic,strong)CBEButton *btnWXLogin;//微信登录按钮
@property(nonatomic,strong)UILabel *midLabel;
@property(nonatomic,strong)CBEButton *btnPhoneLogin;//手机登录按钮
@property(nonatomic,strong)CBEButton *btnClose;//关闭按钮

@end

@implementation CBELoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.backBtn.hidden = YES;
    [self.view addSubview:self.bgImageView];
    [self.bgImageView addSubview:self.logoView];
    [self.bgImageView addSubview:self.btnClose];
    [self.bgImageView addSubview:self.btnWXLogin];
    [self.bgImageView addSubview:self.midLabel];
    [self.bgImageView addSubview:self.btnPhoneLogin];
    [self setupBottomUI];
}
- (void)toHomePage {
    [self closeLoginController];
}
- (void)setupBottomUI {
    UIButton *bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottomBtn setTitle:@"随便逛逛" forState:UIControlStateNormal];
    bottomBtn.titleLabel.font = kFont(14);
    [bottomBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [bottomBtn addTarget:self action:@selector(toHomePage) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImageView addSubview:bottomBtn];
    [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgImageView.mas_bottom).offset(-kPadding15);
        make.right.equalTo(self.bgImageView.mas_right).offset(-kPadding15);
        make.height.mas_equalTo(30);
    }];
}
- (void)toSetIndustry {
    CBEAIndustryController *industryVC = [[CBEAIndustryController alloc] init];
    [self presentViewController:industryVC animated:YES completion:nil];
}
//微信登录
- (void)getAuthWithUserInfoFromWechat
{
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        if (error) {
            [CommonUtils showHUDWithMessage:@"微信授权失败" autoHide:YES];
        } else {
            UMSocialUserInfoResponse *resp = result;
            // 授权信息
            NSLog(@"Wechat uid: %@", resp.uid);
            NSLog(@"Wechat openid: %@", resp.openid);
            NSLog(@"Wechat unionid: %@", resp.unionId);
            NSLog(@"Wechat accessToken: %@", resp.accessToken);
            NSLog(@"Wechat refreshToken: %@", resp.refreshToken);
            NSLog(@"Wechat expiration: %@", resp.expiration);
            // 用户信息
            NSLog(@"Wechat name: %@", resp.name);
            NSLog(@"Wechat iconurl: %@", resp.iconurl);
            NSLog(@"Wechat gender: %@", resp.unionGender);
            // 第三方平台SDK源数据
            NSLog(@"Wechat originalResponse: %@", resp.originalResponse);
            
            //获取到信息后调后台接口 判断是否绑定手机号
            [CommonUtils showHUDWithWaitingMessage:@"登录中..."];
            [CBELoginVM wxDoLoginWithWxId:resp.unionId withWxName:resp.name withWxPhoto:resp.iconurl complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    NSDictionary *result = [data objectForKey:@"data"];
                    
                    NSString *industry = [[result objectForKey:@"userInfo"] objectForKey:@"industry"];
                    NSString *industryDirection = [[result objectForKey:@"userInfo"] objectForKey:@"industryDirection"];
                    
                    [CBEUserModel sharedInstance].isLogin = YES;
                    [CBEUserModel sharedInstance].token = [result objectForKey:@"token"];
                    [[CBENetworkManager sharedInstance] setHttpField];
                    [CBEUserModel sharedInstance].userInfo = [CBEUserInfo mj_objectWithKeyValues:[result objectForKey:@"userInfo"]];
                    
                    //云信登录
                    [[[NIMSDK sharedSDK] loginManager] login:[CBEUserModel sharedInstance].userInfo.userId
                                                       token:[CBEUserModel sharedInstance].userInfo.yxToken
                                                  completion:^(NSError *error) {
                                                      
                                                      if (error == nil){
                                                      }else{
                                                          NSString *toast = [NSString stringWithFormat:@"登录失败 code: %zd",error.code];
                                                          NSLog(@"%@",toast);
                                                      }
                                                  }];
                    
                    //登录成功后
                    [CommonUtils showHUDWithMessage:@"登录成功" autoHide:YES];
                    if ([CommonUtils isBlankString:industry] || [CommonUtils isBlankString:industryDirection]) {
                        [self toSetIndustry];
                    }else{
                        [self presentHomePage];
                    }
                }else{
                    NSInteger errorCode = [[data objectForKey:@"errorCode"] integerValue];
                    if (errorCode == 505) {//去绑定手机号
                        CBEMobileBindController *bindController = [[CBEMobileBindController alloc] init];
                        bindController.resp = resp;
                        [self presentViewController:bindController animated:YES completion:nil];
                    }else{
                        [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                    }
                }
            } fail:^(NSError *error) {
                [CommonUtils hideHUD];
                [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
            }];
            [self presentHomePage];
        }
    }];
}

- (UIImageView *)bgImageView{
    
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = IMAGE_NAMED(@"login_bg");
        [self.view addSubview:_bgImageView];
        [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.mas_equalTo(self.view);
        }];
        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}

- (UIImageView *)logoView
{
    if (!_logoView) {
        _logoView = [UIImageView new];
        _logoView.image = IMAGE_NAMED(@"login_logo");
        [self.bgImageView addSubview:_logoView];
        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bgImageView).offset(kStatusHeight+110);
            make.width.height.mas_equalTo(100);
            make.centerX.equalTo(self.bgImageView.mas_centerX);
        }];
        _logoView.userInteractionEnabled = YES;
    }
    return _logoView;
}

- (CBEButton *)btnWXLogin{
    
    if (!_btnWXLogin) {
        
        _btnWXLogin = [[CBEButton alloc] initWithFrame:CGRectZero title:@"微信快速登录" titleColor:[UIColor whiteColor] titleFontSize:16.0 backgroundColor:kMainBlueColor didClicked:^{
            //登录成功后
            [self getAuthWithUserInfoFromWechat];
        }];
        _btnWXLogin.layer.cornerRadius = 4;
        [_btnWXLogin setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self.bgImageView addSubview:_btnWXLogin];
        [_btnWXLogin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom).offset(-204.0f);
            make.width.mas_equalTo(kSCREEN_WIDTH - 60);
            make.height.mas_equalTo(kHalf(100.0f));
            make.centerX.mas_equalTo(self.view.mas_centerX);
        }];
    }
    return _btnWXLogin;
}

- (UILabel *)midLabel
{
    if (!_midLabel) {
        _midLabel = [[UILabel alloc] init];
        _midLabel.textAlignment = NSTextAlignmentCenter;
        _midLabel.text = @"或";
        _midLabel.textColor = kMainTextColor;
        _midLabel.font = kFont(14);
        [self.bgImageView addSubview:_midLabel];
        [_midLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.btnWXLogin.mas_bottom).offset(20);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(kHalf(40.0f));
            make.centerX.mas_equalTo(self.view.mas_centerX);
        }];
    }
    return _midLabel;
}

- (CBEButton *)btnPhoneLogin{
    
    if (!_btnPhoneLogin) {
        
        _btnPhoneLogin = [[CBEButton alloc] initWithFrame:CGRectZero title:@"手机登录" titleColor:kMainBlueColor titleFontSize:14.0 backgroundColor:[UIColor clearColor] didClicked:^{
            [self.view endEditing:YES];
            //手机登录
            [self presentMobileLogin];
        }];
        [_btnPhoneLogin setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self.bgImageView addSubview:_btnPhoneLogin];
        [_btnPhoneLogin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.midLabel.mas_bottom).offset(20);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(kHalf(40.0f));
            make.centerX.mas_equalTo(self.view.mas_centerX);
        }];
    }
    return _btnPhoneLogin;
}

- (CBEButton *)btnClose
{
    if(!_btnClose){
        _btnClose = [[CBEButton alloc] initWithFrame:CGRectZero image:IMAGE_NAMED(@"login_close") highImage:IMAGE_NAMED(@"login_close") didClicked:^{
            [self.view endEditing:YES];
            [self closeLoginController];
        }];
        _btnClose.layer.cornerRadius = 4;
        [_btnClose setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self.bgImageView addSubview:_btnClose];
        [_btnClose mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bgImageView.mas_top).offset(kHalf(KNavigationBarHeight));
            make.width.mas_equalTo(kHalf(88.0f));
            make.height.mas_equalTo(kHalf(88.0f));
            make.left.mas_equalTo(kHalf(20.0f));
        }];
    }
    return _btnClose;
}

- (void)presentHomePage
{
    CBEBaseTabBarController *tabBar = [[CBEBaseTabBarController alloc] init];
    [[UIApplication sharedApplication] keyWindow].rootViewController = tabBar;
}

- (void)presentMobileLogin
{
    CBEMobileLoginController *mLoginController = [[CBEMobileLoginController alloc] init];
    [self presentViewController:mLoginController animated:YES completion:nil];
}

- (void)closeLoginController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

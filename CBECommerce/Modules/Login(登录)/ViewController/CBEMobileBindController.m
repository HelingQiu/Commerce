//
//  CBEMobileBindController.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMobileBindController.h"
#import "CBEBaseTabBarController.h"
#import "CBELoginVM.h"
#import "CBEAIndustryController.h"

@interface CBEMobileBindController ()<UITextFieldDelegate>

@property(nonatomic,strong)UIView * inputView;//用户名、密码输入白色背景
@property(nonatomic,strong)UIImageView * bgImageView;//背景图
@property(nonatomic,strong)UIImageView *logoView;
@property(nonatomic,strong)CBEButton * btnLogin;//登录按钮
@property(nonatomic,strong)CBEButton *btnClose;//关闭按钮
@property(nonatomic,strong)UITextField *mobileField;
@property(nonatomic,strong)UITextField *codeField;
@property(nonatomic,strong)CBEButton *codeButton;

@property(nonatomic, assign) int countTime;
@property(nonatomic, strong) NSTimer *timer;

@end

@implementation CBEMobileBindController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)setupUI
{
    [self.view addSubview:self.bgImageView];
    [self.bgImageView addSubview:self.logoView];
    //    [self.bgImageView addSubview:self.inputView];
    [self.bgImageView addSubview:self.btnLogin];
    [self.bgImageView addSubview:self.btnClose];
    self.btnLogin.enabled = YES;
    
    _mobileField = [[UITextField alloc] init];
    _mobileField.placeholder = @"绑定手机号";
    _mobileField.font = kFont(16);
    _mobileField.keyboardType = UIKeyboardTypeNumberPad;
    _mobileField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_mobileField addTarget:self action:@selector(textEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    _mobileField.delegate = self;
    [self.bgImageView addSubview:_mobileField];
    [_mobileField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logoView.mas_bottom).offset(58);
        make.width.mas_equalTo(kSCREEN_WIDTH - 60);
        make.height.mas_equalTo(kHalf(44.0f));
        make.centerX.equalTo(self.logoView.mas_centerX);
    }];
    
    UIView *line1 = [UIView new];
    line1.backgroundColor = [UIColor ls_colorWithHexString:@"E6E6E6"];
    [self.bgImageView addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_mobileField.mas_bottom).offset(kPadding10);
        make.width.mas_equalTo(kSCREEN_WIDTH - 60);
        make.centerX.equalTo(self.logoView.mas_centerX);
        make.height.mas_equalTo(0.5);
    }];
    
    _codeField = [[UITextField alloc] init];
    _codeField.placeholder = @"验证码";
    _codeField.font = kFont(16);
    _codeField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_codeField addTarget:self action:@selector(textEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.bgImageView addSubview:_codeField];
    _codeField.delegate = self;
    _codeField.keyboardType = UIKeyboardTypeNumberPad;
    [_codeField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_mobileField.mas_bottom).offset(38);
        make.width.mas_equalTo(kSCREEN_WIDTH - 60);
        make.height.mas_equalTo(kHalf(44.0f));
        make.centerX.equalTo(self.logoView.mas_centerX);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = [UIColor ls_colorWithHexString:@"E6E6E6"];
    [self.bgImageView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_codeField.mas_bottom).offset(kPadding10);
        make.width.mas_equalTo(kSCREEN_WIDTH - 60);
        make.centerX.equalTo(self.logoView.mas_centerX);
        make.height.mas_equalTo(0.5);
    }];
    
    _codeButton = [[CBEButton alloc] initWithFrame:CGRectZero title:@"获取验证码" titleColor:[UIColor whiteColor] titleFontSize:14.0 backgroundColor:kMainBlueColor didClicked:^{
        //获取验证码
        [self getVerify:_codeButton];
    }];
    _codeButton.imageView.clipsToBounds = YES;
    _codeButton.layer.cornerRadius = 4;
    [_codeButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.bgImageView addSubview:_codeButton];
    [_codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.codeField.mas_right);
        make.width.mas_equalTo(98.f);
        make.height.mas_equalTo(26.f);
        make.centerY.mas_equalTo(self.codeField.mas_centerY);
    }];
}

#pragma - action
//获取验证码
- (void)getVerify:(UIButton *)sender
{
    if ([CommonUtils isBlankString:self.mobileField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入手机号码" autoHide:YES];
        return;
    }
    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString]?:@""; NSLog(@"identifierForVendor == %@,%@",identifierForVendor,kGetSmsVerifyByImg);
    NSDictionary *params = @{@"mobile":self.mobileField.text,
                             @"hasImgCode":@"2",
                             @"onlySign":identifierForVendor
                             };
    
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    //网络请求
    [CommonUtils showHUDWithWaitingMessage:nil];
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kGetSmsVerifyByImg parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        NSDictionary *dic = [data mj_JSONObject];
        NSLog(@"%@",dic);
        [CommonUtils hideHUD];
        if ([[dic objectForKey:@"success"] integerValue] == 1) {
            //成功后
            [CommonUtils showHUDWithMessage:@"验证码发送成功，请留意短信" autoHide:YES];
            //倒计时
            self->_countTime = 59;
            [sender setTitle:[NSString stringWithFormat:@"%ds",self->_countTime] forState:UIControlStateNormal];
            sender.enabled = NO;
            [self addTimer];
        }else{
            [CommonUtils showHUDWithMessage:@"获取验证码失败，请重试" autoHide:YES];
        }
    } failure:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"获取验证码失败，请重试" autoHide:YES];
    }];
}

- (void)addTimer{
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
}

- (void)countdown {
    _countTime --;
    if (_countTime == 0) {
        
        _countTime = 60;
        
        _codeButton.enabled = YES;
        [_codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self removeTimer];
        _timer = nil;
        
    }else if(self.countTime < 10){
        _codeButton.enabled = NO;
        [_codeButton setTitle:[NSString stringWithFormat:@"0%ds",_countTime] forState:UIControlStateNormal];
    }else{
        _codeButton.enabled = NO;
        [_codeButton setTitle:[NSString stringWithFormat:@"%ds",_countTime] forState:UIControlStateNormal];
    }
}

- (void)removeTimer {
    [_timer invalidate];
    _timer = nil;
}

- (void)textEditingChanged:(UITextField *)textField {
    self.btnLogin.enabled = _mobileField.text.length > 0 && _codeField.text.length > 0;
    if (self.btnLogin.enabled) {
        self.btnLogin.backgroundColor = kMainBlueColor;
    }else{
        self.btnLogin.backgroundColor = UIColorFromRGBA(0x53C6F0, 0.5);
    }
    if (textField == _mobileField) {
        _codeField.text = nil;
    }
}

- (void)doBindMobileLogin {
    if ([CommonUtils isBlankString:self.mobileField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入手机号码" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.codeField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入短信验证码" autoHide:YES];
        return;
    }
    [CommonUtils showHUDWithWaitingMessage:@"登录中..."];
    [CBELoginVM bindWXAccountWith:self.mobileField.text withMsmCode:self.codeField.text withWxId:self.resp.unionId withWxName:self.resp.name withWxPhoto:self.resp.iconurl complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] integerValue] == 1) {
            NSDictionary *result = [data objectForKey:@"data"];
            [CBEUserModel sharedInstance].userName = self.mobileField.text;
            [CBEUserModel sharedInstance].isLogin = YES;
            [CBEUserModel sharedInstance].token = [result objectForKey:@"token"];
            [[CBENetworkManager sharedInstance] setHttpField];
            [CBEUserModel sharedInstance].userInfo = [CBEUserInfo mj_objectWithKeyValues:[result objectForKey:@"userInfo"]];
            
            //云信登录
            [[[NIMSDK sharedSDK] loginManager] login:[CBEUserModel sharedInstance].userInfo.userId
                                               token:[CBEUserModel sharedInstance].userInfo.yxToken
                                          completion:^(NSError *error) {
                                              
                                              if (error == nil){
                                              }else{
                                                  NSString *toast = [NSString stringWithFormat:@"登录失败 code: %zd",error.code];
                                                  NSLog(@"%@",toast);
                                              }
                                          }];
            //登录成功后
            [CommonUtils showHUDWithMessage:@"登录成功" autoHide:YES];
            NSString *industry = [[result objectForKey:@"userInfo"] objectForKey:@"industry"];
            NSString *industryDirection = [[result objectForKey:@"userInfo"] objectForKey:@"industryDirection"];
            if ([CommonUtils isBlankString:industry] || [CommonUtils isBlankString:industryDirection]) {
                [self toSetIndustry];
            }else{
                [self presentHomePage];
            }
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"登录失败，请重试" autoHide:YES];
    }];
}
- (void)toSetIndustry {
    CBEAIndustryController *industryVC = [[CBEAIndustryController alloc] init];
    [self presentViewController:industryVC animated:YES completion:nil];
}
- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
//        _bgImageView.image = IMAGE_NAMED(@"login_bg");
        [self.view addSubview:_bgImageView];
        [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.mas_equalTo(self.view);
        }];
        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}

- (UIImageView *)logoView {
    if (!_logoView) {
        _logoView = [UIImageView new];
        _logoView.image = IMAGE_NAMED(@"login_logo");
        [self.bgImageView addSubview:_logoView];
        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bgImageView).offset(kStatusHeight+110);
            make.width.height.mas_equalTo(100);
            make.centerX.equalTo(self.bgImageView.mas_centerX);
        }];
        _logoView.userInteractionEnabled = YES;
    }
    return _logoView;
}

-(UIView *)inputView{
    
    if (!_inputView) {
        _inputView = [UIView new];
        [self.bgImageView addSubview:_inputView];
        [_inputView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(kHalf(450.0f)/k6sHeight * kSCREEN_HEIGHT);
            CGFloat width = kSCREEN_WIDTH == 320.0f ? kSCREEN_WIDTH - 50.0f : kHalf(644.0f);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(kHalf(88.0f)*2 + kHalf(1.0f));
            make.centerX.mas_equalTo(self.view.mas_centerX);
        }];
        _inputView.layer.cornerRadius = 4;
        _inputView.backgroundColor = [UIColor whiteColor];
        
        UIView * sepLine = [UIView new];
        sepLine.backgroundColor = [UIColor lightGrayColor];
        [_inputView addSubview:sepLine];
        [sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kHalf(88.0f));
            make.left.right.equalTo(self->_inputView);
            make.height.mas_equalTo(kHalf(1.0f));
        }];
    }
    return _inputView;
}

- (CBEButton *)btnLogin {
    if (!_btnLogin) {
        _btnLogin = [[CBEButton alloc] initWithFrame:CGRectZero title:@"立即登录" titleColor:[UIColor whiteColor] titleFontSize:16.0 backgroundColor:UIColorFromRGBA(0x53C6F0, 0.5) didClicked:^{
            [self.view endEditing:YES];
            //登录成功后
            
            [self doBindMobileLogin];
        }];
        _btnLogin.layer.cornerRadius = 4;
        _btnLogin.enabled = NO;
        [self.bgImageView addSubview:_btnLogin];
        [_btnLogin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.logoView.mas_bottom).offset(247.0f);
            make.width.mas_equalTo(kSCREEN_WIDTH - 60);
            make.height.mas_equalTo(kHalf(100.0f));
            make.centerX.mas_equalTo(self.view.mas_centerX);
        }];
    }
    return _btnLogin;
}

- (CBEButton *)btnClose {
    if(!_btnClose) {
        _btnClose = [[CBEButton alloc] initWithFrame:CGRectZero image:IMAGE_NAMED(@"arrow_back") highImage:IMAGE_NAMED(@"arrow_back") didClicked:^{
            [self.view endEditing:YES];
            [self closeLoginController];
        }];
        _btnClose.layer.cornerRadius = 4;
        [_btnClose setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self.bgImageView addSubview:_btnClose];
        [_btnClose mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bgImageView.mas_top).offset(kHalf(KNavigationBarHeight));
            make.width.mas_equalTo(kHalf(88.0f));
            make.height.mas_equalTo(kHalf(88.0f));
            make.left.mas_equalTo(kHalf(20.0f));
        }];
    }
    return _btnClose;
}

- (void)presentHomePage {
    CBEBaseTabBarController *tabBar = [[CBEBaseTabBarController alloc] init];
    [[UIApplication sharedApplication] keyWindow].rootViewController = tabBar;
}

- (void)closeLoginController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CBEMobileBindController.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import <UMShare/UMShare.h>

@interface CBEMobileBindController : CBEBaseController

@property (nonatomic, strong) UMSocialUserInfoResponse *resp;

@end

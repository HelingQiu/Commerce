//
//  CBEIndustryController.m
//  CBECommerce
//
//  Created by Rainer on 2019/2/19.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBEAIndustryController.h"
#import "CBEConnectVM.h"
#import "CBENewsCollectionCell.h"
#import "CBEFooterReusableView.h"
#import "CBEAIndustyDictrectController.h"

@interface CBEAIndustryController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, copy) NSString *selectTag;

@end

@implementation CBEAIndustryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self requestTagData];
}
- (void)setupUI {
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 94, kSCREEN_WIDTH, 25)];
    lab1.text = @"您是";
    lab1.textColor = UIColorFromRGB(0x4A4A4A);
    lab1.textAlignment = NSTextAlignmentCenter;
    lab1.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:lab1];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowLayout.headerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 0);
    flowLayout.footerReferenceSize = CGSizeMake(kSCREEN_WIDTH, 162);
    
    UICollectionView *normCollectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, lab1.bottom + 30,kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight - (lab1.bottom + 30)) collectionViewLayout:flowLayout];
    normCollectView.backgroundColor =  [UIColor whiteColor];;
    normCollectView.dataSource = self;
    normCollectView.delegate = self;
    [self.view addSubview:normCollectView];
    self.collectView = normCollectView;
    [normCollectView registerNib:[UINib nibWithNibName:@"CBENewsCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"CBENewsCollectionCell"];
    [normCollectView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"CBEFooterReusableView"];
}

- (void)requestTagData {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEConnectVM getIndustryWith:@"0" keyword:nil complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            self.dataSource = [data objectForKey:@"data"];
            [self.collectView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
- (void)nextStep {
    if ([CommonUtils isBlankString:self.selectTag]) {
        [CommonUtils showHUDWithMessage:@"请选择行业" autoHide:YES];
        return;
    }
    NSLog(@"%@",self.selectTag);
    CBEAIndustyDictrectController *ditrctVC = [[CBEAIndustyDictrectController alloc] init];
    ditrctVC.industry = self.selectTag;
    [self presentViewController:ditrctVC animated:YES completion:nil];
}
#pragma mark --
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kSCREEN_WIDTH - 23 * 2 - kPadding15 * 2)/3, 44);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 23;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 23;
}
#pragma mark - 定义每个UICollectionView 的 margin
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, kPadding15, 0, kPadding15);
}
#pragma mark - 每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CBENewsCollectionCell" forIndexPath:indexPath];
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.row];
    [cell.itemLabel setText:[body objectForKey:@"name"]];
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"CBEFooterReusableView" forIndexPath:indexPath];
        UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, kSCREEN_WIDTH, 16)];
        lab1.text = @"选择您的定位，可获个性化推荐哦！";
        lab1.textColor = UIColorFromRGB(0x666666);
        lab1.textAlignment = NSTextAlignmentCenter;
        lab1.font = [UIFont systemFontOfSize:16];
        [footView addSubview:lab1];
        
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.frame = CGRectMake(33, 162 - 50, kSCREEN_WIDTH - 66, 50);
        nextBtn.backgroundColor = kMainBlueColor;
        [nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
        nextBtn.layer.cornerRadius = 4;
        [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [nextBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [nextBtn addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:nextBtn];
        return footView;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = (CBENewsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setItemSelect:YES];
    cell.isSelected = YES;
    NSDictionary *body = [self.dataSource objectAtIndex:indexPath.row];
    NSString *tag = [body objectForKey:@"name"];
    self.selectTag = tag;
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell = (CBENewsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setItemSelect:NO];
    cell.isSelected = NO;
    NSLog(@"1第%ld区，1第%ld个",(long)indexPath.section,(long)indexPath.row);
    self.selectTag = nil;
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    CBENewsCollectionCell *cell =  (CBENewsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setItemSelect:NO];
    cell.isSelected = NO;
}


@end

//
//  CBEAIndustyDictrectController.h
//  CBECommerce
//
//  Created by Rainer on 2019/2/23.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEAIndustyDictrectController : CBEBaseController

@property (nonatomic, copy) NSString *industry;//行业

@end

NS_ASSUME_NONNULL_END

//
//  CBELoginVM.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/9.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBELoginVM : NSObject

//微信登录接口
+ (void)wxDoLoginWithWxId:(NSString *)wxId
               withWxName:(NSString *)wxName
              withWxPhoto:(NSString *)wxPhoto
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed;

//绑定微信
+ (void)bindWXAccountWith:(NSString *)mobile
              withMsmCode:(NSString *)msmCode
                 withWxId:(NSString *)wxId
               withWxName:(NSString *)wxName
              withWxPhoto:(NSString *)wxPhoto
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed;

//更新行业方向
+ (void)updateIndustryWith:(NSDictionary *)params
                  complete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed;

@end

NS_ASSUME_NONNULL_END

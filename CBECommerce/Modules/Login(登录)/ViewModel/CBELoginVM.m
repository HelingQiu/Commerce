//
//  CBELoginVM.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/9.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBELoginVM.h"

@implementation CBELoginVM

//微信登录接口
+ (void)wxDoLoginWithWxId:(NSString *)wxId
               withWxName:(NSString *)wxName
              withWxPhoto:(NSString *)wxPhoto
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"wxId":wxId,
                             @"wxName":wxName,
                             @"wxPhoto":wxPhoto
                             };
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kWxLogin parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
//    [manager GET:StringFormat(@"%@%@",Host,kWxLogin) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        successed(responseObject);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        failed(error);
//    }];
}

//绑定微信
+ (void)bindWXAccountWith:(NSString *)mobile
              withMsmCode:(NSString *)msmCode
                 withWxId:(NSString *)wxId
               withWxName:(NSString *)wxName
              withWxPhoto:(NSString *)wxPhoto
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"mobile":mobile,
                             @"msmCode":msmCode,
                             @"wxId":wxId,
                             @"wxName":wxName,
                             @"wxPhoto":wxPhoto,
                             @"channel":@"2"
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kBindWXAccount parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//更新行业方向
+ (void)updateIndustryWith:(NSDictionary *)params
                  complete:(CBENetWorkSucceed)successed
                      fail:(CBENetWorkFailure)failed {
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kUpdateIndustry parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end


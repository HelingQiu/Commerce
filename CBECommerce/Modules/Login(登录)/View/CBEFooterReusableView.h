//
//  CBEFooterReusableView.h
//  CBECommerce
//
//  Created by Rainer on 2019/2/23.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEFooterReusableView : UICollectionReusableView

@end

NS_ASSUME_NONNULL_END

//
//  CBEFooterReusableView.m
//  CBECommerce
//
//  Created by Rainer on 2019/2/23.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import "CBEFooterReusableView.h"

@implementation CBEFooterReusableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, kSCREEN_WIDTH, 16)];
        lab1.text = @"选择您的定位，可获个性化推荐哦！";
        lab1.textColor = UIColorFromRGB(0x666666);
        lab1.textAlignment = NSTextAlignmentCenter;
        lab1.font = [UIFont systemFontOfSize:16];
        [self addSubview:lab1];
        
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.frame = CGRectMake(33, self.frame.size.height - 50, kSCREEN_WIDTH - 66, 50);
        nextBtn.backgroundColor = kMainBlueColor;
        [nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
        [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [nextBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [self addSubview:nextBtn];
    }
    return self;
}

@end

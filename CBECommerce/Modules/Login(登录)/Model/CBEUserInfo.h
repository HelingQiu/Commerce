//
//  CBEUserInfo.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/6.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEUserInfo : NSObject

@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *birthdayStr;
@property (nonatomic, copy) NSString *companyJobTitle;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *headPhoto;
@property (nonatomic, copy) NSString *industry;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *industryDirection;//行业方向
@property (nonatomic, copy) NSString *industryId;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *modifyTime;
@property (nonatomic, copy) NSString *nativePlace;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userNameSpell;
@property (nonatomic, copy) NSString *userRole;
@property (nonatomic, copy) NSString *userRoleText;
@property (nonatomic, copy) NSString *wxId;
@property (nonatomic, copy) NSString *wxName;
@property (nonatomic, copy) NSString *wxPhoto;
@property (nonatomic, copy) NSString *yxToken;

@end

NS_ASSUME_NONNULL_END

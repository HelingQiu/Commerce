//
//  CBEUserModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/2.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEUserModel.h"
#import <MMKV.h>

@implementation CBEUserModel

//创建单例
+ (instancetype)sharedInstance{
    
    static CBEUserModel * instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSUserDefaults *)defaults {
    
    return [NSUserDefaults standardUserDefaults];
}

- (MMKV *)mmkv {
    return [MMKV defaultMMKV];
}

- (void)setObject:(id)value forKey:(NSString *)key
{
    [self.mmkv setObject:value forKey:key];
//    [self.defaults setObject:value forKey:key];
//    [self.defaults synchronize];
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)key
{
    [self.mmkv setUInt32:value forKey:key];
//    [self.defaults setInteger:value forKey:key];
//    [self.defaults synchronize];
}

- (void)setBool:(BOOL)value forKey:(NSString *)key
{
    [self.mmkv setBool:value forKey:key];
//    [self.defaults setBool:value forKey:key];
//    [self.defaults synchronize];
}

//取出数据
- (id)objectForKey:(NSString *)key {
    
//    return [self.defaults objectForKey:key];
    return [self.mmkv getObjectOfClass:NSString.class forKey:key];
}

- (NSInteger)integerForKey:(NSString *)key
{
    
//    return [self.defaults integerForKey:key];
    return [self.mmkv getInt32ForKey:key];
}

- (BOOL)boolForKey:(NSString *)key
{
    
//    return [self.defaults boolForKey:key];
    return [self.mmkv getBoolForKey:key];
}

#pragma mark --
//是否已经登录
- (void)setIsLogin:(BOOL)isLogin
{
    [self setBool:isLogin forKey:@"isLogin"];
}
- (BOOL)isLogin
{
    return [self boolForKey:@"isLogin"];
}

- (void)setUserName:(NSString *)userName
{
    [self setObject:userName forKey:@"userName"];
}

- (NSString *)userName
{
    return [self objectForKey:@"userName"];
}

- (void)setToken:(NSString *)token
{
    [self setObject:token forKey:@"token"];
}

- (NSString *)token
{
    return [self objectForKey:@"token"];
}

- (void)setUserInfo:(CBEUserInfo *)userInfo
{
    if ([userInfo respondsToSelector:@selector(encodeWithCoder:)] == NO) {
        NSLog(@"对象存入失败！对象必须实现NSCoding 协议的 encodeWithCoder:方法");
        return;
    }
    
    NSData *encodeObject = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    [self setObject:encodeObject forKey:@"userInfo"];
}

- (CBEUserInfo *)userInfo
{
    NSData *encodeObject = [self.mmkv getObjectOfClass:NSData.class forKey:@"userInfo"];
    if (encodeObject == nil) {
        return nil;
    }
    CBEUserInfo *obj = [NSKeyedUnarchiver unarchiveObjectWithData:encodeObject];
    return obj;
}

- (void)clearAllUserData
{
//    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [self.mmkv clearAll];
}


@end

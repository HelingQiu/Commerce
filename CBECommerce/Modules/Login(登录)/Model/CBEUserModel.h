//
//  CBEUserModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/2.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseModel.h"
#import "CBEUserInfo.h"

@interface CBEUserModel : CBEBaseModel

@property(nonatomic, copy) NSString *userName;
@property(nonatomic, copy) NSString *password;//暂无没有用到
@property(nonatomic, copy) NSString *token;
@property(nonatomic, strong) CBEUserInfo *userInfo;//用户信息
@property(nonatomic, assign) BOOL isLogin;//是否登录

+ (instancetype)sharedInstance;

/**
 清空数据
 */
- (void)clearAllUserData;

@end

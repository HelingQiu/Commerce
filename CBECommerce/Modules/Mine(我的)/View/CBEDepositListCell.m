//
//  CBEDepositListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEDepositListCell.h"
#import "CBEDepostModel.h"

@implementation CBEDepositListCell

+ (CBEDepositListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEDepositListCell";
    CBEDepositListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEDepositListCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (IBAction)depostAction:(UIButton *)sender {
    if (self.depositBlock) {
        self.depositBlock();
    }
}

- (void)refreshDataWith:(CBEDepostModel *)model
{
    [_imgView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"news_default")];
    [_titleLabel setText:model.title];
    __block NSString *timeString = @"";
    NSArray *timeArray = model.timeList;
    [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *start = [obj objectForKey:@"startTime"];
        NSString *end = [obj objectForKey:@"endTime"];
        timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
        if (idx == timeArray.count - 1) {
            [self->_titleLabel setText:timeString];
        }
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEMineCutomCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/8.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMineCutomCell.h"

@implementation CBEMineCutomCell

+ (CBEMineCutomCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEMineCutomCell";
    CBEMineCutomCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEMineCutomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    _leftView = [[UIImageView alloc] initWithFrame:CGRectMake(kPadding15, 13, 24, 24)];
    [self.contentView addSubview:_leftView];
    
    _leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15+30, 15, 100, 20)];
    [_leftLabel setFont:kFont(16)];
    [_leftLabel setTextColor:kMainBlackColor];
    [self.contentView addSubview:_leftLabel];
    
    UIImageView *rightView = [[UIImageView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 32, 13, 24, 24)];
    [rightView setImage:IMAGE_NAMED(@"arrow_right")];
    [self.contentView addSubview:rightView];
    
    _rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15 + 25 + 100, 15, kSCREEN_WIDTH - 15 - 100 - 32 - 30, 20)];
    [_rightLabel setFont:kFont(14)];
    [_rightLabel setTextAlignment:NSTextAlignmentRight];
    [_rightLabel setTextColor:kMainTextColor];
    [self.contentView addSubview:_rightLabel];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 49.5, kSCREEN_WIDTH, 0.5)];
    line.backgroundColor = kMainLineColor;
    [self.contentView addSubview:line];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

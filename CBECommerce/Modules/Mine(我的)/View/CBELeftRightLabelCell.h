//
//  CBELeftRightLabelCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBELeftRightLabelCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowView;
@property (weak, nonatomic) IBOutlet UIView *line;
    
+ (CBELeftRightLabelCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

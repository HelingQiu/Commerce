//
//  CBEBillListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBillListCell.h"
#import "CBEMineBillModel.h"

@implementation CBEBillListCell

+ (CBEBillListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEBillListCell";
    CBEBillListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEBillListCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEMineBillModel *)model
{
    [self.titleLabel setText:model.activityName];
    [self.moneyLabel setText:model.billPrice];
    [self.timeLabel setText:model.createTime];
    if (model.tradeType == 1) {
        [self.statusLabel setText:@"收入"];
        [self.statusLabel setBackgroundColor:kMainBlueColor];
        [self.statusLabel setTextColor:[UIColor whiteColor]];
    }else if (model.tradeType == 2) {//提现
        if (model.approverStatus == 0) {//未审批
            [self.statusLabel setText:@"提现中"];
            [self.statusLabel setBackgroundColor:kMainBlueColor];
        }else if (model.approverStatus == 1) {//审批通过
            if (model.remitStatus == 0) {//未打款
                [self.statusLabel setText:@"提现中"];
                [self.statusLabel setBackgroundColor:kMainBlueColor];
            }else if (model.remitStatus == 1) {
                [self.statusLabel setText:@"提现成功"];
                [self.statusLabel setBackgroundColor:kMainBlueColor];
            }
        }else if (model.approverStatus == 2) {//审批不通过
            [self.statusLabel setText:@"提现失败"];
            [self.statusLabel setBackgroundColor:[UIColor ls_colorWithHexString:@"#CCCCCC"]];
        }
    }else if (model.tradeType == 3) {
        if (model.approverStatus == 0) {//未审批
            [self.statusLabel setText:@"退款中"];
            [self.statusLabel setBackgroundColor:kMainOrangeColor];
        }else if (model.approverStatus == 1) {//审批通过
            if (model.remitStatus == 0) {//未打款
                [self.statusLabel setText:@"退款中"];
                [self.statusLabel setBackgroundColor:kMainOrangeColor];
            }else if (model.remitStatus == 1) {
                [self.statusLabel setText:@"退款成功"];
                [self.statusLabel setBackgroundColor:kMainOrangeColor];
            }
        }else if (model.approverStatus == 2) {//审批不通过
            [self.statusLabel setText:@"退款失败"];
            [self.statusLabel setBackgroundColor:[UIColor ls_colorWithHexString:@"#CCCCCC"]];
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

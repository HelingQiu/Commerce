//
//  CBEStatusView.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, StatusType) {
    StatusTypeAll = -1,//全部
    StatusTypeIncome = 1,//收入
    StatusTypeDeposit = 2,//提现
    StatusTypeRefund = 3 //退款
};
typedef void(^StatusTypeBlock)(StatusType type);
@interface CBEStatusView : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *thirdBtn;
@property (weak, nonatomic) IBOutlet UIButton *forthBtn;

@property (copy, nonatomic) StatusTypeBlock statusBlock;

+ (CBEStatusView *)createView;

- (void)selectedStatus:(StatusType)type;

@end

NS_ASSUME_NONNULL_END

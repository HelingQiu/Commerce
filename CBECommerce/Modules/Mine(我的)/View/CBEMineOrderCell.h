//
//  CBEMineOrderCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEMineOrderCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

+ (CBEMineOrderCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

//
//  CBEPwTextField.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEPwTextField : UITextField

@end

NS_ASSUME_NONNULL_END

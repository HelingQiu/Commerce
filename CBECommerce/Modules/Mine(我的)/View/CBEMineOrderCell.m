//
//  CBEMineOrderCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMineOrderCell.h"
#import "CBEMineInfoModel.h"
#import "CBEOrderListModel.h"

@implementation CBEMineOrderCell

+ (CBEMineOrderCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEMineOrderCell";
    CBEMineOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEMineOrderCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEMineInfoModel *)model
{
    NSArray *newOrderList = model.newsOrderList;
    if (newOrderList.count) {
        NSDictionary *body = [newOrderList firstObject];
        CBEOrderListModel *orderModel = [CBEOrderListModel mj_objectWithKeyValues:body];
        [_imgView sd_setImageWithURL:[NSURL URLWithString:[orderModel.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"news_default")];
        [_titleLabel setText:orderModel.title];
        __block NSString *timeString = @"时间：";
        NSArray *timeArray = orderModel.timeList;
        [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *start = [obj objectForKey:@"startTime"];
            NSString *end = [obj objectForKey:@"endTime"];
            timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
            if (idx == timeArray.count - 1) {
                [self->_timeLabel setText:timeString];
            }
        }];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

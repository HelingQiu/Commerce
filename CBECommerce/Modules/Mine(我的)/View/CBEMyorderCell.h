//
//  CBEMyorderCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/3.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

typedef NS_ENUM(NSUInteger, OrderStatus) {
    OrderStatusWaitPay = 0,//待付款
    OrderStatusCanyu,      //带参与
    OrderStatusRefund,     //退款
    OrderStatusDone        //完成
};
typedef void(^OrderActionBlock)(OrderStatus status);
@interface CBEMyorderCell : CBEBaseTableViewCell

@property (nonatomic, copy) OrderActionBlock orderBlock;

+ (CBEMyorderCell *)cellForTableView:(UITableView *)tableView;

@end

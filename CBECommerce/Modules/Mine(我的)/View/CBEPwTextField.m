//
//  CBEPwTextField.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEPwTextField.h"

@implementation CBEPwTextField

//设置默认文字的显示区域
- (CGRect)placeholderRectForBounds:(CGRect)bounds{
    
    return CGRectMake(15,0,bounds.size.width - 15
                        ,bounds.size.height);
}

//设置文本显示的区域
- (CGRect)textRectForBounds:(CGRect)bounds{
    
    return CGRectMake(15,0,bounds.size.width - 15
                        ,bounds.size.height);
}

//编辑区域，即光标的位置
- (CGRect)editingRectForBounds:(CGRect)bounds{
    
    return CGRectMake(15,0,bounds.size.width - 15
                        ,bounds.size.height);
}


@end

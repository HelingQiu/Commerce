//
//  CBEMyorderCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/3.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMyorderCell.h"
#import "CBEMineInfoModel.h"

@implementation CBEMyorderCell

+ (CBEMyorderCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"myOrderCell";
    CBEMyorderCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEMyorderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor whiteColor];
    self.layer.masksToBounds = YES;
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 80)];
    backView.backgroundColor = [UIColor whiteColor];
    backView.clipsToBounds = YES;
    [self.contentView addSubview:backView];
    
    CGFloat width = (kSCREEN_WIDTH)/4;
    
    NSArray *array = @[@{@"image":@"me_icon_daifukuan",@"title":@"待付款"},
                       @{@"image":@"me_icon_daicanyu",@"title":@"待参与"},
                       @{@"image":@"me_icon_tuikuan",@"title":@"退款"},
                       @{@"image":@"me_icon_wancheng",@"title":@"已完成"}];
    
    for (int i = 0; i < array.count; i ++) {
        CBEBaseButton *firstButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
        [firstButton setFrame:CGRectMake(width * i, 0, width, self.contentView.height)];
        [firstButton setImage:IMAGE_NAMED([[array objectAtIndex:i] objectForKey:@"image"]) forState:UIControlStateNormal];
        [firstButton setTitle:[[array objectAtIndex:i] objectForKey:@"title"] forState:UIControlStateNormal];
        [firstButton.titleLabel setFont:kFont(12)];
        [firstButton setTitleColor:[UIColor ls_colorWithHexString:@"#92A1B3"] forState:UIControlStateNormal];
        [firstButton setImageViewRect:CGRectMake((width-32)/2, kPadding15, 32, 32)];
        [firstButton setTitleLabelRect:CGRectMake(0, kPadding15 + 32 + 6, width, 17)];
        [firstButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        firstButton.tag = i;
        [firstButton addTarget:self action:@selector(didAction:) forControlEvents:UIControlEventTouchUpInside];
        [backView addSubview:firstButton];
        
        UILabel *badge = [[UILabel alloc] initWithFrame:CGRectMake(width/2+16-9, kPadding15, 18, 18)];
        badge.clipsToBounds = YES;
        badge.layer.cornerRadius = 9;
        badge.layer.borderWidth = 0.5;
        badge.layer.borderColor = kMainOrangeColor.CGColor;
        badge.textColor = kMainOrangeColor;
        badge.textAlignment = NSTextAlignmentCenter;
        badge.font = kFont(9);
        badge.tag = 100+i;
        badge.hidden = YES;
        [firstButton addSubview:badge];
    }
}

- (void)didAction:(CBEBaseButton *)btn
{
    if (self.orderBlock) {
        if (btn.tag == 0) {
            self.orderBlock(OrderStatusWaitPay);
        }else if (btn.tag == 1) {
            self.orderBlock(OrderStatusCanyu);
        }else if (btn.tag == 2) {
            self.orderBlock(OrderStatusRefund);
        }else{
            self.orderBlock(OrderStatusDone);
        }
    }
}

- (void)refreshDataWith:(CBEMineInfoModel *)model
{
    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *badge = obj;
            if (obj.tag == 100) {
                if (model.stayAttendCount.intValue > 0) {
                    badge.text = model.stayAttendCount;
                    badge.hidden = NO;
                }else{
                    badge.hidden = YES;
                    badge.text = @"";
                }
            }else if (obj.tag == 101) {
                if (model.tayPayCount.intValue > 0) {
                    badge.text = model.tayPayCount;
                    badge.hidden = NO;
                }else{
                    badge.text = @"";
                    badge.hidden = YES;
                }
            }else if (obj.tag == 102) {
                if (model.refundCount.intValue > 0) {
                    badge.text = model.refundCount;
                    badge.hidden = NO;
                }else{
                    badge.text = @"";
                    badge.hidden = YES;
                }
            }else {
                if (model.finishCount.intValue > 0) {
                    badge.text = model.finishCount;
                    badge.hidden = NO;
                }else{
                    badge.text = @"";
                    badge.hidden = YES;
                }
            }
        }
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEMineCutomCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/8.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

@interface CBEMineCutomCell : CBEBaseTableViewCell

@property (nonatomic, strong) UIImageView *leftView;
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *rightLabel;

+ (CBEMineCutomCell *)cellForTableView:(UITableView *)tableView;

@end

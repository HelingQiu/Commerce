//
//  CBEScanResultCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEScanResultCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

+ (CBEScanResultCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

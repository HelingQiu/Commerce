//
//  CBEMySponsorOrderCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/8.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

typedef void(^DepositBlock)(void);
@interface CBEMySponsorOrderCell : CBEBaseTableViewCell

@property (nonatomic, copy) DepositBlock depositBlock;
+ (CBEMySponsorOrderCell *)cellForTableView:(UITableView *)tableView;

@end

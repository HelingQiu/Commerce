//
//  CBETimeView.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBETimeView.h"

#define DateFormat @"yyyy.MM"

#define kITLocalDate(date) \

@interface CBETimeView()
/** 最大时间 */
@property (nullable, weak, nonatomic) NSDate *maximumDate;
/** 最小时间 */
@property (nullable, weak, nonatomic) NSDate *minimumDate;
/** 年份Max */
@property (nonatomic, assign) NSInteger maxYear;
/** 月份Max */
@property (nonatomic, assign) NSInteger maxMonth;
/** 年份min */
@property (nonatomic, assign) NSInteger minYear;
/** 月份min */
@property (nonatomic, assign) NSInteger minMonth;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
/** 年份数组 */
@property (nonatomic,strong) NSMutableArray *yearArray;
/** 月份数组 */
@property (nonatomic,strong) NSMutableArray *monthArray;
/** 选中的年份 */
@property (nonatomic, copy) NSString *choosedYear;
/** 选中的月份 */
@property (nonatomic, copy) NSString *choosedMonth;
/** 是否是当前年份 */
@property (nonatomic, assign) BOOL isCurrentYear;
/** 是否是第一年 */
@property (nonatomic, assign) BOOL isFirstYear;

@end

@implementation CBETimeView

+ (CBETimeView *)createView
{
    CBETimeView *cell = [[NSBundle mainBundle] loadNibNamed:@"CBETimeView" owner:self options:nil].firstObject;
    return cell;
}

- (IBAction)sureAction:(UIButton *)sender {
    if (self.timeBlock) {
        self.timeBlock(self.choosedYear,self.choosedMonth);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self getData];
    
    self.pickerView.backgroundColor = [UIColor whiteColor];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.pickerView.showsSelectionIndicator = YES;
    
    //默认选中当前年
    [self.pickerView selectRow:self.maxYear - self.minYear inComponent:0 animated:NO];
    [self pickerView:self.pickerView didSelectRow:self.maxYear - self.minYear inComponent:0];
    
    //默认选中当前月
    [self.pickerView selectRow:self.maxMonth-1 inComponent:1 animated:NO];
    [self pickerView:self.pickerView didSelectRow:self.maxMonth-1 inComponent:1];
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = DateFormat;
    }
    return _dateFormatter;
}

- (void)getData {
    // 最大时间
    if (self.maximumDate == nil) {
        self.maximumDate = [self.dateFormatter dateFromString:[self getCurrentTimes]];
    }
    
    kITLocalDate(_maximumDate);
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:_maximumDate];
    self.maxYear = [components year];
    self.maxMonth = [components month];
    
    // 最小时间
    if (self.minimumDate == nil) {
        self.minimumDate = [self.dateFormatter dateFromString:@"2010.01"];
    }
    
    kITLocalDate(_minimumDate);
    
    NSCalendar *calendar2 = [NSCalendar currentCalendar];
    NSDateComponents *components2 = [calendar2 components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:self.minimumDate];
    self.minYear = [components2 year];
    self.minMonth = [components2 month];
    
    // 年份数组
    for (NSInteger i = self.minYear; i<=self.maxYear; i++) {
        
        [self.yearArray addObject:[NSString stringWithFormat:@"%ld",i]];
    }
}

- (NSString *)getCurrentTimes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy.MM"];
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
}

- (NSMutableArray *)yearArray{
    if (_yearArray == nil) {
        _yearArray = [[NSMutableArray alloc] init];
    }
    return _yearArray;
}

- (NSMutableArray *)monthArray{
    if (_monthArray == nil) {
        _monthArray = [[NSMutableArray alloc] init];
        for (int i = 1; i <= 12; i++) {
            [_monthArray addObject:[NSString stringWithFormat:@"%02d",i]];
        }
    }
    return _monthArray;
}

// 返回选择器有几列.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// 返回每组有几行
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.yearArray.count;
    }else{
        if (_isCurrentYear == YES) { // 当前年
            
            if (_isFirstYear == YES) {
                
                return (self.maxMonth - self.minMonth +1);
            }else{
                
                return self.maxMonth;
            }
            
        }else{
            
            if (_isFirstYear == YES) {
                
                return (12 - self.minMonth +1);
            }else{
                
                return 12;
            }
        }
    }
}

#pragma mark - 代理
// 返回第component列第row行的内容（标题）
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        NSString *year = [NSString stringWithFormat:@"%@年",self.yearArray[row]];
        return year;
        
    }else{
        
        if (_isFirstYear == YES) {
            NSString *month = [NSString stringWithFormat:@"%@月",self.monthArray[self.minMonth + row -1]];
            return month;
        }else{
            NSString *month = [NSString stringWithFormat:@"%@月",self.monthArray[row]];
            return month;
        }
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 35.f;
}
// 选中第component第row的时候调用
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        
        if (row == self.yearArray.count - 1) { // 当前年份
            
            _isCurrentYear = YES; // 当前年
        }else{
            
            _isCurrentYear = NO;
        }
        if (row == 0) {
            
            _isFirstYear = YES;
        }else{
            _isFirstYear = NO;
        }
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:NO];
        
        self.choosedYear = self.yearArray[row];
    }else{
        
        if (_isFirstYear == YES) { // 第一年
            
            self.choosedMonth = self.monthArray[row + self.minMonth - 1];
        }else{
            
            self.choosedMonth = self.monthArray[row];
        }
    }
}

// 重写方法设置字体
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    //设置分割线的颜色
    for(UIView *singleLine in pickerView.subviews){
        
        if (singleLine.frame.size.height < 1){
            
            singleLine.backgroundColor = kMainLineColor;
        }
    }
    
    UILabel *pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:kFont(24)];
        pickerLabel.textColor = kMainBlackColor;
    }
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    
    return pickerLabel;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

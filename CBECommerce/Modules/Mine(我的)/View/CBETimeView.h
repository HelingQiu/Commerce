//
//  CBETimeView.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^TimeSelectBlock)(NSString *year,NSString *month);
@interface CBETimeView : CBEBaseTableViewCell<UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (nonatomic, copy) TimeSelectBlock timeBlock;

+ (CBETimeView *)createView;

@end

NS_ASSUME_NONNULL_END

//
//  UserInfoView.m
//  YBTableViewHeaderAnimationDemo
//
//  Created by fengbang on 2018/7/11.
//  Copyright © 2018年 王颖博. All rights reserved.
//

#import "UserInfoView.h"
#import "CBECenterView.h"

@interface UserInfoView()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, assign) CGFloat headerViewHeight;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic, strong) UIImageView *arrowView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIView *sponsorBottomView;
@property (nonatomic, strong) UILabel *friendCountLabel;
@property (nonatomic, strong) UILabel *activityCountLabel;
@property (nonatomic, strong) UILabel *newsCountLabel;
@property (nonatomic, strong) CBECenterView *firendView;
@property (nonatomic, strong) CBECenterView *activityView;
@property (nonatomic, strong) CBECenterView *newsView;

@property (nonatomic, strong) UILabel *sponsorLabel;

@end

@implementation UserInfoView

-(instancetype)initWithFrame:(CGRect)frame {
    self= [super initWithFrame:frame];
    if (self) {
        self.headerViewHeight = kUSER_INFO_HEADERVIEW_H;
        
        [self configUI];
    }
    return self;
}

-(void)configUI{
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
    self.userInteractionEnabled = YES;
    
//    CAShapeLayer *shapeLayer = [CAShapeLayer new];
//    shapeLayer.fillColor = [UIColor redColor].CGColor; //填充颜色
//    [self.layer addSublayer:shapeLayer];
//    self.shapeLayer = shapeLayer;
    
    _backGroundView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, self.headerViewHeight)];
    _backGroundView.backgroundColor = kMainDefaltGrayColor;
    _backGroundView.clipsToBounds = YES;
    _backGroundView.userInteractionEnabled = YES;
    [self addSubview:_backGroundView];
    
//    UITapGestureRecognizer *infoRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infoAction)];
    
    _backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 200)];
    [_backView setBackgroundColor:kMainBlueColor];
    _backView.userInteractionEnabled = YES;
    [_backGroundView addSubview:_backView];
//    [_backView addGestureRecognizer:infoRecognizer];
    
    _userLogoImageView = [[UIImageView alloc] init];
    _userLogoImageView.backgroundColor = [UIColor whiteColor];
    _userLogoImageView.clipsToBounds = YES;
    _userLogoImageView.userInteractionEnabled = YES;
    _userLogoImageView.multipleTouchEnabled = YES;
    [_backView addSubview:_userLogoImageView];
//    [_userLogoImageView addGestureRecognizer:infoRecognizer];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.font = kFont(16);
    _titleLabel.userInteractionEnabled = YES;
    [_backView addSubview:_titleLabel];
//    [_titleLabel addGestureRecognizer:infoRecognizer];
    
    _subTitleLabel = [[UILabel alloc]init];
    _subTitleLabel.textAlignment = NSTextAlignmentLeft;
    _subTitleLabel.textColor = [UIColor whiteColor];
    _subTitleLabel.font = kFont(14);
    _subTitleLabel.userInteractionEnabled = YES;
    [_backView addSubview:_subTitleLabel];
//    [_subTitleLabel addGestureRecognizer:infoRecognizer];
    
    _arrowView = [UIImageView new];
    [_arrowView setImage:IMAGE_NAMED(@"arrow_right")];
    _arrowView.userInteractionEnabled = YES;
    [_backView addSubview:_arrowView];
//    [_arrowView addGestureRecognizer:infoRecognizer];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(15, KNavigationBarHeight + 20, kSCREEN_WIDTH - 23, 48);
    btn.backgroundColor = [UIColor clearColor];
    [_backView addSubview:btn];
    [btn addTarget:self action:@selector(infoAction) forControlEvents:UIControlEventTouchUpInside];
    
    _bottomView = [UIView new];
    [_bottomView setBackgroundColor:[UIColor whiteColor]];
    [_bottomView setClipsToBounds:YES];
    [_bottomView.layer setCornerRadius:10];
    _bottomView.userInteractionEnabled = YES;
    [_backGroundView addSubview:_bottomView];
    
    UITapGestureRecognizer *friendRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(firendAction)];
    _friendCountLabel = [UILabel new];
    _friendCountLabel.font = kFont(18);
    _friendCountLabel.textColor = kMainBlueColor;
    _friendCountLabel.textAlignment = NSTextAlignmentCenter;
    _friendCountLabel.text = @"0";
    _friendCountLabel.userInteractionEnabled = YES;
    [_bottomView addSubview:_friendCountLabel];
    [_friendCountLabel addGestureRecognizer:friendRecognizer];
    
    UITapGestureRecognizer *friendRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(firendAction)];
    _firendView = [[CBECenterView alloc] init];
    [_firendView configWith:@"" imageSize:CGSizeMake(0, 0) textFont:kFont(12) textColor:[UIColor ls_colorWithHexString:@"#92A1B3"] textAlignment:NSTextAlignmentLeft text:@"好友" margin:0];
    _firendView.userInteractionEnabled = YES;
    [_bottomView addSubview:_firendView];
    [_firendView addGestureRecognizer:friendRecognizer1];
    
    UITapGestureRecognizer *activityRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityAction)];
    _activityCountLabel = [UILabel new];
    _activityCountLabel.font = kFont(18);
    _activityCountLabel.textColor = kMainBlueColor;
    _activityCountLabel.textAlignment = NSTextAlignmentCenter;
    _activityCountLabel.text = @"0";
    _activityCountLabel.userInteractionEnabled = YES;
    [_bottomView addSubview:_activityCountLabel];
    [_activityCountLabel addGestureRecognizer:activityRecognizer];
    
    UITapGestureRecognizer *activityRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityAction)];
    _activityView = [[CBECenterView alloc] init];
    [_activityView configWith:@"" imageSize:CGSizeMake(0, 0) textFont:kFont(12) textColor:[UIColor ls_colorWithHexString:@"#92A1B3"] textAlignment:NSTextAlignmentLeft text:@"活动收藏" margin:0];
    _activityView.userInteractionEnabled = YES;
    [_bottomView addSubview:_activityView];
    [_activityView addGestureRecognizer:activityRecognizer1];
    
    UITapGestureRecognizer *newsRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newsAction)];
    _newsCountLabel = [UILabel new];
    _newsCountLabel.font = kFont(18);
    _newsCountLabel.textColor = kMainBlueColor;
    _newsCountLabel.textAlignment = NSTextAlignmentCenter;
    _newsCountLabel.text = @"0";
    _newsCountLabel.userInteractionEnabled = YES;
    [_bottomView addSubview:_newsCountLabel];
    [_newsCountLabel addGestureRecognizer:newsRecognizer];
    
    UITapGestureRecognizer *newsRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newsAction)];
    _newsView = [[CBECenterView alloc] init];
    [_newsView configWith:@"" imageSize:CGSizeMake(0, 0) textFont:kFont(12) textColor:[UIColor ls_colorWithHexString:@"#92A1B3"] textAlignment:NSTextAlignmentLeft text:@"资讯收藏" margin:0];
    _newsView.userInteractionEnabled = YES;
    [_bottomView addSubview:_newsView];
    [_newsView addGestureRecognizer:newsRecognizer1];
    
#pragma mark - 主办方
    _sponsorBottomView = [UIView new];
    [_sponsorBottomView setBackgroundColor:[UIColor whiteColor]];
    [_sponsorBottomView setClipsToBounds:YES];
    [_sponsorBottomView.layer setCornerRadius:10];
    [_backGroundView addSubview:_sponsorBottomView];
    _sponsorBottomView.hidden = YES;
    
    _sponsorLabel = [UILabel new];
    _sponsorLabel.font = kFont(12);
    _sponsorLabel.textColor = kMainTextColor;
    _sponsorLabel.numberOfLines = 0;
    _sponsorLabel.textAlignment = NSTextAlignmentCenter;
    _sponsorLabel.text = @"";
    [_sponsorBottomView addSubview:_sponsorLabel];
}

- (void)setAuthorDataWith:(CBEMineInfoModel *)model
{
    _titleLabel.text = ![CommonUtils isBlankString:model.userName]?model.userName:@"参与方名称";
    _subTitleLabel.text = ![CommonUtils isBlankString:model.companyName]?model.companyName:@"xxxx公司";
    _sponsorBottomView.hidden = YES;
    [_userLogoImageView sd_setImageWithURL:[NSURL URLWithString:[model.photo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"me_logo_default")];
    _bottomView.hidden = NO;
    [_friendCountLabel setText:model.friendCount];
    [_activityCountLabel setText:model.activityCollectCount];
    [_newsCountLabel setText:model.inforCollectCount];
}

- (void)setSponnarDataWith:(CBEMineSponsorModel *)model
{
    _titleLabel.text = ![CommonUtils isBlankString:model.sponsorName]?model.sponsorName:@"主办方名称";
    _subTitleLabel.text = model.mobile;
    [_userLogoImageView sd_setImageWithURL:[NSURL URLWithString:[model.photo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"me_logo_default")];
    _sponsorLabel.text = ![CommonUtils isBlankString:model.introduce]?model.introduce:@"主办方简介";
    _sponsorBottomView.hidden = NO;
    _bottomView.hidden = YES;
}

//主办方资料或者参与方简历
- (void)infoAction
{
    if (self.clickBlock) {
        self.clickBlock(ActionType_Info);
    }
}

//好友
- (void)firendAction
{
    if (self.clickBlock) {
        self.clickBlock(ActionType_Friends);
    }
}

//活动收藏
- (void)activityAction
{
    if (self.clickBlock) {
        self.clickBlock(ActionType_Activity);
    }
}

//资讯收藏
- (void)newsAction
{
    if (self.clickBlock) {
        self.clickBlock(ActionType_News);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat logoImageView_h = 48.0;
    CGFloat titleLabel_h = 20.0;
    
//    self.backGroundView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.backgroundColor = [UIColor clearColor];
    if (self.animationType == FBUserInfoHeaderViewAnimationTypeScale) {
        self.backGroundView.frame = self.bounds;
        self.backView.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height - 50);
    }
    self.userLogoImageView.frame = CGRectMake(self.frame.size.width/2-kSCREEN_WIDTH/2 + 15, KNavigationBarHeight + 20, logoImageView_h, logoImageView_h);
    self.userLogoImageView.layer.cornerRadius = 24;
    
    [_arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(24);
        make.right.equalTo(self->_backView.mas_right).offset(-8);
        make.centerY.equalTo(self->_userLogoImageView.mas_centerY);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_userLogoImageView.mas_top);
        make.left.equalTo(self->_userLogoImageView.mas_right).offset(10);
        make.height.mas_equalTo(titleLabel_h);
        make.right.equalTo(self->_arrowView.mas_left).offset(-2);
    }];
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(6);
        make.left.equalTo(self->_titleLabel.mas_left);
        make.height.mas_equalTo(titleLabel_h);
        make.right.equalTo(self->_arrowView.mas_left).offset(-2);
    }];
    
    CGFloat itemWidth = (self.bounds.size.width - kPadding15 * 2)/3.0;
    
    _bottomView.frame = CGRectMake(kPadding15, self.bounds.size.height - 70, self.bounds.size.width - kPadding15 * 2, 70);
    
    [_friendCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_bottomView.mas_top).offset(kPadding10);
        make.left.equalTo(self->_bottomView.mas_left);
        make.width.mas_equalTo(itemWidth);
        make.height.mas_equalTo(20);
    }];
    [_firendView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self->_bottomView.mas_bottom).offset(-kPadding10);
        make.left.equalTo(self->_bottomView.mas_left);
        make.width.mas_equalTo(itemWidth);
        make.height.mas_equalTo(20);
    }];
    
    [_newsCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_bottomView.mas_top).offset(kPadding10);
        make.right.equalTo(self->_bottomView.mas_right);
        make.width.mas_equalTo(itemWidth);
        make.height.mas_equalTo(20);
    }];
    [_newsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self->_bottomView.mas_bottom).offset(-kPadding10);
        make.right.equalTo(self->_bottomView.mas_right);
        make.width.mas_equalTo(itemWidth);
        make.height.mas_equalTo(20);
    }];
    
    [_activityCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_bottomView.mas_top).offset(kPadding10);
        make.left.equalTo(self->_friendCountLabel.mas_right);
        make.right.equalTo(self->_newsCountLabel.mas_left);
        make.height.mas_equalTo(20);
    }];
    
    [_activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self->_bottomView.mas_bottom).offset(-kPadding10);
        make.left.equalTo(self->_firendView.mas_right);
        make.right.equalTo(self->_newsView.mas_left);
        make.height.mas_equalTo(20);
    }];
    
    _sponsorBottomView.frame = CGRectMake(kPadding15, self.bounds.size.height - 70, self.bounds.size.width - kPadding15 * 2, 70);
    [_sponsorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self->_sponsorBottomView).offset(kPadding15);
        make.right.bottom.equalTo(self->_sponsorBottomView).offset(-kPadding15);
    }];
    
    if (self.animationType == FBUserInfoHeaderViewAnimationTypeCircle) {
        [self setNeedsDisplay];
    }
    
}


// 绘制曲线
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat h1 = self.headerViewHeight;
    CGFloat w = rect.size.width;
    CGFloat h = rect.size.height;
    CGPoint controlPoint = CGPointMake(w/2, h + (h - h1));//(h - h1) * 0.5
    
    if (self.animationType == FBUserInfoHeaderViewAnimationTypeCircle)
    {
        UIBezierPath *bezierPath = [UIBezierPath new];
        [bezierPath moveToPoint:CGPointMake(w, h1)];
        [bezierPath addLineToPoint:CGPointMake(w, 0)];
        [bezierPath addLineToPoint:CGPointMake(0, 0)];
        [bezierPath addLineToPoint:CGPointMake(0, h1)];
        [bezierPath addQuadCurveToPoint:CGPointMake(w, h1) controlPoint:controlPoint];
        [bezierPath closePath];//将起点与结束点相连接
        self.shapeLayer.path = bezierPath.CGPath;
        self.backGroundView.layer.mask = self.shapeLayer;
        
    }else if (self.animationType == FBUserInfoHeaderViewAnimationTypeScale) {
        
    }
    
}

//这个函数会被 hitTest 调用，返回 false 表示点击的不是自己，返回 true 表示点击的是自己
//- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
//    // 判断点击的点，在不在圆内
//    CGPoint center = self.userLogoImageView.center;
//    CGFloat r = self.userLogoImageView.frame.size.width * 0.5;
//    CGFloat newR = sqrt((center.x - point.x) * (center.x - point.x) + (center.y - point.y) * (center.y - point.y));
//
//    // 浮点数比较不推荐用等号，虽然 ios 底层已经处理了这种情况
//    if (newR > r) {
//        return false;
//    } else {
//        return true;
//    }
//}

@end

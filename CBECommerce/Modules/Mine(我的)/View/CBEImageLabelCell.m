//
//  CBEImageLabelCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEImageLabelCell.h"

@implementation CBEImageLabelCell

+ (CBEImageLabelCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEImageLabelCell";
    CBEImageLabelCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEImageLabelCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

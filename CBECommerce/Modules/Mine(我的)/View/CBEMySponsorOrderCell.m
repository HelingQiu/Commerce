//
//  CBEMySponsorOrderCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/8.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMySponsorOrderCell.h"
#import "CBEMineSponsorModel.h"

@implementation CBEMySponsorOrderCell
{
    UILabel *_firstLabel;
    UILabel *_secondLabel;
    UILabel *_thirdLabel;
    UILabel *_forthLabel;
    UILabel *_fifthLabel;
}
+ (CBEMySponsorOrderCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEMySponsorOrderCell";
    CBEMySponsorOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEMySponsorOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    NSArray *array = @[@"总收入",@"可提现",@"提现中",@"待结算",@"已提现"];
    
    CGFloat orginY = 0;
    for (int i = 0; i < array.count; i++) {
        UILabel *leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, orginY, 80, 44)];
        [leftLabel setTextColor:[UIColor ls_colorWithHexString:@"#92A1B3"]];
        [leftLabel setFont:kFont(14)];
        [leftLabel setText:[array objectAtIndex:i]];
        [self.contentView addSubview:leftLabel];
        
        UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, orginY, kSCREEN_WIDTH - 125, 44)];
        [rightLabel setTextColor:kMainBlackColor];
        [rightLabel setFont:kFont(16)];
        [rightLabel setText:@"¥0"];
        [rightLabel setTextAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:rightLabel];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(kPadding15, orginY+44, kSCREEN_WIDTH - kPadding15 * 2, 0.5)];
        lineView.backgroundColor = kMainLineColor;
        
        if (i == 0) {
            _firstLabel = rightLabel;
            [self.contentView addSubview:lineView];
        }else if (i == 1) {
            _secondLabel = rightLabel;
            [self.contentView addSubview:lineView];
        }else if (i == 2) {
            _thirdLabel = rightLabel;
            [self.contentView addSubview:lineView];
        }else if (i == 3) {
            _forthLabel = rightLabel;
            [self.contentView addSubview:lineView];
        }else{
            _fifthLabel = rightLabel;
        }
        orginY += 44;
    }
    
    UIButton *deposit = [UIButton buttonWithType:UIButtonTypeCustom];
    deposit.frame = CGRectMake(20, orginY, kSCREEN_WIDTH - 20 * 2, 44);
    [deposit setTitle:@"去提现" forState:UIControlStateNormal];
    [deposit setBackgroundColor:kMainBlueColor];
    [deposit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deposit.titleLabel setFont:kFont(18)];
    [deposit.layer setCornerRadius:4];
    [deposit addTarget:self action:@selector(depositAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:deposit];
}

- (void)refreshDataWith:(CBEMineSponsorModel *)model
{
    [_firstLabel setText:StringFormat(@"¥%.2f",[model.sumMoney floatValue])];
    [_secondLabel setText:StringFormat(@"¥%.2f",[model.mayDepositMoney floatValue])];
    [_thirdLabel setText:StringFormat(@"¥%.2f",[model.startDepositMoney floatValue])];
    [_forthLabel setText:StringFormat(@"¥%.2f",[model.settleMoney floatValue])];
    [_fifthLabel setText:StringFormat(@"¥%.2f",[model.depositMoney floatValue])];
}

- (void)depositAction
{
    if (self.depositBlock) {
        self.depositBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

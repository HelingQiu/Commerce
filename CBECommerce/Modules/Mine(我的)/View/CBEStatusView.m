//
//  CBEStatusView.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEStatusView.h"

@implementation CBEStatusView

+ (CBEStatusView *)createView
{
    CBEStatusView *cell = [[NSBundle mainBundle] loadNibNamed:@"CBEStatusView" owner:self options:nil].firstObject;
    return cell;
}

- (IBAction)firstAction:(UIButton *)sender {
    sender.selected = YES;
    sender.backgroundColor = [UIColor ls_colorWithHex:0xFC6B3F andAlpha:0.12];
    sender.borderColor = kMainOrangeColor;
    
    _secondBtn.selected = NO;
    _secondBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _secondBtn.borderColor = [UIColor clearColor];
    
    _thirdBtn.selected = NO;
    _thirdBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _thirdBtn.borderColor = [UIColor clearColor];
    
    _forthBtn.selected = NO;
    _forthBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _forthBtn.borderColor = [UIColor clearColor];
}
- (IBAction)secondAction:(UIButton *)sender {
    sender.selected = YES;
    sender.backgroundColor = [UIColor ls_colorWithHex:0xFC6B3F andAlpha:0.12];
    sender.borderColor = kMainOrangeColor;
    
    _firstBtn.selected = NO;
    _firstBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _firstBtn.borderColor = [UIColor clearColor];
    
    _thirdBtn.selected = NO;
    _thirdBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _thirdBtn.borderColor = [UIColor clearColor];
    
    _forthBtn.selected = NO;
    _forthBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _forthBtn.borderColor = [UIColor clearColor];
}
- (IBAction)thirdAction:(UIButton *)sender {
    sender.selected = YES;
    sender.backgroundColor = [UIColor ls_colorWithHex:0xFC6B3F andAlpha:0.12];
    sender.borderColor = kMainOrangeColor;
    
    _secondBtn.selected = NO;
    _secondBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _secondBtn.borderColor = [UIColor clearColor];
    
    _firstBtn.selected = NO;
    _firstBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _firstBtn.borderColor = [UIColor clearColor];
    
    _forthBtn.selected = NO;
    _forthBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _forthBtn.borderColor = [UIColor clearColor];
}
- (IBAction)forthAction:(UIButton *)sender {
    sender.selected = YES;
    sender.backgroundColor = [UIColor ls_colorWithHex:0xFC6B3F andAlpha:0.12];
    sender.borderColor = kMainOrangeColor;
    
    _secondBtn.selected = NO;
    _secondBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _secondBtn.borderColor = [UIColor clearColor];
    
    _thirdBtn.selected = NO;
    _thirdBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _thirdBtn.borderColor = [UIColor clearColor];
    
    _firstBtn.selected = NO;
    _firstBtn.backgroundColor = [UIColor ls_colorWithHexString:@"F4F4F4"];
    _firstBtn.borderColor = [UIColor clearColor];
}

- (IBAction)sureAction:(UIButton *)sender {
    if (self.statusBlock) {
        if (_firstBtn.selected) {
            self.statusBlock(StatusTypeAll);
        }
        if (_secondBtn.selected) {
            self.statusBlock(StatusTypeIncome);
        }
        if (_thirdBtn.selected) {
            self.statusBlock(StatusTypeDeposit);
        }
        if (_forthBtn.selected) {
            self.statusBlock(StatusTypeRefund);
        }
    }
}

- (void)selectedStatus:(StatusType)type
{
    switch (type) {
        case StatusTypeAll:
            [self firstAction:_firstBtn];
            break;
        case StatusTypeIncome:
            [self secondAction:_secondBtn];
            break;
        case StatusTypeDeposit:
            [self thirdAction:_thirdBtn];
            break;
        case StatusTypeRefund:
            [self forthAction:_forthBtn];
            break;
        default:
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _firstBtn.borderColor = [UIColor clearColor];
    _secondBtn.borderColor = [UIColor clearColor];
    _thirdBtn.borderColor = [UIColor clearColor];
    _forthBtn.borderColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEDepositListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^DepositActionBlock)(void);
@interface CBEDepositListCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (copy, nonatomic) DepositActionBlock depositBlock;

+ (CBEDepositListCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

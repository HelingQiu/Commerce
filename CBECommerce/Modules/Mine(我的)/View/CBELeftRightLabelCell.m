//
//  CBELeftRightLabelCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBELeftRightLabelCell.h"

@implementation CBELeftRightLabelCell

+ (CBELeftRightLabelCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBELeftRightLabelCell";
    CBELeftRightLabelCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBELeftRightLabelCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

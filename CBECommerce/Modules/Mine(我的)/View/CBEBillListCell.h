//
//  CBEBillListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEBillListCell : CBEBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

+ (CBEBillListCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

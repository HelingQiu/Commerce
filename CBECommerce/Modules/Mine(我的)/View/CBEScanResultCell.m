//
//  CBEScanResultCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEScanResultCell.h"
#import "CBEOrderDetailModel.h"

@implementation CBEScanResultCell

+ (CBEScanResultCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEScanResultCell";
    CBEScanResultCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CBEScanResultCell" owner:self options:nil].firstObject;
    }
    return cell;
}

- (void)refreshDataWith:(CBEOrderDetailModel *)model
{
    [self.topLabel setText:StringFormat(@"发布机构：%@",model.sponsorName)];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"news_default")];
    [self.titleLabel setText:model.title];
    __block NSString *timeString = @"";
    NSArray *timeArray = model.timeList;
    [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *start = [obj objectForKey:@"startTime"];
        NSString *end = [obj objectForKey:@"endTime"];
        timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
        if (idx == timeArray.count - 1) {
            [self->_timeLabel setText:timeString];
        }
    }];
    
    [self.addressLabel setText:StringFormat(@"地址：%@",model.address)];
    [self.priceLabel setText:StringFormat(@"%@：%@",model.name,model.price)];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

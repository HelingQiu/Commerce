//
//  CBEBillSearchController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBillSearchController.h"
#import "LSSearchBarView.h"
#import "CBEBillListCell.h"
#import "CBEMineVM.h"
#import "CBEMineBillModel.h"

@interface CBEBillSearchController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) LSSearchBarView *searchView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger pageNum;

@end

@implementation CBEBillSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _searchView = [[LSSearchBarView alloc] initWithAlignment:(SearchBarAlignmentAlignmentLeft) placeholder:@"搜索关键字" didClicked:nil];
    [self.navigationItem.titleView addSubview:_searchView];
    [self setBarButton:(BarButtonItemRight) action:@selector(cancelAction) title:@"取消" titleColor:kMainBlueColor];
    
    _searchView.textField.placeholder = @"输入搜索内容";
    self.textField = _searchView.textField;
    self.textField.delegate = self;
    self.textField.userInteractionEnabled = YES;
    [self.textField becomeFirstResponder];
    
    // 监控当前输入框的值
    [self.textField addTarget:self action:@selector(textFieldValueChangedAction:) forControlEvents:UIControlEventEditingChanged];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight)) style:(UITableViewStylePlain)];
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
        [self.tableView setSeparatorColor:[UIColor ls_colorWithHex:0xf0f2f5]];
    }
    self.extendedLayoutIncludesOpaqueBars = YES;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)textFieldValueChangedAction:(UITextField *)field
{
    NSString *keyword = field.text;
    [self searchDataWith:keyword];
}

- (void)searchDataWith:(NSString *)keyword
{
    [CBEMineVM requestBillListWithtradeType:@"" andQueryTime:@"" andKeyword:keyword complete:^(id data) {
        NSLog(@"%@",data);
        [self.tableView.mj_header endRefreshing];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *list = [data objectForKey:@"data"];
            
            NSInteger totalPages = [[data objectForKey:@"totalPages"] integerValue];
            if (totalPages <= self.pageNum) {
                [self.tableView.mj_footer resetNoMoreData];
            }
            if (self.pageNum == 0) {
                [self.dataSource removeAllObjects];
            }
            [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CBEMineBillModel *model = [CBEMineBillModel mj_objectWithKeyValues:obj];
                [self.dataSource addObject:model];
            }];
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
    }];
}

- (void)cancelAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 108.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEBillListCell *cell = [CBEBillListCell cellForTableView:tableView];
    CBEMineBillModel *model = [self.dataSource objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}

@end

//
//  CBEBillDetailController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBillDetailController.h"
#import "CBEStatusView.h"
#import "CBETimeView.h"
#import "CBENoOrderView.h"
#import "CBEBillListCell.h"
#import "CBEDepositRecordController.h"
#import "CBEBillSearchController.h"
#import "CBEMineVM.h"
#import "CBEMineBillModel.h"

@interface CBEBillDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet CBEBaseButton *statusBtn;
@property (weak, nonatomic) IBOutlet CBEBaseButton *timeBtn;

@property (nonatomic, strong) CBEStatusView *statusView;
@property (nonatomic, assign) StatusType selectType;//当前选择的选项

@property (nonatomic, strong) CBETimeView *timeView;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBENoOrderView *noDataView;

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger pageNum;

@property (nonatomic, copy) NSString *tradeType;
@property (nonatomic, copy) NSString *queryTime;

@end

@implementation CBEBillDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"账单明细"];
    
    UIButton *deButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [deButton setTitle:@"提现记录" forState:UIControlStateNormal];
    [deButton setTitleColor:kMainBlackColor forState:UIControlStateNormal];
    [deButton.titleLabel setFont:kFont(16)];
    deButton.frame = CGRectMake(kSCREEN_WIDTH - 83, 10, 68, 24);
    [deButton addTarget:self action:@selector(depositAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem.titleView addSubview:deButton];
    
    [self.statusBtn setTitleLabelRect:CGRectMake(0, 0, 38, 44)];
    [self.statusBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.statusBtn setImageViewRect:CGRectMake(38, 18, 12, 12)];
    [self.timeBtn setTitleLabelRect:CGRectMake(0, 0, 38, 44)];
    [self.timeBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.timeBtn setImageViewRect:CGRectMake(38, 18, 12, 12)];
    
    [self.view addSubview:self.tableView];
    
    self.tradeType = @"";
    self.queryTime = @"";
    [self.tableView.mj_header beginRefreshing];
}

- (void)requestBillData
{
    [CBEMineVM requestBillListWithtradeType:self.tradeType andQueryTime:self.queryTime andKeyword:@"" complete:^(id data) {
        NSLog(@"%@",data);
        [self.tableView.mj_header endRefreshing];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *list = [data objectForKey:@"data"];
            
            NSInteger totalPages = [[data objectForKey:@"totalPages"] integerValue];
            if (totalPages <= self.pageNum) {
                [self.tableView.mj_footer resetNoMoreData];
            }
            if (self.pageNum == 0) {
                [self.dataSource removeAllObjects];
            }
            [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CBEMineBillModel *model = [CBEMineBillModel mj_objectWithKeyValues:obj];
                [self.dataSource addObject:model];
            }];
            if (self.dataSource.count) {
                _noDataView.hidden = YES;
            }else{
                _noDataView.hidden = NO;
            }
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
    }];
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

//搜索
- (IBAction)searchAction:(UIButton *)sender {
    CBEBillSearchController *searchController = [[CBEBillSearchController alloc] init];
    [self.navigationController pushViewController:searchController animated:YES];
}
//状态
- (IBAction)statusAction:(CBEBaseButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.statusView = [CBEStatusView createView];
        self.statusView.frame = CGRectMake(0, 44, kSCREEN_WIDTH, kSCREEN_HEIGHT);
        [self.view addSubview:self.statusView];
        [self.statusView selectedStatus:self.selectType];
        kWeakSelf
        self.statusView.statusBlock = ^(StatusType type) {
            weakSelf.selectType = type;
            weakSelf.tradeType = StringFormat(@"%ld",type);
            if (type == StatusTypeAll) {
                weakSelf.tradeType = @"";
            }
            weakSelf.pageNum = 0;
            [weakSelf requestBillData];
            //刷新页面
            [weakSelf.statusView removeFromSuperview];
            sender.selected = NO;
        };
    }else{
        if (self.statusView) {
            [self.statusView removeFromSuperview];
        }
    }
}
//时间
- (IBAction)timeAction:(CBEBaseButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.timeView = [CBETimeView createView];
        self.timeView.frame = CGRectMake(0, 44, kSCREEN_WIDTH, kSCREEN_HEIGHT);
        [self.view addSubview:self.timeView];
        kWeakSelf
        self.timeView.timeBlock = ^(NSString *year,NSString *month){
            [weakSelf.timeView removeFromSuperview];
            sender.selected = NO;
            NSLog(@"%@-%@",year,month);
            weakSelf.pageNum = 0;
            weakSelf.queryTime = StringFormat(@"%@-%@",year,month);
            [weakSelf requestBillData];
        };
    }else{
        if (self.timeView) {
            [self.timeView removeFromSuperview];
        }
    }
}

//提现记录
- (void)depositAction
{
    CBEDepositRecordController *depositController = [[CBEDepositRecordController alloc] init];
    [self.navigationController pushViewController:depositController animated:YES];
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 108.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEBillListCell *cell = [CBEBillListCell cellForTableView:tableView];
    CBEMineBillModel *model = [self.dataSource objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - 50) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
        _tableView.backgroundColor = kMainDefaltGrayColor;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(requestBillData)];
        
        _noDataView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _tableView.width, _tableView.height)];
        _noDataView.hidden = YES;
        _noDataView.title = @"暂无账单";
        [_tableView addSubview:_noDataView];
    }
    return _tableView;
}

@end

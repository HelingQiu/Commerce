//
//  LSScanQRCodeViewController.m
//  LSQRCode
//
//  Created by 韩云彬 on 2018/6/25.
//  Copyright © 2018年 杨荣. All rights reserved.
//

#import "CBEScanQRCodeViewController.h"
#import "LSQRCodeManager.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import <AVFoundation/AVFoundation.h>
#import "CBEPwCheckCodeController.h"
#import "CBEScanResultController.h"
#import "CBEMineVM.h"
#import "CBEOrderDetailModel.h"

#define kKuajingHelp @"kuajinghelp:"

@interface CBEScanQRCodeViewController ()<TZImagePickerControllerDelegate>

@property (nonatomic, strong) LSQRCodeManager *manager;

@end

@implementation CBEScanQRCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor ls_colorWithHex:0x000000 andAlpha:0.7];
    
    self.manager = [[LSQRCodeManager alloc] init];
    NSDictionary *scanningOptions = @{LSQRCodeScanningViewColorKey: [UIColor whiteColor],
                                      LSQRCodeScanningLineImageKey: [UIImage imageNamed:@"scanLine"]
                                      };
    
    [self.manager scanQRCodeOnView:self.view scanAreaSize:CGSizeMake(250, 250) isNeedScanningAnimation:YES scanningOptions:scanningOptions completionHandler:^(NSString * _Nullable result) {
        NSLog(@"扫描结果是: ================> %@", result);
        
        if ([result containsString:kKuajingHelp]) {
            
            result = [result stringByReplacingOccurrencesOfString:kKuajingHelp withString:@""];
            [CommonUtils showHUDWithWaitingMessage:nil];
            [CBEMineVM verifyETicketWithticketId:result complete:^(id data) {
                [CommonUtils hideHUD];
                if ([[data objectForKey:@"success"] boolValue]) {
                    CBEOrderDetailModel *detailModel = [CBEOrderDetailModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
                    
                    CBEScanResultController *resultController = [[CBEScanResultController alloc] init];
                    resultController.ticketId = result;
                    resultController.detailModel = detailModel;
                    [self.navigationController pushViewController:resultController animated:YES];
                }else{
                    [self.manager startScanning];
                    [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
                }
             } fail:^(NSError *error) {
                 [self.manager startScanning];
                 [CommonUtils hideHUD];
                 [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
             }];
        }else{
            [self.manager startScanning];
            [CommonUtils showHUDWithMessage:@"请扫描正确的二维码" autoHide:YES];
        }
        
    } failureHandler:^(NSError * _Nullable error) {
        NSLog(@"error ===============> %@", error);
    }];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(15, kStatusHeight, 24, 44);
    [backButton setImage:IMAGE_NAMED(@"arrow_back") forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 1;
    label.frame = CGRectMake(24, kStatusHeight, kSCREEN_WIDTH-24*2, 44);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"扫码验票";
    label.textColor = [UIColor whiteColor];
    label.font = kBoldFont(17);
    [self.view addSubview:label];
    
    UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kSCREEN_HEIGHT/2 - 125 - 50, kSCREEN_WIDTH, 18)];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.text = @"对准电子票到框内，即可自动验票";
    topLabel.textColor = [UIColor whiteColor];
    topLabel.font = kBoldFont(13);
    [self.view addSubview:topLabel];
    
    UIButton *lightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [lightBtn setFrame:CGRectMake(kSCREEN_WIDTH - 60, kSCREEN_HEIGHT/2 - 125 - 50 - 40, 40, 40)];
    [lightBtn setImage:IMAGE_NAMED(@"me_icon_unlight") forState:UIControlStateNormal];
    [lightBtn addTarget:self action:@selector(lightAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:lightBtn];
    
    UIButton *pwBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    pwBtn.frame = CGRectMake(kSCREEN_WIDTH/2 - 55, kSCREEN_HEIGHT/2 + 125 + 61, 110, 36);
    [pwBtn setTitle:@"密码验票" forState:UIControlStateNormal];
    [pwBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [pwBtn.titleLabel setFont:kFont(13)];
    pwBtn.layer.cornerRadius = 20;
    pwBtn.layer.borderWidth = 1;
    pwBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [pwBtn addTarget:self action:@selector(pwAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:pwBtn];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    [self.manager startScanning];
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)lightAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOn];//开
        [device unlockForConfiguration];
        
    } else{
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOff];
        [device unlockForConfiguration];
    }
}

- (void)pwAction
{
    CBEPwCheckCodeController *checkController = [[CBEPwCheckCodeController alloc] init];
    [self.navigationController pushViewController:checkController animated:NO];
}

- (void)gotoAlbum
{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//
////    [self.manager startScanning];
//}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.manager stopScanning];
}

- (void)dealloc {
    NSLog(@"%s", __func__);
}

@end

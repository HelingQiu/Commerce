//
//  CBEDepositMoneyController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEDepositMoneyController.h"
#import "CBEMineVM.h"

@interface CBEDepositMoneyController ()
@property (weak, nonatomic) IBOutlet UITextField *totalField;
@property (weak, nonatomic) IBOutlet UITextField *depositField;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;

@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) int countTime;

@end

@implementation CBEDepositMoneyController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"申请提现"];
    
    [self.totalField setText:StringFormat(@"%@元",self.model.mayDepositMoney)];
}

- (IBAction)codeAction:(UIButton *)sender {
    
    if ([CommonUtils isBlankString:self.depositField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入提现金额" autoHide:YES];
        return;
    }
    if ([self.depositField.text integerValue] <= 0) {
        [CommonUtils showHUDWithMessage:@"提现金额必须大于0元" autoHide:YES];
        return;
    }
//    if ([self.depositField.text integerValue] > [self.model.mayDepositMoney integerValue]) {
//        [CommonUtils showHUDWithMessage:@"提现金额不能大于最大可提现金额" autoHide:YES];
//        return;
//    }
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM getWithdrawSmsCodeWithActivityId:self.model.depositId withBillPrice:self.depositField.text complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"验证码发送成功，请留意短信" autoHide:YES];
            //请求成功后倒计时
            _countTime = 59;
            [sender setTitle:[NSString stringWithFormat:@"%ds",_countTime] forState:UIControlStateNormal];
            sender.enabled = NO;
            [self addTimer];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
    
}

- (IBAction)submit:(UIButton *)sender {
    if ([CommonUtils isBlankString:self.depositField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入提现金额" autoHide:YES];
        return;
    }
    //加个金额不能大于可提现金额
//    if ([self.depositField.text integerValue] > [self.model.mayDepositMoney integerValue]) {
//        [CommonUtils showHUDWithMessage:@"提现金额不能大于最大可提现金额" autoHide:YES];
//        return;
//    }
    
    if ([CommonUtils isBlankString:self.codeField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入验证码" autoHide:YES];
        return;
    }
    
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM applyDepositWithActivityId:self.model.depositId withBillPrice:self.depositField.text withSmsCode:self.codeField.text complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"提现申请成功，等待审核" autoHide:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
    
}

- (void)addTimer
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
}

- (void)countdown {
    _countTime --;
    if (_countTime == 0) {
        _countTime = 60;
        _codeBtn.enabled = YES;
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self removeTimer];
        _timer = nil;
        
    }else if(self.countTime < 10){
        _codeBtn.enabled = NO;
        [_codeBtn setTitle:[NSString stringWithFormat:@"0%ds",_countTime] forState:UIControlStateNormal];
    }else{
        _codeBtn.enabled = NO;
        [_codeBtn setTitle:[NSString stringWithFormat:@"%ds",_countTime] forState:UIControlStateNormal];
    }
}

- (void)removeTimer
{
    [_timer invalidate];
    _timer = nil;
}

@end

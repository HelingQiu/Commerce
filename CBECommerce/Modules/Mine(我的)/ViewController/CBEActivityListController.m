//
//  CBEActivityListController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEActivityListController.h"
#import "CBEActivityCell.h"
#import "CBENoOrderView.h"
#import "CBEMineVM.h"
#import "CBEActivityModel.h"
#import "CBEActivityDetailController.h"

@interface CBEActivityListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBENoOrderView *noDataView;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation CBEActivityListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"活动收藏"];
    [self.view addSubview:self.tableView];
    _dataSource = [NSMutableArray array];
    [self.tableView.mj_header beginRefreshing];
}

- (void)requestListData
{
    [CBEMineVM requestActivityCollectListWithPageNum:self.pageIndex complete:^(id data) {
        [self.tableView.mj_header endRefreshing];
        if ([[data objectForKey:@"success"] boolValue]) {
            if (self.pageIndex == 1) {
                [_dataSource removeAllObjects];
            }
            NSInteger totalNum = [[data objectForKey:@"totalPages"] integerValue];
            if (self.pageIndex == totalNum) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self.tableView.mj_footer endRefreshing];
            }
            NSArray *array = [data objectForKey:@"data"];
            NSArray *resultArray = [CBEActivityModel mj_objectArrayWithKeyValuesArray:array];
            [self.dataSource addObjectsFromArray:resultArray];
            if (!self.dataSource.count) {
                self.noDataView.hidden = NO;
            }else{
                self.noDataView.hidden = YES;
                [self.tableView reloadData];
            }
        }
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 165+(kSCREEN_WIDTH - 2*kPadding15)*(180/345.0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEActivityCell *cell = [CBEActivityCell cellForTableView:tableView];
    CBEActivityModel *model = [self.dataSource objectAtIndex:indexPath.section];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEActivityModel *model = [self.dataSource objectAtIndex:indexPath.section];
    if (model.activeStatus > 1) {
        [CommonUtils showHUDWithMessage:@"活动已结束" autoHide:YES];
    }else{
        CBEActivityDetailController *detailController = [[CBEActivityDetailController alloc] init];
        detailController.detailModel = model;
        [self.navigationController pushViewController:detailController animated:YES];
    }
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.pageIndex = 1;
            [self requestListData];
        }];
        _tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
            self.pageIndex ++;
            [self requestListData];
        }];
        
        _noDataView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _tableView.width, _tableView.height)];
        _noDataView.hidden = YES;
        [_tableView addSubview:_noDataView];
    }
    return _tableView;
}

@end

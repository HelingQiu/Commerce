//
//  CBEDepositRecordController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEDepositRecordController.h"
#import "CBENoOrderView.h"
#import "CBEBillListCell.h"
#import "CBEMineVM.h"
#import "CBEMineBillModel.h"

@interface CBEDepositRecordController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBENoOrderView *noDataView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation CBEDepositRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"提现记录"];
     [self.view addSubview:self.tableView];
    [self requestDepositRecordData];
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)requestDepositRecordData
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM requstDepositRecordDataComplete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *dataArray = [data objectForKey:@"data"];
            self.dataSource = [dataArray mutableCopy];
            if (self.dataSource.count) {
                _noDataView.hidden = YES;
            }else{
                _noDataView.hidden = NO;
            }
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [[self.dataSource objectAtIndex:section] objectForKey:@"data"];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 28.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 28.f)];
    headView.backgroundColor = kMainDefaltGrayColor;
    
    NSString *showDate = [[self.dataSource objectAtIndex:section] objectForKey:@"showDate"];
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, kSCREEN_WIDTH - 30, 28.f)];
    [headLabel setFont:kFont(13)];
    [headLabel setText:showDate];
    [headLabel setTextColor:kMainTextColor];
    [headView addSubview:headLabel];
    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 108.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEBillListCell *cell = [CBEBillListCell cellForTableView:tableView];
    NSArray *array = [[self.dataSource objectAtIndex:indexPath.section] objectForKey:@"data"];
    NSDictionary *body = [array objectAtIndex:indexPath.row];
    CBEMineBillModel *model = [CBEMineBillModel mj_objectWithKeyValues:body];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
        _tableView.backgroundColor = kMainDefaltGrayColor;
        
        _noDataView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _tableView.width, _tableView.height)];
        _noDataView.hidden = YES;
        _noDataView.title = @"暂无提现记录";
        [_tableView addSubview:_noDataView];
    }
    return _tableView;
}

@end

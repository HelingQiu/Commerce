//
//  CBEAlipayAcountController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAlipayAcountController.h"
#import "CBEMineVM.h"

@interface CBEAlipayAcountController ()

@property (weak, nonatomic) IBOutlet UITextField *alipayField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;

@end

@implementation CBEAlipayAcountController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"填写支付宝账号"];
    [self setBarButton:BarButtonItemRight action:@selector(saveAccountAction) title:@"保存"];
}

- (void)saveAccountAction
{
    [self.view endEditing:YES];
    if ([CommonUtils isBlankString:self.alipayField.text]) {
        [CommonUtils showHUDWithMessage:@"请填写支付宝账号" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.nameField.text]) {
        [CommonUtils showHUDWithMessage:@"请填写您的真实姓名" autoHide:YES];
        return;
    }
    
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM addZfbAccountWith:self.alipayField.text withName:self.nameField.text complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"保存成功" autoHide:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

@end

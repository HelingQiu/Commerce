//
//  CBEMineInfoController.h
//  CBECommerce
//
//  Created by don on 2018/9/28.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEMineSponsorModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CBEMineInfoController : CBEBaseController

@property (nonatomic, strong) CBEMineSponsorModel *sponsorInfoModel;

@end

NS_ASSUME_NONNULL_END

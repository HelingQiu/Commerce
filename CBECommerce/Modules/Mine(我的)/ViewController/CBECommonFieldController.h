//
//  CBECommonFieldController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "UIViewController+NavigationItem.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, PageFieldType) {
    PageFieldTypePhone = 0,//主办方联系方式
    PageFieldTypeName,//主办方名称
    PageFieldTypeSchool,//学校
};

@interface CBECommonFieldController : CBEBaseController

@property (nonatomic, assign) PageFieldType type;

@end

NS_ASSUME_NONNULL_END

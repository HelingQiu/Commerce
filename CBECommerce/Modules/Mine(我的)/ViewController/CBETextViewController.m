//
//  CBECommonTextViewController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBETextViewController.h"
#import "CBEMineVM.h"

@interface CBETextViewController ()

@property (nonatomic, strong) UITextView *textView;

@end

@implementation CBETextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"主办方简介"];
    [self setBarButton:BarButtonItemRight action:@selector(doneAction) title:@"完成"];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 200)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(15, 0, kSCREEN_WIDTH - 30, 200)];
    self.textView.placeholder = @"输入主办方简介，最多200字";
    self.textView.textColor = kMainTextColor;
    self.textView.font = kFont(15);
    [backView addSubview:self.textView];
}

- (void)doneAction
{
    if ([CommonUtils isBlankString:self.textView.text]) {
        [CommonUtils showHUDWithMessage:@"请输入主办方简介" autoHide:YES];
        return;
    }
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM modifyOrAddSponsorInfo:nil withLogo:nil withMobile:nil withIntroduce:self.textView.text complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"提交成功" autoHide:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

@end

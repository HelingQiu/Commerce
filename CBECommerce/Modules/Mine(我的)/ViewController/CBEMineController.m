//
//  CBEMineController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/2.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMineController.h"
#import "UserInfoView.h"
#import "CBEHomeHeaderView.h"
#import "CBEScanQRCodeViewController.h"
#import "CBEMyorderCell.h"
#import "CBEMySponsorOrderCell.h"
#import "CBEMineCutomCell.h"
#import "CBEOrderListController.h"
#import "CBEMineInfoController.h"
#import "CBEOrderListController.h"
#import "CBEMineOrderCell.h"
#import "CBEAlipayAcountController.h"
#import "CBEPersonalCertificateController.h"
#import "CBEDepositListController.h"
#import "CBESettingController.h"
#import "CBEActivityListController.h"
#import "CBENewsCollectController.h"
#import "CBEAddressBookController.h"
#import "CBEBillDetailController.h"
#import "CBEMineVM.h"
#import "CBEMineInfoModel.h"
#import "CBEMineSponsorModel.h"
#import "CBEOrderListModel.h"
#import "CBEWaitPayController.h"
#import "CBEOrderDetailController.h"
#import "CBEEditCardController.h"

typedef NS_ENUM(NSUInteger, CBEUserType) {
    CBEUserTypeAuther = 0,//参与方
    CBEUserTypeSponsor    //主办方
};
@interface CBEMineController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataArray;
@property (assign, nonatomic) CGFloat headerViewHeight;
@property (nonatomic,strong) UserInfoView *userInfoView;
@property (nonatomic, strong) UIButton *scanButton;
@property (nonatomic, assign) CBEUserType userType;
@property (nonatomic, strong) CBEBaseButton *leftButton;

@property (nonatomic, strong) CBEMineInfoModel *mineInfoModel;
@property (nonatomic, strong) CBEMineSponsorModel *sponsorInfoModel;

@end

@implementation CBEMineController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.headerViewHeight = kUSER_INFO_HEADERVIEW_H;
    
    [self configUI];
    
    [self configData];
    
    [self configTopView];
    
    [self configTopButton];
    
    [self requestCustomerData];
}
#pragma mark - request
- (void)requestCustomerData
{
    [CBEMineVM requestMineInfoComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            self.mineInfoModel = [CBEMineInfoModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
            [_userInfoView setAuthorDataWith:self.mineInfoModel];
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
};

- (void)requestSponsorData
{
    [CBEMineVM getMineSponsorComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            self.sponsorInfoModel = [CBEMineSponsorModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
            [_userInfoView setSponnarDataWith:self.sponsorInfoModel];
            [self.tableView reloadData];
        }else if ([[data objectForKey:@"errorInfo"] containsString:@"未申请认证"]) {
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            /*
             self.userType = CBEUserTypeAuther;
             [btn setTitle:@"切换为主办方" forState:UIControlStateNormal];
             self.scanButton.hidden = YES;
             [self requestCustomerData];
             [_userInfoView setAuthorDataWith:self.mineInfoModel];
             [self.tableView reloadData];
             */
        }
    } fail:^(NSError *error) {
        
    }];
}

#pragma amrk - private
- (void)configData {
    self.dataArray = @[@[@"修改密码",@"绑定手机号"],@[@"新消息通知",@"关于我们"]];
}

- (void)configUI {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)configTopButton {
    self.userType = CBEUserTypeAuther;
    NSString *title = @"切换为主办方";
    CGFloat titleWidth = [CommonUtils widthForString:title Font:kFont(12) andWidth:80];
    CBEBaseButton *leftButton = [[CBEBaseButton alloc] initWithFrame:CGRectMake(kPadding15, (KIsiPhoneX?44:20), 24 + titleWidth + 2, 44)];
    [leftButton setImage:[UIImage imageNamed:@"me_icon_exchange"] forState:UIControlStateNormal];
    [leftButton setTitle:title forState:UIControlStateNormal];
    [leftButton setTitleLabelRect:CGRectMake(26, 14, titleWidth, 16)];
    [leftButton setImageViewRect:CGRectMake(0, 12, 24, 24)];
    [leftButton.titleLabel setFont:kFont(12)];
    [leftButton addTarget:self action:@selector(leftAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(kSCREEN_WIDTH - 24 - kPadding15,(KIsiPhoneX?44:20) + 10, 24, 24)];
    [rightButton setBackgroundImage:IMAGE_NAMED(@"me_icon_setting") forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightButton];
    
    self.scanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.scanButton setFrame:CGRectMake(kSCREEN_WIDTH - 24 - kPadding15*2 - 24 ,(KIsiPhoneX?44:20) + 10, 24, 24)];
    [self.scanButton setBackgroundImage:IMAGE_NAMED(@"me_icon_scan") forState:UIControlStateNormal];
    [self.scanButton addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.scanButton];
    self.scanButton.hidden = YES;
}

- (UIView *)configTopView
{
    _userInfoView = [[UserInfoView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, self.headerViewHeight)];
    _userInfoView.animationType = FBUserInfoHeaderViewAnimationTypeScale;
//    [self.view addSubview:_userInfoView];
    [self.view addSubview:_tableView];
    _tableView.tableHeaderView = _userInfoView;
    kWeakSelf
    _userInfoView.clickBlock = ^(ActionType type) {
        if (type == ActionType_Info) {
            if (self.userType == CBEUserTypeAuther) {//参与方
                //编辑名片
                CBEEditCardController *editCardVC = [[CBEEditCardController alloc] init];
                [weakSelf.navigationController pushViewController:editCardVC animated:YES];
            }else{
                //主办方资料
                CBEMineInfoController *infoController = [[CBEMineInfoController alloc] init];
                infoController.sponsorInfoModel = weakSelf.sponsorInfoModel;
                [weakSelf.navigationController pushViewController:infoController animated:YES];
            }
        }else if (type == ActionType_Activity) {
            CBEActivityListController *activityController = [[CBEActivityListController alloc] init];
            [weakSelf.navigationController pushViewController:activityController animated:YES];
        }else if (type == ActionType_News) {
            CBENewsCollectController *newsController = [[CBENewsCollectController alloc] init];
            [weakSelf.navigationController pushViewController:newsController animated:YES];
        }else if (type == ActionType_Friends) {
            CBEAddressBookController *addressController = [[CBEAddressBookController alloc] init];
            [weakSelf.navigationController pushViewController:addressController animated:YES];
        }
    };
    
    return _userInfoView;
}

- (void)leftAction:(CBEBaseButton *)btn
{
    if (self.userType == CBEUserTypeAuther) {
        //切换为主办方
        self.userType = CBEUserTypeSponsor;
        [btn setTitle:@"切换为参与方" forState:UIControlStateNormal];
        self.scanButton.hidden = NO;
        [self requestSponsorData];
        [_userInfoView setSponnarDataWith:self.sponsorInfoModel];
        [self.tableView reloadData];
    }else{
        self.userType = CBEUserTypeAuther;
        [btn setTitle:@"切换为主办方" forState:UIControlStateNormal];
        self.scanButton.hidden = YES;
        [self requestCustomerData];
        [_userInfoView setAuthorDataWith:self.mineInfoModel];
        [self.tableView reloadData];
    }
}
//设置
- (void)rightAction
{
    CBESettingController *setController = [[CBESettingController alloc] init];
    [self.navigationController pushViewController:setController animated:YES];
}
//扫码
- (void)scanAction
{
    CBEScanQRCodeViewController *scanController = [[CBEScanQRCodeViewController alloc] init];
    [self.navigationController pushViewController:scanController animated:YES];
}

//全部订单
- (void)moreOrderAction
{
    CBEOrderListController *orderController = [[CBEOrderListController alloc] init];
    orderController.pageType = OrderPageTypeAll;
    [self.navigationController pushViewController:orderController animated:YES];
}
//账单明细
- (void)allOrderAction
{
    CBEBillDetailController *billController = [[CBEBillDetailController alloc] initWithNibName:@"CBEBillDetailController" bundle:nil];
    [self.navigationController pushViewController:billController animated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.userType == CBEUserTypeSponsor) {
        return 2;
    }
    if (self.mineInfoModel && self.mineInfoModel.newsOrderList.count) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.userType == CBEUserTypeSponsor) {
        if (section == 1) {
            return 3;
        }
        return 1;
    }
    if (section == 0) {
        return 1;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    if (self.userType == CBEUserTypeSponsor) {
        if (section == 0) {
            CBEMySponsorOrderCell *cell = [CBEMySponsorOrderCell cellForTableView:tableView];
            cell.depositBlock = ^{
              //去提现
                CBEDepositListController *depositController = [[CBEDepositListController alloc] init];
                [self.navigationController pushViewController:depositController animated:YES];
            };
            [cell refreshDataWith:self.sponsorInfoModel];
            return cell;
        }
        CBEMineCutomCell *cell = [CBEMineCutomCell cellForTableView:tableView];
        if (indexPath.row == 0) {
            [cell.leftView setImage:IMAGE_NAMED(@"me_icon_renzheng")];
            [cell.leftLabel setText:@"个人认证"];
            if (self.sponsorInfoModel) {
                if (self.sponsorInfoModel.approverStatus == -1) {
                    [cell.rightLabel setText:@"未认证"];
                }else if (self.sponsorInfoModel.approverStatus == 0) {
                    [cell.rightLabel setText:@"认证中"];
                }else if (self.sponsorInfoModel.approverStatus == 1) {
                    [cell.rightLabel setText:@"认证完成"];
                }else if (self.sponsorInfoModel.approverStatus == 2) {
                    [cell.rightLabel setText:@"认证失败"];
                }else{
                    [cell.rightLabel setText:@"未认证"];
                }
            }else{
                [cell.rightLabel setText:@"未认证"];
            }
        }else if (indexPath.row == 1) {
            [cell.leftView setImage:IMAGE_NAMED(@"me_icon_alipay")];
            [cell.leftLabel setText:@"支付宝账号"];
            if ([CommonUtils isBlankString:self.sponsorInfoModel.zfbAccount]) {
                [cell.rightLabel setText:@"未填写"];
            }else{
                 [cell.rightLabel setText:self.sponsorInfoModel.zfbAccount];
            }
        }else{
            [cell.leftView setImage:IMAGE_NAMED(@"me_icon_service")];
            [cell.leftLabel setText:@"联系客服"];
            [cell.rightLabel setText:@"400-345-500"];
        }
        return cell;
    }
    if (section == 0) {
        CBEMyorderCell *cell = [CBEMyorderCell cellForTableView:tableView];
        cell.orderBlock = ^(OrderStatus status) {
            CBEOrderListController *orderController = [[CBEOrderListController alloc] init];
            if (status == OrderStatusWaitPay) {
                orderController.pageType = OrderPageTypeWaitPay;
            }else if (status ==OrderStatusCanyu) {
                orderController.pageType = OrderPageTypeWaitJoin;
            }else if (status == OrderStatusRefund) {
                orderController.pageType = OrderPageTypeRefund;
            }else if (status == OrderStatusDone) {
                orderController.pageType = OrderPageTypeComplete;
            }
            [self.navigationController pushViewController:orderController animated:YES];
        };
        [cell refreshDataWith:self.mineInfoModel];
        return cell;
    }
    CBEMineOrderCell *cell = [CBEMineOrderCell cellForTableView:tableView];
    [cell refreshDataWith:self.mineInfoModel];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    if (self.userType == CBEUserTypeSponsor) {
        if (section == 0) {
            return 275.f;
        }
        return 50.f;
    }
    if (section == 0) {
        return 80.f;
    }
    return 105.f;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.userType == CBEUserTypeSponsor) {
        if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                if (self.sponsorInfoModel) {
                    NSInteger approverStatus = self.sponsorInfoModel.approverStatus;
                    if (approverStatus == -1 || approverStatus == 2) {
                        CBEPersonalCertificateController *pcController = [[CBEPersonalCertificateController alloc] initWithNibName:@"CBEPersonalCertificateController" bundle:nil];
                        [self.navigationController pushViewController:pcController animated:YES];
                    }
                }else{
                    CBEPersonalCertificateController *pcController = [[CBEPersonalCertificateController alloc] initWithNibName:@"CBEPersonalCertificateController" bundle:nil];
                    [self.navigationController pushViewController:pcController animated:YES];
                }
            }else if (indexPath.row == 1) {
                if ([CommonUtils isBlankString:self.sponsorInfoModel.zfbAccount]) {
                    CBEAlipayAcountController *alipayController = [[CBEAlipayAcountController alloc] initWithNibName:@"CBEAlipayAcountController" bundle:nil];
                    [self.navigationController pushViewController:alipayController animated:YES];
                }
            }else{
                NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",@"400345500"];
                UIWebView * callWebview = [[UIWebView alloc] init];
                [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
                [self.view addSubview:callWebview];
                
            }
        }
    }else{
        if (indexPath.section != 0) {
            NSDictionary *body = [self.mineInfoModel.newsOrderList firstObject];
            CBEOrderListModel *listModel = [[CBEOrderListModel alloc] init];
            listModel.orderId = [body objectForKey:@"orderId"];
            listModel.activityId = [body objectForKey:@"activityId"];
            listModel.title = [body objectForKey:@"title"];
            listModel.orderType = [[body objectForKey:@"orderType"] integerValue];
            listModel.picture = [body objectForKey:@"picture"];
            listModel.timeList = [body objectForKey:@"timeList"];
            //跳转到订单详情页面
            OrderStatusPageType type = 0;
            
            if (listModel.orderType == 1) {
                type = OrderStatusPageTypeWaitPay;
            }else if (listModel.orderType == 2) {
                type = OrderStatusPageTypeWaitJoin;
            }else if (listModel.orderType == 3) {
                type = OrderStatusPageTypeRefund;
            }else if (listModel.orderType == 4) {
                type = OrderStatusPageTypeComplete;
            }else{
                type = OrderStatusPageTypeCancel;
            }
            
            if (type != OrderStatusPageTypeWaitPay) {
                CBEOrderDetailController *detailController = [[CBEOrderDetailController alloc] init];
                detailController.orderModel = listModel;
                detailController.pageType = type;
                [self.navigationController pushViewController:detailController animated:YES];
            }else{
                
                CBEWaitPayController *payController = [[CBEWaitPayController alloc] init];
                payController.orderModel = listModel;
                [self.navigationController pushViewController:payController animated:YES];
            }
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 59)];
    backView.backgroundColor = kMainDefaltGrayColor;
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, kPadding15, kSCREEN_WIDTH, 44)];
    [headView setBackgroundColor:[UIColor whiteColor]];
    [backView addSubview:headView];
    
    UILabel *labTitle = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 200, 44)];
    [labTitle setFont:kFont(16)];
    [labTitle setTextColor:kMainBlackColor];
    [headView addSubview:labTitle];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, kSCREEN_WIDTH, 0.5)];
    [line setBackgroundColor:kMainLineColor];
    [headView addSubview:line];
    
    if (self.userType == CBEUserTypeAuther) {
        if (section == 0) {
            [labTitle setText:@"我的订单"];
            CBEBaseButton *moreButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
            [moreButton setFrame:CGRectMake(backView.width - 92, 10, 84, 24)];
            [moreButton setTitleLabelRect:CGRectMake(0, 0, 60, 24)];
            [moreButton setImageViewRect:CGRectMake(60, 0, 24, 24)];
            [moreButton setTitle:@"全部订单" forState:UIControlStateNormal];
            [moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [moreButton.titleLabel setFont:kFont(14)];
            moreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [moreButton setImage:IMAGE_NAMED(@"arrow_right") forState:UIControlStateNormal];
            [moreButton addTarget:self action:@selector(moreOrderAction) forControlEvents:UIControlEventTouchUpInside];
            [headView addSubview:moreButton];
        }else{
            [labTitle setText:@"最新订单"];
        }
    }else{
        if (section == 0) {
            [labTitle setText:@"我的账单"];
            CBEBaseButton *moreButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
            [moreButton setFrame:CGRectMake(backView.width - 92, 10, 84, 24)];
            [moreButton setTitleLabelRect:CGRectMake(0, 0, 60, 24)];
            [moreButton setImageViewRect:CGRectMake(60, 0, 24, 24)];
            [moreButton setTitle:@"账单明细" forState:UIControlStateNormal];
            [moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [moreButton.titleLabel setFont:kFont(14)];
            moreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [moreButton setImage:IMAGE_NAMED(@"arrow_right") forState:UIControlStateNormal];
            [moreButton addTarget:self action:@selector(allOrderAction) forControlEvents:UIControlEventTouchUpInside];
            [headView addSubview:moreButton];
        }else{
            return [[UIView alloc] init];
        }
    }
    
    return backView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.userType == CBEUserTypeSponsor) {
        if (section == 0) {
            return 59.f;
        }
        return kPadding15;
    }
    return 59.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (_tableView.contentOffset.y <= 0) {
        _tableView.bounces = NO;
        NSLog(@"禁止下拉");
    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.view.mj_y, self.view.width, kSCREEN_HEIGHT - kTabbarHeight - 1) style:UITableViewStyleGrouped];
        if (@available(iOS 11.0, *)) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        tableView.backgroundColor = kMainDefaltGrayColor;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        tableView.estimatedSectionFooterHeight = 0.1;
        //占位用的view
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, self.headerViewHeight)];
//        view.backgroundColor = [UIColor clearColor];
//        tableView.tableHeaderView = view;
//        tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0.01)];
//        [self.view addSubview:tableView];
        _tableView = tableView;
    }
    return _tableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CBEMineInfoController.m
//  CBECommerce
//
//  Created by don on 2018/9/28.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMineInfoController.h"
#import "CBELeftRightLabelCell.h"
#import "CBEImageLabelCell.h"
#import "CBEPersonalCertificateController.h"
#import "CBECommonFieldController.h"
#import "CBETextViewController.h"
#import "CBEMineVM.h"
#import <QiniuSDK.h>
#import "CBESponsorInfoModel.h"

@interface CBEMineInfoController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) UIImageView *imgView;
//@property (nonatomic, strong) CBESponsorInfoModel *sponsorInfoModel;

@end

@implementation CBEMineInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"主办方资料"];
    self.dataSource = @[@{@"title":@"主办方logo",@"type":@(0)},
                        @{@"title":@"主办方名称",@"type":@(1)},
                        @{@"title":@"主办方简介",@"type":@(2)},
                        @{@"title":@"联系方式",@"type":@(3)},
                        @{@"title":@"个人认证",@"type":@(4)}
                        ];
    [self.view addSubview:self.tableView];
//    [self requestSponsorData];
}

- (void)requestSponsorData
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM requestSponsorDataComplete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
//            self.sponsorInfoModel = [CBESponsorInfoModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 88.f;
    }
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [self.dataSource objectAtIndex:indexPath.row];
    NSInteger type = [[dict objectForKey:@"type"] integerValue];
    if (indexPath.row == 0) {
        CBEImageLabelCell *cell = [CBEImageLabelCell cellForTableView:tableView];
        [cell.titleLabel setText:[dict objectForKey:@"title"]];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:[self.sponsorInfoModel.photo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"me_logo_default")];
        self.imgView = cell.imgView;
        return cell;
    }
    CBELeftRightLabelCell *cell = [CBELeftRightLabelCell cellForTableView:tableView];
    [cell.titleLabel setText:[dict objectForKey:@"title"]];
    if (type == 1) {
        [cell.rightLabel setText:self.sponsorInfoModel.sponsorName?:@""];
    }else if (type == 2) {
        [cell.rightLabel setText:self.sponsorInfoModel.introduce?:@""];
    }else if (type == 3) {
        [cell.rightLabel setText:self.sponsorInfoModel.mobile?:@""];
    }else if (type == 4) {
        if (self.sponsorInfoModel.sponsorStatus == -1) {
            [cell.rightLabel setText:@"未认证"];
        }else if (self.sponsorInfoModel.sponsorStatus == 0) {
            [cell.rightLabel setText:@"认证中"];
        }else if (self.sponsorInfoModel.sponsorStatus == 1) {
            [cell.rightLabel setText:@"认证完成"];
        }else if (self.sponsorInfoModel.sponsorStatus == 2) {
            [cell.rightLabel setText:@"认证失败"];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = [self.dataSource objectAtIndex:indexPath.row];
    NSInteger type = [[dict objectForKey:@"type"] integerValue];
    if (type == 0) {
        [self actionSheetShow];
    }else if (type == 1) {
        CBECommonFieldController *fieldController = [[CBECommonFieldController alloc] init];
        fieldController.type = PageFieldTypeName;
        [self.navigationController pushViewController:fieldController animated:YES];
    }else if (type == 2) {
        CBETextViewController *textController = [[CBETextViewController alloc] init];
        [self.navigationController pushViewController:textController animated:YES];
    }else if (type == 3) {
        CBECommonFieldController *fieldController = [[CBECommonFieldController alloc] init];
        fieldController.type = PageFieldTypePhone;
        [self.navigationController pushViewController:fieldController animated:YES];
    }else if (type == 4) {
        NSInteger approverStatus = self.sponsorInfoModel.sponsorStatus;
        if (approverStatus == -1 || approverStatus == 2) {
            CBEPersonalCertificateController *pcController = [[CBEPersonalCertificateController alloc] initWithNibName:@"CBEPersonalCertificateController" bundle:nil];
            [self.navigationController pushViewController:pcController animated:YES];
        }
    }
}

- (void)actionSheetShow
{
    [UIActionSheet ls_actionSheetWithCallBackBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {// 判断系统是否支持相机
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1001;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相机权限" autoHide:YES];
            }
        }else if (buttonIndex == 1){// 判断系统是否支持相册
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1001;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相册权限" autoHide:YES];
            }
        }
    } showInView:self.view title:@"主办方logo照片" destructiveButtonTitle:@"相机" cancelButtonName:@"取消" otherButtonTitles:@"相册", nil];
}

#pragma mark - 相机相册
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage]; //通过key值获取到图片
    self.imgView.image = image;
    //上传图片到服务器--在这里进行图片上传的网络请求
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM getQiniuTokenComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSString *token = [data objectForKey:@"data"];
            QNUploadManager *upManager = [[QNUploadManager alloc] init];
            NSData *imageData = UIImagePNGRepresentation(image);
            NSString *uuid = [CommonUtils getUUID];
            [upManager putData:imageData key:uuid token:token
                      complete: ^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                          NSLog(@"%@", info);
                          NSLog(@"%@", resp);
                          //成功后调后台接口上传
                          if (info.statusCode == 200) {
                              [CBEMineVM modifyOrAddSponsorInfo:nil withLogo:uuid withMobile:nil withIntroduce:nil complete:^(id data) {
                                  [CommonUtils hideHUD];
                              } fail:^(NSError *error) {
                                  [CommonUtils hideHUD];
                              }];
                          }else{
                              [CommonUtils hideHUD];
                          }
                      } option:nil];
        }else{
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

//当用户取消选择的时候，调用该方法
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.indicatorStyle = UITableViewCellAccessoryDisclosureIndicator;
    }
    return _tableView;
}

@end

//
//  CBECommonFieldController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBECommonFieldController.h"
#import "CBEMineVM.h"

@interface CBECommonFieldController ()

@property (nonatomic, strong) UITextField *textField;

@end

@implementation CBECommonFieldController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBar];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 44)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, kSCREEN_WIDTH - 30, 44)];
    if (self.type == PageFieldTypeName) {
        self.textField.placeholder = @"名称";
    }else if (self.type == PageFieldTypePhone) {
        self.textField.placeholder = @"联系方式";
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    self.textField.textColor = kMainTextColor;
    self.textField.font = kFont(15);
    [backView addSubview:self.textField];
}

- (void)setNavBar
{
    if (self.type == PageFieldTypeName) {
        [self setBarTitle:@"主办方名称"];
    }else if (self.type == PageFieldTypePhone) {
        [self setBarTitle:@"主办方联系方式"];
    }
    [self setBarButton:BarButtonItemRight action:@selector(doneAction) title:@"完成"];
}

- (void)doneAction
{
    if (self.type == PageFieldTypeName) {
        if ([CommonUtils isBlankString:self.textField.text]) {
            [CommonUtils showHUDWithMessage:@"请输入主办方名称" autoHide:YES];
            return;
        }
        [CommonUtils showHUDWithWaitingMessage:nil];
        [CBEMineVM modifyOrAddSponsorInfo:self.textField.text withLogo:nil withMobile:nil withIntroduce:nil complete:^(id data) {
            [CommonUtils hideHUD];
            if ([[data objectForKey:@"success"] boolValue]) {
                [CommonUtils showHUDWithMessage:@"提交成功" autoHide:YES];
            }else{
                [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
        }];
    }else if (self.type == PageFieldTypePhone) {
        if ([CommonUtils isBlankString:self.textField.text]) {
            [CommonUtils showHUDWithMessage:@"请输入主办方联系方式" autoHide:YES];
            return;
        }
        [CommonUtils showHUDWithWaitingMessage:nil];
        [CBEMineVM modifyOrAddSponsorInfo:nil withLogo:nil withMobile:self.textField.text withIntroduce:nil complete:^(id data) {
            [CommonUtils hideHUD];
            if ([[data objectForKey:@"success"] boolValue]) {
                [CommonUtils showHUDWithMessage:@"提交成功" autoHide:YES];
            }else{
                [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
        }];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

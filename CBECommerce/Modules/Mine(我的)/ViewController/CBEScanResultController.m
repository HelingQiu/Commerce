//
//  CBEScanResultController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEScanResultController.h"
#import "CBEApplyCell.h"
#import "CBEOrderPriceCell.h"
#import "CBEOrderInfoCell.h"
#import "CBEScanResultCell.h"
#import "CBEMineVM.h"
#import "CBEDetailBottomCell.h"

static CGFloat kBottomHeight = 50.f;

@interface CBEScanResultController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBEDetailBottomCell *bottomView;

@end

@implementation CBEScanResultController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBar];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
}

- (void)setNavBar
{
    [self setBarTitle:@"验票详情"];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CBEApplyCell *cell = [CBEApplyCell cellForTableView:tableView];
        [cell.applyInfoLabel setText:StringFormat(@"%@(%@)",self.detailModel.applyName,self.detailModel.applyPhone)];
        return cell;
    }else if (indexPath.section == 1) {
        CBEScanResultCell *cell = [CBEScanResultCell cellForTableView:tableView];
        [cell refreshDataWith:self.detailModel];
        return cell;
    }else if (indexPath.section == 2){
        CBEOrderPriceCell *cell = [CBEOrderPriceCell cellForTableView:tableView];
        [cell refreshDataWith:self.detailModel];
        return cell;
    }
    CBEOrderInfoCell *cell = [CBEOrderInfoCell cellForTableView:tableView];
    [cell refreshDataWith:self.detailModel];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44.f;
    }else if (indexPath.section == 1) {
        return 205.f;
    }else if (indexPath.section == 2) {
        return 75.f;
    }
    return 75.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}
#pragma mark - private
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (CBEDetailBottomCell *)bottomView {
    if (!_bottomView) {
        _bottomView = [CBEDetailBottomCell createBottomView];
        _bottomView.frame = CGRectMake(0, kSCREEN_HEIGHT - KNavigationBarHeight - KiPhoneBottomNoSafeAreaDistance - kBottomHeight, kSCREEN_WIDTH, kBottomHeight);
        [_bottomView.leftBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_bottomView.rightBtn setTitle:@"通过验票" forState:UIControlStateNormal];
        kWeakSelf
        _bottomView.leftBlock = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        _bottomView.rightBlock = ^{
            [weakSelf passETicket];
        };
    }
    return _bottomView;
}

- (void)passETicket {
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM modifyETicketStatusWithTicketId:self.ticketId withSponsorId:[CBEUserModel sharedInstance].userInfo.userId complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"验票成功" autoHide:YES];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

@end

//
//  CBEDepositListController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEDepositListController.h"
#import "CBEDepositListCell.h"
#import "CBEDepositMoneyController.h"
#import "CBEMineVM.h"
#import "CBEDepostModel.h"
#import "CBENoOrderView.h"

@interface CBEDepositListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CBENoOrderView *noDataView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation CBEDepositListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBarTitle:@"申请提现"];
    [self.view addSubview:self.tableView];
    [self requestData];
}

- (void)requestData
{
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM requstDepositListDataComplete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *dataArray = [CBEDepostModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]];
            self.dataSource = [dataArray copy];
            if (!self.dataSource.count) {
                _noDataView.hidden = NO;
            }else{
                _noDataView.hidden = YES;
            }
            [self.tableView reloadData];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 135.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBEDepositListCell *cell = [CBEDepositListCell cellForTableView:tableView];
    CBEDepostModel *model = [self.dataSource objectAtIndex:indexPath.section];
    [cell refreshDataWith:model];
    cell.depositBlock = ^{
        CBEDepositMoneyController *depositController = [[CBEDepositMoneyController alloc] initWithNibName:@"CBEDepositMoneyController" bundle:nil];
        depositController.model = model;
        [self.navigationController pushViewController:depositController animated:YES];
    };
    return cell;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _noDataView = [[CBENoOrderView alloc]initWithFrame:CGRectMake(0, 0, _tableView.width, _tableView.height)];
        _noDataView.hidden = YES;
        [_tableView addSubview:_noDataView];
    }
    return _tableView;
}

@end

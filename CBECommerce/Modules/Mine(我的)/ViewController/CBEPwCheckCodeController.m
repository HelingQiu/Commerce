//
//  CBEPwCheckCodeController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEPwCheckCodeController.h"
#import "CBEPwTextField.h"
#import "CBEScanResultController.h"
#import "CBEMineVM.h"
#import "CBEOrderDetailModel.h"

@interface CBEPwCheckCodeController ()

@property (nonatomic, strong) CBEPwTextField *field;

@end

@implementation CBEPwCheckCodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor ls_colorWithHex:0x000000 andAlpha:0.7];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(15, kStatusHeight, 24, 44);
    [backButton setImage:IMAGE_NAMED(@"arrow_back") forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 1;
    label.frame = CGRectMake(24, kStatusHeight, kSCREEN_WIDTH-24*2, 44);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"密码验票";
    label.textColor = [UIColor whiteColor];
    label.font = kBoldFont(17);
    [self.view addSubview:label];
    
    [self addSubViews];
}

- (void)addSubViews
{
    CGRect frame = CGRectMake((kSCREEN_WIDTH - 254)/2, 168, 254, 50);
    _field = [[CBEPwTextField alloc] init];
    _field.frame = frame;
    _field.backgroundColor = [UIColor whiteColor];
    _field.placeholder = @"请输入用户输入的数字码";
    _field.font = kFont(17);
    _field.textColor = kMainTextColor;
    _field.keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:_field];
    
    CGFloat scanAreaOriginX = CGRectGetMinX(frame);
    CGFloat scanAreaOriginY = CGRectGetMinY(frame);
    CGFloat scanAreaWidth = CGRectGetWidth(frame);
    // 添加扫码边框, 8条框
    CGFloat scanAreaHeight = CGRectGetHeight(frame);
    
    UIColor *color = [UIColor whiteColor];
    
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX - 4, scanAreaOriginY -4, 2, 14)];
    lineView1.backgroundColor = color;
    [self.view addSubview:lineView1];
    
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX - 4, scanAreaOriginY - 4, 14, 2)];
    lineView2.backgroundColor = color;
    [self.view addSubview:lineView2];
    
    UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX + scanAreaWidth - 10, scanAreaOriginY - 4, 14, 2)];
    lineView3.backgroundColor = color;
    [self.view addSubview:lineView3];
    
    UIView *lineView4 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX + scanAreaWidth + 2, scanAreaOriginY - 4, 2, 14)];
    lineView4.backgroundColor = color;
    [self.view addSubview:lineView4];
    
    UIView *lineView5 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX - 4, scanAreaOriginY + scanAreaHeight - 10, 2, 14)];
    lineView5.backgroundColor = color;
    [self.view addSubview:lineView5];
    
    UIView *lineView6 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX - 4, scanAreaOriginY + scanAreaHeight + 2, 14, 2)];
    lineView6.backgroundColor = color;
    [self.view addSubview:lineView6];
    
    UIView *lineView7 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX + scanAreaWidth - 10, scanAreaOriginY + scanAreaHeight + 2, 14, 2)];
    lineView7.backgroundColor = color;
    [self.view addSubview:lineView7];
    
    UIView *lineView8 = [[UIView alloc] initWithFrame:CGRectMake(scanAreaOriginX + scanAreaWidth + 2, scanAreaOriginY + scanAreaHeight - 10, 2, 14)];
    lineView8.backgroundColor = color;
    [self.view addSubview:lineView8];
    
    UIButton *scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    scanBtn.frame = CGRectMake(scanAreaOriginX - 4, scanAreaOriginY + scanAreaHeight - 10 + 60, 110, 36);
    [scanBtn setTitle:@"扫码验票" forState:UIControlStateNormal];
    [scanBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scanBtn.titleLabel setFont:kFont(13)];
    scanBtn.layer.cornerRadius = 20;
    scanBtn.layer.borderWidth = 1;
    scanBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [scanBtn addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scanBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(scanAreaOriginX + scanAreaWidth - 10 - 110, scanAreaOriginY + scanAreaHeight - 10 + 60, 110, 36);
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBtn.titleLabel setFont:kFont(13)];
    sureBtn.layer.cornerRadius = 20;
    sureBtn.layer.borderWidth = 1;
    sureBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [sureBtn addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureBtn];
}

- (void)sureAction
{
    if ([CommonUtils isBlankString:_field.text]) {
        [CommonUtils showHUDWithMessage:@"请输入票号码" autoHide:YES];
        return;
    }
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM verifyETicketWithticketId:_field.text complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            CBEOrderDetailModel *detailModel = [CBEOrderDetailModel mj_objectWithKeyValues:[data objectForKey:@"data"]];
            
            CBEScanResultController *resultController = [[CBEScanResultController alloc] init];
            resultController.ticketId = _field.text;
            resultController.detailModel = detailModel;
            [self.navigationController pushViewController:resultController animated:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

- (void)scanAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

@end

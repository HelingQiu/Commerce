//
//  CBEScanResultController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/4.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEOrderDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CBEScanResultController : CBEBaseController

@property (nonatomic, strong) CBEOrderDetailModel *detailModel;
@property (nonatomic, copy) NSString *ticketId;

@end

NS_ASSUME_NONNULL_END

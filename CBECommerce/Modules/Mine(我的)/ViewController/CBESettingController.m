//
//  CBESettingController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/2.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBESettingController.h"
#import "CBEUserModel.h"
#import "CBELoginController.h"
#import "CBEModifyPWController.h"
#import "CBEFeedbackController.h"
#import "CBEAboutController.h"

@interface CBESettingController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation CBESettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"设置"];
    
    self.dataSource = @[@{@"title":@"账号与安全",@"type":@(0)},
                        @{@"title":@"意见反馈",@"type":@(1)},
                        @{@"title":@"关于跨境帮",@"type":@(2)}];
    [self.view addSubview:self.tableView];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.dataSource.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10.f)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"setCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"setCell"];
        
    }
    if (indexPath.section == 0) {
        NSDictionary *dict = [self.dataSource objectAtIndex:indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = [dict objectForKey:@"title"];
    }else{
        UILabel *logOut = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 44)];
        [logOut setText:@"退出登录"];
        [logOut setFont:kFont(15)];
        [logOut setTextColor:kMainOrangeColor];
        [logOut setTextAlignment:NSTextAlignmentCenter];
        [cell addSubview:logOut];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            CBEModifyPWController *modifyController = [[CBEModifyPWController alloc] initWithNibName:@"CBEModifyPWController" bundle:nil];
            [self.navigationController pushViewController:modifyController animated:YES];
        }else if (indexPath.row == 1) {
            CBEFeedbackController *feedbackController = [[CBEFeedbackController alloc] initWithNibName:@"CBEFeedbackController" bundle:nil];
            [self.navigationController pushViewController:feedbackController animated:YES];
        }else{
            CBEAboutController *aboutController = [[CBEAboutController alloc] initWithNibName:@"CBEAboutController" bundle:nil];
            [self.navigationController pushViewController:aboutController animated:YES];
        }
    }
    if (indexPath.section == 1) {
        [[CBEUserModel sharedInstance] clearAllUserData];
        CBELoginController *loginController = [[CBELoginController alloc] init];
        [self presentViewController:loginController animated:YES completion:nil];
        self.tabBarController.selectedIndex = 2;
    }
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

@end

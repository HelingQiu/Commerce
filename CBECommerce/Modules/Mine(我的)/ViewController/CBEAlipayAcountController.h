//
//  CBEAlipayAcountController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEAlipayAcountController : CBEBaseController

@end

NS_ASSUME_NONNULL_END

//
//  CBEDepositMoneyController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBEDepostModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CBEDepositMoneyController : CBEBaseController

@property (nonatomic, strong) CBEDepostModel *model;

@end

NS_ASSUME_NONNULL_END

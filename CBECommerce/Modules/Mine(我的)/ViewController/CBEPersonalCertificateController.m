//
//  CBEPersonalCertificateController.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/1.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEPersonalCertificateController.h"
#import <QiniuSDK.h>
#import "CBEMineVM.h"

@interface CBEPersonalCertificateController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *idtypeField;
@property (weak, nonatomic) IBOutlet UITextField *idNumField;
@property (weak, nonatomic) IBOutlet UIImageView *frontImgView;
@property (weak, nonatomic) IBOutlet UIImageView *backImgView;
@property (weak, nonatomic) IBOutlet UITextField *companyField;
@property (weak, nonatomic) IBOutlet UITextField *positionField;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) NSString *frontImgUrl;//正面身份证照片
@property (nonatomic, strong) NSString *backImgUrl;//反面身份证照片

@end

@implementation CBEPersonalCertificateController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"申请个人认证"];
    [self requestUserAuthInfo];
}

- (void)requestUserAuthInfo
{
    [CBEMineVM getUserAuthByUserIdComplete:^(id data) {
        
    } fail:^(NSError *error) {
        
    }];
}

//身份证正面
- (IBAction)frontAction:(UIButton *)sender {
    [UIActionSheet ls_actionSheetWithCallBackBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {// 判断系统是否支持相机
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1001;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相机权限" autoHide:YES];
            }
        }else if (buttonIndex == 1){// 判断系统是否支持相册
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1001;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相册权限" autoHide:YES];
            }
        }
    } showInView:self.view title:@"身份证正面照片" destructiveButtonTitle:@"相机" cancelButtonName:@"取消" otherButtonTitles:@"相册", nil];
}
//身份证反面
- (IBAction)backAction:(UIButton *)sender {
    [UIActionSheet ls_actionSheetWithCallBackBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {// 判断系统是否支持相机
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1002;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相机权限" autoHide:YES];
            }
        }else if (buttonIndex == 1){// 判断系统是否支持相册
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.view.tag = 1002;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:nil];
                
            }else{
                [CommonUtils showHUDWithMessage:@"请前往设置->跨境帮中开启相册权限" autoHide:YES];
            }
        }
    } showInView:self.view title:@"身份证反面照片" destructiveButtonTitle:@"相机" cancelButtonName:@"取消" otherButtonTitles:@"相册", nil];
}
//
- (IBAction)agreeAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}
//
- (IBAction)submit:(UIButton *)sender {
    
    if (!self.agreeBtn.selected) {
        [CommonUtils showHUDWithMessage:@"请同意跨境帮用户协议" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.nameField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入姓名" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.idNumField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入身份证号" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.companyField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入公司名" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.positionField.text]) {
        [CommonUtils showHUDWithMessage:@"请输入职务" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.frontImgUrl]) {
        [CommonUtils showHUDWithMessage:@"请上传身份证正面照片" autoHide:YES];
        return;
    }
    if ([CommonUtils isBlankString:self.backImgUrl]) {
        [CommonUtils showHUDWithMessage:@"请上传身份证反面照片" autoHide:YES];
        return;
    }
    //开始提交认证
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"idType":@"1",
                             @"idNumber":self.idNumField.text,
                             @"facePhotoGraph":self.frontImgUrl,
                             @"backPhotoGraph":self.backImgUrl,
                             @"companyName":self.companyField.text,
                             @"companyJobTitle":self.positionField.text,
                             @"realName":self.nameField.text
                             };
    [CommonUtils showHUDWithWaitingMessage:@"提交中..."];
    [CBEMineVM modifyOrAddUserAuthInfo:params complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"提交成功，请等待审核" autoHide:YES];
            
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

#pragma mark - 相机相册
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage]; //通过key值获取到图片
    if (picker.view.tag == 1001) {
        self.frontImgView.image = image;
    }else{
        self.backImgView.image = image;
    }
    //上传图片到服务器--在这里进行图片上传的网络请求
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBEMineVM getQiniuTokenComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSString *token = [data objectForKey:@"data"];
            QNUploadManager *upManager = [[QNUploadManager alloc] init];
            NSData *imageData = UIImagePNGRepresentation(image);
            NSString *uuid = [CommonUtils getUUID];
            if (picker.view.tag == 1001) {
                uuid = [uuid stringByAppendingString:@"_front"];
            }else{
                uuid = [uuid stringByAppendingString:@"_back"];
            }
            [upManager putData:imageData key:uuid token:token
                      complete: ^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                          NSLog(@"%@", info);
                          NSLog(@"%@", resp);
                          //成功后调后台接口上传
                          [CommonUtils hideHUD];
                          if (info.statusCode == 200) {
                              if (picker.view.tag == 1001) {
                                  self.frontImgUrl = uuid;
                              }else{
                                  self.backImgUrl = uuid;
                              }
                          }
                      } option:nil];
        }else{
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

//当用户取消选择的时候，调用该方法
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{}];
}

@end

//
//  CBESponsorInfoModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBESponsorInfoModel.h"

@implementation CBESponsorInfoModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"sponsorId":@"id"};
}

@end

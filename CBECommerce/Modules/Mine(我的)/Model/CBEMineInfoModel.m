//
//  CBEMineInfoModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/29.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMineInfoModel.h"

@implementation CBEMineInfoModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"newsOrderList":@"newOrderList"};
}

@end

//
//  CBESponsorInfoModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBESponsorInfoModel : NSObject

@property (nonatomic, assign) NSInteger approveStatus;
@property (nonatomic, copy) NSString *attestationTime;
@property (nonatomic, assign) NSInteger certificateType;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *sponsorId;
@property (nonatomic, copy) NSString *introduce;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *modifyTime;
@property (nonatomic, copy) NSString *sponsorName;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, copy) NSString *userId;


@end

NS_ASSUME_NONNULL_END

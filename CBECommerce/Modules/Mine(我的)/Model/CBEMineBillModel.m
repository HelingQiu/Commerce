//
//  CBEMineBillModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMineBillModel.h"

@implementation CBEMineBillModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"billId":@"id"};
}

@end

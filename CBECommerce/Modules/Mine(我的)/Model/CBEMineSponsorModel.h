//
//  CBEMineSponsorModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/6.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEMineSponsorModel : NSObject

@property (nonatomic, assign) NSInteger approverStatus;//个人认证：-1未认证，0 认证中，1认证成功，2认证失败
@property (nonatomic, copy) NSString *depositMoney;//已提现金额
@property (nonatomic, copy) NSString *introduce;//主办方简介
@property (nonatomic, copy) NSString *mayDepositMoney;//可提现金额
@property (nonatomic, copy) NSString *mobile;//号码
@property (nonatomic, copy) NSString *photo;//头像
@property (nonatomic, copy) NSString *settleMoney;//待结算金额
@property (nonatomic, copy) NSString *sponsorName;//主办方名称
@property (nonatomic, copy) NSString *startDepositMoney;//提现中金额
@property (nonatomic, copy) NSString *sumMoney;//总收入
@property (nonatomic, copy) NSString *userName;//用户名称
@property (nonatomic, copy) NSString *zfbAccount;//支付宝账号
@property (nonatomic, assign) NSInteger sponsorStatus;//主办方认证：-1未认证，0 认证中，1认证成功，2认证失败

@end

NS_ASSUME_NONNULL_END

//
//  CBEDepostModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEDepostModel.h"

@implementation CBEDepostModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"depositId":@"id"};
}

@end

//
//  CBEMineBillModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEMineBillModel : NSObject

@property (nonatomic, copy) NSString *activityId;
@property (nonatomic, copy) NSString *activityName;
@property (nonatomic, copy) NSString *activityUserId;
@property (nonatomic, assign) NSInteger approverStatus;//0未审批（对应提现中或退款中），1审批通过（对应退款成功或提现成功），2审批不通过(退款or提现)
@property (nonatomic, copy) NSString *billPrice;
@property (nonatomic, copy) NSString *checkStatus;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *billId;
@property (nonatomic, copy) NSString *orderCode;
@property (nonatomic, assign) NSInteger remitStatus;//是否打款:1已打款，0未打款(退款和提现用到)
@property (nonatomic, assign) NSInteger tradeType;//交易类型：1收入，2提现,3退款

@end

NS_ASSUME_NONNULL_END

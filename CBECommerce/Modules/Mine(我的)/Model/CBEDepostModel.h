//
//  CBEDepostModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/5.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEDepostModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *depositId;//活动ID
@property (nonatomic, copy) NSString *mayDepositMoney;
@property (nonatomic, copy) NSString *picture;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray *timeList;

@end

NS_ASSUME_NONNULL_END

//
//  CBEMineInfoModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/29.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEMineInfoModel : NSObject

@property (nonatomic, copy) NSString *friendCount;//好友数
@property (nonatomic, copy) NSString *activityCollectCount;//活动收藏数量
@property (nonatomic, copy) NSString *inforCollectCount;//资讯收藏数

@property (nonatomic, copy) NSString *companyJobTitle;//公司职称
@property (nonatomic, copy) NSString *companyName;//公司名
@property (nonatomic, copy) NSString *photo;//头像
@property (nonatomic, copy) NSString *userName;//用户名

@property (nonatomic, copy) NSString *stayAttendCount;//待付款
@property (nonatomic, copy) NSString *tayPayCount;//待参与
@property (nonatomic, copy) NSString *refundCount;//退款
@property (nonatomic, copy) NSString *finishCount;//已完成

@property (nonatomic, strong) NSArray *newsOrderList;//最新订单

@end

NS_ASSUME_NONNULL_END

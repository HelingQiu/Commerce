//
//  CBEMineVM.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBEMineVM : NSObject
//主办方账单列表
+ (void)requestBillListWithtradeType:(NSString *)tradeType
                        andQueryTime:(NSString *)queryTime
                          andKeyword:(NSString *)keywordQuery
                            complete:(CBENetWorkSucceed)successed
                                fail:(CBENetWorkFailure)failed;
//主办方账单详情
+ (void)requestBillDetailWithId:(NSString *)billId
                       complete:(CBENetWorkSucceed)successed
                           fail:(CBENetWorkFailure)failed;
//参与方数据
+ (void)requestMineInfoComplete:(CBENetWorkSucceed)successed
                           fail:(CBENetWorkFailure)failed;

//活动收藏列表
+ (void)requestActivityCollectListWithPageNum:(NSInteger)pageNum
                                     complete:(CBENetWorkSucceed)successed
                                         fail:(CBENetWorkFailure)failed;
//资讯收藏列表
+ (void)requestInformationCollectListWithPageNum:(NSInteger)pageNum
                                        complete:(CBENetWorkSucceed)successed
                                            fail:(CBENetWorkFailure)failed;
//获取主办方我的信息
+ (void)getMineSponsorComplete:(CBENetWorkSucceed)successed
                          fail:(CBENetWorkFailure)failed;

//提现列表
+ (void)requstDepositListDataComplete:(CBENetWorkSucceed)successed
                                 fail:(CBENetWorkFailure)failed;

//添加支付宝账号
+ (void)addZfbAccountWith:(NSString *)account
                 withName:(NSString *)accountName
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed;

//提现记录
+ (void)requstDepositRecordDataComplete:(CBENetWorkSucceed)successed
                                   fail:(CBENetWorkFailure)failed;

//获取七牛token
+ (void)getQiniuTokenComplete:(CBENetWorkSucceed)successed
                         fail:(CBENetWorkFailure)failed;

//获取主办方信息
+ (void)requestSponsorDataComplete:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed;
//提交主办方资料
+ (void)modifyOrAddSponsorInfo:(NSString *)sponsorName
                      withLogo:(NSString *)logo
                    withMobile:(NSString *)mobile
                 withIntroduce:(NSString *)introduce
                      complete:(CBENetWorkSucceed)successed
                          fail:(CBENetWorkFailure)failed;

//申请提现获取验证码
+ (void)getWithdrawSmsCodeWithActivityId:(NSString *)activityId
                           withBillPrice:(NSString *)billPrice     complete:(CBENetWorkSucceed)successed
                                    fail:(CBENetWorkFailure)failed;

//申请提现
+ (void)applyDepositWithActivityId:(NSString *)activityId
                     withBillPrice:(NSString *)billPrice
                       withSmsCode:(NSString *)smsCode
                          complete:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed;
//编辑或新增个人信息认证
+ (void)modifyOrAddUserAuthInfo:(NSDictionary *)params
                       complete:(CBENetWorkSucceed)successed
                           fail:(CBENetWorkFailure)failed;
//获取个人认证信息
+ (void)getUserAuthByUserIdComplete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed;

//验票接口
+ (void)verifyETicketWithticketId:(NSString *)ticketId
                         complete:(CBENetWorkSucceed)successed
                             fail:(CBENetWorkFailure)failed;
//修改电子票状态
+ (void)modifyETicketStatusWithTicketId:(NSString *)ticketId
                          withSponsorId:(NSString *)sponsorId    complete:(CBENetWorkSucceed)successed
                                   fail:(CBENetWorkFailure)failed;
@end


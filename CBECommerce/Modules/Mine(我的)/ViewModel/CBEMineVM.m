//
//  CBEMineVM.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEMineVM.h"

@implementation CBEMineVM

+ (void)requestBillListWithtradeType:(NSString *)tradeType
                        andQueryTime:(NSString *)queryTime
                          andKeyword:(NSString *)keywordQuery
                            complete:(CBENetWorkSucceed)successed
                                fail:(CBENetWorkFailure)failed
{
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@"",
                             @"tradeType":tradeType,
                             @"queryTime":queryTime,
                             @"keywordQuery":keywordQuery?:@""
                             };
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryAccountServiceBillList parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestBillDetailWithId:(NSString *)billId
                       complete:(CBENetWorkSucceed)successed
                           fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"id":billId};
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryAccountBillById parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestMineInfoComplete:(CBENetWorkSucceed)successed
                           fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@""
                             };
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kMineCustomerInfo parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestActivityCollectListWithPageNum:(NSInteger)pageNum
                                     complete:(CBENetWorkSucceed)successed
                                         fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@"",
                             @"pageNum":@(pageNum),
                             @"pageSize":@(10)
                             };
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kCollectRecordList parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestInformationCollectListWithPageNum:(NSInteger)pageNum
                                     complete:(CBENetWorkSucceed)successed
                                         fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@"",
                             @"pageNum":@(pageNum),
                             @"pageSize":@(10)
                             };
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kInformationcollectList parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

#pragma mark - 主办方
//获取我的主办方信息
+ (void)getMineSponsorComplete:(CBENetWorkSucceed)successed fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@""
                             };
//    params = @{@"userId":@"1"};
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kGetSponsorMineService parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requstDepositListDataComplete:(CBENetWorkSucceed)successed
                                 fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@""
                             };
//    params = @{@"userId":@"2"};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kActivityWithdrawList parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)addZfbAccountWith:(NSString *)account
                 withName:(NSString *)accountName
                 complete:(CBENetWorkSucceed)successed
                     fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@"",
                             @"zfbAccount":account,
                             @"zfbName":accountName
                             };
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kAddZfbAccount parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requstDepositRecordDataComplete:(CBENetWorkSucceed)successed
                                   fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@""
                             };
//    params = @{@"userId":@"1"};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kDepositRecordApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//获取七牛token
+ (void)getQiniuTokenComplete:(CBENetWorkSucceed)successed
                         fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"uploadType":@(1)};//1共有，2私有
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kGetQiniuToken parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//编辑主办方信息
+ (void)modifyOrAddSponsorInfo:(NSString *)sponsorName
                      withLogo:(NSString *)logo
                    withMobile:(NSString *)mobile
                 withIntroduce:(NSString *)introduce
                      complete:(CBENetWorkSucceed)successed
                          fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    if (![CommonUtils isBlankString:sponsorName]) {
        [params setObject:sponsorName forKey:@"sponsorName"];
    }
    if (![CommonUtils isBlankString:logo]) {
        [params setObject:logo forKey:@"logo"];
    }
    if (![CommonUtils isBlankString:mobile]) {
        [params setObject:mobile forKey:@"mobile"];
    }
    if (![CommonUtils isBlankString:introduce]) {
        [params setObject:introduce forKey:@"introduce"];
    }
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kModifyOrAddSponsorInfo parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//获取主办方信息
+ (void)getSponsorInfoByUserIdComplete:(CBENetWorkSucceed)successed
                                  fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@""
                             };
//    params = @{@"userId":@"1"};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kGetSponsorByUserId parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//获取主办方信息
+ (void)requestSponsorDataComplete:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@""
                             };
//    params = @{@"userId":@"1"};
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kGetSponsorByUserId parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//申请提现获取验证码
+ (void)getWithdrawSmsCodeWithActivityId:(NSString *)activityId
                           withBillPrice:(NSString *)billPrice     complete:(CBENetWorkSucceed)successed
                                     fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@"",
                             @"activityId":activityId,
                             @"billPrice":billPrice
                             };//
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kGetWithdrawSmsCode parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//申请提现
+ (void)applyDepositWithActivityId:(NSString *)activityId
                     withBillPrice:(NSString *)billPrice
                       withSmsCode:(NSString *)smsCode
                          complete:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId?:@"",
                             @"activityId":activityId,
                             @"billPrice":billPrice,
                             @"msmCode":smsCode
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kApplyDepositApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//编辑或新增个人信息认证
+ (void)modifyOrAddUserAuthInfo:(NSDictionary *)params
                       complete:(CBENetWorkSucceed)successed
                           fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kModifyOrAddUserAuthInfo parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//获取个人认证信息
+ (void)getUserAuthByUserIdComplete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId};
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kModifyOrAddUserAuthInfo parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}
//获取验票信息
+ (void)verifyETicketWithticketId:(NSString *)ticketId
                         complete:(CBENetWorkSucceed)successed
                             fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"applyTicketId":ticketId
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kVerifyEqTicket parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

//修改电子票状态
+ (void)modifyETicketStatusWithTicketId:(NSString *)ticketId
                          withSponsorId:(NSString *)sponsorId    complete:(CBENetWorkSucceed)successed
                                   fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":sponsorId,
                             @"id":ticketId
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kCompleteEqTicket parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end

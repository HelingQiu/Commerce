//
//  CBEAboutController.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/21.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEAboutController.h"

@interface CBEAboutController ()

@property (weak, nonatomic) IBOutlet UITableViewCell *phoneView;

@end

@implementation CBEAboutController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"关于跨境帮"];
    
    UILabel *leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 70, 44)];
    [leftLabel setText:@"联系客服"];
    [leftLabel setFont:kFont(15)];
    [leftLabel setTextColor:kMainBlackColor];
    [_phoneView.contentView addSubview:leftLabel];
    
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 200 - 37, 0, 200, 44)];
    [rightLabel setText:@"400-0000-281"];
    [rightLabel setTextAlignment:NSTextAlignmentRight];
    [rightLabel setFont:kFont(15)];
    [rightLabel setTextColor:kMainTextColor];
    [_phoneView.contentView addSubview:rightLabel];
}
- (IBAction)phoneAction:(UITapGestureRecognizer *)sender {
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",@"400345500"];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
    [self.view addSubview:callWebview];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CBEFeedbackController.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/21.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEFeedbackController.h"
#import "CBESettingVM.h"

@interface CBEFeedbackController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITableViewCell *phoneView;

@end

@implementation CBEFeedbackController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"意见反馈"];
    
    self.textView.textContainerInset = UIEdgeInsetsMake(15, 15, 15, 15);
    self.textView.placeholder = @"请输入反馈，我们将会根据您的意见不断改进";
    [self setBarButton:BarButtonItemRight action:@selector(submit:) title:@"提交"];
    
    UILabel *leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 70, 44)];
    [leftLabel setText:@"联系客服"];
    [leftLabel setFont:kFont(15)];
    [leftLabel setTextColor:kMainBlackColor];
    [_phoneView.contentView addSubview:leftLabel];
    
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH - 200 - 37, 0, 200, 44)];
    [rightLabel setText:@"400-0000-281"];
    [rightLabel setTextAlignment:NSTextAlignmentRight];
    [rightLabel setFont:kFont(15)];
    [rightLabel setTextColor:kMainTextColor];
    [_phoneView.contentView addSubview:rightLabel];
}
- (IBAction)phoneAction:(UITapGestureRecognizer *)sender {
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",@"400345500"];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
    [self.view addSubview:callWebview];
}

- (void)submit:(UIButton *)sender
{
    if ([CommonUtils isBlankString:self.textView.text]) {
        [CommonUtils showHUDWithMessage:@"请输入意见反馈" autoHide:YES];
        return;
    }
    [CommonUtils showHUDWithWaitingMessage:@"正在提交..."];
    [CBESettingVM addFeedbackWith:self.textView.text complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"提交成功" autoHide:YES];
            [self performSelector:@selector(popViewControllerAnimated:) withObject:nil afterDelay:0.3];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

@end

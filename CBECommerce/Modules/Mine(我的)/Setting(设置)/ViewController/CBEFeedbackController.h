//
//  CBEFeedbackController.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/21.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEFeedbackController : CBEBaseController

@end

NS_ASSUME_NONNULL_END

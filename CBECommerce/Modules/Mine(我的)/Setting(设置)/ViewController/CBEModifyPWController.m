//
//  CBEModifyPWController.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/19.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEModifyPWController.h"
#import "CBESettingVM.h"

@interface CBEModifyPWController ()

@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;

@end

@implementation CBEModifyPWController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setBarTitle:@"修改手机号"];
}
- (IBAction)codeAction:(UIButton *)sender {
    NSString *mobile = self.phoneField.text;
    if ([CommonUtils isBlankString:mobile]) {
        [CommonUtils showHUDWithMessage:@"请输入手机号码" autoHide:YES];
        return;
    }
    
    [CommonUtils showHUDWithWaitingMessage:nil];
    [CBESettingVM getModifiMobileSmsCode:mobile complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"发送成功，请留意短信息" autoHide:YES];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}
- (IBAction)doneAction:(UIButton *)sender {
    
    NSString *mobile = self.phoneField.text;
    if ([CommonUtils isBlankString:mobile]) {
        [CommonUtils showHUDWithMessage:@"请输入手机号码" autoHide:YES];
        return;
    }
    NSString *code = self.codeField.text;
    if ([CommonUtils isBlankString:code]) {
        [CommonUtils showHUDWithMessage:@"请输入验证码" autoHide:YES];
        return;
    }
    [CommonUtils showHUDWithWaitingMessage:@"修改中..."];
    [CBESettingVM modifiMobile:mobile withCode:code complete:^(id data) {
        [CommonUtils hideHUD];
        if ([[data objectForKey:@"success"] boolValue]) {
            [CommonUtils showHUDWithMessage:@"修改成功" autoHide:YES];
            [self performSelector:@selector(popViewControllerAnimated:) withObject:nil afterDelay:0.3];
        }else{
            [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
        }
    } fail:^(NSError *error) {
        [CommonUtils hideHUD];
        [CommonUtils showHUDWithMessage:@"网络异常，请重试" autoHide:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

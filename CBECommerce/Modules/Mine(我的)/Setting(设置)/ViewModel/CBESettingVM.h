//
//  CBESettingVM.h
//  CBECommerce
//
//  Created by Rainer on 2018/11/20.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBESettingVM : NSObject

//修改手机号发送短信
+ (void)getModifiMobileSmsCode:(NSString *)mobile
                      complete:(CBENetWorkSucceed)successed
                          fail:(CBENetWorkFailure)failed;
//修改手机号
+ (void)modifiMobile:(NSString *)mobile
            withCode:(NSString *)msmCode
            complete:(CBENetWorkSucceed)successed
                fail:(CBENetWorkFailure)failed;
//新增意见反馈
+ (void)addFeedbackWith:(NSString *)content
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed;
@end

NS_ASSUME_NONNULL_END

//
//  CBESettingVM.m
//  CBECommerce
//
//  Created by Rainer on 2018/11/20.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBESettingVM.h"

@implementation CBESettingVM

+ (void)getModifiMobileSmsCode:(NSString *)mobile
                      complete:(CBENetWorkSucceed)successed
                          fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"mobile":mobile
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kGetModifiMobileSmsCode parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)modifiMobile:(NSString *)mobile
            withCode:(NSString *)msmCode
            complete:(CBENetWorkSucceed)successed
                fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"mobile":mobile,
                             @"msmCode":msmCode
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kModifiMobile parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)addFeedbackWith:(NSString *)content
               complete:(CBENetWorkSucceed)successed
                   fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSDictionary *params = @{@"userId":[CBEUserModel sharedInstance].userInfo.userId,
                             @"content":content
                             };
    [[CBENetworkManager sharedInstance] postRequestWithUrl:kAddFeedback parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end

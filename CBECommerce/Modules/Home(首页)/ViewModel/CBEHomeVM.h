//
//  CBEHomeVM.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CBENetWorkSucceed)(id data);
typedef void(^CBENetWorkFailure)(NSError *error);

@interface CBEHomeVM : NSObject
//首页
+ (void)requestHomePageWithCityCode:(NSString *)cityCode
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed;
//活动列表
+ (void)requestActivityWithPageNum:(NSString *)pageNum
                 andActivityTypeId:(NSString *)typeId
                       andCityCode:(NSString *)cityCode
                      andIsPremium:(NSString *)isPremium
                       andSortType:(NSString *)sortType
                       andTimeType:(NSString *)timeType
                         andTagIds:(NSString *)tagIds
                            comple:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed;
//活动筛选标签
+ (void)requestActivityTagDataComplete:(CBENetWorkSucceed)successed
                                  fail:(CBENetWorkFailure)failed;
//活动类型
+ (void)requestActivityTypeComplete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed;

//首页搜索
+ (void)homeSearchDataWithKeyword:(NSString *)keyword
                         complete:(CBENetWorkSucceed)successed
                             fail:(CBENetWorkFailure)failed;

+ (void)getCityListWithKeyword:(NSString *)keyword
                      complete:(CBENetWorkSucceed)successed
                          fail:(CBENetWorkFailure)failed;

@end

//
//  CBEHomeVM.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEHomeVM.h"
#import "CBECityModel.h"
#import "CBEGPSManager.h"

@implementation CBEHomeVM

+ (void)requestHomePageWithCityCode:(NSString *)cityCode
                           complete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    [[CBENetworkManager sharedInstance] setHttpField];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [params setObject:cityCode forKey:@"cityCode"];
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kHomePageApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)requestActivityWithPageNum:(NSString *)pageNum
                 andActivityTypeId:(NSString *)typeId
                       andCityCode:(NSString *)cityCode
                     andIsPremium:(NSString *)isPremium
                       andSortType:(NSString *)sortType
                       andTimeType:(NSString *)timeType
                         andTagIds:(NSString *)tagIds
                            comple:(CBENetWorkSucceed)successed
                              fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
//    [[CBENetworkManager sharedInstance] setHttpField];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];//[CBEUserModel sharedInstance].userInfo.userId?:@""
    if (![CommonUtils isBlankString:pageNum]) {
        [params setObject:pageNum forKey:@"pageNum"];
    }
    
//    [params setObject:@"10" forKey:@"pageSize"];
    
    if (![CommonUtils isBlankString:typeId]) {
        [params setObject:typeId forKey:@"activityTypeId"];
    }
    if (![CommonUtils isBlankString:cityCode]) {
        [params setObject:cityCode forKey:@"cityCode"];
    }
    if (![CommonUtils isBlankString:isPremium]) {
        [params setObject:isPremium forKey:@"isPremium"];
    }
    //排序类型 1综合，2最新，3离我近
    if (![CommonUtils isBlankString:sortType]) {
        [params setObject:sortType forKey:@"sortType"];
        if ([sortType isEqualToString:@"3"]) {//需要传经纬度
            CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
            if (kCLAuthorizationStatusDenied == status || kCLAuthorizationStatusRestricted == status){
                [params setObject:@"0" forKey:@"latitude"];
                [params setObject:@"0" forKey:@"longitude"];
            }else{
                double latitude = [[CBEGPSManager sharedGpsManager] latitude];
                double longitude = [[CBEGPSManager sharedGpsManager] longitude];
                [params setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
                [params setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
            }
        }
    }
    //时间类型 1，今天，2近一周，3近一个月
    if (![CommonUtils isBlankString:timeType]) {
        [params setObject:timeType forKey:@"queryTimeType"];
    }
    //标签
    if (![CommonUtils isBlankString:tagIds]) {
        [params setObject:tagIds forKey:@"tagIds"];
    }
//    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryActivityListPage parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager GET:StringFormat(@"%@%@",Host,kQueryActivityListPage) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

+ (void)requestActivityTagDataComplete:(CBENetWorkSucceed)successed
                                  fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
//    [[CBENetworkManager sharedInstance] setHttpField];
//    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryActivityTagsAll parameter:nil timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager GET:StringFormat(@"%@%@",Host,kQueryActivityTagsAll) parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

+ (void)requestActivityTypeComplete:(CBENetWorkSucceed)successed
                               fail:(CBENetWorkFailure)failed
{
//    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
//    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
//    [[CBENetworkManager sharedInstance] getRequestWithUrl:kQueryActivityTypeAll parameter:nil timeoutInterval:60 progress:nil succeed:^(id data) {
//        successed(data);
//    } failure:^(NSError *error) {
//        failed(error);
//    }];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[CBEUserModel sharedInstance].token?:@"" forHTTPHeaderField:@"token"];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"image/jpeg", nil];
    [manager GET:StringFormat(@"%@%@",Host,kQueryActivityTypeAll) parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successed(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failed(error);
    }];
}

+ (void)homeSearchDataWithKeyword:(NSString *)keyword
                         complete:(CBENetWorkSucceed)successed
                             fail:(CBENetWorkFailure)failed
{
    [[CBENetworkManager sharedInstance] cancelAllRequest];
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[CBEUserModel sharedInstance].userInfo.userId?:@"" forKey:@"userId"];
    [params setObject:keyword?:@"" forKey:@"keywordQuery"];
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kHomeSearchApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

+ (void)getCityListWithKeyword:(NSString *)keyword
                      complete:(CBENetWorkSucceed)successed
                          fail:(CBENetWorkFailure)failed
{
    [CBENetworkManager sharedInstance].requestType = HTTPRequestTypeHTTP;
    [CBENetworkManager sharedInstance].responseType = HTTPResponseTypeJSON;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:keyword?:@"" forKey:@"keywordQuery"];
    [[CBENetworkManager sharedInstance] getRequestWithUrl:kCityListApi parameter:params timeoutInterval:60 progress:nil succeed:^(id data) {
        successed(data);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end

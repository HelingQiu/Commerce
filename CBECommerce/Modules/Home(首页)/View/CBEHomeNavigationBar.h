//
//  CBEHomeNavigationBar.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBEBaseButton.h"

typedef void(^CityBlock)(void);
typedef void(^RecommandBlock)(void);
typedef void(^ActivityBlock)(void);
typedef void(^SearchBlock)(void);
@interface CBEHomeNavigationBar : UIView

@property (nonatomic, strong) CBEBaseButton *cityButton;
@property (nonatomic, strong) UIButton *activityButton;
@property (nonatomic, strong) UIButton *recommandButton;

@property (nonatomic, copy) CityBlock cityBlock;
@property (nonatomic, copy) RecommandBlock recommandBlock;
@property (nonatomic, copy) ActivityBlock activityBlock;
@property (nonatomic, copy) SearchBlock searchBlock;

- (void)refreshCity:(NSString *)cityName;
- (void)moveLineAtIndex:(NSInteger)index;

@end

//
//  LSSearchBarView.h
//  LSaleClothingForIphone
//
//  Created by 杨荣 on 2017/1/18.
//  Copyright © 2017年 乐售云科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SearchBarClicked)();

typedef enum : NSUInteger {
    SearchBarAlignmentAlignmentLeft,    // 靠左（左边间距10，右边间距64）
    SearchBarAlignmentAlignmentCenter,  // 居中（左边间距54，右边间距64）
    SearchBarAlignmentAlignmentRight,   // 靠右（左边间距54，右边间距10）
} SearchBarAlignment;// titleView位置


@interface LSSearchBarView : UIView


/**
 通过点击回调的方式初始化搜索框，此时textFiled默认不能输入

 @param frame frame
 @param didClicked 点击回调
 @return 搜索框
 */
- (instancetype)initWithFrame:(CGRect)frame placeholder:(NSString *)placeholder didClicked:(SearchBarClicked)didClicked;


/**
 初始化搜索框：有点击回调，默认不能输入
 
 @param leftGap 距离左边间距
 @param rightGap 距离右边间距
 @param placeholder 提示字
 @param didClicked 点击回调
 @return titleView
 */
- (instancetype)initWithLeftGap:(CGFloat)leftGap rightGap:(CGFloat)rightGap placeholder:(NSString *)placeholder didClicked:(SearchBarClicked)didClicked;


/**
 初始化搜索框：有点击回调，并且只需要提供位置
 
 @param alignment 位置（靠左、靠右、居中）相对于导航栏
 @param placeholder 提示字
 @param didClicked 点击回调
 @return titleView
 */
- (instancetype)initWithAlignment:(SearchBarAlignment)alignment placeholder:(NSString *)placeholder didClicked:(SearchBarClicked)didClicked;




// 点击了搜索框
@property (nonatomic,copy) SearchBarClicked didClicked;

// 输入框
@property (nonatomic,strong) UITextField *textField;

@end

//
//  CBECycleViewCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/28.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "CBEBaseTableViewCell.h"
#import "CBEBannarModel.h"

@protocol CBECycleDelegate <NSObject>

@optional

- (void)cycleViewDidSelectedAtIndex:(NSInteger)index;

@end

@interface CBECycleViewCell : CBEBaseTableViewCell<SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic, weak) id<CBECycleDelegate>delegate;

+ (CBECycleViewCell *)cellForTableView:(UITableView *)tableView;

- (void)setCycleDataSource:(NSArray<CBEBannarModel *> *)imgArray;

- (void)setNewsBannarDataSource:(NSArray<CBEBannarModel *> *)imgArray;

@end

//
//  CBEMayKnowCollectionCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/29.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMayKnowCollectionCell.h"

@implementation CBEMayKnowCollectionCell
{
    UIImageView *_headView;
    UILabel *_nameLabel;//昵称
    UILabel *_directLabel;//行业
    UILabel *_positionLabel;//职业
    UILabel *_friendCountLabel;//好友
    UIButton *_addFriendButton;
    UILabel *_statusLab;//
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.clipsToBounds = YES;
    self.contentView.layer.cornerRadius = 2;
    
    _headView = [UIImageView new];
    [_headView sd_setImageWithURL:[NSURL URLWithString:@"https://cdn.duitang.com/uploads/item/201508/30/20150830175812_sYikS.jpeg"]];
    [_headView setClipsToBounds:YES];
    [_headView.layer setCornerRadius:25];
    [self.contentView addSubview:_headView];
    
    _statusLab = [UILabel new];
    [_statusLab setFont:kFont(9)];
    [_statusLab setTextColor:[UIColor whiteColor]];
    [_statusLab setTextAlignment:NSTextAlignmentCenter];
    [_statusLab setText:@""];
    _statusLab.clipsToBounds = YES;
    _statusLab.layer.cornerRadius = 8;
    [self.contentView addSubview:_statusLab];
    
    _nameLabel = [UILabel new];
    [_nameLabel setFont:kFont(14)];
    [_nameLabel setTextColor:kMainBlackColor];
    [_nameLabel setTextAlignment:NSTextAlignmentCenter];
    [_nameLabel setText:@""];
    [self.contentView addSubview:_nameLabel];
    
    _directLabel = [UILabel new];
    [_directLabel setFont:kFont(12)];
    [_directLabel setTextColor:kMainTextColor];
    [_directLabel setTextAlignment:NSTextAlignmentCenter];
    [_directLabel setText:@""];
    _directLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    [self.contentView addSubview:_directLabel];
    
    _positionLabel = [UILabel new];
    [_positionLabel setFont:kFont(12)];
    [_positionLabel setTextColor:kMainTextColor];
    [_positionLabel setTextAlignment:NSTextAlignmentCenter];
    [_positionLabel setText:@""];
    _positionLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    [self.contentView addSubview:_positionLabel];
    
    _friendCountLabel = [UILabel new];
    [_friendCountLabel setFont:kFont(9)];
    [_friendCountLabel setTextColor:[UIColor ls_colorWithHexString:@"#FC6B3F"]];
    [_friendCountLabel setTextAlignment:NSTextAlignmentCenter];
    [_friendCountLabel setClipsToBounds:YES];
    [_friendCountLabel.layer setCornerRadius:2];
    [_friendCountLabel.layer setBorderColor:[UIColor ls_colorWithHexString:@"#FC6B3F"].CGColor];
    [_friendCountLabel.layer setBorderWidth:0.5f];
    [_friendCountLabel setBackgroundColor:[UIColor ls_colorWithHex:0xFC6B3F andAlpha:0.12]];
    [_friendCountLabel setText:@"0个共同好友"];
    [self.contentView addSubview:_friendCountLabel];
    
    _addFriendButton = [UIButton new];
    [_addFriendButton setTitle:@"加好友" forState:UIControlStateNormal];
    [_addFriendButton setBackgroundColor:kMainBlueColor];
    [_addFriendButton.titleLabel setFont:kFont(14)];
    [_addFriendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_addFriendButton setClipsToBounds:YES];
    [_addFriendButton.layer setCornerRadius:12];
    [_addFriendButton addTarget:self action:@selector(addFriendAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_addFriendButton];
}
- (void)refreshDataWith:(CBEConnectListModel *)model {
    [_headView sd_setImageWithURL:[NSURL URLWithString:[model.headPhoto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"cv_icon_user")];
    [_nameLabel setText:model.userName?:@"***"];
    [_friendCountLabel setText:model.showTag];
    [_directLabel setText:StringFormat(@"%@",model.industry?:@"")];
    [_positionLabel setText:StringFormat(@"%@",model.companyJobTitle?:@"")];
    if (![CommonUtils isBlankString:model.industry] && ![CommonUtils isBlankString:model.companyJobTitle]) {
        _statusLab.text = @"已认证";
        _statusLab.backgroundColor = kMainBlueColor;
    }else{
        _statusLab.text = @"未认证";
        _statusLab.backgroundColor = kMainTextColor;
    }
}
- (void)addFriendAction {
    if (self.addFriendBlock) {
        self.addFriendBlock();
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(12);
        make.width.height.mas_equalTo(50);
        make.centerX.equalTo(self.contentView.mas_centerX);
    }];
    
    [_statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(12);
        make.width.mas_equalTo(44);
        make.height.mas_equalTo(16);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_bottom).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.mas_equalTo(20);
    }];
    
    [_directLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_nameLabel.mas_bottom).offset(2);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.mas_equalTo(17);
    }];
    
    [_positionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_directLabel.mas_bottom).offset(0);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.mas_equalTo(17);
    }];
    
    [_friendCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_positionLabel.mas_bottom).offset(5);
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(16);
    }];
    
    [_addFriendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_friendCountLabel.mas_bottom).offset(8);
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.width.mas_equalTo(72);
        make.height.mas_equalTo(24);
    }];
}

@end

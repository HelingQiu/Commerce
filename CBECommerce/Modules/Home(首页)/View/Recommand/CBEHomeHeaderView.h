//
//  CBEHomeHeaderView.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/29.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^MoreBlock)(void);

@interface CBEHomeHeaderView : UIView

@property (nonatomic, copy) MoreBlock moreBlock;

- (instancetype)initWithFrame:(CGRect)frame;

@end

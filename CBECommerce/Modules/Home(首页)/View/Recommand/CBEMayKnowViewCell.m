//
//  CBEMayKnowViewCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/28.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMayKnowViewCell.h"
#import "CBEBaseButton.h"
#import "CBEMayKnowCollectionCell.h"
#import "CBEConnectListModel.h"
#import "CBEMessageVM.h"

@implementation CBEMayKnowViewCell
{
    NSMutableArray *_userListArray;
    UICollectionView *_collectionView;
}
+ (CBEMayKnowViewCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"MayKnowCell";
    CBEMayKnowViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEMayKnowViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *labTitle = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 200, 50)];
    [labTitle setText:@"为您推荐的人脉"];
    [labTitle setFont:kFont(14)];
    [labTitle setTextColor:kMainBlackColor];
    [self.contentView addSubview:labTitle];
    
    CBEBaseButton *moreButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    [moreButton setFrame:CGRectMake(kSCREEN_WIDTH - 54, 13, 54, 24)];
    [moreButton setTitleLabelRect:CGRectMake(0, 0, 30, 24)];
    [moreButton setImageViewRect:CGRectMake(30, 0, 24, 24)];
    [moreButton setTitle:@"更多" forState:UIControlStateNormal];
    [moreButton setTitleColor:kMainTextColor forState:UIControlStateNormal];
    [moreButton.titleLabel setFont:kFont(14)];
    moreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [moreButton setImage:IMAGE_NAMED(@"arrow_right") forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreButton];
    
    CGRect collectionViewFrame = CGRectMake(0, 50, kSCREEN_WIDTH, 185);
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 8;
    flowLayout.minimumInteritemSpacing = 8;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, kPadding15, 0, kPadding15);
    flowLayout.minimumInteritemSpacing = 1;
    flowLayout.itemSize = CGSizeMake(150, 185);
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:flowLayout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = kMainDefaltGrayColor;
    [self.contentView addSubview:collectionView];
    _collectionView = collectionView;
    [collectionView registerClass:[CBEMayKnowCollectionCell class] forCellWithReuseIdentifier:@"mayKnowCollectionCell"];
}

- (void)refreshDataWith:(NSArray *)userList
{
    _userListArray = [userList mutableCopy];
    [_collectionView reloadData];
}
#pragma mark -delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _userListArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBEMayKnowCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mayKnowCollectionCell" forIndexPath:indexPath];
    CBEConnectListModel *model = [_userListArray objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    cell.addFriendBlock = ^{
        //点击添加好友触发事件
        [CommonUtils showHUDWithWaitingMessage:@"申请中..."];
        [CBEMessageVM addFriendWithMobile:model.userId Complete:^(id data) {
            [CommonUtils hideHUD];
            if ([[data objectForKey:@"success"] boolValue]) {
                [CommonUtils showHUDWithMessage:@"添加好友成功，等待对方通过" autoHide:YES];
                [_userListArray removeObject:model];
                [collectionView reloadData];
            }else{
                [CommonUtils showHUDWithMessage:[data objectForKey:@"errorInfo"] autoHide:YES];
            }
        } fail:^(NSError *error) {
            [CommonUtils hideHUD];
            [CommonUtils showHUDWithMessage:@"添加申请失败，请重试" autoHide:YES];
        }];
    };
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //选中数据模型
    CBEConnectListModel *model = [_userListArray objectAtIndex:indexPath.row];
    if (self.mayknowSelectedBlock) {
        self.mayknowSelectedBlock(model);
    }
}

- (void)moreAction
{
    if (self.moreBlock) {
        self.moreBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

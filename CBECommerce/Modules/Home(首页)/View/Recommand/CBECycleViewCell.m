//
//  CBECycleViewCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/28.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBECycleViewCell.h"

@implementation CBECycleViewCell

+ (CBECycleViewCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"cycleCell";
    CBECycleViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBECycleViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kHeightScale(160)) delegate:self placeholderImage:IMAGE_NAMED(@"banner_default")];
    self.cycleScrollView.currentPageDotColor = [UIColor whiteColor];
    self.cycleScrollView.imageURLStringsGroup = @[@""];
    self.cycleScrollView.autoScrollTimeInterval = 3;
    [self.contentView addSubview:self.cycleScrollView];
}

- (void)setCycleDataSource:(NSArray<CBEBannarModel *> *)imgArray
{
    NSMutableArray *urlArray = [NSMutableArray array];
    [imgArray enumerateObjectsUsingBlock:^(CBEBannarModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [urlArray addObject:[obj.imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }];
    self.cycleScrollView.imageURLStringsGroup = urlArray;
}

- (void)setNewsBannarDataSource:(NSArray<CBEBannarModel *> *)imgArray
{
    NSMutableArray *urlArray = [NSMutableArray array];
    [imgArray enumerateObjectsUsingBlock:^(CBEBannarModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [urlArray addObject:[obj.imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }];
    self.cycleScrollView.imageURLStringsGroup = [urlArray copy];
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if ([self.delegate respondsToSelector:@selector(cycleViewDidSelectedAtIndex:)]) {
        [self.delegate cycleViewDidSelectedAtIndex:index];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

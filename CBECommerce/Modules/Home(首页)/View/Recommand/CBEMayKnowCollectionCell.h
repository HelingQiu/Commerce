//
//  CBEMayKnowCollectionCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/29.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBEConnectListModel.h"

typedef void(^AddFriendBlock)(void);

@interface CBEMayKnowCollectionCell : UICollectionViewCell

@property (nonatomic, copy) AddFriendBlock addFriendBlock;
- (void)refreshDataWith:(CBEConnectListModel *)model;

@end

//
//  CBEHomeHeaderView.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/29.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEHomeHeaderView.h"

@implementation CBEHomeHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *labTitle = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 200, kHeightScale(44))];
        [labTitle setText:@"您可能感兴趣的资讯"];
        [labTitle setFont:kFont(14)];
        [labTitle setTextColor:kMainBlackColor];
        [self addSubview:labTitle];
        
        CBEBaseButton *moreButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
        [moreButton setFrame:CGRectMake(kSCREEN_WIDTH - 54, 14, 54, 24)];
        [moreButton setTitleLabelRect:CGRectMake(0, 0, 30, 24)];
        [moreButton setImageViewRect:CGRectMake(30, 0, 24, 24)];
        [moreButton setTitle:@"更多" forState:UIControlStateNormal];
        [moreButton setTitleColor:kMainTextColor forState:UIControlStateNormal];
        [moreButton.titleLabel setFont:kFont(14)];
        moreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [moreButton setImage:IMAGE_NAMED(@"arrow_right") forState:UIControlStateNormal];
        [moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:moreButton];
    }
    return self;
}

- (void)moreAction
{
    if (self.moreBlock) {
        self.moreBlock();
    }
}

@end

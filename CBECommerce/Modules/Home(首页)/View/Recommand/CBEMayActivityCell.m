//
//  CBEMayActivityCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMayActivityCell.h"
#import "CBEMayActivityCollectionViewCell.h"
#import "CBEActivityModel.h"

@implementation CBEMayActivityCell
{
    NSArray *_activityArray;
    UICollectionView *_collectionView;
}
+ (CBEMayActivityCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"MayActivityCell";
    CBEMayActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEMayActivityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    UILabel *labTitle = [[UILabel alloc] initWithFrame:CGRectMake(kPadding15, 0, 200, 50)];
    [labTitle setText:@"您可能感兴趣的活动"];
    [labTitle setFont:kFont(14)];
    [labTitle setTextColor:kMainBlackColor];
    [self.contentView addSubview:labTitle];
    
    CBEBaseButton *moreButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    [moreButton setFrame:CGRectMake(kSCREEN_WIDTH - 54, 13, 54, 24)];
    [moreButton setTitleLabelRect:CGRectMake(0, 0, 30, 24)];
    [moreButton setImageViewRect:CGRectMake(30, 0, 24, 24)];
    [moreButton setTitle:@"更多" forState:UIControlStateNormal];
    [moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [moreButton.titleLabel setFont:kFont(14)];
    moreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [moreButton setImage:IMAGE_NAMED(@"arrow_right") forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreButton];
    
    CGRect collectionViewFrame= CGRectMake(0, 50, kSCREEN_WIDTH, 175);
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 10;
    flowLayout.minimumInteritemSpacing = 10;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, kPadding15, 0, kPadding15);
    flowLayout.minimumInteritemSpacing = 1;
    flowLayout.itemSize = CGSizeMake(335, 175);
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:flowLayout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:collectionView];
    _collectionView = collectionView;
    [collectionView registerClass:[CBEMayActivityCollectionViewCell class] forCellWithReuseIdentifier:@"mayActivityCollectionCell"];
}

- (void)refreshDataWith:(NSArray<CBEActivityModel *> *)activityArray
{
    _activityArray = activityArray;
    [_collectionView reloadData];
}
#pragma mark -delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _activityArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBEMayActivityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mayActivityCollectionCell" forIndexPath:indexPath];
    CBEActivityModel *model = [_activityArray objectAtIndex:indexPath.row];
    [cell refreshDataWithData:model];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //选中数据模型
    CBEActivityModel *model = [_activityArray objectAtIndex:indexPath.row];
    if (self.activitySelectedBlock) {
        self.activitySelectedBlock(model);
    }
}

- (void)moreAction
{
    if (self.moreBlock) {
        self.moreBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

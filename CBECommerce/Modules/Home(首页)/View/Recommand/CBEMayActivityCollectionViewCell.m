//
//  CBEMayActivityCollectionViewCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEMayActivityCollectionViewCell.h"
#import "CBEActivityModel.h"

@implementation CBEMayActivityCollectionViewCell
{
    UIImageView *_imageView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    _imageView = [UIImageView new];
    [self.contentView addSubview:_imageView];
}

- (void)refreshDataWithData:(CBEActivityModel *)model
{
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"event_default")];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.contentView);
    }];
}

@end

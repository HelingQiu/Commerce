//
//  CBEMayKnowViewCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/28.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBEBaseTableViewCell.h"

typedef void(^MoreBlock)(void);
typedef void(^ItemSelectBlock)(id model);

@interface CBEMayKnowViewCell : CBEBaseTableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, copy) MoreBlock moreBlock;
@property (nonatomic, copy) ItemSelectBlock mayknowSelectedBlock;

+ (CBEMayKnowViewCell *)cellForTableView:(UITableView *)tableView;

@end

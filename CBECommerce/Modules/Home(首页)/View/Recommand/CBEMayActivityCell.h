//
//  CBEMayActivityCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

typedef void(^MoreBlock)(void);
typedef void(^ItemSelectBlock)(id model);

@interface CBEMayActivityCell : CBEBaseTableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, copy) MoreBlock moreBlock;
@property (nonatomic, copy) ItemSelectBlock activitySelectedBlock;

+ (CBEMayActivityCell *)cellForTableView:(UITableView *)tableView;

@end

//
//  LSSearchBarView.m
//  LSaleClothingForIphone
//
//  Created by 杨荣 on 2017/1/18.
//  Copyright © 2017年 乐售云科技有限公司. All rights reserved.
//

#import "LSSearchBarView.h"
#import "UIViewController+NavigationItem.h"
@interface LSSearchBarView ()

// 左边搜索🔍图标
@property (nonatomic,strong) UIImageView *imageView;

@end

@implementation LSSearchBarView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame placeholder:(NSString *)placeholder didClicked:(SearchBarClicked)didClicked {
    self = [super initWithFrame:frame];
    if (self) {

        self.didClicked = didClicked;
        [self addInit];
        
        // 既然通过点击来初始化，那么textFiled应该是不能输入
        self.textField.userInteractionEnabled = NO;
        self.textField.placeholder = placeholder;
        self.textField.tintColor = [UIColor blueColor];
    }
    return self;
}

- (instancetype)initWithLeftGap:(CGFloat)leftGap rightGap:(CGFloat)rightGap placeholder:(NSString *)placeholder didClicked:(SearchBarClicked)didClicked {
    
    return  [self initWithFrame:CGRectMake(leftGap, 6, kSCREEN_WIDTH-leftGap-rightGap, 32.f) placeholder:placeholder didClicked:didClicked];
    
}
- (instancetype)initWithAlignment:(SearchBarAlignment)alignment placeholder:(NSString *)placeholder didClicked:(SearchBarClicked)didClicked {
    
    CGRect frame = CGRectZero;
    
    switch (alignment) {
        case SearchBarAlignmentAlignmentLeft:
        {
            frame = CGRectMake(10, 6, kSCREEN_WIDTH-10-KItemWidth, 32);
        }
            break;
        case SearchBarAlignmentAlignmentCenter:
        {
            frame = CGRectMake(KItemWidth, 6, kSCREEN_WIDTH-KItemWidth*2, 32);
        }
            break;
        case SearchBarAlignmentAlignmentRight:
        {
            frame = CGRectMake(KItemWidth, 6, kSCREEN_WIDTH-10-KItemWidth, 32);
        }
            break;
            
        default:
            break;
    }
    return [self initWithFrame:frame placeholder:placeholder didClicked:didClicked];
}


- (void)addInit {
    
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.layer.cornerRadius = 16.f;
    
    [self addSubview:self.imageView];
    [self addSubview:self.textField];
    
    [self addTapGestureRecognizer];
    
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        UIImage *image = [UIImage imageNamed:@"home_icon_search_gray"];
//        _imageView = [[UIImageView alloc] initWithFrame:(CGRectMake(0, (self.height-image.size.height)/2.0, image.size.height, image.size.height))];
        
        // 为了保持美观，左边搜索图标和搜索框的的高度一致
        _imageView = [[UIImageView alloc] initWithFrame:(CGRectMake(2, 0, self.height, self.height))];
        
        _imageView.image = image;
        _imageView.contentMode = UIViewContentModeCenter;
        
    }
    return _imageView;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.font = kFont(14);
        _textField.frame = CGRectMake(self.imageView.right, 0, self.width-self.imageView.right, self.height);
        
        // 默认的
        _textField.clearButtonMode = UITextFieldViewModeAlways;
        
    }
    return _textField;
}
- (void)addTapGestureRecognizer {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickedAction)];
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:tap];
}

- (void)didClickedAction {
    if (self.didClicked) {
        self.didClicked();
    }
}

@end

//
//  CBEAcClassfyCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"
#import "CBEActivityTypeModel.h"

typedef void(^ActivityTypeBlock)(CBEActivityTypeModel *typeModel);
@interface CBEAcClassfyCell : CBEBaseTableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, copy) ActivityTypeBlock typeBlock;
+ (CBEAcClassfyCell *)cellForTableView:(UITableView *)tableView;

@end

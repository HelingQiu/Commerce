//
//  CBEAcClassfyCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEAcClassfyCell.h"
#import "CBEAcClassCollectionCell.h"

@implementation CBEAcClassfyCell
{
    NSArray *_dataSource;
    UICollectionView *_collectionView;
}
+ (CBEAcClassfyCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"acClassCell";
    CBEAcClassfyCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEAcClassfyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    CGRect collectionViewFrame= CGRectMake(0, kPadding10, kSCREEN_WIDTH, 95);
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    flowLayout.minimumInteritemSpacing = 1;
    flowLayout.itemSize = CGSizeMake(kSCREEN_WIDTH/5.0, 95);
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:flowLayout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:collectionView];
    _collectionView = collectionView;
    [collectionView registerClass:[CBEAcClassCollectionCell class] forCellWithReuseIdentifier:@"acClassCell"];
}

- (void)refreshDataWith:(NSArray *)array
{
    _dataSource = array;
    [_collectionView reloadData];
}

#pragma mark -delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBEAcClassCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"acClassCell" forIndexPath:indexPath];
    
    CBEActivityTypeModel *model = [_dataSource objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBEActivityTypeModel *model = [_dataSource objectAtIndex:indexPath.row];
    if (self.typeBlock) {
        self.typeBlock(model);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

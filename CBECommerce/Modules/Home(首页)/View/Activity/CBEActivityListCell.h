//
//  CBEActivityListCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEActivityListCell : CBEBaseTableViewCell

+ (CBEActivityListCell *)cellForTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END

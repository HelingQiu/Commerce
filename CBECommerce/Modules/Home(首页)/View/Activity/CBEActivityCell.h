//
//  CBEActivityCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTableViewCell.h"
#import "CBEActivityModel.h"

@interface CBEActivityCell : CBEBaseTableViewCell

+ (CBEActivityCell *)cellForTableView:(UITableView *)tableView;

@end

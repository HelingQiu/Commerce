//
//  CBEActivityCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEActivityCell.h"

@implementation CBEActivityCell
{
    UIView *_backView;
    UIImageView *_headView;
    UIImageView *_statusView;
    UILabel *_statusLabel;
    UILabel *_titleLabel;
    UILabel *_addressLabel;
    UILabel *_timeLabel;
    UIView *_botBackView;
}
+ (CBEActivityCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"activityCell";
    CBEActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEActivityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = kMainDefaltGrayColor;
    
    _backView = [UIView new];
    _backView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_backView];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(kPadding10);
        make.left.right.bottom.equalTo(self.contentView);
    }];

    _headView = [UIImageView new];
    [_backView addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_backView.mas_left).offset(kPadding15);
        make.right.equalTo(self->_backView.mas_right).offset(-kPadding15);
        make.top.equalTo(self->_backView.mas_top).offset(kPadding15);
        make.width.mas_equalTo(kSCREEN_WIDTH - 2*kPadding15);
        make.height.mas_equalTo((kSCREEN_WIDTH - 2*kPadding15)*(180/345.0));
    }];
    
    _statusView = [UIImageView new];
    [_backView addSubview:_statusView];
    [_statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_top).offset(kPadding10);
        make.left.equalTo(self->_backView.mas_left).offset(kPadding10);
        make.width.mas_equalTo(48);
        make.height.mas_equalTo(20);
    }];
    _statusLabel = [UILabel new];
    [_statusLabel setTextColor:[UIColor whiteColor]];
    [_statusLabel setFont:kFont(12)];
    [_statusLabel setTextAlignment:NSTextAlignmentCenter];
    [_backView addSubview:_statusLabel];
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_top).offset(kPadding10);
        make.left.equalTo(self->_backView.mas_left).offset(kPadding10);
        make.width.mas_equalTo(48);
        make.height.mas_equalTo(16);
    }];
    
    _titleLabel = [UILabel new];
    [_titleLabel setFont:kFont(16)];
    [_titleLabel setTextColor:kMainBlackColor];
    [_titleLabel setNumberOfLines:2];
    [_titleLabel setText:@""];
    [_backView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headView.mas_bottom).offset(kPadding15);
        make.left.equalTo(self->_headView.mas_left);
        make.right.equalTo(self->_headView.mas_right);
        make.height.mas_equalTo(44);
    }];
    
    _addressLabel = [UILabel new];
    [_addressLabel setFont:kFont(12)];
    [_addressLabel setTextColor:kMainTextColor];
    [_addressLabel setText:@""];
    [_backView addSubview:_addressLabel];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(kPadding10/2.0);
        make.left.equalTo(self->_headView.mas_left);
        make.right.equalTo(self->_headView.mas_right);
        make.height.mas_equalTo(17);
    }];
    
    _timeLabel = [UILabel new];
    [_timeLabel setFont:kFont(12)];
    [_timeLabel setTextColor:kMainTextColor];
    [_timeLabel setText:@""];
    [_backView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_addressLabel.mas_bottom).offset(kPadding10/2.0);
        make.left.equalTo(self->_headView.mas_left);
        make.right.equalTo(self->_headView.mas_right);
        make.height.mas_equalTo(17);
    }];
    _botBackView = [UIView new];
    [_backView addSubview:_botBackView];
    [_botBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_timeLabel.mas_bottom);
        make.left.right.equalTo(self->_backView);
        make.bottom.equalTo(self->_backView.mas_bottom);
    }];
}

- (void)refreshDataWith:(CBEActivityModel *)model
{
    [_titleLabel setText:model.title];
    [_titleLabel ls_setRowSpace:8];
    [_addressLabel setText:StringFormat(@"地址：%@%@%@",model.cityName,model.regionName,model.address)];
    if (model.activeStatus == 0) {
        [_statusLabel setText:@"报名中"];
        [_statusView setImage:IMAGE_NAMED(@"label_baomingzhong")];
    }else if (model.activeStatus == 1){
        [_statusLabel setText:@"进行中"];
        [_statusView setImage:IMAGE_NAMED(@"label_jinxingzhong")];
    }else{
        _statusLabel.hidden = YES;
        _statusView.hidden = YES;
    }
    [_headView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"event_default")];
    __block NSString *timeString = @"时间：";
    NSArray *timeArray = model.timeList;
    [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *start = [obj objectForKey:@"startTime"];
        NSString *end = [obj objectForKey:@"endTime"];
        timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
        if (idx == timeArray.count - 1) {
            [self->_timeLabel setText:timeString];
        }
    }];
    
    [_botBackView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UILabel class]]) {
            [obj removeFromSuperview];
        }
    }];
    //自由配置
    NSArray *array = model.telationTagList;
    float aWidth = 0;
    for (int i = 0;i < array.count;i ++) {
        if (![CommonUtils isBlankString:[array[i] objectForKey:@"tagName"]]) {
            UILabel *label = [UILabel new];
            [label setFont:kFont(9)];
            [label setTextColor:[UIColor ls_colorWithHexString:@"#FC6B3F"]];
            [label setTextAlignment:NSTextAlignmentCenter];
            [label setClipsToBounds:YES];
            [label.layer setCornerRadius:2];
            [label.layer setBorderColor:[UIColor ls_colorWithHexString:@"#FC6B3F"].CGColor];
            [label.layer setBorderWidth:0.5];
            [label setBackgroundColor:[UIColor ls_colorWithHex:0xFC6B3F andAlpha:0.12]];
            [label setText:[array[i] objectForKey:@"tagName"]];
            [_botBackView addSubview:label];
            
            float iWidth = [CommonUtils widthForString:[array[i] objectForKey:@"tagName"] Font:kFont(9) andWidth:kSCREEN_WIDTH]+12;
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self->_timeLabel.mas_bottom).offset(kPadding10);
                make.left.equalTo(self->_headView.mas_left).offset(aWidth + i * 5);
                make.width.mas_equalTo(iWidth);
                make.height.mas_equalTo(16);
            }];
            
            aWidth += iWidth;
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CBEAcClassCollectionCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEAcClassCollectionCell.h"

@implementation CBEAcClassCollectionCell
{
    UIImageView *_imageView;
    UILabel *_nameLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    _imageView = [UIImageView new];
    _imageView.clipsToBounds = YES;
    _imageView.layer.cornerRadius = 12;
    [self.contentView addSubview:_imageView];
    
    _nameLabel = [UILabel new];
    [_nameLabel setFont:kFont(14)];
    [_nameLabel setTextColor:kMainBlackColor];
    [_nameLabel setTextAlignment:NSTextAlignmentCenter];
    [_nameLabel setText:@""];
    [self.contentView addSubview:_nameLabel];
}

- (void)refreshDataWith:(CBEActivityTypeModel *)model
{
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[model.imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"event_icon_fabu")];
    [_nameLabel setText:model.name];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(15);
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.width.height.mas_equalTo(44);
    }];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-16);
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.height.mas_equalTo(20);
    }];
}

@end

//
//  CBEAcClassCollectionCell.h
//  CBECommerce
//
//  Created by Rainer on 2018/8/31.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBEActivityTypeModel.h"

@interface CBEAcClassCollectionCell : UICollectionViewCell

- (void)refreshDataWith:(CBEActivityTypeModel *)model;

@end

//
//  CBEActivityListCell.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEActivityListCell.h"
#import "CBEActivityModel.h"
#import "CBEAlignTopLabel.h"

@implementation CBEActivityListCell
{
    UIImageView *_thumbView;
    CBEAlignTopLabel *_titleLabel;
    UILabel *_cityLabel;
    UILabel *_timeLabel;
}

+ (CBEActivityListCell *)cellForTableView:(UITableView *)tableView
{
    static NSString *identify = @"CBEActivityListCell";
    CBEActivityListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[CBEActivityListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    _thumbView = [UIImageView new];
    [_thumbView sd_setImageWithURL:nil placeholderImage:IMAGE_NAMED(@"event_default")];
    [self.contentView addSubview:_thumbView];
    [_thumbView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(kPadding15);
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(75);
    }];
    
    _titleLabel = [CBEAlignTopLabel new];
    [_titleLabel setFont:kFont(14)];
    [_titleLabel setTextColor:kMainBlackColor];
    [_titleLabel setNumberOfLines:2];
    [_titleLabel setText:@""];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_thumbView.mas_top);
        make.left.equalTo(self->_thumbView.mas_right).offset(kPadding15);
        make.right.equalTo(self.contentView.mas_right).offset(-kPadding15);
        make.height.mas_equalTo(44);
    }];
    
    _timeLabel = [UILabel new];
    [_timeLabel setFont:kFont(12)];
    [_timeLabel setTextColor:kMainTextColor];
    [_timeLabel setText:@""];
    [self.contentView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self->_thumbView.mas_bottom);
        make.left.equalTo (self->_thumbView.mas_right).offset(kPadding15);
        make.height.mas_equalTo(17);
    }];
    
    _cityLabel = [UILabel new];
    [_cityLabel setFont:kFont(12)];
    [_cityLabel setTextColor:kMainTextColor];
    [_cityLabel setText:@""];
    [self.contentView addSubview:_cityLabel];
    [_cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_timeLabel.mas_right).offset(kPadding10);
        make.bottom.equalTo(self->_thumbView.mas_bottom);
        make.height.mas_equalTo(17);
    }];
}
- (void)refreshDataWith:(CBEActivityModel *)model {
    [_thumbView sd_setImageWithURL:[NSURL URLWithString:[model.picture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:IMAGE_NAMED(@"event_default")];
    __block NSString *timeString = @"时间：";
    NSArray *timeArray = model.timeList;
    [timeArray enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *start = [obj objectForKey:@"startTime"];
        NSString *end = [obj objectForKey:@"endTime"];
        timeString = StringFormat(@"%@ %@ %@",timeString,start,end);
        if (idx == timeArray.count - 1) {
            [self->_timeLabel setText:timeString];
        }
    }];
    [_timeLabel setText:model.createTime];
    [_titleLabel setText:model.title];
    [_titleLabel ls_setRowSpace:8];
    [_cityLabel setText:model.cityName];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

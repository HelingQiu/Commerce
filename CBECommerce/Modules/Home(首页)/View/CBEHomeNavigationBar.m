//
//  CBEHomeNavigationBar.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEHomeNavigationBar.h"

@implementation CBEHomeNavigationBar
{
    UIView *_moveLine;
    CGRect _leftRect;
    CGRect _rightRect;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    
    NSString *cityName = [CommonUtils getStoreDataWithKey:kLocationCityName];
    CGFloat cityWidth = [CommonUtils widthForString:cityName Font:kFont(14) andWidth:80];
    
    CGFloat orginY = KIsiPhoneX?44:20;
    
    self.cityButton = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    [self.cityButton setFrame:CGRectMake(kPadding15, orginY, cityWidth + 24, 44)];
    [self.cityButton setTitleLabelRect:CGRectMake(0, 14, cityWidth, 16)];
    [self.cityButton setImageViewRect:CGRectMake(cityWidth, 10, 24, 24)];
    [self.cityButton setTitle:cityName forState:UIControlStateNormal];
    [self.cityButton setTitleColor:UIColorFromRGB(0x222222) forState:UIControlStateNormal];
    [self.cityButton.titleLabel setFont:kFont(14)];
    [self.cityButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingMiddle];
    [self.cityButton setImage:IMAGE_NAMED(@"arrow_down") forState:UIControlStateNormal];
    [self.cityButton addTarget:self action:@selector(cityAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.cityButton];
    
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(kSCREEN_WIDTH - 30 - kPadding15, orginY+7, 30, 30)];
    [searchButton setBackgroundImage:IMAGE_NAMED(@"home_icon_search") forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:searchButton];
    
    self.recommandButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.recommandButton setFrame:CGRectMake(kSCREEN_WIDTH/2 - 60, orginY+2, 40, 40)];
    [self.recommandButton setTitle:@"推荐" forState:UIControlStateNormal];
    [self.recommandButton setTitleColor:kMainBlackColor forState:UIControlStateSelected];
    [self.recommandButton setTitleColor:[UIColor ls_colorWithHexString:@"#92A1B3"] forState:UIControlStateNormal];
    [self.recommandButton.titleLabel setFont:kBoldFont(17)];
    [self.recommandButton addTarget:self action:@selector(recommandAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.recommandButton];
    self.recommandButton.selected = YES;
    
    self.activityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.activityButton setFrame:CGRectMake(kSCREEN_WIDTH/2 + 20, orginY+2, 40, 40)];
    [self.activityButton setTitle:@"活动" forState:UIControlStateNormal];
    [self.activityButton setTitleColor:kMainBlackColor forState:UIControlStateSelected];
    [self.activityButton setTitleColor:[UIColor ls_colorWithHexString:@"#92A1B3"] forState:UIControlStateNormal];
    [self.activityButton.titleLabel setFont:kBoldFont(17)];
    [self.activityButton addTarget:self action:@selector(activityAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.activityButton];
    
    _leftRect = CGRectMake(kSCREEN_WIDTH/2 - 60, orginY+40, 40, 3);
    _rightRect = CGRectMake(kSCREEN_WIDTH/2 + 20, orginY+40, 40, 3);
    _moveLine = [[UIView alloc] initWithFrame:_leftRect];
    [_moveLine setBackgroundColor:kMainBlueColor];
    _moveLine.clipsToBounds = YES;
    _moveLine.layer.cornerRadius = 2;
    [self addSubview:_moveLine];
}

- (void)refreshCity:(NSString *)cityName
{
    CGFloat cityWidth = [CommonUtils widthForString:cityName Font:kFont(14) andWidth:80];
    
    CGFloat orginY = KIsiPhoneX?44:20;
    [self.cityButton setFrame:CGRectMake(kPadding15, orginY, cityWidth + 24, 44)];
    [self.cityButton setTitleLabelRect:CGRectMake(0, 14, cityWidth, 16)];
    [self.cityButton setImageViewRect:CGRectMake(cityWidth - 5, 10, 24, 24)];
    [self.cityButton setTitle:cityName forState:UIControlStateNormal];
}

- (void)cityAction
{
    if (self.cityBlock) {
        self.cityBlock();
    }
}

- (void)searchAction
{
    if (self.searchBlock) {
        self.searchBlock();
    }
}

- (void)recommandAction:(UIButton *)sender
{
    if (_moveLine.left == _rightRect.origin.x) {
        [UIView animateWithDuration:0.3 animations:^{
            self->_moveLine.frame = self->_leftRect;
        } completion:^(BOOL finished) {
            sender.selected = YES;
            self->_activityButton.selected = NO;
            
        }];
    }
    if (self.recommandBlock) {
        self.recommandBlock();
    }
}

- (void)activityAction:(UIButton *)sender
{
    if (_moveLine.left == _leftRect.origin.x) {
        [UIView animateWithDuration:0.5 animations:^{
            self->_moveLine.frame = self->_rightRect;
        } completion:^(BOOL finished) {
            self->_recommandButton.selected = NO;
            sender.selected = YES;
            
        }];
    }
    if (self.activityBlock) {
        self.activityBlock();
    }
}

- (void)moveLineAtIndex:(NSInteger)index
{
    if (index == 1) {//从第1页移动到第0页
        if (_moveLine.left == _leftRect.origin.x) {
            [UIView animateWithDuration:0.5 animations:^{
                self->_moveLine.frame = self->_rightRect;
            } completion:^(BOOL finished) {
                self->_recommandButton.selected = NO;
                self->_activityButton.selected = YES;
                
            }];
        }
    }else{//从第0页移动到第1页
        if (_moveLine.left == _rightRect.origin.x) {
            [UIView animateWithDuration:0.3 animations:^{
                self->_moveLine.frame = self->_leftRect;
            } completion:^(BOOL finished) {
                self->_recommandButton.selected = YES;
                self->_activityButton.selected = NO;
                
            }];
        }
    }
}

@end

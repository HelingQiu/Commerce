//
//  CBEHomeController.m
//  CBECommerce
//
//  Created by Rainer on 2018/8/27.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEHomeController.h"
#import "CBEHomeNavigationBar.h"
#import "CBECycleViewCell.h"
#import "CBEMayKnowViewCell.h"
#import "CBEHomeHeaderView.h"
#import "CBEMayActivityCell.h"
#import "CBENewsListCell.h"
#import "CBEActivityCell.h"
#import "CBEAcClassfyCell.h"
#import "CBEGPSManager.h"
#import "CBENewsDetailViewController.h"
#import "CBEHomeSearchController.h"

#import "CBECityController.h"
#import "CBECityModel.h"
#import "ZYSideSlipFilterController.h"
#import "ZYSideSlipFilterRegionModel.h"
#import "CommonItemModel.h"
#import "SideSlipCommonTableViewCell.h"
#import "CBEActivityDetailController.h"
#import "CBEHomeVM.h"
#import "CBEActivityModel.h"
#import "CBEBannarModel.h"
#import "CBENewsListModel.h"
#import "CBEActivityTypeModel.h"
#import "CBEWebController.h"
#import "CBELoginController.h"
#import "CBEConnectListModel.h"
#import "CBECardController.h"

@interface CBEHomeController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,CBECycleDelegate>

@property (nonatomic, strong) CBEHomeNavigationBar *navigationBar;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, strong) UITableView *rightTableView;
@property (nonatomic, strong) UIButton *filterButton;
@property (nonatomic, assign) NSInteger pageIndex;

@property (strong, nonatomic) ZYSideSlipFilterController *filterController;

@property (nonatomic, strong) NSMutableArray<CBEBannarModel *> *bannerList;//轮播图
@property (nonatomic, strong) NSMutableArray<CBEConnectListModel *> *userList;//推荐好友
@property (nonatomic, strong) NSMutableArray<CBEActivityModel *> *activityList;//活动列表
@property (nonatomic, strong) NSMutableArray<CBENewsListModel *> *informationList;//资讯

@property (strong, nonatomic) NSMutableArray<CBEActivityModel *> *rDataSource;
@property (assign, nonatomic) NSInteger rPageNum;

@property (nonatomic, strong) NSMutableArray *tagArray;

@property (nonatomic, strong) NSMutableArray<CBEActivityTypeModel *> *activityTypeArray;

@property (nonatomic, copy) NSString *cityCode;

@property (nonatomic, strong) CBECityModel *locationCityModel;

@end

@implementation CBEHomeController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    if (![CBEUserModel sharedInstance].isLogin) {
        self.navigationBar.activityButton.selected = YES;
        self.navigationBar.recommandButton.enabled = NO;
        [self.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH, 0) animated:NO];
        [self.filterButton setHidden:NO];
    }else{
        self.navigationBar.recommandButton.enabled = YES;
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavibar];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.leftTableView];
    [self.scrollView addSubview:self.rightTableView];
    [self.view addSubview:self.filterButton];
    
    [self startGPSLocation];
    [self setFilterVC];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchCityResult:) name:kSearchCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeSearchResult:) name:kHomeSearchMore object:nil];
    
    [self.leftTableView.mj_header beginRefreshing];
    [self requestTagData];
    self.rPageNum = 1;
    
    [self requestActivityTypeData];
    [self loadRightDataWithActivityTypeId:nil withIsPremium:nil withSortType:nil withTimeType:nil withTagIds:nil];
    
    [self judgeIslocationPromiss];
    if ([CBEUserModel sharedInstance].isLogin) {
        self.scrollView.scrollEnabled = YES;
    }else{
        self.scrollView.scrollEnabled = NO;
    }
}
- (void)judgeIslocationPromiss {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (kCLAuthorizationStatusDenied == status || kCLAuthorizationStatusRestricted == status)
    {
        //读取本地数据
        NSString * isPositioning = [[NSUserDefaults standardUserDefaults] valueForKey:@"isPositioning"];
        if (isPositioning == nil)//提示
        {
            UIAlertView * positioningAlertivew = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"为了更好的体验,请到设置->隐私->定位服务中开启!【跨境帮】定位服务,以便获取附近信息!" delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil];
            [positioningAlertivew show];
        }
    }else//开启的
    {
        //需要删除本地字符
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:@"isPositioning"];
        [userDefaults synchronize];
    }
}

- (void)setupNavibar
{
    self.navigationBar = [[CBEHomeNavigationBar alloc] init];
    [self.navigationBar setFrame:CGRectMake(0, 0, kSCREEN_WIDTH, KNavigationBarHeight)];
    [self.view addSubview:self.navigationBar];
    kWeakSelf
    self.navigationBar.recommandBlock = ^{
        [weakSelf.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [weakSelf.filterButton setHidden:YES];
    };
    self.navigationBar.activityBlock = ^{
        [weakSelf.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH, 0) animated:YES];
        [weakSelf.filterButton setHidden:NO];
    };
    self.navigationBar.searchBlock = ^{
        CBEHomeSearchController *searchController = [[CBEHomeSearchController alloc] init];
        [weakSelf.navigationController pushViewController:searchController animated:YES];
    };
    self.navigationBar.cityBlock = ^{
        CBECityController *cityController = [[CBECityController alloc] init];
        [weakSelf.navigationController pushViewController:cityController animated:YES];
        cityController.cityBlock = ^(CBECityModel *model) {
            [weakSelf.navigationBar refreshCity:model.name];
            [CommonUtils storageDataWithObject:model.name Key:kLocationCityName Completion:^(BOOL finish, id obj) {
                
            }];
            [CommonUtils storageDataWithObject:model.cityCode Key:kLocationCityCode Completion:^(BOOL finish, id obj) {
                
            }];
            //刷新数据
            [weakSelf loadLeftData];
        };
    };
}
#pragma mark - 网络请求
//获取推荐数据
- (void)loadLeftData
{
    self.cityCode = [CommonUtils getStoreDataWithKey:kLocationCityCode]?:@"";
    [CBEHomeVM requestHomePageWithCityCode:self.cityCode complete:^(id data) {
        [self.leftTableView.mj_header endRefreshing];
//        NSLog(@"%@",data);
        if ([[data objectForKey:@"success"] boolValue]) {
            NSDictionary *result = [data objectForKey:@"data"];
            
            [self.bannerList removeAllObjects];
            self.bannerList = [CBEBannarModel mj_objectArrayWithKeyValuesArray:[result objectForKey:@"bannerList"]];
            
            [self.userList removeAllObjects];
            self.userList = [CBEConnectListModel mj_objectArrayWithKeyValuesArray:[result objectForKey:@"userList"]];
            [self.activityList removeAllObjects];
            self.activityList = [CBEActivityModel mj_objectArrayWithKeyValuesArray:[result objectForKey:@"activityList"]];
            
            [self.informationList removeAllObjects];
            self.informationList = [CBENewsListModel mj_objectArrayWithKeyValuesArray:[result objectForKey:@"informationList"]];
            
            [self.leftTableView reloadData];
        }else{
            if ([[data objectForKey:@"errorInfo"] isEqualToString:@"请重新登陆"]) {
                [UIAlertView ls_alertWithCallBackBlock:^(NSInteger buttonIndex) {
                    [[CBEUserModel sharedInstance] clearAllUserData];
                    CBELoginController *loginController = [[CBELoginController alloc] init];
                    [self presentViewController:loginController animated:YES completion:nil];
                    self.tabBarController.selectedIndex = 2;
                } title:@"登录过期" message:@"请重新登录" cancelButtonName:nil otherButtonTitles:@"确认", nil];
            }
        }
        
    } fail:^(NSError *error) {
        [self.leftTableView.mj_header endRefreshing];
    }];
}

//获取活动数据
- (void)loadRightDataWithActivityTypeId:(NSString *)activityTypeId withIsPremium:(NSString *)isPremium withSortType:(NSString *)sortType withTimeType:(NSString *)timeType withTagIds:(NSString *)tagIds
{
//    self.cityCode = [CommonUtils getStoreDataWithKey:kLocationCityCode]?:@"";
    [CBEHomeVM requestActivityWithPageNum:StringFormat(@"%ld",(long)self.rPageNum) andActivityTypeId:activityTypeId andCityCode:nil andIsPremium:isPremium andSortType:sortType andTimeType:timeType andTagIds:tagIds comple:^(id data) {
        [self.rightTableView.mj_header endRefreshing];
        [self.rightTableView.mj_footer endRefreshing];
        NSDictionary *dict = [data mj_JSONObject];
//        NSLog(@"%@",dict);
        if ([[dict objectForKey:@"success"] boolValue]) {
            NSArray *list = [dict objectForKey:@"data"];
            NSInteger totalPage = [[dict objectForKey:@"totalPages"] integerValue];
            if (totalPage <= self.rPageNum) {
                [self.rightTableView.mj_footer endRefreshingWithNoMoreData];
            }
            if (self.rPageNum == 1) {
                self.rDataSource = [CBEActivityModel mj_objectArrayWithKeyValuesArray:list];
            }else{
                [self.rDataSource addObjectsFromArray:[CBEActivityModel mj_objectArrayWithKeyValuesArray:list]];
            }
            [self.rightTableView reloadData];
        }
    } fail:^(NSError *error) {
        [self.rightTableView.mj_header endRefreshing];
        [self.rightTableView.mj_footer endRefreshing];
    }];
}

//获取活动顶部分类
- (void)requestActivityTypeData
{
    [CBEHomeVM requestActivityTypeComplete:^(id data) {
        NSLog(@"%@",data);
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *list = [data objectForKey:@"data"];
            self.activityTypeArray = [CBEActivityTypeModel mj_objectArrayWithKeyValuesArray:list];
            [self.rightTableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}

//获取标签
- (void)requestTagData
{
    [CBEHomeVM requestActivityTagDataComplete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *resultArray = [data objectForKey:@"data"];
            self.tagArray = [resultArray mutableCopy];
            self->_filterController.dataList = [self getFilterData];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)searchCityResult:(NSNotification *)note {
    CBECityModel *model = [note object];
    [self.navigationBar refreshCity:model.name];
    [CommonUtils storageDataWithObject:model.name Key:kLocationCityName Completion:^(BOOL finish, id obj) {
        
    }];
    [CommonUtils storageDataWithObject:model.cityCode Key:kLocationCityCode Completion:^(BOOL finish, id obj) {
        
    }];
    //刷新数据
    [self loadLeftData];
}
//搜索的通知
- (void)homeSearchResult:(NSNotification *)note {
    NSInteger type = [[note object] integerValue];
    if (type == 2) {
        //跳转到更多资讯
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tabBarController setSelectedIndex:2];
        });
    }else{
        //切换到活动nav
        [self.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH, 0) animated:YES];
        [self.filterButton setHidden:NO];
    }
}
//是否定位城市在业务范围内
- (void)getCityListDataWithKeyword:(NSString *)keyword
{
    //查询定位城市是否在业务范围内
    dispatch_semaphore_t  sema = dispatch_semaphore_create(0);
    [CBEHomeVM getCityListWithKeyword:keyword complete:^(id data) {
        dispatch_semaphore_signal(sema);
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *resultArray = [data objectForKey:@"data"];
            NSArray<CBECityModel *> *array = [CBECityModel mj_objectArrayWithKeyValuesArray:resultArray];
            if (array.count) {
                _locationCityModel = [array firstObject];
            }
        }
    } fail:^(NSError *error) {
        dispatch_semaphore_signal(sema);
    }];
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}

- (void)startGPSLocation
{
    //只获取一次定位
    __block  BOOL isOnece = YES;
    [CBEGPSManager getGps:^(double lat, double lng) {
        isOnece = NO;
        if (!isOnece) {
            [CBEGPSManager stop];
            //只打印一次经纬度
            NSLog(@"lat lng (%f, %f)", lat, lng);
            //本地选择城市
            NSString *cityName = [CommonUtils getStoreDataWithKey:kLocationCityName];
            //定位城市
            NSString *locationCityName = [[CBEGPSManager sharedGpsManager] locality];
            
            dispatch_group_t group = dispatch_group_create(); dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self getCityListDataWithKeyword:locationCityName];
                
            });
            dispatch_group_notify(group, dispatch_get_main_queue(), ^{
                //判断当前本地选择的城市是否是全国
                if (![cityName isEqualToString:kDefaultCityName]) {//不是全国
                    //定位城市是否在业务范围内
                    if (_locationCityModel) {
                        //是否本地保存的城市与定位城市一致
                        if (![[CommonUtils deleteShi:cityName] isEqualToString:[CommonUtils deleteShi:locationCityName]]) {
                            //提示是否切换城市 切换城市后刷新数据
                            [UIAlertView ls_alertWithCallBackBlock:^(NSInteger buttonIndex) {
                                if (buttonIndex) {
                                    //切换城市 刷新数据
                                    [self.navigationBar refreshCity:locationCityName];
                                    [CommonUtils storageDataWithObject:locationCityName Key:kLocationCityName Completion:^(BOOL finish, id obj) {
                                        
                                    }];
                                    [CommonUtils storageDataWithObject:_locationCityModel.cityCode Key:kLocationCityCode Completion:^(BOOL finish, id obj) {
                                        
                                    }];
                                    [self loadLeftData];
                                }
                            } title:[NSString stringWithFormat:@"是否切换到%@",locationCityName] message:nil cancelButtonName:@"否" otherButtonTitles:@"是", nil];
                        }
                    }else{
                        //还是请求原来城市数据
                        [self loadLeftData];
                    }
                }else{//全国
                    //判断是否业务已拓展到该城市
                    if (_locationCityModel) {
                        //切换城市后刷新数据
                        [self.navigationBar refreshCity:locationCityName];
                        [CommonUtils storageDataWithObject:locationCityName Key:kLocationCityName Completion:^(BOOL finish, id obj) {
                            
                        }];
                        [CommonUtils storageDataWithObject:_locationCityModel.cityCode Key:kLocationCityCode Completion:^(BOOL finish, id obj) {
                            
                        }];
                        [self loadLeftData];
                    }else{
                        //还是请求原来城市数据
                        [self loadLeftData];
                    }
                }
            });
            
        }
    }fail:^(NSError *error) {
        [self loadLeftData];
    }];
}

- (NSArray *)getFilterData
{
    NSMutableArray *dataArray = [NSMutableArray array];
    [dataArray addObject:[self commonFilterRegionModelWithKeyword:@"标签" selectionType:BrandTableViewCellSelectionTypeMultiple]];
    [dataArray addObject:[self commonFilterRegionModelWithKeyword:@"排序" selectionType:BrandTableViewCellSelectionTypeSingle]];
    [dataArray addObject:[self commonFilterRegionModelWithKeyword:@"费用" selectionType:BrandTableViewCellSelectionTypeSingle]];
    [dataArray addObject:[self commonFilterRegionModelWithKeyword:@"时间" selectionType:BrandTableViewCellSelectionTypeSingle]];
    return [dataArray mutableCopy];
}

- (ZYSideSlipFilterRegionModel *)commonFilterRegionModelWithKeyword:(NSString *)keyword selectionType:(CommonTableViewCellSelectionType)selectionType {
    ZYSideSlipFilterRegionModel *model = [[ZYSideSlipFilterRegionModel alloc] init];
    model.containerCellClass = @"SideSlipCommonTableViewCell";
    model.regionTitle = keyword;
    model.customDict = @{REGION_SELECTION_TYPE:@(selectionType)};
    model.isShowAll = YES;
    if ([keyword isEqualToString:@"标签"]) {
        NSMutableArray *tagArrays = [NSMutableArray arrayWithCapacity:0];
//        [tagArrays addObject:[self createItemModelWithTitle:@"全部" itemId:@"0" selected:YES]];
        [self.tagArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *itemId = [obj objectForKey:@"id"];
            NSString *name = [obj objectForKey:@"name"];
            [tagArrays addObject:[self createItemModelWithTitle:name itemId:itemId selected:NO]];
        }];
        model.itemList = tagArrays;
    }else if ([keyword isEqualToString:@"排序"]) {
        model.itemList = @[[self createItemModelWithTitle:@"全部" itemId:@"1" selected:YES],
                           [self createItemModelWithTitle:@"最新" itemId:@"2" selected:NO],
                           [self createItemModelWithTitle:@"离我最近" itemId:@"3" selected:NO]
                           ];
    }else if ([keyword isEqualToString:@"费用"]) {
        model.itemList = @[[self createItemModelWithTitle:@"全部" itemId:@"" selected:YES],
                           [self createItemModelWithTitle:@"免费" itemId:@"0" selected:NO],
                           [self createItemModelWithTitle:@"收费" itemId:@"1" selected:NO]
                           ];
    }else{
        model.itemList = @[[self createItemModelWithTitle:@"不限" itemId:@"" selected:YES],
                           [self createItemModelWithTitle:@"今天" itemId:@"1" selected:NO],
                           [self createItemModelWithTitle:@"近一周" itemId:@"2" selected:NO],
                           [self createItemModelWithTitle:@"近一月" itemId:@"3" selected:NO]
                           ];
    }
    
    return model;
}

- (CommonItemModel *)createItemModelWithTitle:(NSString *)itemTitle
                                       itemId:(NSString *)itemId
                                     selected:(BOOL)selected {
    CommonItemModel *model = [[CommonItemModel alloc] init];
    model.itemId = itemId;
    model.itemName = itemTitle;
    model.selected = selected;
    return model;
}
#pragma mark - 筛选侧边栏
- (void)filterAction
{
    [_filterController show];
}

#pragma - delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _rightTableView) {
        return 2;
    }
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _rightTableView) {
        if (section == 0) {
            return 1;
        }
        return self.rDataSource.count;
    }
    if (section == 0 || section == 1 || section == 2) {
        return 1;
    }
    return self.informationList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == _rightTableView) {
        return 0.f;
    }
    if (section == 0 || section == 1 || section == 2) {
        return 0.f;
    }
    return kHeightScale(54.f);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == _rightTableView) {
        return nil;
    }
    if (section == 3) {
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kHeightScale(54))];
        backView.backgroundColor = kMainDefaltGrayColor;
        
        CBEHomeHeaderView *header = [[CBEHomeHeaderView alloc] initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, kHeightScale(44))];
        header.backgroundColor = [UIColor whiteColor];
        [backView addSubview:header];
        header.moreBlock = ^{
            //跳转到更多资讯
            [self.tabBarController setSelectedIndex:2];
        };
        
        return backView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    //    NSInteger row = indexPath.row;
    if (tableView == _rightTableView) {
        if (section == 0) {
            return 105;
        }
        return 165+(kSCREEN_WIDTH - 2*kPadding15)*(180/345.0);
    }
    
    if (section == 0) {
        return kHeightScale(156);
    }
    if (section == 1) {
        if (!self.userList.count) {
            return 0.f;
        }
        return 243.f;
    }
    if (section == 2) {
        return 235.f;
    }
    return 95;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (tableView == _rightTableView) {
        if (section == 0) {
            CBEAcClassfyCell *cell = [CBEAcClassfyCell cellForTableView:tableView];
            [cell refreshDataWith:self.activityTypeArray];
            
            kWeakSelf
            cell.typeBlock = ^(CBEActivityTypeModel *typeModel) {
                [weakSelf loadRightDataWithActivityTypeId:typeModel.activityTypeId withIsPremium:nil withSortType:nil withTimeType:nil withTagIds:nil];
            };
            return cell;
        }
        CBEActivityCell *cell = [CBEActivityCell cellForTableView:tableView];
        CBEActivityModel *model = [self.rDataSource objectAtIndex:row];
        [cell refreshDataWith:model];
        
        return cell;
    }
    if (section == 0) {
        CBECycleViewCell *cell = [CBECycleViewCell cellForTableView:tableView];
        [cell setCycleDataSource:self.bannerList];
        cell.delegate = self;
        
        return cell;
    }else if (section == 1) {
        if (!self.userList.count) {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"defaultCell"];
            return cell;
        }
        CBEMayKnowViewCell *cell = [CBEMayKnowViewCell cellForTableView:tableView];
        [cell refreshDataWith:self.userList];
        cell.moreBlock = ^{
            //跳转到更多人脉
            [self.tabBarController setSelectedIndex:3];
        };
        cell.mayknowSelectedBlock = ^(id model) {
            CBECardController *cardVC = [[CBECardController alloc] init];
            cardVC.model = model;
            [self.navigationController pushViewController:cardVC animated:YES];
        };
        return cell;
    }else if (section == 2) {
        CBEMayActivityCell *cell = [CBEMayActivityCell cellForTableView:tableView];
        [cell refreshDataWith:self.activityList];
        cell.moreBlock = ^{
            //跳转到活动
            [self.scrollView setContentOffset:CGPointMake(kSCREEN_WIDTH, 0) animated:YES];
            [self.filterButton setHidden:NO];
        };
        cell.activitySelectedBlock = ^(id model) {
            CBEActivityDetailController *detailController = [[CBEActivityDetailController alloc] init];
            detailController.detailModel = model;
            [self.navigationController pushViewController:detailController animated:YES];
        };
        return cell;
    }
    CBENewsListCell *cell = [CBENewsListCell cellForTableView:tableView];
    CBENewsListModel *model = [self.informationList objectAtIndex:indexPath.row];
    [cell refreshDataWith:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (tableView == _rightTableView) {
        if (section == 1) {
            CBEActivityModel *model = [self.rDataSource objectAtIndex:row];
            CBEActivityDetailController *detailController = [[CBEActivityDetailController alloc] init];
            detailController.detailModel = model;
            [self.navigationController pushViewController:detailController animated:YES];
        }
    }else{
        if (section == 3) {
            CBENewsListModel *model = [self.informationList objectAtIndex:row];
            CBENewsDetailViewController *newsController = [[CBENewsDetailViewController alloc] init];
            newsController.model = model;
            [self.navigationController pushViewController:newsController animated:YES];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView) {
        CGFloat distance = scrollView.contentOffset.x;
        CGFloat width = scrollView.frame.size.width;
        NSInteger index = distance / width;
        [self.navigationBar moveLineAtIndex:index];
        if (index == 0) {
            [self.filterButton setHidden:YES];
        }else{
            [self.filterButton setHidden:NO];
        }
    }else if (scrollView == self.leftTableView) {
        CGFloat sectionHeaderHeight = 54;
        if (scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}
#pragma mark - 轮播图选中
- (void)cycleViewDidSelectedAtIndex:(NSInteger)index
{
    //点击轮播图事件
    NSLog(@"selected index:%ld",index);
    CBEBannarModel *model = [self.bannerList objectAtIndex:index];
    CBEWebController *webController = [[CBEWebController alloc] init];
    webController.urlString = model.bannerUrl;
    [self.navigationController pushViewController:webController animated:YES];
}
#pragma mark - 人脉好友选中

#pragma - setter
- (UITableView *)leftTableView
{
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight) style:UITableViewStylePlain];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _leftTableView.backgroundColor = kMainDefaltGrayColor;
        _leftTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadLeftData)];
    }
    return _leftTableView;
}

- (UITableView *)rightTableView
{
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(kSCREEN_WIDTH, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight) style:UITableViewStylePlain];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rightTableView.backgroundColor = kMainDefaltGrayColor;
        _rightTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.rPageNum = 1;
            [self loadRightDataWithActivityTypeId:nil withIsPremium:nil withSortType:nil withTimeType:nil withTagIds:nil];
        }];
        _rightTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            self.rPageNum++;
            [self loadRightDataWithActivityTypeId:nil withIsPremium:nil withSortType:nil withTimeType:nil withTagIds:nil];
        }];
    }
    return _rightTableView;
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT - KNavigationBarHeight - kTabbarHeight)];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH * 2, 0);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
    }
    return _scrollView;
}

- (UIButton *)filterButton
{
    if (!_filterButton) {
        _filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _filterButton.frame = CGRectMake(kSCREEN_WIDTH - 46 - kPadding15, kSCREEN_HEIGHT - kTabbarHeight - kPadding15 - 46, 46, 46);
        [_filterButton setBackgroundImage:IMAGE_NAMED(@"event_icon_filter") forState:UIControlStateNormal];
        [_filterButton setClipsToBounds:YES];
        [_filterButton.layer setCornerRadius:25];
        [_filterButton addTarget:self action:@selector(filterAction) forControlEvents:UIControlEventTouchUpInside];
        [_filterButton setHidden:YES];
    }
    return _filterButton;
}

- (void)setFilterVC
{
    kWeakSelf
    self.filterController = [[ZYSideSlipFilterController alloc] initWithSponsor:self resetBlock:^(NSArray *dataList) {
        //重置
        weakSelf.filterController.dataList = [self getFilterData];
    } commitBlock:^(NSArray *dataList) {
        //确定
        [weakSelf filtterSelectData:dataList];
        [weakSelf.filterController dismiss];
    }];
    _filterController.animationDuration = .3f;
    _filterController.sideSlipLeading = 90;//0.15*[UIScreen mainScreen].bounds.size.width;
    self->_filterController.dataList = [self getFilterData];
}

- (void)filtterSelectData:(NSArray *)dataArray
{
    //标签
    ZYSideSlipFilterRegionModel *firstModel = [dataArray firstObject];
    NSArray *tagArray = firstModel.selectedItemList;
    __block NSString *tagIds = @"";
    [tagArray enumerateObjectsUsingBlock:^(CommonItemModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx != tagArray.count - 1) {
            tagIds = [tagIds stringByAppendingString:StringFormat(@"%@,",obj.itemId)];
        }else{
            tagIds = [tagIds stringByAppendingString:StringFormat(@"%@",obj.itemId)];
        }
    }];
    //排序
    ZYSideSlipFilterRegionModel *secondModel = [dataArray objectAtIndex:1];
    NSArray *sortArray = secondModel.selectedItemList;
    CommonItemModel *sortModel = [sortArray lastObject];
    NSString *sortId = sortModel.itemId;
    //费用
    ZYSideSlipFilterRegionModel *thirdModel = [dataArray objectAtIndex:2];
    NSArray *feeArray = thirdModel.selectedItemList;
    CommonItemModel *feeModel = [feeArray lastObject];
    NSString *feeId = feeModel.itemId;
    //时间
    ZYSideSlipFilterRegionModel *forthModel = [dataArray objectAtIndex:3];
    NSArray *timeArray = forthModel.selectedItemList;
    CommonItemModel *timeModel = [timeArray lastObject];
    NSString *timeId = timeModel.itemId;
    
    [self.rDataSource removeAllObjects];
    self.rPageNum = 1;
    [self loadRightDataWithActivityTypeId:nil withIsPremium:feeId withSortType:sortId withTimeType:timeId withTagIds:tagIds];
}

- (NSMutableArray *)bannerList {
    if (!_bannerList) {
        _bannerList = [NSMutableArray array];
    }
    return _bannerList;
}
- (NSMutableArray *)userList {
    if (!_userList) {
        _userList = [NSMutableArray array];
    }
    return _userList;
}
- (NSMutableArray *)activityList {
    if (!_activityList) {
        _activityList = [NSMutableArray array];
    }
    return _activityList;
}
- (NSMutableArray *)informationList {
    if (!_informationList) {
        _informationList = [NSMutableArray array];
    }
    return _informationList;
}
- (NSMutableArray *)rDataSource {
    if (!_rDataSource) {
        _rDataSource = [NSMutableArray array];
    }
    return _rDataSource;
}
- (NSMutableArray *)activityTypeArray {
    if (!_activityTypeArray) {
        _activityTypeArray = [NSMutableArray array];
    }
    return _activityTypeArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

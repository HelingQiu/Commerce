//
//  CBEHomeSearchController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/10.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEHomeSearchController.h"
#import "LSSearchBarView.h"
#import "CBEConnectionsCell.h"
#import "CBENewsListCell.h"
#import "CBEActivityListCell.h"
#import "CBEHomeVM.h"
#import "CBENewsListModel.h"
#import "CBEActivityModel.h"
#import "CBEActivityDetailController.h"
#import "CBENewsDetailViewController.h"
#import "CBESearchMoreController.h"
#import "CBEConnectListModel.h"

@interface CBEHomeSearchController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    LSSearchBarView *_searchView;
}
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UITextField *textField;

@property (nonatomic, strong) NSArray *userList;

@property (nonatomic, strong) NSArray *activityList;
@property (nonatomic, assign) NSInteger activityCount;

@property (nonatomic, strong) NSArray *informationList;
@property (nonatomic, assign) NSInteger informationCount;

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger inputCount;     //用户输入次数，用来控制延迟搜索请求

@end

@implementation CBEHomeSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _searchView = [[LSSearchBarView alloc] initWithAlignment:(SearchBarAlignmentAlignmentLeft) placeholder:@"输入搜索内容" didClicked:nil];
    [self.navigationItem.titleView addSubview:_searchView];
    [self setBarButton:(BarButtonItemRight) action:@selector(cancelAction) title:@"取消" titleColor:kMainBlueColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, KNavigationBarHeight, kSCREEN_WIDTH, kSCREEN_HEIGHT-KNavigationBarHeight)) style:(UITableViewStyleGrouped)];
    _searchView.textField.placeholder = @"输入搜索内容";
    self.textField = _searchView.textField;
    self.textField.delegate = self;
    self.textField.userInteractionEnabled = YES;
    [self.textField becomeFirstResponder];
    
    // 监控当前输入框的值
    [self.textField addTarget:self action:@selector(textFieldValueChangedAction:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = kMainDefaltGrayColor;
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
        [self.tableView setSeparatorColor:[UIColor ls_colorWithHex:0xf0f2f5]];
    }
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)textFieldValueChangedAction:(UITextField *)textField {
    self.inputCount ++;
    [self performSelector:@selector(requestKeyWorld:) withObject:@(self.inputCount) afterDelay:1.0f];
    
}
- (void)requestKeyWorld:(NSNumber *)count {
     if (self.inputCount == [count integerValue]) {
         [self requestDataWithKeyword:self.textField.text];
     }
}
- (void)requestDataWithKeyword:(NSString *)keyword
{
    self.dataSource = [NSMutableArray array];
    [CBEHomeVM homeSearchDataWithKeyword:keyword complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSDictionary *result = [data objectForKey:@"data"];
            self.userList = [result objectForKey:@"userList"];
            if (self.userList.count > 0) {
                BOOL isFoot = NO;
                if (self.userList.count > 2) {
                    isFoot = YES;
                }
                NSDictionary *dic1 = @{@"type":@(1),@"list":self.userList,@"foot":@(isFoot)};
                [self.dataSource addObject:dic1];
            }
            
            self.informationCount = [[result objectForKey:@"informationCount"] integerValue];
            self.informationList = [result objectForKey:@"informationList"];
            if (self.informationCount > 0) {
                BOOL isFoot = NO;
                if (self.informationCount > 2) {
                    isFoot = YES;
                }
                NSDictionary *dic3 = @{@"type":@(2),@"list":self.informationList,@"foot":@(isFoot)};
                [self.dataSource addObject:dic3];
            }
            
            self.activityCount = [[result objectForKey:@"activityCount"] integerValue];
            self.activityList = [result objectForKey:@"activityList"];
            if (self.activityCount > 0) {
                BOOL isFoot = NO;
                if (self.activityCount > 2) {
                    isFoot = YES;
                }
                NSDictionary *dic2 = @{@"type":@(3),@"list":self.activityList,@"foot":@(isFoot)};
                [self.dataSource addObject:dic2];
            }
            
            sleep(1);
            [self.tableView reloadData];
        }else{
            [self.tableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)cancelAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footAction:(UIButton *)sender
{
    if (sender.tag == 1) {
        CBESearchMoreController *moreController = [[CBESearchMoreController alloc] init];
        [self.navigationController pushViewController:moreController animated:YES];
    }else if (sender.tag == 2) {
        [self cancelAction];
        [[NSNotificationCenter defaultCenter] postNotificationName:kHomeSearchMore object:@(2)];
    }else{
        [self cancelAction];
        [[NSNotificationCenter defaultCenter] postNotificationName:kHomeSearchMore object:@(3)];
    }
}

#pragma mark- Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *data = [self.dataSource objectAtIndex:section];
    NSArray *dataArray = [data objectForKey:@"list"];
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 40.f)];
    headView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 200, 20)];
    [headLabel setTextColor:kMainTextColor];
    [headLabel setFont:kFont(14)];
    
    NSDictionary *data = [self.dataSource objectAtIndex:section];
    NSInteger type = [[data objectForKey:@"type"] integerValue];
    if (type == 1) {
        [headLabel setText:@"人脉"];
    }else if (type == 2) {
        [headLabel setText:@"资讯"];
    }else{
        [headLabel setText:@"活动"];
    }
    
    [headView addSubview:headLabel];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, kSCREEN_WIDTH, 0.5)];
    line.backgroundColor = kMainLineColor;
    [headView addSubview:line];
    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSDictionary *data = [self.dataSource objectAtIndex:section];
    BOOL isFoot = [[data objectForKey:@"foot"] boolValue];
    if (!isFoot) {
        return 10.f;
    }
    return 50.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 50.f)];
    footView.backgroundColor = kMainDefaltGrayColor;
    
    CBEBaseButton *footBtn = [CBEBaseButton buttonWithType:UIButtonTypeCustom];
    footBtn.frame = CGRectMake(0, 0, kSCREEN_WIDTH, 40.f);
    [footBtn setImageViewRect:CGRectMake(15, 10, 20, 20)];
    [footBtn setTitleLabelRect:CGRectMake(43, 10, 200, 20)];
    [footBtn setImage:IMAGE_NAMED(@"home_icon_search_little") forState:UIControlStateNormal];
    
    NSDictionary *data = [self.dataSource objectAtIndex:section];
    NSInteger type = [[data objectForKey:@"type"] integerValue];
    if (type == 1) {
        [footBtn setTitle:@"查看更多人脉信息" forState:UIControlStateNormal];
    }else if (type == 2) {
        [footBtn setTitle:@"查看更多资讯信息" forState:UIControlStateNormal];
    }else{
        [footBtn setTitle:@"查看更多活动信息" forState:UIControlStateNormal];
    }
    
    [footBtn setTitleColor:[UIColor ls_colorWithHexString:@"#92A1B3"] forState:UIControlStateNormal];
    [footBtn.titleLabel setFont:kFont(14)];
    [footBtn setBackgroundColor:[UIColor whiteColor]];
    [footBtn addTarget:self action:@selector(footAction:) forControlEvents:UIControlEventTouchUpInside];
    footBtn.tag = type;
    [footView addSubview:footBtn];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, kSCREEN_WIDTH, 0.5)];
    line.backgroundColor = kMainLineColor;
    [footView addSubview:line];
    
    BOOL isFoot = [[data objectForKey:@"foot"] boolValue];
    if (!isFoot) {
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10)];
    }
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSDictionary *data = [self.dataSource objectAtIndex:section];
    NSInteger type = [[data objectForKey:@"type"] integerValue];
    if (type == 1) {
        return 80.f;
    }
    return 95.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSDictionary *data = [self.dataSource objectAtIndex:section];
    NSInteger type = [[data objectForKey:@"type"] integerValue];
    if (type == 1) {
        CBEConnectionsCell *cell = [CBEConnectionsCell cellForTableView:tableView];
        NSArray *userList = [data objectForKey:@"list"];
        CBEConnectListModel *model = [CBEConnectListModel mj_objectWithKeyValues:[userList objectAtIndex:indexPath.row]];
        [cell refreshDataWith:model];
        return cell;
    }
    if (type == 2) {
        CBENewsListCell *cell = [CBENewsListCell cellForTableView:tableView];
        NSArray *infoList = [data objectForKey:@"list"];
        CBENewsListModel *model = [CBENewsListModel mj_objectWithKeyValues:[infoList objectAtIndex:indexPath.row]];
        [cell refreshDataWith:model];
        
        return cell;
    }
    CBEActivityListCell *cell = [CBEActivityListCell cellForTableView:tableView];
    NSArray *infoList = [data objectForKey:@"list"];
    CBEActivityModel *model = [CBEActivityModel mj_objectWithKeyValues:[infoList objectAtIndex:indexPath.row]];
    [cell refreshDataWith:model];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    NSDictionary *data = [self.dataSource objectAtIndex:section];
    NSInteger type = [[data objectForKey:@"type"] integerValue];
    if (type == 1) {
    }else if (type == 2) {
        NSArray *infoList = [data objectForKey:@"list"];
        CBENewsListModel *model = [CBENewsListModel mj_objectWithKeyValues:[infoList objectAtIndex:indexPath.row]];
        CBENewsDetailViewController *newsController = [[CBENewsDetailViewController alloc] init];
        newsController.model = model;
        [self.navigationController pushViewController:newsController animated:YES];
    }else{
        NSArray *infoList = [data objectForKey:@"list"];
        CBEActivityModel *model = [CBEActivityModel mj_objectWithKeyValues:[infoList objectAtIndex:indexPath.row]];
        CBEActivityDetailController *detailController = [[CBEActivityDetailController alloc] init];
        detailController.detailModel = model;
        [self.navigationController pushViewController:detailController animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

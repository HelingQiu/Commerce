//
//  CBECityController.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/20.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBECityModel.h"

typedef void(^CitySelectBlock)(CBECityModel *model);
@interface CBECityController : CBEBaseController

@property (nonatomic, copy) CitySelectBlock cityBlock;

@end

//
//  CBECityController.m
//  CBECommerce
//
//  Created by Rainer on 2018/9/20.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBECityController.h"
#import "CBECityModel.h"
#import "CBEGPSManager.h"
#import "LSSearchBarView.h"
#import "CBECitySearchController.h"
#import "CBEHomeVM.h"

@interface CBECityController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UILabel *_locaLabel;
    UIButton *_refreshBtn;
    UILabel *_cityLabel;
    UILabel *_tipLabel;
    
    LSSearchBarView *_searchView;
}
@property (nonatomic, strong) UITableView *cityTableView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@property (nonatomic, strong) NSArray *indexSourceArr;

@property (nonatomic, strong) UIButton *topBtn;

@end

@implementation CBECityController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getCityListData];
    [self initTableView];
    [self startGPSLocation];
}

- (void)startGPSLocation
{
    //定位不可用 或者 定位权限未开启
    if ([CLLocationManager locationServicesEnabled] == FALSE || [CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        [_locaLabel setText:@"定位失败"];
        _locaLabel.hidden = NO;
        _refreshBtn.hidden = NO;
        _cityLabel.hidden = YES;
        _tipLabel.hidden = YES;
    }else{
        //只获取一次定位
        __block  BOOL isOnece = YES;
        [CBEGPSManager getGps:^(double lat, double lng) {
            isOnece = NO;
            //定位城市
            NSString *locationCityName = [[CBEGPSManager sharedGpsManager] locality];
            self->_locaLabel.hidden = YES;
            self->_refreshBtn.hidden = YES;
            self->_cityLabel.hidden = NO;
            [self->_cityLabel setText:locationCityName];
            self->_tipLabel.hidden = NO;
            if (!isOnece) {
                [CBEGPSManager stop];
            }
        }fail:^(NSError *error) {
            [self->_locaLabel setText:@"定位失败"];
            self->_locaLabel.hidden = NO;
            self->_refreshBtn.hidden = NO;
            self->_cityLabel.hidden = YES;
            self->_tipLabel.hidden = YES;
        }];
    }
}

- (void)getCityListData
{
    [CBEHomeVM getCityListWithKeyword:nil complete:^(id data) {
        if ([[data objectForKey:@"success"] boolValue]) {
            NSArray *resultArray = [data objectForKey:@"data"];
            NSArray<CBECityModel *> *array = [CBECityModel mj_objectArrayWithKeyValuesArray:resultArray];
            NSDictionary *dict = [self sortObjectsAccordingToInitialWith:array SortKey:@"name"];
            self.dataSourceArr = [dict objectForKey:@"array"];
            self.indexSourceArr = [dict objectForKey:@"char"];
            [self.cityTableView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)initTableView
{
    kWeakSelf
    _searchView = [[LSSearchBarView alloc] initWithFrame:CGRectMake(40, 6, kSCREEN_WIDTH - 50, 32) placeholder:@"输入城市名字" didClicked:^{
        //跳转搜索
        CBECitySearchController *cityController = [[CBECitySearchController alloc] init];
        [weakSelf.navigationController pushViewController:cityController animated:YES];
    }];
    [self.navigationItem.titleView addSubview:_searchView];
    
    UIView *backView = [[UIView alloc] init];
    backView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, 56);
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _topBtn.frame = CGRectMake(0, 0, kSCREEN_WIDTH, 56);
    _topBtn.backgroundColor = [UIColor whiteColor];
    [backView addSubview:_topBtn];
    
    _locaLabel = [[UILabel alloc] init];
    _locaLabel.frame = CGRectMake(15, 22, 60, 17);
    _locaLabel.textColor = kMainTextColor;
    _locaLabel.font = kFont(12);
    _locaLabel.text = @"定位中";
    [backView addSubview:_locaLabel];
    
    _refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _refreshBtn.frame = CGRectMake(kSCREEN_WIDTH - 12 - 30, 15, 30, 30);
    [_refreshBtn setImage:IMAGE_NAMED(@"city_icon_refresh") forState:UIControlStateNormal];
    [_refreshBtn addTarget:self action:@selector(startGPSLocation) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:_refreshBtn];
    _refreshBtn.hidden = YES;
    
    _cityLabel = [[UILabel alloc] init];
    _cityLabel.textColor = kMainBlackColor;
    _cityLabel.font = kFont(18);
    _cityLabel.text = @"全国";
    [backView addSubview:_cityLabel];
    [_cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_topBtn.mas_left).offset(15);
        make.height.mas_equalTo(25);
        make.centerY.equalTo(self->_topBtn.mas_centerY);
    }];
    _cityLabel.hidden = YES;
    
    _tipLabel = [[UILabel alloc] init];
    _tipLabel.textColor = kMainTextColor;
    _tipLabel.font = kFont(12);
    _tipLabel.text = @"当前定位城市";
    [backView addSubview:_tipLabel];
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_cityLabel.mas_right).offset(8);
        make.height.mas_equalTo(17);
        make.bottom.equalTo(self->_cityLabel.mas_bottom);
    }];
    _tipLabel.hidden = YES;
    
    self.cityTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 56, kSCREEN_WIDTH, kSCREEN_HEIGHT - 64 - 56) style:UITableViewStylePlain];
    self.cityTableView.dataSource = self;
    self.cityTableView.delegate = self;
    self.cityTableView.sectionIndexColor = [UIColor colorWithRed:252/255.0f green:74/255.0f blue:132/255.0f alpha:1.0f];
    self.cityTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
    [self.view addSubview:self.cityTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSourceArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSourceArr[section] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [_indexSourceArr objectAtIndex:section];
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _indexSourceArr;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIndentifier = @"cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
        
    }
    CBECityModel *model = [self.dataSourceArr[indexPath.section] objectAtIndex:indexPath.row];
    cell.textLabel.text = model.name;
    return cell;
    
}
// 设置距离左右各15的距离
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, kPadding15, 0, kPadding15)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CBECityModel *model = [self.dataSourceArr[indexPath.section] objectAtIndex:indexPath.row];
    [UIAlertController showAlertInViewController:self
                                       withTitle:StringFormat(@"是否切换到%@",model.name)
                                         message:nil
                               cancelButtonTitle:@"取消"
                          destructiveButtonTitle:@"确定"
                               otherButtonTitles:nil
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                            
                                            if (buttonIndex == controller.destructiveButtonIndex) {
                                                if (self.cityBlock) {
                                                    self.cityBlock(model);
                                                }
                                                [self.navigationController popViewControllerAnimated:YES];
                                            }
                                        }];
}

// 按首字母分组排序数组
- (NSDictionary *)sortObjectsAccordingToInitialWith:(NSArray *)willSortArr SortKey:(NSString *)sortkey {
    // 初始化UILocalizedIndexedCollation
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    //得出collation索引的数量，这里是27个（26个字母和1个#）
    NSInteger sectionTitlesCount = [[collation sectionTitles] count];
    //初始化一个数组newSectionsArray用来存放最终的数据，我们最终要得到的数据模型应该形如@[@[以A开头的数据数组], @[以B开头的数据数组], @[以C开头的数据数组], ... @[以#(其它)开头的数据数组]]
    NSMutableArray *newSectionsArray = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
    
    //初始化27个空数组加入newSectionsArray
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [newSectionsArray addObject:array];
    }
    
    NSLog(@"newSectionsArray %@ %@",newSectionsArray,collation.sectionTitles);
    
    NSMutableArray *firstChar = [NSMutableArray arrayWithCapacity:10];
    
    //将每个名字分到某个section下
    for (id Model in willSortArr) {
        //获取name属性的值所在的位置，比如"林丹"，首字母是L，在A~Z中排第11（第一位是0），sectionNumber就为11
        NSInteger sectionNumber = [collation sectionForObject:Model collationStringSelector:NSSelectorFromString(sortkey)];
        
        //把name为“林丹”的p加入newSectionsArray中的第11个数组中去
        NSMutableArray *sectionNames = newSectionsArray[sectionNumber];
        [sectionNames addObject:Model];
        
        //拿出每名字的首字母
        NSString * str= collation.sectionTitles[sectionNumber];
        [firstChar addObject:str];
        NSLog(@"sectionNumbersectionNumber %ld %@",sectionNumber,str);
    }
    
    //返回首字母排好序的数据
    NSArray *firstCharResult = [self SortFirstChar:firstChar];
    
    
    NSLog(@"firstCharResult== %@",firstCharResult);
    
    //对每个section中的数组按照name属性排序
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *personArrayForSection = newSectionsArray[index];
        NSArray *sortedPersonArrayForSection = [collation sortedArrayFromArray:personArrayForSection collationStringSelector:NSSelectorFromString(sortkey)];
        newSectionsArray[index] = sortedPersonArrayForSection;
    }
    
    //删除空的数组
    NSMutableArray *finalArr = [NSMutableArray new];
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        if (((NSMutableArray *)(newSectionsArray[index])).count != 0) {
            [finalArr addObject:newSectionsArray[index]];
        }
    }
    return @{@"array":finalArr,
             @"char":firstCharResult};
}

- (NSArray *)SortFirstChar:(NSArray *)firstChararry{
    //数组去重复
    
    NSMutableArray *noRepeat = [[NSMutableArray alloc]initWithCapacity:8];
    
    NSMutableSet *set = [[NSMutableSet alloc]initWithArray:firstChararry];
    
    [set enumerateObjectsUsingBlock:^(id obj , BOOL *stop){
        
        
        [noRepeat addObject:obj];
        
    }];
    
    //字母排序
    NSArray *resultkArrSort1 = [noRepeat sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    //把”#“放在最后一位
    NSMutableArray *resultkArrSort2 = [[NSMutableArray alloc]initWithArray:resultkArrSort1];
    if ([resultkArrSort2 containsObject:@"#"]) {
        
        [resultkArrSort2 removeObject:@"#"];
        [resultkArrSort2 addObject:@"#"];
    }
    
    return resultkArrSort2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CBECitySearchController.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/3.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBaseController.h"
#import "CBECityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBECitySearchController : CBEBaseController

@end

NS_ASSUME_NONNULL_END

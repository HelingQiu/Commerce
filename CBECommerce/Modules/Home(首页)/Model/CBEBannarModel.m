//
//  CBEBannarModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/14.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEBannarModel.h"

@implementation CBEBannarModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"bannerId":@"id"};
}

@end

//
//  CBEActivityTypeModel.m
//  CBECommerce
//
//  Created by don on 2018/10/15.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEActivityTypeModel.h"

@implementation CBEActivityTypeModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"activityTypeId":@"id"};
}

@end

//
//  CBECityModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/20.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBECityModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *cityCode;
@property (nonatomic, copy) NSString *first;

@end

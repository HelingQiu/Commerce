//
//  CBEBannarModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/14.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEBannarModel : NSObject

@property (nonatomic, copy) NSString *bannerDesc;
@property (nonatomic, copy) NSString *bannerTitle;
@property (nonatomic, assign) NSInteger bannerType;
@property (nonatomic, copy) NSString *bannerUrl;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *bannerId;
@property (nonatomic, copy) NSString *modifyTime;
@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSString *imageUrl;

@end

NS_ASSUME_NONNULL_END

//
//  CBEActivityModel.h
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEActivityModel : NSObject

@property (nonatomic, copy) NSString *actiivtyTypeName;
@property (nonatomic, assign) NSInteger activeStatus;
@property (nonatomic, copy) NSString *activeStatusStr;
@property (nonatomic, copy) NSString *activityTypeId;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) NSInteger approveStatus;
@property (nonatomic, copy) NSString *approveStatusStr;
@property (nonatomic, copy) NSString *cityCode;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, assign) NSInteger collectCount;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *createTimeStr;
@property (nonatomic, copy) NSString *actId;//id转换
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *modifyTime;
@property (nonatomic, copy) NSString *modifyTimeStr;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger payType;
@property (nonatomic, copy) NSString *picture;
@property (nonatomic, assign) NSInteger publishStatus;
@property (nonatomic, copy) NSString *readCount;
@property (nonatomic, copy) NSString *regionName;
@property (nonatomic, strong) NSArray *telationTagList;//标签列表
@property (nonatomic, strong) NSArray *timeList;//时间列表
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *userId;//创建人Id
@property (nonatomic, assign) NSInteger currentStatus;//1报名中，2进行中

@end

NS_ASSUME_NONNULL_END

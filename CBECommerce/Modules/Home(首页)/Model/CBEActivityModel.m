//
//  CBEActivityModel.m
//  CBECommerce
//
//  Created by Rainer on 2018/10/7.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import "CBEActivityModel.h"

@implementation CBEActivityModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"actId":@"id"};
}

@end

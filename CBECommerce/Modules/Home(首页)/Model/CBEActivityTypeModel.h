//
//  CBEActivityTypeModel.h
//  CBECommerce
//
//  Created by don on 2018/10/15.
//  Copyright © 2018 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEActivityTypeModel : NSObject

@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *activityTypeId;
@property (nonatomic, copy) NSString *modifyTime;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *imageUrl;

@end

NS_ASSUME_NONNULL_END

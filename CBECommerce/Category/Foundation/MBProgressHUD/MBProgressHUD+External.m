//
//  MBProgressHUD+External.m
//  CBECommerce
//
//  Created by don on 2018/9/13.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "MBProgressHUD+External.h"

@implementation MBProgressHUD (External)

+ (MBProgressHUD *)showLoadingWithView:(UIView *)view title:(NSString *)title {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.label.text = title;
    return hud;
}
+ (MBProgressHUD *)showLoadingWithTitle:(NSString *)title {
    MBProgressHUD *hud = [self showLoadingWithView:[UIApplication sharedApplication].keyWindow title:title];
    return hud;
}
+ (MBProgressHUD *)showLoadingWithView:(UIView *)view {
    MBProgressHUD *hud = [self showLoadingWithView:view title:kDefaultLoadingTitle];
    return hud;
}
+ (MBProgressHUD *)showLoading {
    MBProgressHUD *hud = [self showLoadingWithTitle:kDefaultLoadingTitle];
    return hud;
}

+ (void)showMessageWithView:(UIView *)view message:(NSString *)message duration:(CGFloat)duration includingReminder:(BOOL)includingReminder {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    if (includingReminder) {
        hud.label.text = @"提示";
        hud.label.font = [UIFont systemFontOfSize:18];
    }
    hud.detailsLabel.text = message;
    hud.detailsLabel.font = [UIFont systemFontOfSize:15];
    hud.minShowTime = duration;
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:duration];
    
}
+ (void)showMessageIncludingReminderWithView:(UIView *)view message:(NSString *)message duration:(CGFloat)duration {
    [self showMessageWithView:view message:message duration:duration includingReminder:YES];
}

+ (void)showMessageIncludingReminderWithView:(UIView *)view message:(NSString *)message {
    [self showMessageWithView:view message:message duration:kDefaultDuration includingReminder:YES];
}
+ (void)showMessageIncludingReminderWithMessage:(NSString *)message duration:(CGFloat)duration {
    [self showMessageIncludingReminderWithView:[UIApplication sharedApplication].keyWindow message:message duration:duration];
}
+ (void)showMessageIncludingReminderWithMessage:(NSString *)message {
    [self showMessageIncludingReminderWithView:[UIApplication sharedApplication].keyWindow message:message duration:kDefaultDuration];
}
- (void)showMessageIncludingReminderWithMessage:(NSString *)message duration:(CGFloat)duration {
    self.mode = MBProgressHUDModeText;
    self.label.text = @"提示";
    self.detailsLabel.text = message;
    [self hideAnimated:YES afterDelay:duration];
}
- (void)showMessageIncludingReminderWithMessage:(NSString *)message {
    [self showMessageIncludingReminderWithMessage:message duration:kDefaultDuration];
}

+ (void)showMessageWithView:(UIView *)view message:(NSString *)message duration:(CGFloat)duration {
    [self showMessageWithView:view message:message duration:duration includingReminder:NO];
}
+ (void)showMessageWithView:(UIView *)view message:(NSString *)message {
    [self showMessageWithView:view message:message duration:kDefaultDuration includingReminder:NO];
}
+ (void)showMessage:(NSString *)message duration:(CGFloat)duration {
    [self showMessageWithView:[UIApplication sharedApplication].keyWindow message:message duration:duration];
}
+ (void)showMessage:(NSString *)message {
    [self showMessageWithView:[UIApplication sharedApplication].keyWindow message:message duration:kDefaultDuration];
}
- (void)showMessage:(NSString *)message duration:(CGFloat)duration {
    self.mode = MBProgressHUDModeText;
    self.detailsLabel.text = message;
    [self hideAnimated:YES afterDelay:duration];
}
- (void)showMessage:(NSString *)message {
    [self showMessage:message duration:kDefaultDuration];
}


- (void)hide {
    [self hideAnimated:YES];
}

@end

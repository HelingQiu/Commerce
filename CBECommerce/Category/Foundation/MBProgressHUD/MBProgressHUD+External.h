//
//  MBProgressHUD+External.h
//  CBECommerce
//
//  Created by don on 2018/9/13.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

static NSString *kDefaultLoadingTitle = @"加载中...";
static CGFloat kDefaultDuration = 2.0;

@interface MBProgressHUD (External)

#pragma mark - 加载动画
+ (MBProgressHUD *)showLoadingWithView:(UIView *)view title:(NSString *)title;// 在view上加载动画，内容是title
+ (MBProgressHUD *)showLoadingWithTitle:(NSString *)title;// 在window上加载动画，内容是title
+ (MBProgressHUD *)showLoadingWithView:(UIView *)view;// 在view上加载动画，内容是默认的kDefaultLoadingTitle
+ (MBProgressHUD *)showLoading;// 在window上加载动画，内容是默认的kDefaultLoadingTitle

#pragma mark - 显示：提示+内容
+ (void)showMessageIncludingReminderWithView:(UIView *)view message:(NSString *)message duration:(CGFloat)duration;// 在view上显示：提示+message，持续时间是duration
+ (void)showMessageIncludingReminderWithView:(UIView *)view message:(NSString *)message;// 在view上显示：提示+message，持续时间是默认的kDefaultDuration
+ (void)showMessageIncludingReminderWithMessage:(NSString *)message duration:(CGFloat)duration;// 在window上显示：提示+message，持续时间是duration
+ (void)showMessageIncludingReminderWithMessage:(NSString *)message;// 在window上显示：提示+message，持续时间是默认的kDefaultDuration
- (void)showMessageIncludingReminderWithMessage:(NSString *)message duration:(CGFloat)duration;// 在window上显示：提示+message，持续时间是duration（实例方法）
- (void)showMessageIncludingReminderWithMessage:(NSString *)message;// 在window上显示：提示+message，持续时间是默认的kDefaultDuration（实例方法）

#pragma mark - 显示：内容
+ (void)showMessageWithView:(UIView *)view message:(NSString *)message duration:(CGFloat)duration;// 在view上显示：message，持续时间是duration
+ (void)showMessageWithView:(UIView *)view message:(NSString *)message;// 在view上显示：message，持续时间是默认的kDefaultDuration
+ (void)showMessage:(NSString *)message duration:(CGFloat)duration;// 在window上显示：message，持续时间是duration
+ (void)showMessage:(NSString *)message;// 在window上显示：message，持续时间是默认的kDefaultDuration
- (void)showMessage:(NSString *)message duration:(CGFloat)duration;// 在window上显示：message，持续时间是duration（实例方法）
- (void)showMessage:(NSString *)message;// 在window上显示：message，持续时间是默认的kDefaultDuration（实例方法）

#pragma mark - 隐藏动画
- (void)hide;// 隐藏加载动画

@end

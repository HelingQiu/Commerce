//
//  UIImage+LSLongCutPicture.h
//  CBECommerce
//
//  Created by Rainer on 2018/9/4.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface UIImage (LSLongCutPicture)
//web页面长截图
- (UIImage *)ls_cutLongPictureWebViewFrom:(WKWebView *)webView withRect:(CGRect)rect withCapInsets:(UIEdgeInsets)capInsets;

//scrollview长截图
- (UIImage *)ls_cutLongPictureScrollViewFrom:(UIScrollView *)scrollView;

- (UIImage *)ls_makeImageWithView:(UIView *)view withSize:(CGSize)size;

@end

//
//  UILabel+VerticalAlign.h
//  CBECommerce
//
//  Created by Rainer on 2019/1/21.
//  Copyright © 2019 Rainer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (VerticalAlign)

- (void)alignTop;
- (void)alignBottom;
    
@end

NS_ASSUME_NONNULL_END
